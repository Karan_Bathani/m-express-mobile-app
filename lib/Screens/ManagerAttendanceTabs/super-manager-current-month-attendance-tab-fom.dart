import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/super-manager-visit-card.dart';
import 'package:market_express/Models/super-manager-visit.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SuperManagerCurrentMonthAttendanceTab extends StatefulWidget {
  final String superManager;
  SuperManagerCurrentMonthAttendanceTab(this.superManager);
  @override
  _SuperManagerCurrentMonthAttendanceTabState createState() =>
      _SuperManagerCurrentMonthAttendanceTabState();
}

class _SuperManagerCurrentMonthAttendanceTabState
    extends State<SuperManagerCurrentMonthAttendanceTab>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  Future<List<SuperManagerVisit>> _futureCurrentMonthTerrotiryVisit;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _futureCurrentMonthTerrotiryVisit = _getUserCurrentMonthTerritoryVisit();
  }

  Size screenSize;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    screenSize = MediaQuery.of(context).size;
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: () {
        setState(() {
          _futureCurrentMonthTerrotiryVisit =
              _getUserCurrentMonthTerritoryVisit();
        });
        return _futureCurrentMonthTerrotiryVisit;
      },
      child: FutureBuilder<List<SuperManagerVisit>>(
        future: _futureCurrentMonthTerrotiryVisit,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            if (snapshot.error.toString().contains("404")) {
              return buildEmptyWidget(screenSize,
                  text: "No TerritoryVisits Found!");
            }
            return buildErrorWidget(screenSize);
          } else if (snapshot.hasData &&
              snapshot.connectionState == ConnectionState.done &&
              snapshot.data.length != 0) {
            return _buildRecordsListView(snapshot.data);
          } else if (snapshot.connectionState == ConnectionState.waiting) {
            _refreshIndicatorKey.currentState?.show();
            return _buildRecordsListView(snapshot.data);
          }
          return SizedBox.expand(
            child: ListView(),
          );
        },
      ),
    );
  }

  Widget _buildRecordsListView(List<SuperManagerVisit> superManagerVisits) {
    return ListView.builder(
      itemCount: superManagerVisits != null ? superManagerVisits.length : 0,
      itemBuilder: (context, index) =>
          SuperManagerVisitCard(widget.superManager, superManagerVisits[index]),
    );
  }

  Future<List<SuperManagerVisit>> _getUserCurrentMonthTerritoryVisit() async {
    print("debug FOM-CurrentMonthAttendanceTab");
    List<SuperManagerVisit> superManagerVisits = [];
    try {
      Response response = await MyApi.postDataWithAuthorization({
        "month": DateTime.now().month,
      }, "users/monthly-visit-plan");
      print("FOMCurrentMonthAttendanceTab Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var storeVisit in response.data["data"]) {
          superManagerVisits.add(SuperManagerVisit.fromJson(storeVisit));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("FOMCurrentMonthAttendanceTab SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      print("FOMCurrentMonthAttendanceTab Exception = $error");
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      return Future.error(error);
    }
    return superManagerVisits;
  }
}
