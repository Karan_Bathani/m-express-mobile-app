import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/store-visit-card.dart';
import 'package:market_express/Components/my-date-picker.dart';
import 'package:market_express/Models/store-visit.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// Tab to show the current month attendance records for managers
class CurrentMonthAttendanceTab extends StatefulWidget {
  @override
  _CurrentMonthAttendanceTabState createState() =>
      _CurrentMonthAttendanceTabState();
}

class _CurrentMonthAttendanceTabState extends State<CurrentMonthAttendanceTab>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  Future<List<StoreVisit>> _futureCurrentMonthVisitRecords;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _futureCurrentMonthVisitRecords = _getUserCurrentMonthVisitRecords();
  }

  DateTime _selectedDateTime = DateTime.now();
  Size screenSize;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    screenSize = MediaQuery.of(context).size;
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: () {
        setState(() {
          _futureCurrentMonthVisitRecords = _getUserCurrentMonthVisitRecords();
        });
        return _futureCurrentMonthVisitRecords;
      },
      child: Column(
        children: [
//> Date
          GestureDetector(
            onTap: () => _showMyDatePicker(context),
            child: GestureDetector(
              onTap: () => _showMyDatePicker(context),
              child: Container(
                padding: EdgeInsets.symmetric(
                  horizontal: subMargin,
                  vertical: subMargin,
                ),
                color: dropDownBackground,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      _selectedDateTime.formatDate(pattern: "dd MMM, yyyy"),
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w600,
                        color: Colors.grey[600],
                      ),
                    ),
                    Icon(
                      Icons.date_range,
                      color: Colors.grey[600],
                    ),
                  ],
                ),
              ),
            ),
          ),
          FutureBuilder<List<StoreVisit>>(
            future: _futureCurrentMonthVisitRecords,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                _refreshIndicatorKey.currentState?.show();
                return _buildRecordsListView(snapshot.data);
              } else if (snapshot.hasError) {
                if (snapshot.error.toString().contains("404")) {
                  return buildEmptyWidget(screenSize,
                      text: "No StoreVisits Found!");
                }
                return buildErrorWidget(screenSize);
              } else if (snapshot.hasData &&
                  snapshot.connectionState == ConnectionState.done &&
                  snapshot.data.length != 0) {
                return _buildRecordsListView(snapshot.data);
              }
              return SizedBox.expand(
                child: ListView(),
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _buildRecordsListView(List<StoreVisit> storeVisits) {
    return Expanded(
      child: ListView.builder(
        itemCount: storeVisits != null ? storeVisits.length : 0,
        itemBuilder: (context, index) => StoreVisitCard(storeVisits[index]),
      ),
    );
  }

  Future<List<StoreVisit>> _getUserCurrentMonthVisitRecords() async {
    List<StoreVisit> storeVisits = [];
    try {
      // SharedPreferences preferences = await SharedPreferences.getInstance();
      // String userType = preferences.getString("user_type");
      // Map<String, dynamic> data = {
      //   "supervisor_mode": !(userType == "SEC"),
      // };

      Response response = await MyApi.postDataWithAuthorization({
        "date": _selectedDateTime.formatDate(pattern: "yyyy-MM-dd"),
        "month": 0,
      }, "users/store-visits");
      print("$CurrentMonthAttendance_Tab Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var storeVisit in response.data["data"]) {
          storeVisits.add(StoreVisit.fromJson(storeVisit));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("$CurrentMonthAttendance_Tab SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      print("$CurrentMonthAttendance_Tab Exception = $error");
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      return Future.error(error);
    }
    return storeVisits;
  }

  void _showMyDatePicker(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        barrierColor: Colors.black.withOpacity(.3),
        builder: (context) {
          return WillPopScope(
            onWillPop: () async {
              return true;
            },
            child: Dialog(
              insetPadding: EdgeInsets.all(mainMargin),
              child: MyDatePicker(
                enablePastDates: true,
                initialSelectedDate: _selectedDateTime,
                enableRange: false,

                /// This gives the current month last date
                maxDate: DateTime(
                    _selectedDateTime.year, _selectedDateTime.month + 1, 0),

                /// This gives the current month first date
                minDate: DateTime(
                    _selectedDateTime.year, _selectedDateTime.month, 1),

                allowViewNavigation: false,
              ),
            ),
          );
        }).then((value) {
      if (value != null) {
        // print("ManagerAttendanceHistoryTab Data = $value");
        setState(() {
          _selectedDateTime = value[0];
          // _endDate = value[1] ?? value[0];
          _futureCurrentMonthVisitRecords = _getUserCurrentMonthVisitRecords();
        });
        // print("ManagerAttendanceHistoryTab StartDate = $_selectedDateTime");
        // print("ManagerAttendanceHistoryTab EndDate = $_endDate");
      }
    });
  }
}
