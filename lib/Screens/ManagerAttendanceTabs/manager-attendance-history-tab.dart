import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/manager-attendance-card.dart';
import 'package:market_express/Components/my-date-picker.dart';
import 'package:market_express/Models/manager-attendance.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// Tab to show all the attendance history for managers of SEC OPS
class ManagerAttendanceHistoryTab extends StatefulWidget {
  @override
  _ManagerAttendanceHistoryTabState createState() =>
      _ManagerAttendanceHistoryTabState();
}

class _ManagerAttendanceHistoryTabState
    extends State<ManagerAttendanceHistoryTab>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;
  Future<List<ManagerAttendance>> _futureManagerAttendance;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _futureManagerAttendance = _getManagerAttendance();
  }

  DateTime _startDate = DateTime.now();
  DateTime _endDate = DateTime.now();
  Size screenSize;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    screenSize = MediaQuery.of(context).size;
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: () {
        setState(() {
          _futureManagerAttendance = _getManagerAttendance();
        });
        return _futureManagerAttendance;
      },
      child: Column(
        children: [
//> Date
          GestureDetector(
            onTap: () => _showMyDatePicker(context),
            child: GestureDetector(
              onTap: () => _showMyDatePicker(context),
              child: Container(
                padding: EdgeInsets.symmetric(
                  horizontal: subMargin,
                  vertical: subMargin,
                ),
                color: dropDownBackground,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      _startDate.formatDate(pattern: "dd MMM, yyyy"),
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w600,
                        color: Colors.grey[600],
                      ),
                    ),
                    Icon(
                      Icons.date_range,
                      color: Colors.grey[600],
                    ),
                  ],
                ),
              ),
            ),
          ),
          FutureBuilder<List<ManagerAttendance>>(
            future: _futureManagerAttendance,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                _refreshIndicatorKey.currentState?.show();
                return _buildManagerAttendanceListView(snapshot.data);
              } else if (snapshot.hasError) {
                if (snapshot.error.toString().contains("404")) {
                  return buildEmptyWidget(screenSize,
                      text: "No Attendance Found!");
                }
                return buildErrorWidget(screenSize);
              } else if (snapshot.hasData &&
                  snapshot.connectionState == ConnectionState.done &&
                  snapshot.data.length != 0) {
                return _buildManagerAttendanceListView(snapshot.data);
              }
              return SizedBox.expand(
                child: ListView(),
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _buildManagerAttendanceListView(
      List<ManagerAttendance> managerAttendances) {
    return Expanded(
      child: ListView.builder(
        itemCount: managerAttendances != null ? managerAttendances.length : 0,
        itemBuilder: (context, index) =>
            ManagerAttendanceCard(managerAttendances[index]),
      ),
    );
  }

  Future<List<ManagerAttendance>> _getManagerAttendance() async {
    List<ManagerAttendance> attendances = [];
    try {
      Response response = await MyApi.getDataWithAuthorization(
          "attendance/user-attendance",
          queryParams: {
            "start_date": _startDate.toIso8601String(),
            "end_date": _endDate.toIso8601String(),
          });
      print("$AttendanceHistory_Tab Response = ${response.data}");
      if (response.statusCode == 401) {
//> Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var attendance in response.data["data"]) {
          attendances.add(ManagerAttendance.fromJson(attendance));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("$AttendanceHistory_Tab SocketException");
      return Future.error(SocketException);
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        }
      }
      print("$AttendanceHistory_Tab Exception = $error");
      return Future.error(error);
    }
    return attendances;
  }

  void _showMyDatePicker(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        barrierColor: Colors.black.withOpacity(.3),
        builder: (context) {
          return WillPopScope(
            onWillPop: () async {
              return true;
            },
            child: Dialog(
              insetPadding: EdgeInsets.all(mainMargin),
              child: MyDatePicker(
                enablePastDates: true,
                initialSelectedDate: _startDate,
                enableRange: false,
                maxDate: DateTime.now(),
              ),
            ),
          );
        }).then((value) {
      if (value != null) {
        print("ManagerAttendanceHistoryTab Data = $value");

        // dateController.text = value[1] == null ||
        //         DateFormat('dd-MM-yyyy').format(value[0]).toString() ==
        //             DateFormat('dd-MM-yyyy').format(value[1]).toString()
        //     ? DateFormat('dd-MM-yyyy').format(value[0]).toString()
        //     : DateFormat('dd-MM-yyyy').format(value[0]).toString() +
        //         " to " +
        //         DateFormat('dd-MM-yyyy').format(value[1]).toString();
        setState(() {
          _startDate = value[0];
          _endDate = value[1] ?? value[0];
          _futureManagerAttendance = _getManagerAttendance();
        });
        print("ManagerAttendanceHistoryTab StartDate = $_startDate");
        print("ManagerAttendanceHistoryTab EndDate = $_endDate");
      }
    });
  }
}
