import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/sales-card.dart';
import 'package:market_express/Models/sales.dart';
import 'package:market_express/Screens/Forms/sales-form.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SalesScreen extends StatefulWidget {
  final DateTime selectedDateTime;
  final String userId;
  final String userName;
  final String message;

  SalesScreen(
      {this.selectedDateTime, this.userId, this.userName, this.message});

  @override
  _SalesScreenState createState() => _SalesScreenState();
}

class _SalesScreenState extends State<SalesScreen> {
  Future<List<Sales>> _futureSales;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    if (widget.message != null)
      WidgetsBinding.instance.addPostFrameCallback((_) {
        //* executes after build
        showToast(
          widget.message,
          context: context,
          animation: StyledToastAnimation.slideFromBottom,
          reverseAnimation: StyledToastAnimation.slideToBottom,
          startOffset: Offset(0.0, 3.0),
          reverseEndOffset: Offset(0.0, 3.0),
          position: StyledToastPosition.bottom,
          duration: Duration(seconds: 3),
          animDuration: Duration(seconds: 1),
          curve: Curves.elasticOut,
          backgroundColor: success,
          reverseCurve: Curves.fastOutSlowIn,
        );
      });
    _futureSales = _getUserSalesData();
  }

  Size screenSize;

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          widget.userName ?? 'My Sales',
          style: TextStyle(color: Colors.white),
        ),
      ),
      floatingActionButton: FutureBuilder<SharedPreferences>(
        future: SharedPreferences.getInstance(),
        builder: (buildContext, snapshot) {
          if (snapshot.hasData &&
              snapshot.data.getString("user_type") != "SEC") {
            return SizedBox.shrink();
          }
          return FloatingActionButton(
            backgroundColor: colorPrimary,
            child: Icon(Icons.add),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => SalesForm(),
                ),
              ).then(
                (value) {
                  if (value != null) {
                    Scaffold.of(buildContext).showSnackBar(
                        createSnackBar(msg: value, isSuccess: true));
                    setState(() {
                      _futureSales = _getUserSalesData();
                    });
                  }
                },
              );
            },
          );
        },
      ),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: greyBackground,
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          child: RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () {
              setState(() {
                _futureSales = _getUserSalesData();
              });
              return _futureSales;
            },
            child: FutureBuilder<List<Sales>>(
              future: _futureSales,
              builder: (context, snapshot) {
                if (snapshot.hasData &&
                    snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.data.length != 0) {
                    return _buildSalesListView(snapshot.data);
                  } else {
                    // ! TODO: Use a lottie or something UI attractive instead
                    return SingleChildScrollView(
                      physics: AlwaysScrollableScrollPhysics(),
                      child: Expanded(
                        child: Center(
                          child: Text('No Sales Found!'),
                        ),
                      ),
                    );
                  }
                } else if (snapshot.connectionState ==
                    ConnectionState.waiting) {
                  _refreshIndicatorKey.currentState?.show();
                  return _buildSalesListView(snapshot.data);
                }
                return SizedBox.expand(
                  child: ListView(),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildSalesListView(List<Sales> sales) {
    return ListView.builder(
      itemCount: sales != null ? sales.length : 0,
      itemBuilder: (context, index) => SalesCard(sales[index]),
    );
  }

  Future<List<Sales>> _getUserSalesData() async {
    List<Sales> sales = [];
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      String userType = preferences.getString("user_type");
      Response response = widget.userId != null
          ? await MyApi.postDataWithAuthorization({
              "month": widget.selectedDateTime.month,
              "year": widget.selectedDateTime.year,
              "user_id": widget.userId
            }, "stores/monthly-user-sales")
          : await MyApi.postDataWithAuthorization({
              "supervisor_mode": !(userType == "SEC"),
            }, "stores/sales-list");
      print("$Sales_Screen Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var sale in response.data["data"]) {
          sales.add(Sales.fromJson(sale));
        }
      } else {
        throw Future.error("Error from Future");
      }
    } on SocketException {
      print("$Sales_Screen SocketException");
      throw Future.error(SocketException);
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        }
      }
      print("$Sales_Screen Exception = $error");
      throw Future.error(error);
    }
    return sales;
  }
}
