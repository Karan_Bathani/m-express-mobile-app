import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/notice-card.dart';
import 'package:market_express/Models/notice.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  Future<List<Notice>> _futureNotices;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _selectedDateTime = todaysDateTime;
    _futureNotices = _getNotices();
  }

  DateTime todaysDateTime = DateTime.now();
  DateTime _selectedDateTime;
  Size screenSize;

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: FittedBox(
          child: Text(
            "Notice(s)",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: greyBackground,
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          child: RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () {
              setState(() {
                _futureNotices = _getNotices();
              });
              return _futureNotices;
            },
            child: Column(
              children: [
//> Month Selector
                GestureDetector(
                  onTap: () {
                    showMonthPicker(
                      context: context,
                      firstDate: DateTime(DateTime.now().year - 1),
                      lastDate: DateTime(DateTime.now().year + 1),
                      initialDate: _selectedDateTime ?? DateTime.now(),
                      locale: Locale("en"),
                    ).then((date) {
                      if (date != null) {
                        setState(() {
                          _selectedDateTime = date;
                          _futureNotices = _getNotices();
                        });
                      }
                    });
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: subMargin,
                      vertical: subMargin,
                    ),
                    color: dropDownBackground,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          _selectedDateTime.formatDate(pattern: "MMMM, yyyy"),
                          style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.w600,
                            color: Colors.grey[600],
                          ),
                        ),
                        SizedBox(width: subMargin),
                        Icon(
                          Icons.date_range,
                          color: Colors.grey[600],
                        ),
                      ],
                    ),
                  ),
                ),
                FutureBuilder<List<Notice>>(
                  future: _futureNotices,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      _refreshIndicatorKey.currentState?.show();
                      return _buildOnHandStockList(snapshot.data);
                    } else if (snapshot.hasError) {
                      if (snapshot.error.toString().contains("404")) {
                        return buildEmptyWidget(screenSize,
                            text: "No Data Found");
                      }
                      return buildErrorWidget(screenSize);
                    } else if (snapshot.hasData &&
                        snapshot.connectionState == ConnectionState.done &&
                        snapshot.data.length != 0) {
                      return _buildOnHandStockList(snapshot.data);
                    }
                    return Expanded(
                      child: ListView(),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildOnHandStockList(List<Notice> notices) {
    return Expanded(
      child: ListView.builder(
        itemCount: notices != null ? notices.length : 0,
        itemBuilder: (context, index) {
          return NoticeCard(
            notices[notices.length - 1 - index],
            shouldHighlight: DateTime.now().day ==
                    notices[notices.length - 1 - index].date.day &&
                DateTime.now().month ==
                    notices[notices.length - 1 - index].date.month,
          );
        },
      ),
    );
  }

  Future<List<Notice>> _getNotices() async {
    List<Notice> notices = [];
    try {
      Response response = await MyApi.postDataWithAuthorization({
        "month": _selectedDateTime.month,
        "year": _selectedDateTime.year,
      }, "users/get-filtered-notices");
      print("debug NotificationScreen Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var notice in response.data["data"]) {
          notices.add(Notice.fromJson(notice));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("debug NotificationScreen SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      print("debug NotificationScreen Exception = $error");
      throw Future.error(error);
    }
    return notices;
  }
}
