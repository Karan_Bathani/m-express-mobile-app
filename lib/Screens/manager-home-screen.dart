import 'dart:io';
import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:intl/intl.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/notice-card.dart';
import 'package:market_express/Components/List-Items/todays-visits-card.dart';
import 'package:market_express/Components/donut-pie-chart.dart';
import 'package:market_express/Components/manager-navigation-drawer.dart';
import 'package:market_express/Components/my-custom-button.dart';
import 'package:market_express/Models/dashboard.dart';
import 'package:market_express/Models/store-visit.dart';
import 'package:market_express/Models/super-manager-visit.dart';
import 'package:market_express/Screens/Summary/attendance-summary-screen.dart';
import 'package:market_express/Screens/Summary/leave-summary-screen.dart';
import 'package:market_express/Screens/Summary/sales-summary-screen.dart';
import 'package:market_express/Screens/Summary/survey-summary-screen.dart';
import 'package:market_express/Screens/Summary/training-material-summary-screen.dart';
import 'package:market_express/Screens/notification-screen.dart';
import 'package:market_express/Screens/unlisted-mark-attendance-screen.dart';
import 'package:market_express/Screens/leave-approval-screen.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Screens/fom-mark-attendance-screen.dart';
import 'package:market_express/Screens/mark-attendance-screen.dart';
import 'package:market_express/Screens/Forms/sales-form.dart';
import 'package:market_express/Screens/sales-screen.dart';
import 'package:market_express/Screens/training-material-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class ManagerHomeScreen extends StatefulWidget {
  @override
  _ManagerHomeScreenState createState() => _ManagerHomeScreenState();
}

class _ManagerHomeScreenState extends State<ManagerHomeScreen> {
  Size screenSize;
  bool isMorningAttendanceMarked = false;
  Future<Dashboard> _futureDashboard;
  int _totalSurveys;
  Future<List<StoreVisit>> _futureTodaysStoreVisits;
  Future<List<SuperManagerVisit>> _futureTodaysTerritoryVisits;
  String _userRecentAttendancePhoto;
  DateTime todaysDateTime = DateTime.now();
  @override
  void initState() {
    super.initState();
    _futureDashboard = _getDashboardData();
    _futureTodaysStoreVisits = _getTodaysStoreVisits();
    _futureTodaysTerritoryVisits = _getTodaysTerritoryVisits();
  }

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: FutureBuilder<SharedPreferences>(
          future: SharedPreferences.getInstance(),
          builder: (context, snapshot) {
            if (snapshot.hasData && snapshot.data.containsKey("user_name")) {
              return FittedBox(
                child: Text(
                  snapshot.data.getString("user_name"),
                  style: TextStyle(color: Colors.white),
                ),
              );
            }
            return Text(
              'Home',
              style: TextStyle(color: Colors.white),
            );
          },
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.notifications,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => NotificationScreen(),
                  ));
            },
          ),
          IconButton(
            icon: Icon(
              Icons.file_download,
              // color: Colors.amberAccent,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => TrainingMaterialScreen(),
                  ));
            },
          ),
        ],
      ),
      drawer: ManagerNavigationDrawer(
          userRecentAttendancePhoto: _userRecentAttendancePhoto),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: greyBackground,
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          child: ListView(
            padding: EdgeInsets.all(subMargin),
            addAutomaticKeepAlives: true,
            shrinkWrap: true,
            children: [
              FutureBuilder<Dashboard>(
                future: _futureDashboard,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Column(
                      children: [
                        snapshot.data.mostRecentNotice != null
                            ? NoticeCard(
                                snapshot.data.mostRecentNotice,
                                shouldHighlight: true,
                                isHomeScreen: true,
                              )
                            : SizedBox(),
                        _createFirstCard(snapshot.data),
                        snapshot.data.actualSales != null &&
                                snapshot.data.salesTarget != null &&
                                snapshot.data.salesTarget != 0
                            ? _createSalesTargetCard(
                                context,
                                snapshot.data.actualSales,
                                snapshot.data.salesTarget)
                            : SizedBox.shrink(),
                      ],
                    );
                  } else if (snapshot.connectionState ==
                      ConnectionState.waiting) {
                    return Center(
                      child: Container(
                        margin: EdgeInsets.all(subMargin),
                        width: 30,
                        height: 30,
                        child: CircularProgressIndicator(),
                      ),
                    );
                  } else if (snapshot.hasError) {
                    print("$Home_Screen FutureError = ${snapshot.error}");

                    return Card(
                      elevation: cardElevation,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(cardCornerRadius)),
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.all(cardContentPadding),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Something went Wrong!",
                              style: TextStyle(color: error),
                            ),
                            FlatButton(
                              onPressed: () {
                                setState(() {
                                  _futureDashboard = _getDashboardData();
                                });
                              },
                              child: Text("Retry"),
                            ),
                          ],
                        ),
                      ),
                    );
                  }
                  return SizedBox.shrink();
                },
              ),
              FutureBuilder<SharedPreferences>(
                future: SharedPreferences.getInstance(),
                builder: (context, snapshot) {
                  if (snapshot.hasData &&
                      snapshot.data.containsKey("user_type")) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        _createSummaryCardForManager(
                            snapshot.data.getString("user_type")),
                        snapshot.data.getString("user_type") == "FOE"
                            ? _createTodaysStoreVisitsListCard(context)
                            : _createTodaysTerritoryVisitsCard(context),
                      ],
                    );
                  }
                  return SizedBox.shrink();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Will show manager's contact-details and assigned store-data
  Widget _createFirstCard(Dashboard dashboard) {
    return Column(
      children: [
        Card(
          elevation: cardElevation,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(cardCornerRadius)),
          color: Colors.white,
          child: Padding(
            padding: EdgeInsets.all(cardContentPadding),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                dashboard.store != null
                    ? Text(
                        dashboard.store.storeName ?? 'Store Name',
                        style: TextStyle(
                            color: textPrimary,
                            fontWeight: FontWeight.w600,
                            fontSize: 14),
                      )
                    : SizedBox.shrink(),
                dashboard.store != null
                    ? Text(
                        'Store Assigned Date ' +
                                DateFormat.yMMMd()
                                    .format(dashboard.store.updatedAt) ??
                            'Date Assigned',
                        style: TextStyle(
                          color: colorPrimary,
                          fontSize: 12,
                        ),
                      )
                    : SizedBox.shrink(),
                dashboard.store != null
                    ? Padding(
                        padding: EdgeInsets.only(top: subMarginHalf),
                        child: Text(
                          "${dashboard.store.location}, ${dashboard.store.area}, ${dashboard.store.district}, ${dashboard.store.division}." ??
                              "Assigned Store Address",
                          style: TextStyle(
                            color: textSecondary,
                            fontSize: 12,
                          ),
                        ),
                      )
                    : SizedBox.shrink(),
                ListTile(
                  contentPadding: EdgeInsets.all(0),
                  // visualDensity: VisualDensity.compact,
                  dense: true,
                  leading:
                      //  IconButton(
                      //   onPressed: () {},
                      //   icon: Container(
                      //     width: double.infinity,
                      //     height: double.infinity,
                      //     decoration: BoxDecoration(
                      //       color: Color(0xffa9f8d5),
                      //       borderRadius: BorderRadius.circular(miniCardCornerRadius),
                      //     ),
                      //     child: Icon(
                      //       MyFlutterApp.group_76,
                      //       color: Color(0xff55d19b),
                      //     ),
                      //   ),
                      // ),
                      Image.asset(
                    'assets/images/user.png',
                    width: 40,
                    height: 40,
                    alignment: Alignment.center,
                  ),
                  title: Text(
                    dashboard.managerName ?? 'Manager Name',
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: textPrimary,
                    ),
                  ),
                  subtitle: Text(
                    'Reporting Manager',
                    style: TextStyle(
                      color: textSecondary,
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                    ),
                  ),
                  trailing: Builder(
                    builder: (context) => Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        IconButton(
                          constraints: BoxConstraints(
                            maxWidth: 45,
                            maxHeight: 45,
                            minWidth: 45,
                            minHeight: 45,
                          ),
                          padding: EdgeInsets.all(0),
                          visualDensity: VisualDensity.compact,
                          onPressed: () => sendEmail(
                              dashboard.managerEmail ?? "test@xcitech.com",
                              context),
                          icon: Image.asset(
                            'assets/images/email.png',
                          ),
                        ),
                        SizedBox(width: subMarginHalf),
                        IconButton(
                          constraints: BoxConstraints(
                            maxWidth: 45,
                            maxHeight: 45,
                            minWidth: 45,
                            minHeight: 45,
                          ),
                          padding: EdgeInsets.all(0),
                          visualDensity: VisualDensity.compact,
                          onPressed: () => callnow(
                              dashboard.managerPhone ?? "1234567890", context),
                          icon: Image.asset(
                            'assets/images/phone.png',
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                FutureBuilder<SharedPreferences>(
                    future: SharedPreferences.getInstance(),
                    builder: (context, snapshot) {
                      //TODO Could be removed when DashboardProvider is implemented.
                      // String attendance =
                      //     dashboard.monthlyAttendanceCount.toString();
                      String yearlyLeavesTaken =
                          dashboard.yearlyLeavesTaken.toString();
                      String yearlyLeavesRemaining =
                          dashboard.yearlyLeavesRemaining.toString();

                      // if (snapshot.hasData &&
                      //     snapshot.data
                      //         .containsKey("monthly_attendance_count")) {
                      //   attendance = snapshot.data
                      //       .getDouble("monthly_attendance_count")
                      //       .toString();
                      // }
                      if (snapshot.hasData &&
                          snapshot.data.containsKey("yearly_leaves_taken")) {
                        yearlyLeavesTaken = snapshot.data
                            .getDouble("yearly_leaves_taken")
                            .toString();
                      }
                      if (snapshot.hasData &&
                          snapshot.data
                              .containsKey("yearly_leaves_remaining")) {
                        yearlyLeavesRemaining = snapshot.data
                            .getDouble("yearly_leaves_remaining")
                            .toString();
                      }
                      return Table(
                        defaultVerticalAlignment:
                            TableCellVerticalAlignment.middle,
                        columnWidths: {1: FixedColumnWidth(1)},
                        children: [
                          TableRow(
                              decoration: BoxDecoration(
                                color: greyBackground,
                              ),
                              children: [
                                Center(
                                  child: RichText(
                                    textAlign: TextAlign.center,
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                          text:
                                              "${dashboard.monthlyAttendanceCount ?? 0}\n",
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                          ),
                                        ),
                                        TextSpan(
                                          text: "day(s)\n",
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: textSecondary,
                                          ),
                                        ),
                                        TextSpan(
                                          text:
                                              "Attendance: (${DateTime.now().formatDate(pattern: "MMM")})",
                                          style: TextStyle(
                                            color: Colors.black,
                                            height: 1.3,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                ColoredBox(
                                  color: myGreyColor,
                                  child: Text(
                                    "",
                                    style: TextStyle(height: 4),
                                  ),
                                ),
                                Center(
                                  child: RichText(
                                    textAlign: TextAlign.center,
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                          text:
                                              "${yearlyLeavesTaken ?? 0}/${yearlyLeavesRemaining ?? 0}\n",
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                          ),
                                        ),
                                        TextSpan(
                                          text: "availed/remaining\n",
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: textSecondary,
                                          ),
                                        ),
                                        TextSpan(
                                          text:
                                              "Leaves : (${DateTime.now().formatDate(pattern: "yyyy")})",
                                          style: TextStyle(
                                            color: Colors.black,
                                            height: 1.3,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ]),
                        ],
                      );
                    }),
              ],
            ),
          ),
        ),
// Current Day Employee Attendance DonutChart
        Card(
          elevation: cardElevation,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(cardCornerRadius)),
          color: Colors.white,
          child: Padding(
            padding: EdgeInsets.only(bottom: cardContentPadding),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(
                  width: screenSize.width / 2 - subMarginDouble,
                  height: screenSize.width / 2 -
                      subMarginDouble -
                      cardContentPadding,
                  child: DonutPieChart.withDesiredData(
                    dashboard.teamMembersPresent,
                    dashboard.teamMembersCount,
                    text1: "Present",
                    text2: "Absent",
                  ),
                ),
                Container(
                  width: screenSize.width / 2 - subMarginDouble,
                  height: screenSize.width / 2 -
                      subMarginDouble -
                      cardContentPadding,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Today's Attendance:\n",
                              style: TextStyle(
                                // fontWeight: FontWeight.bold,
                                fontSize: 14,
                                color: textPrimary,
                              ),
                            ),
                            TextSpan(
                              text:
                                  DateTime.now().formatDate(pattern: "dd MMM"),
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 14,
                                color: textSecondary,
                              ),
                            ),
                          ],
                        ),
                      ),
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Present: ",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                                color: textPrimary,
                              ),
                            ),
                            TextSpan(
                              text: "${dashboard.teamMembersPresent}",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                                color: textSecondary,
                              ),
                            ),
                          ],
                        ),
                      ),
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Absent: ",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                                color: textPrimary,
                              ),
                            ),
                            TextSpan(
                              text: "${dashboard.teamMembersAbsent}",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                                color: textSecondary,
                              ),
                            ),
                          ],
                        ),
                      ),
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Total: ",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                                color: textPrimary,
                              ),
                            ),
                            TextSpan(
                              text: "${dashboard.teamMembersCount}",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                                color: textSecondary,
                              ),
                            ),
                          ],
                        ),
                      ),
                      // Expanded(
                      //   child: Center(
                      //     child: RichText(
                      //       textAlign: TextAlign.center,
                      //       text: TextSpan(
                      //         children: [
                      //           TextSpan(
                      //             text: 'Present / Absent / Total\n',
                      //             style: TextStyle(
                      //               fontWeight: FontWeight.bold,
                      //               fontSize: 18,
                      //               color: textPrimary,
                      //             ),
                      //           ),
                      //           TextSpan(
                      //             text:
                      //                 "${dashboard.teamMembersPresent} / ${dashboard.teamMembersAbsent} / ${dashboard.teamMembersCount}",
                      //             style: TextStyle(
                      //               fontWeight: FontWeight.bold,
                      //               fontSize: 20,
                      //               color: textSecondary,
                      //             ),
                      //           ),
                      //         ],
                      //       ),
                      //     ),
                      //   ),
                      // ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
// Leave approval card for the senior users to be shown when there is a leave application by junior users.
        dashboard.pendingLeavesCount != null
            ? Card(
                clipBehavior: Clip.antiAlias,
                elevation: cardElevation,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(cardCornerRadius)),
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ListTile(
                      // trailing: FlatButton(
                      //   onPressed: () {},
                      //   child: Text("See All",
                      //       style: TextStyle(color: colorPrimaryLight)),
                      // ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => LeaveApprovalScreen(),
                          ),
                        ).then((value) {
                          setState(() {
                            _futureDashboard = _getDashboardData();
                          });
                        });
                      },
                      title: Text(
                        "You have ${dashboard.pendingLeavesCount} pending leave request(s).",
                        maxLines: 2,
                      ),
                    ),
                  ],
                ),
              )
            : SizedBox.shrink(),
      ],
    );
  }

  Widget _createSalesTargetCard(
      BuildContext myContext, int actualSales, int salesTarget) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.only(bottom: cardContentPadding),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: screenSize.width / 2 - subMarginDouble,
              height:
                  screenSize.width / 2 - subMarginDouble - cardContentPadding,
              child: DonutPieChart.withDesiredData(actualSales, salesTarget),
            ),
            Container(
              width: screenSize.width / 2 - subMarginDouble,
              height:
                  screenSize.width / 2 - subMarginDouble - cardContentPadding,
              // color: Colors.amber,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    child: Center(
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          // text: "Coming Soon...",
                          // style: TextStyle(
                          //   color: Colors.black,
                          //   fontWeight: FontWeight.w600,
                          // ),
                          children: [
                            TextSpan(
                              text: 'Sales / Target\n',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                                color: textPrimary,
                              ),
                            ),
                            TextSpan(
                              text: "$actualSales / $salesTarget",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                                color: textSecondary,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: colorPrimaryLight.withOpacity(.34),
                                blurRadius: 4,
                                offset: Offset(0, 4),
                              ),
                            ],
                          ),
                          child: FlatButton(
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.circular(miniCardCornerRadius),
                            ),
                            color: colorPrimary,
                            padding: EdgeInsets.symmetric(
                              vertical: subMarginHalf,
                              horizontal: subMargin,
                            ),
                            child: Text(
                              'History',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                            onPressed: isMorningAttendanceMarked
                                ? () {
                                    Navigator.push(
                                      myContext,
                                      MaterialPageRoute(
                                        builder: (context) => SalesScreen(),
                                      ),
                                    );
                                  }
                                : () {
                                    Scaffold.of(myContext).showSnackBar(
                                        createSnackBar(
                                            msg:
                                                "Mark attendance to proceed!"));
                                  },
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(left: subMarginHalf),
                          decoration: BoxDecoration(boxShadow: [
                            BoxShadow(
                              color: colorSecondaryLight.withOpacity(.34),
                              blurRadius: 4,
                              offset: Offset(0, 4),
                            ),
                          ]),
                          child: FlatButton(
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.circular(miniCardCornerRadius),
                            ),
                            color: colorSecondary,
                            padding: EdgeInsets.symmetric(
                              vertical: subMarginHalf,
                              horizontal: subMargin,
                            ),
                            child: Text(
                              '+Add',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                            onPressed: isMorningAttendanceMarked
                                ? () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => SalesForm(),
                                      ),
                                    ).then(
                                      (value) {
                                        if (value != null) {
                                          Scaffold.of(myContext).showSnackBar(
                                              createSnackBar(
                                                  msg: value, isSuccess: true));
                                          setState(() {
                                            _futureDashboard =
                                                _getDashboardData();
                                          });
                                        }
                                      },
                                    );
                                  }
                                : () {
                                    Scaffold.of(myContext).showSnackBar(
                                        createSnackBar(
                                            msg:
                                                "Mark attendance to proceed!"));
                                  },
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _createSummaryCardForManager(String userType) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      clipBehavior: Clip.antiAlias,
      child: Padding(
        // padding: EdgeInsets.only(bottom: cardContentPadding),
        padding: EdgeInsets.only(bottom: 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: subMargin,
                vertical: subMarginHalf,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Summary:",
                    style: TextStyle(
                      color: textPrimary,
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                    ),
                  ),
                  _totalSurveys != null
                      ? Text(
                          "Total Surveys(${DateFormat("MMM").format(DateTime.now())}): ${_totalSurveys ?? 0}")
                      : SizedBox.shrink(),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.max,
              children: [
                Column(
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => AttendanceSummaryScreen(),
                          ),
                        );
                      },
                      icon: Image.asset("assets/images/attendance.png"),
                      iconSize: 50,
                    ),
                    Text(
                      "Attendance",
                      style: TextStyle(
                        color: textPrimary,
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => LeaveSummaryScreen(),
                          ),
                        );
                      },
                      icon: Image.asset("assets/images/calendar.png"),
                      iconSize: 50,
                    ),
                    Text(
                      "Leave",
                      style: TextStyle(
                        color: textPrimary,
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SurveySummaryScreen(),
                          ),
                        );
                      },
                      icon: Image.asset("assets/images/survey.png"),
                      iconSize: 50,
                    ),
                    Text(
                      "Survey",
                      style: TextStyle(
                        color: textPrimary,
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SalesSummaryScreen(),
                          ),
                        );
                      },
                      icon: Image.asset("assets/images/sales.png"),
                      iconSize: 50,
                    ),
                    Text(
                      "Sales",
                      style: TextStyle(
                        color: textPrimary,
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(height: miniCardCornerRadius),
            Divider(height: 0, color: myGreyColor),
            ListTile(
              dense: true,
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        TrainingMaterialSummaryScreen(userType),
                  ),
                );
              },
              title: Text(
                "Training Materials Summary",
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  color: textPrimary,
                ),
              ),
              trailing: Icon(Icons.arrow_forward),
            ),
          ],
        ),
      ),
    );
  }

  /// Custom-Method to get Today's-Visit for FOE (Endpoint=>users/store-visits)
  Future<List<StoreVisit>> _getTodaysStoreVisits() async {
    List<StoreVisit> storeVisits = [];
    try {
      Response response = await MyApi.postDataWithAuthorization({
        "date":
            // DateTime(
            //         todaysDateTime.year, todaysDateTime.month, todaysDateTime.day)
            //     .toIso8601String(),
            // "2021-02-19T18:30:00.000Z",
            DateTime.now().toIso8601String(),
        "month": 0,
      }, "users/store-visits");
      print("$Home_Screen StoreVisitResponse = ${response.data}");
      if (response.data["code"] == 401 || response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.data["code"] == 200) {
        for (var storeVisit in response.data["data"]) {
          storeVisits.add(StoreVisit.fromJson(storeVisit));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("$Home_Screen StoreVisit SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      print("$Home_Screen StoreVisitResponse Exception = $error");
      return Future.error(error);
    }
    return storeVisits;
  }

  /// Will show current day's store visits for FOE
  Widget _createTodaysStoreVisitsListCard(BuildContext context) {
    return FutureBuilder<List<StoreVisit>>(
      future: _futureTodaysStoreVisits,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: Container(
              margin: EdgeInsets.all(subMargin),
              width: 30,
              height: 30,
              child: CircularProgressIndicator(),
            ),
          );
        } else if (snapshot.hasData) {
          //< SUCCESS
          return Card(
            elevation: cardElevation,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(cardCornerRadius)),
            clipBehavior: Clip.antiAlias,
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ListTile(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => UnlistedMarkAttendanceScreen(),
                      ),
                    ).then(
                      (value) {
                        if (value != null) {
                          showToast(value,
                              context: context,
                              animation: StyledToastAnimation.slideFromBottom,
                              reverseAnimation:
                                  StyledToastAnimation.slideToBottom,
                              startOffset: Offset(0.0, 3.0),
                              reverseEndOffset: Offset(0.0, 3.0),
                              position: StyledToastPosition.bottom,
                              duration: Duration(seconds: 3),
                              animDuration: Duration(seconds: 1),
                              curve: Curves.elasticOut,
                              reverseCurve: Curves.fastOutSlowIn);
                          setState(() {
                            _futureDashboard = _getDashboardData();
                          });
                        }
                      },
                    );
                  },
                  title: Text(
                    "Mark Attendance for unlisted store.",
                    style: TextStyle(
                      color: textPrimary,
                      // fontWeight: FontWeight.bold,
                    ),
                  ),
                  trailing: Icon(Icons.arrow_forward),
                ),
                Divider(height: 0),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: subMargin,
                    vertical: subMarginHalf,
                  ),
                  child: Text(
                    "Today's Visit(s): ${DateTime.now().formatDate(pattern: "dd MMM, yy")}",
                    style: TextStyle(
                      color: textPrimary,
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                    ),
                  ),
                ),
                Column(
                  children: snapshot.data.map((val) {
                    return TodaysVisitCard(val, () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => MarkAttendanceScreen(
                            type: "daily_visits",
                            storeId: val.storeId,
                          ),
                        ),
                      ).then(
                        (value) {
                          if (value != null) {
                            showToast(value,
                                context: context,
                                animation: StyledToastAnimation.slideFromBottom,
                                reverseAnimation:
                                    StyledToastAnimation.slideToBottom,
                                startOffset: Offset(0.0, 3.0),
                                reverseEndOffset: Offset(0.0, 3.0),
                                position: StyledToastPosition.bottom,
                                duration: Duration(seconds: 3),
                                animDuration: Duration(seconds: 1),
                                curve: Curves.elasticOut,
                                reverseCurve: Curves.fastOutSlowIn);
                            setState(() {
                              _futureTodaysStoreVisits =
                                  _getTodaysStoreVisits();
                              _futureDashboard = _getDashboardData();
                            });
                          }
                        },
                      );
                    });
                  }).toList(),
                ),
              ],
            ),
          );
        } else if (snapshot.hasError) {
          //~ ERROR
          print("$Home_Screen StoreVisit FutureError = ${snapshot.error}");
          if (snapshot.error.toString().contains("404")) {
            return Card(
              elevation: cardElevation,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(cardCornerRadius)),
              color: Colors.white,
              child: Padding(
                // padding: EdgeInsets.all(cardContentPadding),
                padding: EdgeInsets.all(0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ListTile(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                UnlistedMarkAttendanceScreen(),
                          ),
                        ).then(
                          (value) {
                            if (value != null) {
                              showToast(value,
                                  context: context,
                                  animation:
                                      StyledToastAnimation.slideFromBottom,
                                  reverseAnimation:
                                      StyledToastAnimation.slideToBottom,
                                  startOffset: Offset(0.0, 3.0),
                                  reverseEndOffset: Offset(0.0, 3.0),
                                  position: StyledToastPosition.bottom,
                                  duration: Duration(seconds: 3),
                                  animDuration: Duration(seconds: 1),
                                  curve: Curves.elasticOut,
                                  reverseCurve: Curves.fastOutSlowIn);
                              setState(() {
                                _futureDashboard = _getDashboardData();
                              });
                            }
                          },
                        );
                      },
                      title: Text(
                        "Mark Attendance for unlisted store.",
                        style: TextStyle(
                          color: textPrimary,
                          // fontWeight: FontWeight.bold,
                        ),
                      ),
                      trailing: Icon(Icons.arrow_forward),
                    ),
                    Divider(height: 0),
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: subMargin,
                        vertical: subMarginHalf,
                      ),
                      child: Text(
                        "Today's Visit(s): ${DateTime.now().formatDate(pattern: "dd MMM, yy")}",
                        style: TextStyle(
                          color: textPrimary,
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                        ),
                      ),
                    ),
                    Text("No Visits found for today!"),
                    SizedBox(height: cardContentPadding),
                  ],
                ),
              ),
            );
          }
          return Card(
            elevation: cardElevation,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(cardCornerRadius)),
            color: Colors.white,
            child: Padding(
              padding: EdgeInsets.all(cardContentPadding),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: subMargin,
                      vertical: subMarginHalf,
                    ),
                    child: Text(
                      "Today's Visit(s): ${DateTime.now().formatDate(pattern: "dd MMM, yy")}",
                      style: TextStyle(
                        color: textPrimary,
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Something went Wrong!",
                        style: TextStyle(color: error),
                      ),
                      FlatButton(
                        onPressed: () {
                          setState(() {
                            _futureTodaysStoreVisits = _getTodaysStoreVisits();
                          });
                        },
                        child: Text("Retry"),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        }
        return SizedBox.shrink();
      },
    );
  }

  /// Custom-Method to get Today's-Territory-Visit for FOM (Endpoint=>users/daily-visit-plan)
  Future<List<SuperManagerVisit>> _getTodaysTerritoryVisits() async {
    List<SuperManagerVisit> territoryVisits = [];
    try {
      Response response =
          await MyApi.getDataWithAuthorization("users/daily-visit-plan");
      print("$Home_Screen TerritoryVisitResponse = ${response.data}");
      if (response.data["code"] == 401 || response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.data["code"] == 200) {
        for (var territoryVisit in response.data["data"]) {
          territoryVisits.add(SuperManagerVisit.fromJson(territoryVisit));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("$Home_Screen TerritoryVisit SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      print("$Home_Screen TerritoryVisitResponse Exception = $error");
      return Future.error(error);
    }
    return territoryVisits;
  }

  /// Will show current day's territory visits for FOM
  Widget _createTodaysTerritoryVisitsCard(BuildContext context) {
    return FutureBuilder<List<SuperManagerVisit>>(
      future: _futureTodaysTerritoryVisits,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: Container(
              margin: EdgeInsets.all(subMargin),
              width: 30,
              height: 30,
              child: CircularProgressIndicator(),
            ),
          );
        } else if (snapshot.hasData) {
          //< snapshot.hasData
          return Card(
            elevation: cardElevation,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(cardCornerRadius),
            ),
            clipBehavior: Clip.antiAlias,
            color: Colors.white,
            child: Column(
              children: [
                //? Mark Unlisted Attendance
                ListTile(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => UnlistedMarkAttendanceScreen(),
                      ),
                    ).then(
                      (value) {
                        if (value != null) {
                          showToast(value,
                              context: context,
                              animation: StyledToastAnimation.slideFromBottom,
                              reverseAnimation:
                                  StyledToastAnimation.slideToBottom,
                              startOffset: Offset(0.0, 3.0),
                              reverseEndOffset: Offset(0.0, 3.0),
                              position: StyledToastPosition.bottom,
                              duration: Duration(seconds: 3),
                              animDuration: Duration(seconds: 1),
                              curve: Curves.elasticOut,
                              reverseCurve: Curves.fastOutSlowIn);
                          setState(() {
                            _futureDashboard = _getDashboardData();
                          });
                        }
                      },
                    );
                  },
                  title: Text("Mark Unlisted Attendance"),
                  trailing: Icon(Icons.arrow_forward),
                ),
                ListTile(
                  title: Text(
                    "You are supposed to visit ${snapshot.data[0].territory.join(", ")}.",
                  ),
                  trailing: MyCustomButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              SuperManagerMarkAttendanceScreen("territory",
                                  snapshot.data[0].territory.join(",")),
                        ),
                      ).then(
                        (value) {
                          if (value != null) {
                            showToast(value,
                                context: context,
                                animation: StyledToastAnimation.slideFromBottom,
                                reverseAnimation:
                                    StyledToastAnimation.slideToBottom,
                                startOffset: Offset(0.0, 3.0),
                                reverseEndOffset: Offset(0.0, 3.0),
                                position: StyledToastPosition.bottom,
                                duration: Duration(seconds: 3),
                                animDuration: Duration(seconds: 1),
                                curve: Curves.elasticOut,
                                reverseCurve: Curves.fastOutSlowIn);
                            setState(() {
                              _futureDashboard = _getDashboardData();
                            });
                          }
                        },
                      );
                    },
                    child: Text(
                      "Visit",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        } else if (snapshot.hasError) {
          //~ snapshot.hasError
          print("$Home_Screen TerritoryVisit FutureError = ${snapshot.error}");
          if (snapshot.error.toString().contains("404")) {
            return Card(
              elevation: cardElevation,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(cardCornerRadius)),
              color: Colors.white,
              child: Column(
                children: [
                  //? Mark Unlisted Attendance
                  ListTile(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => UnlistedMarkAttendanceScreen(),
                        ),
                      ).then(
                        (value) {
                          if (value != null) {
                            showToast(value,
                                context: context,
                                animation: StyledToastAnimation.slideFromBottom,
                                reverseAnimation:
                                    StyledToastAnimation.slideToBottom,
                                startOffset: Offset(0.0, 3.0),
                                reverseEndOffset: Offset(0.0, 3.0),
                                position: StyledToastPosition.bottom,
                                duration: Duration(seconds: 3),
                                animDuration: Duration(seconds: 1),
                                curve: Curves.elasticOut,
                                reverseCurve: Curves.fastOutSlowIn);
                            setState(() {
                              _futureDashboard = _getDashboardData();
                            });
                          }
                        },
                      );
                    },
                    title: Text("Mark Unlisted Attendance"),
                    trailing: Icon(Icons.arrow_forward),
                  ),
                  ListTile(
                    title: Text("No Visits for today!"),
                  ),
                ],
              ),
            );
          }
          return Card(
            elevation: cardElevation,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(cardCornerRadius)),
            color: Colors.white,
            child: Padding(
              padding: EdgeInsets.all(cardContentPadding),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Something went Wrong!",
                        style: TextStyle(color: error),
                      ),
                      FlatButton(
                        onPressed: () {
                          setState(() {
                            _futureTodaysTerritoryVisits =
                                _getTodaysTerritoryVisits();
                          });
                        },
                        child: Text("Retry"),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        }
        return SizedBox.shrink();
      },
    );
  }

  /// Custom-Method to get Dashboard-Data (Endpoint=>users/dashboard-details)
  Future<Dashboard> _getDashboardData() async {
    Dashboard dashboard;
    try {
      Response response =
          await MyApi.getDataWithAuthorization("users/dashboard-details");
      print("$Home_Screen DashBoardResponse = ${response.data}");
      if (response.data['code'] == 401 || response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.data['code'] == 200) {
        dashboard = Dashboard.fromJson(response.data["data"]);
        setState(() {
          _userRecentAttendancePhoto = dashboard.recentAttendancePhoto;
          _totalSurveys = dashboard.cummulativeSurveysCount;
        });
        SharedPreferences.getInstance().then((value) {
          value.setDouble(
              "sick_leaves_available", dashboard.sickLeavesAvailable);
          value.setDouble(
              "casual_leaves_available", dashboard.casualLeavesAvailable);
          // value.setDouble(
          //     "monthly_attendance_count", dashboard.monthlyAttendanceCount);
          value.setDouble("yearly_leaves_taken", dashboard.yearlyLeavesTaken);
          value.setDouble(
              "yearly_leaves_remaining", dashboard.yearlyLeavesRemaining);
        });
      } else {
        throw Future.error("Error from Future");
      }
    } on SocketException {
      print("$Home_Screen DashBoard SocketException");
      throw Future.error(SocketException);
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage

          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        }
      }
      print("$Home_Screen DashBoard Exception = $error");
      throw Future.error(error);
    }
    return dashboard;
  }

  Future<void> callnow(String phoneNumber, BuildContext context) async {
    if (await canLaunch('tel:$phoneNumber')) {
      await launch('tel:$phoneNumber');
    } else {
      Scaffold.of(context)
          .showSnackBar(createSnackBar(msg: "Call Not Possible!"));
    }
  }

  Future<void> sendEmail(String email, BuildContext context) async {
    if (await canLaunch('mailto:$email')) {
      await launch('mailto:$email');
    } else {
      Scaffold.of(context)
          .showSnackBar(createSnackBar(msg: "Can't send Email now!"));
    }
  }
}
