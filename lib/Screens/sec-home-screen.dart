import 'dart:io';
import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:intl/intl.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/notice-card.dart';
import 'package:market_express/Components/donut-pie-chart.dart';
import 'package:market_express/Components/sec-navigation-drawer.dart';
import 'package:market_express/Models/attendance-status.dart';
import 'package:market_express/Models/dashboard.dart';
import 'package:market_express/Models/notice.dart';
import 'package:market_express/Screens/Forms/inventory-form.dart';
import 'package:market_express/Screens/leave-approval-screen.dart';
import 'package:market_express/Screens/Forms/leave-form.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Screens/mark-attendance-screen.dart';
import 'package:market_express/Screens/Forms/sales-form.dart';
import 'package:market_express/Screens/Forms/survey-form.dart';
import 'package:market_express/Screens/notification-screen.dart';
import 'package:market_express/Screens/sales-screen.dart';
import 'package:market_express/Screens/training-material-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class SECHomeScreen extends StatefulWidget {
  @override
  _SECHomeScreenState createState() => _SECHomeScreenState();
}

class _SECHomeScreenState extends State<SECHomeScreen> {
  Size screenSize;
  Future<AttendanceStatus> _futureAttendanceStatus;
  bool isMorningAttendanceMarked = false;
  Future<Dashboard> _futureDashboard;
  // Notice _notice;
  int _totalSurveys;
  String _userRecentAttendancePhoto;
  DateTime todaysDateTime = DateTime.now();
  @override
  void initState() {
    super.initState();
    _futureAttendanceStatus = _getAttendanceStatus();
    _futureDashboard = _getDashboardData();
  }

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: FutureBuilder<SharedPreferences>(
          future: SharedPreferences.getInstance(),
          builder: (context, snapshot) {
            if (snapshot.hasData && snapshot.data.containsKey("user_name")) {
              return FittedBox(
                child: Text(
                  snapshot.data.getString("user_name"),
                  style: TextStyle(color: Colors.white),
                ),
              );
            }
            return Text(
              'Home',
              style: TextStyle(color: Colors.white),
            );
          },
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.notifications,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => NotificationScreen(),
                  ));
            },
          ),
          IconButton(
            icon: Icon(
              Icons.file_download,
              // color: Colors.amberAccent,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => TrainingMaterialScreen(),
                  ));
            },
          ),
        ],
      ),
      drawer: SECNavigationDrawer(
          userRecentAttendancePhoto: _userRecentAttendancePhoto),
      bottomNavigationBar: _createFutureBuilderForBottomBar(context),
      //  FutureBuilder<SharedPreferences>(
      //   future: SharedPreferences.getInstance(),
      //   builder: (context, snapshot) {
      //     if (snapshot.hasData && snapshot.data.containsKey("user_type")) {
      //       return snapshot.data.getString("user_type") == "SEC"
      //           ? _createFutureBuilderForBottomBar(context)
      //           : SizedBox.shrink();
      //     }
      //     return SizedBox.shrink();
      //   },
      // ),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: greyBackground,
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          child: ListView(
            padding: EdgeInsets.all(subMargin),
            children: [
              FutureBuilder<Dashboard>(
                future: _futureDashboard,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Column(
                      children: [
                        snapshot.data.mostRecentNotice != null
                            ? NoticeCard(
                                snapshot.data.mostRecentNotice,
                                shouldHighlight: true,
                                isHomeScreen: true,
                              )
                            : SizedBox(),
                        _createFirstCard(snapshot.data),
                        snapshot.data.actualSales != null &&
                                snapshot.data.salesTarget != null &&
                                snapshot.data.salesTarget != 0
                            ? _createSalesTargetCard(
                                context,
                                snapshot.data.achievedValueOfMonth,
                                snapshot.data.targetValueOfMonth)
                            : SizedBox.shrink(),
                      ],
                    );
                  } else if (snapshot.connectionState ==
                      ConnectionState.waiting) {
                    return Center(
                      child: Container(
                        margin: EdgeInsets.all(subMargin),
                        width: 30,
                        height: 30,
                        child: CircularProgressIndicator(),
                      ),
                    );
                  } else if (snapshot.hasError) {
                    print("$Home_Screen FutureError = ${snapshot.error}");

                    return Card(
                      elevation: cardElevation,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(cardCornerRadius)),
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.all(cardContentPadding),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Something went Wrong!",
                              style: TextStyle(color: error),
                            ),
                            FlatButton(
                              onPressed: () {
                                setState(() {
                                  _futureDashboard = _getDashboardData();
                                });
                              },
                              child: Text("Retry"),
                            ),
                          ],
                        ),
                      ),
                    );
                  }
                  return SizedBox.shrink();
                },
              ),
              Builder(
                builder: (context) => _createQuickActionsForSEC(context),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Will show manager's contact-details and assigned store-data
  Widget _createFirstCard(Dashboard dashboard) {
    return Column(
      children: [
        Card(
          elevation: cardElevation,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(cardCornerRadius)),
          color: Colors.white,
          child: Padding(
            padding: EdgeInsets.all(cardContentPadding),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                dashboard.store != null
                    ? Text(
                        dashboard.store.storeName ?? 'Store Name',
                        style: TextStyle(
                            color: textPrimary,
                            fontWeight: FontWeight.w600,
                            fontSize: 14),
                      )
                    : SizedBox.shrink(),
                dashboard.store != null
                    ? Text(
                        'Store Assigned Date ' +
                                DateFormat.yMMMd()
                                    .format(dashboard.store.updatedAt) ??
                            'Date Assigned',
                        style: TextStyle(
                          color: colorPrimary,
                          fontSize: 12,
                        ),
                      )
                    : SizedBox.shrink(),
                dashboard.store != null
                    ? Padding(
                        padding: EdgeInsets.only(top: subMarginHalf),
                        child: Text(
                          "${dashboard.store.location}, ${dashboard.store.area}, ${dashboard.store.district}, ${dashboard.store.division}." ??
                              "Assigned Store Address",
                          style: TextStyle(
                            color: textSecondary,
                            fontSize: 12,
                          ),
                        ),
                      )
                    : SizedBox.shrink(),
                ListTile(
                  contentPadding: EdgeInsets.all(0),
                  dense: true,
                  leading: Image.asset(
                    'assets/images/user.png',
                    width: 40,
                    height: 40,
                    alignment: Alignment.center,
                  ),
                  title: Text(
                    dashboard.managerName ?? 'Manager Name',
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: textPrimary,
                    ),
                  ),
                  subtitle: Text(
                    'Reporting Manager',
                    style: TextStyle(
                      color: textSecondary,
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                    ),
                  ),
                  trailing: Builder(
                    builder: (context) => Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        IconButton(
                          constraints: BoxConstraints(
                            maxWidth: 45,
                            maxHeight: 45,
                            minWidth: 45,
                            minHeight: 45,
                          ),
                          padding: EdgeInsets.all(0),
                          visualDensity: VisualDensity.compact,
                          onPressed: () => sendEmail(
                              dashboard.managerEmail ?? "test@xcitech.com",
                              context),
                          icon: Image.asset(
                            'assets/images/email.png',
                          ),
                        ),
                        SizedBox(width: subMarginHalf),
                        IconButton(
                          constraints: BoxConstraints(
                            maxWidth: 45,
                            maxHeight: 45,
                            minWidth: 45,
                            minHeight: 45,
                          ),
                          padding: EdgeInsets.all(0),
                          visualDensity: VisualDensity.compact,
                          onPressed: () => callnow(
                              dashboard.managerPhone ?? "1234567890", context),
                          icon: Image.asset(
                            'assets/images/phone.png',
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
//> Total Attendace Count
                Table(
                  defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                  columnWidths: {1: FixedColumnWidth(1)},
                  children: [
                    TableRow(
                        decoration: BoxDecoration(
                          color: greyBackground,
                        ),
                        children: [
                          Center(
                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text:
                                        "${dashboard.monthlyAttendanceCount ?? 0} ",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                    ),
                                  ),
                                  TextSpan(
                                    text: "day(s)\n",
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: textSecondary,
                                    ),
                                  ),
                                  TextSpan(
                                    text:
                                        "Attendance: (${DateTime.now().formatDate(pattern: "MMM")})",
                                    style: TextStyle(
                                      color: Colors.black,
                                      height: 1.3,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          ColoredBox(
                            color: myGreyColor,
                            child: Text(
                              "",
                              style: TextStyle(height: 3.4),
                            ),
                          ),
                          Center(
                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text:
                                        // "${dashboard.yearlyLeavesTaken ?? 0}/${dashboard.yearlyLeavesRemaining ?? 0}\n",
                                        "${dashboard.totalSurveys ?? 0}\n",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                    ),
                                  ),
                                  // TextSpan(
                                  //   text: "availed/remaining\n",
                                  //   style: TextStyle(
                                  //     fontSize: 12,
                                  //     color: textSecondary,
                                  //   ),
                                  // ),
                                  TextSpan(
                                    text:
                                        "Surveys : (${DateTime.now().formatDate(pattern: "MMM")})",
                                    style: TextStyle(
                                      color: Colors.black,
                                      height: 1.3,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ]),
                  ],
                ),
              ],
            ),
          ),
        ),
// Leave approval card for the senior users to be shown when there is a leave application by junior users.
        dashboard.pendingLeavesCount != null
            ? Card(
                clipBehavior: Clip.antiAlias,
                elevation: cardElevation,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(cardCornerRadius)),
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ListTile(
                      // trailing: FlatButton(
                      //   onPressed: () {},
                      //   child: Text("See All",
                      //       style: TextStyle(color: colorPrimaryLight)),
                      // ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => LeaveApprovalScreen(),
                          ),
                        ).then((value) {
                          setState(() {
                            _futureDashboard = _getDashboardData();
                          });
                        });
                      },
                      title: Text(
                        "You have ${dashboard.pendingLeavesCount} pending leave request(s).",
                        maxLines: 2,
                      ),
                    ),
                  ],
                ),
              )
            : SizedBox.shrink(),
      ],
    );
  }

  Widget _createSalesTargetCard(
      BuildContext myContext, int actualSales, int salesTarget) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.only(bottom: cardContentPadding),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: screenSize.width / 2 - subMarginDouble,
              height:
                  screenSize.width / 2 - subMarginDouble - cardContentPadding,
              child: DonutPieChart.withDesiredData(
                actualSales,
                salesTarget,
                completedPercentage: ((actualSales / salesTarget) * 100),
                pendingPercentage:
                    ((salesTarget - actualSales) / salesTarget) * 100,
              ),
            ),
            Container(
              width: screenSize.width / 2 - subMarginDouble,
              height: (screenSize.width / 2 -
                      subMarginDouble -
                      cardContentPadding) /
                  1.3,
              // color: Colors.amber,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    child: Center(
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: 'Sales / Target\n',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16.5,
                                color: textPrimary,
                              ),
                            ),
                            TextSpan(
                              text: "$actualSales Tk /\n $salesTarget Tk",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                                color: textSecondary,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
//> History and Add+ Buttons
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: colorPrimaryLight.withOpacity(.34),
                                blurRadius: 4,
                                offset: Offset(0, 4),
                              ),
                            ],
                          ),
                          child: FlatButton(
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.circular(miniCardCornerRadius),
                            ),
                            color: colorPrimary,
                            padding: EdgeInsets.symmetric(
                              vertical: subMarginHalf,
                              horizontal: subMargin,
                            ),
                            child: Text(
                              'History',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                            onPressed: isMorningAttendanceMarked
                                ? () {
                                    Navigator.push(
                                      myContext,
                                      MaterialPageRoute(
                                        builder: (context) => SalesScreen(),
                                      ),
                                    );
                                  }
                                : () {
                                    Scaffold.of(myContext).showSnackBar(
                                        createSnackBar(
                                            msg:
                                                "Mark attendance to proceed!"));
                                  },
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(left: subMarginHalf),
                          decoration: BoxDecoration(boxShadow: [
                            BoxShadow(
                              color: colorSecondaryLight.withOpacity(.34),
                              blurRadius: 4,
                              offset: Offset(0, 4),
                            ),
                          ]),
                          child: FlatButton(
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.circular(miniCardCornerRadius),
                            ),
                            color: colorSecondary,
                            padding: EdgeInsets.symmetric(
                              vertical: subMarginHalf,
                              horizontal: subMargin,
                            ),
                            child: Text(
                              '+Add',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                            onPressed: isMorningAttendanceMarked
                                ? () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => SalesForm(),
                                      ),
                                    ).then(
                                      (value) {
                                        if (value != null) {
                                          Scaffold.of(myContext).showSnackBar(
                                              createSnackBar(
                                                  msg: value, isSuccess: true));
                                          setState(() {
                                            _futureDashboard =
                                                _getDashboardData();
                                          });
                                        }
                                      },
                                    );
                                  }
                                : () {
                                    Scaffold.of(myContext).showSnackBar(
                                        createSnackBar(
                                            msg:
                                                "Mark attendance to proceed!"));
                                  },
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// A card for Quick-Access to forms.
  Widget _createQuickActionsForSEC(BuildContext myContext) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.only(bottom: cardContentPadding),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: subMargin, vertical: subMarginHalf),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Quick Actions:",
                    style: TextStyle(
                      color: textPrimary,
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                    ),
                  ),
                  // _totalSurveys != null
                  //     ? Text(
                  //         "Total Surveys(${DateFormat("MMM").format(DateTime.now())}): ${_totalSurveys ?? 0}")
                  //     : SizedBox.shrink(),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.max,
              children: [
                Column(
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => LeaveForm(),
                          ),
                        );
                      },
                      icon: Image.asset("assets/images/calendar.png"),
                      iconSize: 50,
                    ),
                    Text(
                      "Leave",
                      style: TextStyle(
                        color: textPrimary,
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    IconButton(
                      onPressed: isMorningAttendanceMarked
                          ? () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => SurveyForm(),
                                ),
                              ).then(
                                (value) {
                                  if (value != null) {
                                    Scaffold.of(myContext).showSnackBar(
                                        createSnackBar(
                                            msg: value, isSuccess: true));
                                    setState(() {
                                      _futureDashboard = _getDashboardData();
                                    });
                                  }
                                },
                              );
                            }
                          : () {
                              Scaffold.of(myContext).showSnackBar(
                                  createSnackBar(
                                      msg: "Mark attendance to proceed!"));
                            },
                      icon: Image.asset("assets/images/survey.png"),
                      iconSize: 50,
                    ),
                    Text(
                      "Survey",
                      style: TextStyle(
                        color: textPrimary,
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    IconButton(
                      onPressed: isMorningAttendanceMarked
                          ? () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => SalesForm(),
                                ),
                              ).then(
                                (value) {
                                  print(
                                      "SECHome_Screen Navigator returned = $value");
                                  if (value != null) {
                                    Scaffold.of(myContext).showSnackBar(
                                        createSnackBar(
                                            msg: value, isSuccess: true));
                                    setState(() {
                                      _futureDashboard = _getDashboardData();
                                    });
                                  }
                                },
                              );
                            }
                          : () {
                              Scaffold.of(myContext).showSnackBar(
                                  createSnackBar(
                                      msg: "Mark attendance to proceed!"));
                            },
                      icon: Image.asset("assets/images/sales.png"),
                      iconSize: 50,
                    ),
                    Text(
                      "Sales",
                      style: TextStyle(
                        color: textPrimary,
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    IconButton(
                      onPressed: isMorningAttendanceMarked
                          ? () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => InventoryForm(),
                                ),
                              );
                            }
                          : () {
                              Scaffold.of(myContext).showSnackBar(
                                  createSnackBar(
                                      msg: "Mark attendance to proceed!"));
                            },
                      icon: Image.asset("assets/images/inventory.png"),
                      iconSize: 50,
                    ),
                    Text(
                      "Inventory",
                      style: TextStyle(
                        color: textPrimary,
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  /// Custom-Method to get Dashboard-Data (Endpoint=>users/dashboard-details)
  Future<Dashboard> _getDashboardData() async {
    Dashboard dashboard;
    try {
      Response response =
          await MyApi.getDataWithAuthorization("users/dashboard-details");
      print("$Home_Screen DashBoardResponse = ${response.data}");
      if (response.data['code'] == 401 || response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.data['code'] == 200) {
        dashboard = Dashboard.fromJson(response.data["data"]);
        setState(() {
          _userRecentAttendancePhoto = dashboard.recentAttendancePhoto;
          // _notice = dashboard.mostRecentNotice;
          _totalSurveys = dashboard.totalSurveys;
        });
        SharedPreferences.getInstance().then((value) {
          value.setDouble(
              "sick_leaves_available", dashboard.sickLeavesAvailable);
          value.setDouble(
              "casual_leaves_available", dashboard.casualLeavesAvailable);
          if (dashboard.store != null) {
            value.setString("store_id", dashboard.store.id);
          }
        });
      } else {
        throw Future.error("Error from Future");
      }
    } on SocketException {
      print("$Home_Screen DashBoard SocketException");
      throw Future.error(SocketException);
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage

          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        }
      }
      print("$Home_Screen DashBoard Exception = $error");
      throw Future.error(error);
    }
    return dashboard;
  }

  ///Future-Builder for bottom-status-bar
  Widget _createFutureBuilderForBottomBar(BuildContext context) {
    return FutureBuilder<AttendanceStatus>(
      future: _futureAttendanceStatus,
      builder: (context, snapshot) {
        if (snapshot.hasData &&
            snapshot.connectionState == ConnectionState.done) {
          return Container(
            padding: EdgeInsets.all(cardContentPadding),
            alignment: Alignment.center,
            height: 70,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(.05),
                  blurRadius: 4,
                  offset: Offset(0, -4),
                ),
              ],
              color: snapshot.data.isMorningAttendanceMarked &&
                      snapshot.data.isEveningAttendanceMarked
                  ? colorSecondary
                  : Colors.white,
            ),
            child: _createBottomStatusBar(snapshot.data),
          );
        } else if (snapshot.connectionState == ConnectionState.waiting) {
          return Container(
            padding: EdgeInsets.all(cardContentPadding),
            alignment: Alignment.center,
            height: 70,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(.05),
                  blurRadius: 4,
                  offset: Offset(0, -4),
                ),
              ],
              color: Colors.white,
            ),
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        } else if (snapshot.hasError) {
          if (snapshot.error == SocketException) {
            Future.delayed(Duration(milliseconds: 500)).then(
                (value) => Scaffold.of(context).showSnackBar(createSnackBar()));
          }
        }
        return Container(
          padding: EdgeInsets.all(cardContentPadding),
          alignment: Alignment.center,
          height: 70,
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(.05),
                blurRadius: 4,
                offset: Offset(0, -4),
              ),
            ],
            color: Colors.white,
          ),
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Failed to fetch Attendance Status!",
                  // style: TextStyle(color: error),
                ),
                FlatButton(
                  onPressed: () {
                    setState(() {
                      _futureAttendanceStatus = _getAttendanceStatus();
                    });
                  },
                  color: error,
                  child: Text(
                    "Retry",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  /// To show attendance status and mark attendance
  Widget _createBottomStatusBar(AttendanceStatus attendanceStatus) {
    if (attendanceStatus.isMorningAttendanceMarked &&
        attendanceStatus.isEveningAttendanceMarked) {
      // isAttendanceMarked = true;
      return Text(
        "You have marked today's attendance!",
        style: TextStyle(
          color: Colors.white,
          fontSize: 16,
          fontWeight: FontWeight.w600,
        ),
      );
    }
    return Table(
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      columnWidths: {
        0: FlexColumnWidth(2),
        1: FlexColumnWidth(1),
      },
      children: [
        TableRow(
          children: [
            Center(
                child: !attendanceStatus.isMorningAttendanceMarked
                    ? Text("Please mark your morning attendance.")
                    : Text("Please mark your evening attendance.")),
            Container(
              decoration: BoxDecoration(boxShadow: [
                BoxShadow(
                  color: colorSecondaryLight.withOpacity(.34),
                  blurRadius: 4,
                  offset: Offset(0, 4),
                ),
              ]),
              child: FlatButton(
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(miniCardCornerRadius),
                ),
                color: colorSecondary,
                padding: EdgeInsets.symmetric(vertical: subMargin),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Mark',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                      ),
                    ),
                    SizedBox(width: 5),
                    !attendanceStatus.isMorningAttendanceMarked
                        ? Icon(
                            Icons.brightness_7,
                            color: Colors.white,
                            size: 20,
                          )
                        : Icon(
                            Icons.brightness_3,
                            color: Colors.white,
                            size: 20,
                          ),
                  ],
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MarkAttendanceScreen(
                          type: !attendanceStatus.isMorningAttendanceMarked
                              ? 'morning'
                              : 'evening'),
                    ),
                  ).then((value) {
                    if (value != null) {
                      showToast(value,
                          context: context,
                          animation: StyledToastAnimation.slideFromBottom,
                          reverseAnimation: StyledToastAnimation.slideToBottom,
                          startOffset: Offset(0.0, 3.0),
                          reverseEndOffset: Offset(0.0, 3.0),
                          position: StyledToastPosition.bottom,
                          duration: Duration(seconds: 3),
                          animDuration: Duration(seconds: 1),
                          curve: Curves.elasticOut,
                          reverseCurve: Curves.fastOutSlowIn);
                      _getAttendanceStatus().then((value) {
                        setState(() {
                          _futureAttendanceStatus = Future.value(value);
                          _futureDashboard = _getDashboardData();
                          isMorningAttendanceMarked =
                              value.isMorningAttendanceMarked;
                        });
                      });
                    }
                  });
                },
              ),
            ),
          ],
        ),
      ],
    );
  }

  /// Custom-Method to get Attendace Status (Endpoint=>attendance/todays-attendance)
  Future<AttendanceStatus> _getAttendanceStatus() async {
    AttendanceStatus attendanceStatus;
    try {
      Response response =
          await MyApi.getDataWithAuthorization("attendance/todays-attendance");
      print("$Home_Screen Response = ${response.data}");
      if (response.data['code'] == 401 || response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.data['code'] == 200) {
        attendanceStatus = AttendanceStatus.fromJson(response.data["data"]);
        // if (mounted) {
        //   setState(() {});
        // }
        isMorningAttendanceMarked = attendanceStatus.isMorningAttendanceMarked;
        // if (attendanceStatus.isMorningAttendanceMarked &&
        //     attendanceStatus.isEveningAttendanceMarked) {
        //   isAttendanceMarked = true;
        // }
      } else {
        throw Future.error("Error from Future");
      }
    } on SocketException {
      print("$Home_Screen SocketException");
      throw Future.error(SocketException);
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage

          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        }
      }
      print("$Home_Screen Exception = $error");
      throw Future.error(error);
    }
    return attendanceStatus;
  }

  Future<void> callnow(String phoneNumber, BuildContext context) async {
    if (await canLaunch('tel:$phoneNumber')) {
      await launch('tel:$phoneNumber');
    } else {
      Scaffold.of(context)
          .showSnackBar(createSnackBar(msg: "Call Not Possible!"));
    }
  }

  Future<void> sendEmail(String email, BuildContext context) async {
    if (await canLaunch('mailto:$email')) {
      await launch('mailto:$email');
    } else {
      Scaffold.of(context)
          .showSnackBar(createSnackBar(msg: "Can't send Email now!"));
    }
  }
}
