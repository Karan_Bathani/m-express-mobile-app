import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/leave-card.dart';
import 'package:market_express/Models/leave.dart';
import 'package:market_express/Screens/Forms/leave-form.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LeaveHistoryScreen extends StatefulWidget {
  final String userId;
  final String userName;
  final String message;
  LeaveHistoryScreen({this.userId, this.userName, this.message});
  @override
  _LeaveHistoryScreenState createState() => _LeaveHistoryScreenState();
}

class _LeaveHistoryScreenState extends State<LeaveHistoryScreen> {
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();
  Future<List<Leave>> _futureLeaves;

  @override
  void initState() {
    super.initState();
    if (widget.message != null)
      WidgetsBinding.instance.addPostFrameCallback((_) {
        // executes after build
        showToast(
          widget.message,
          context: context,
          animation: StyledToastAnimation.slideFromBottom,
          reverseAnimation: StyledToastAnimation.slideToBottom,
          startOffset: Offset(0.0, 3.0),
          reverseEndOffset: Offset(0.0, 3.0),
          position: StyledToastPosition.bottom,
          duration: Duration(seconds: 3),
          animDuration: Duration(seconds: 1),
          curve: Curves.elasticOut,
          backgroundColor: success,
          reverseCurve: Curves.fastOutSlowIn,
        );
      });
    _futureLeaves = _getUserLeaveHistory();
  }

  int totalCount;
  Size screenSize;

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          widget.userName ?? 'My Leaves',
          style: TextStyle(color: Colors.white),
        ),
        actions: [
          totalCount != null
              ? Container(
                  margin: EdgeInsets.only(right: subMarginHalf),
                  height: kToolbarHeight,
                  width: kTextTabBarHeight,
                  child: Center(
                    child: Text(
                      "Total:\n$totalCount",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                )
              : SizedBox.shrink(),
        ],
      ),
      floatingActionButton: widget.userId != null
          ? null
          : Builder(
              builder: (buildContext) {
                return FloatingActionButton(
                  backgroundColor: colorPrimary,
                  child: Icon(Icons.add),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => LeaveForm(),
                      ),
                    )
                        // .then(
                        //   (value) {
                        //     if (value != null) {
                        //       Scaffold.of(context).showSnackBar(
                        //           createSnackBar(msg: value, isSuccess: true));
                        //       setState(() {
                        //         _futureLeaves = _getUserLeaveHistory();
                        //       });
                        //     }
                        //   },
                        // )
                        ;
                  },
                );
              },
            ),
      body: Container(
        clipBehavior: Clip.antiAlias,
        width: screenSize.width,
        height: screenSize.height,
        decoration: BoxDecoration(
          color: greyBackground,
          // Colors.white,
          borderRadius:
              BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
        ),
        // padding: EdgeInsets.all(subMargin),
        child: RefreshIndicator(
          key: _refreshIndicatorKey,
          onRefresh: () {
            setState(() {
              _futureLeaves = _getUserLeaveHistory();
            });
            return _futureLeaves;
          },
          child: FutureBuilder<List<Leave>>(
            future: _futureLeaves,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                //~ Error
                if (snapshot.error.toString().contains("404")) {
                  return buildEmptyWidget(screenSize, text: "No Leaves Found");
                }
                return buildErrorWidget(screenSize);
              } else if (snapshot.hasData &&
                  snapshot.connectionState == ConnectionState.done) {
                //< SUCCESSS
                if (snapshot.data.length != 0) {
                  return _buildLeaveListView(snapshot.data);
                }
              } else if (snapshot.connectionState == ConnectionState.waiting) {
                _refreshIndicatorKey.currentState?.show();
                return _buildLeaveListView(snapshot.data);
              }
              return SizedBox.expand(
                child: ListView(),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _buildLeaveListView(List<Leave> leaves) {
    return ListView.builder(
      itemCount: leaves != null ? leaves.length : 0,
      itemBuilder: (context, index) => LeaveCard(leaves[index]),
    );
  }

  Future<List<Leave>> _getUserLeaveHistory() async {
    List<Leave> leaves = [];
    try {
      Response response = widget.userId != null
          ? await MyApi.postDataWithAuthorization(
              {"year": DateTime.now().year, "user_id": widget.userId},
              "users/yearly-leaves-history")
          : await MyApi.postDataWithAuthorization({
              "status": ["accepted", "rejected", "pending"],
            }, "users/leave-history");
      print("$LeaveHistory_Screen Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var leave in response.data["data"]) {
          leaves.add(Leave.fromJson(leave));
        }
      } else {
        throw Future.error("Error from Future");
      }
    } on SocketException {
      print("$LeaveHistory_Screen SocketException");
      throw Future.error(SocketException);
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        }
      }
      print("$LeaveHistory_Screen Exception = $error");
      throw Future.error(error);
    }
    return leaves;
  }
}
