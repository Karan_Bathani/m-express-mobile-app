import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/my-custom-button.dart';
import 'package:market_express/Models/product-suggestion.dart';
import 'package:market_express/Screens/inventory-transaction-screen.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_autocomplete_formfield/simple_autocomplete_formfield.dart';

class InventoryForm extends StatefulWidget {
  @override
  _InventoryFormState createState() => _InventoryFormState();
}

class _InventoryFormState extends State<InventoryForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autovalidate = false;

  Size screenSize;
  String productId;

  TextEditingController deviceModelController = TextEditingController();
  TextEditingController quantityController = TextEditingController();
  TextEditingController remarkController = TextEditingController();

  Future<Response<dynamic>> _futureProductSuggestions;
  List<String> productSuggestions = [];
  List<ProductSuggestion> modelProductSuggestions = [];

  @override
  void initState() {
    super.initState();
    _futureProductSuggestions = MyApi.getDataWithAuthorization("products/list");
  }

  @override
  void dispose() {
    quantityController.dispose();
    remarkController.dispose();
    deviceModelController.dispose();
    super.dispose();
  }

  FocusScopeNode currentNode;

  @override
  Widget build(BuildContext context) {
    currentNode = FocusScope.of(context);
    screenSize = MediaQuery.of(context).size;

    return GestureDetector(
      onTap: () {
        currentNode.unfocus();
      },
      child: Scaffold(
        backgroundColor: colorPrimary,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: Text(
            'Add Inventory',
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: SafeArea(
          child: Container(
            clipBehavior: Clip.antiAlias,
            width: screenSize.width,
            height: screenSize.height,
            decoration: BoxDecoration(
              color:
                  //  Colors.black,
                  Color(0xfff5f5f5),
              borderRadius:
                  BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
            ),
            child: Form(
              key: _formKey,
              autovalidate: _autovalidate,
              child: ListView(
                padding: EdgeInsets.fromLTRB(
                  subMargin,
                  0,
                  subMargin,
                  subMargin,
                ),
                children: [
                  SizedBox(height: subMargin),
// Device Model
                  FutureBuilder<Response<dynamic>>(
                    future: _futureProductSuggestions,
                    builder: (context, snapshot) {
                      if (snapshot.hasError) {
                        if (snapshot.error == SocketException ||
                            snapshot.error
                                .toString()
                                .contains("SocketException")) {
                          Future.delayed(Duration(milliseconds: 500))
                              .then((value) {
                            Scaffold.of(context).showSnackBar(createSnackBar());
                          });
                        }
                      } else if (snapshot.hasData) {
                        if (productSuggestions != null &&
                            productSuggestions.length != 0) {
                          productSuggestions.clear();
                          modelProductSuggestions.clear();
                        }
                        snapshot.data.data["data"].forEach((element) {
                          // print(
                          //     "$Inventory_Form ${ProductSuggestion.fromJson(element).deviceModel}");
                          productSuggestions.add(
                              ProductSuggestion.fromJson(element).deviceModel);
                          modelProductSuggestions
                              .add(ProductSuggestion.fromJson(element));
                        });
                      }
                      return SimpleAutocompleteFormField<ProductSuggestion>(
                        controller: deviceModelController,
                        keyboardType: TextInputType.text,
                        style: TextStyle(
                          color: textPrimary,
                          fontWeight: FontWeight.w600,
                        ),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.grey[600],
                            ),
                          ),
                          labelText: 'Device Model',
                          hintText: 'Enter the Device Model',
                        ),
                        validator: (value) {
                          print("$Inventory_Form Value = $value");
                          // print(
                          //     "$Inventory_Form TextValue = ${deviceModelController.text}");
                          if (deviceModelController.text == null ||
                              deviceModelController.text.isEmpty ||
                              deviceModelController.text.length == 0) {
                            return "Please enter a Device-Model";
                          } else if (!productSuggestions
                              .contains(deviceModelController.text)) {
                            return "Select a model from the list";
                          } else if (value != null &&
                              !productSuggestions.contains(value.deviceModel)) {
                            return "Select a model from the list";
                          } else if (value != null) {
                            int tempIndex = modelProductSuggestions.indexWhere(
                                (element) =>
                                    element.deviceModel == value.deviceModel);
                            productId = modelProductSuggestions[tempIndex].id;
                            // print(
                            //     "$Inventory_Form Else ProductId = $productId");
                          }
                          return null;
                        },
                        itemToString: (item) =>
                            item == null ? "" : item.deviceModel,
                        onSearch: (search) async {
                          if (productSuggestions.isEmpty ||
                              productSuggestions.length == 0) {
                            if (!mounted) {
                              setState(() {
                                _futureProductSuggestions =
                                    MyApi.getDataWithAuthorization(
                                        "products/list");
                              });
                            }
                          }
                          return modelProductSuggestions
                              .where(
                                (element) =>
                                    element.deviceModel.toLowerCase().contains(
                                          search.toLowerCase(),
                                        ),
                              )
                              .toList();
                        },
                        itemBuilder: (context, item) {
                          return Padding(
                            padding: EdgeInsets.all(12),
                            child: Text(item.deviceModel),
                          );
                        },
                        maxSuggestions: modelProductSuggestions != null &&
                                modelProductSuggestions.length != 0
                            ? modelProductSuggestions.length
                            : 0,
                        suggestionsBuilder: (context, items) {
                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return Card(
                              elevation: cardElevation,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.circular(cardCornerRadius)),
                              color: Colors.white,
                              clipBehavior: Clip.antiAlias,
                              margin: EdgeInsets.symmetric(
                                // horizontal: subMargin,
                                vertical: subMarginHalf,
                              ),
                              child: Center(
                                child: Container(
                                  margin: EdgeInsets.all(subMargin),
                                  child: CircularProgressIndicator(),
                                ),
                              ),
                            );
                          } else if (snapshot.hasError) {
                            return SizedBox.shrink();
                            return Card(
                              elevation: cardElevation,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.circular(cardCornerRadius)),
                              color: Colors.white,
                              clipBehavior: Clip.antiAlias,
                              margin: EdgeInsets.symmetric(
                                // horizontal: subMargin,
                                vertical: subMarginHalf,
                              ),
                              child: Center(
                                child: Container(
                                  margin: EdgeInsets.all(subMargin),
                                  child: Text("Something Went Wrong!"),
                                ),
                              ),
                            );
                          }
                          return Card(
                            elevation: cardElevation,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.circular(cardCornerRadius)),
                            color: Colors.white,
                            clipBehavior: Clip.antiAlias,
                            margin: EdgeInsets.symmetric(
                              // horizontal: subMargin,
                              vertical: subMarginHalf,
                            ),
                            child: SizedBox(
                              width: double.infinity,
                              height: 200,
                              child: ListView(
                                children: items,
                              ),
                            ),
                          );
                        },
                      );
                    },
                  ),
                  SizedBox(height: subMargin),
// Quantity
                  TextFormField(
                    controller: quantityController,
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Invalid Quantity";
                      }
                      if (int.parse(value) == 0) {
                        return "Cannot be 0";
                      }
                      return null;
                    },
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    keyboardType: TextInputType.number,
                    style: TextStyle(
                      color: textPrimary,
                      fontWeight: FontWeight.w600,
                    ),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.grey[600],
                        ),
                      ),
                      labelText: 'Quantity',
                      helperStyle: TextStyle(
                        color: textTertiary,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  SizedBox(height: subMargin),
// Remarks
                  TextField(
                    controller: remarkController,
                    // validator: (value) {
                    //   if (value.isEmpty) {
                    //     return "Please enter remark(s)";
                    //   }
                    //   return null;
                    // },
                    keyboardType: TextInputType.text,
                    style: TextStyle(
                      color: textPrimary,
                      fontWeight: FontWeight.w600,
                    ),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.grey[600],
                        ),
                      ),
                      labelText: 'Remark(s)',
                      hintText: 'Enter Remark(s)',
                      helperText: "Optional",
                      helperStyle: TextStyle(
                        color: textTertiary,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  SizedBox(height: subMargin),
// Submit Button
                  MyCustomButton(
                    padding: EdgeInsets.symmetric(
                      vertical: subMargin,
                      horizontal: subMarginDouble,
                    ),
                    onPressed: () => _onSubmitInventory(context),
                  ),
                  SizedBox(height: subMargin),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _onSubmitInventory(BuildContext context) async {
    currentNode.unfocus();
    try {
      if (!_formKey.currentState.validate() ||
          productId == null ||
          productId.isEmpty) {
        setState(() {
          _autovalidate = true;
        });
        return;
      }
      showMyLoadingDialog(dialogText: "Submitting inventory ...\nPlease Wait!");
      Map<String, String> data = {
        "product_id": productId,
        "quantity": quantityController.text,
      };
      if (remarkController.text.isNotEmpty) {
        data.addAll({
          "remark": remarkController.text,
        });
      }

      Response response =
          await MyApi.postDataWithAuthorization(data, "stores/add-inventory");
      print("$Inventory_Form Response = $response");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        // Survey Submitted Successfully...
        dismissMyDialog();
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => InventoryTransactionScreen(
              message: response.data["message"],
            ),
          ),
          (route) {
            // print("$Leave_Form Route = ${route.settings.name}");
            return route.settings.name == "/home";
          },
        );
        // Navigator.pop(context, response.data["message"]);
      } else if (response.statusCode == 422) {
        // Leave already applied for this date...
        dismissMyDialog();
        print("$Inventory_Form Error 422");
        showToast(response.data["message"],
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
      } else {
        dismissMyDialog();
        print("$Inventory_Form Error => Else");
        Scaffold.of(context)
            .showSnackBar(createSnackBar(msg: "Something Went Wrong!"));
      }
    } on SocketException {
      dismissMyDialog();
      print("$Inventory_Form Exception = $error");
      Scaffold.of(context).showSnackBar(createSnackBar());
    } catch (error) {
      dismissMyDialog();
      if (error is DioError) {
        if (error.response.statusCode == 422) {
          print("$Inventory_Form Dio Error 422 = $error");
          showToast(error.response.data["message"],
              context: context,
              animation: StyledToastAnimation.slideFromBottom,
              reverseAnimation: StyledToastAnimation.slideToBottom,
              startOffset: Offset(0.0, 3.0),
              reverseEndOffset: Offset(0.0, 3.0),
              position: StyledToastPosition.bottom,
              duration: Duration(seconds: 3),
              animDuration: Duration(seconds: 1),
              curve: Curves.elasticOut,
              reverseCurve: Curves.fastOutSlowIn);
        }
        print("$Inventory_Form Dio Error = $error");
        showToast(error.response.data["message"],
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
      } else {
        print("$Inventory_Form Exception = $error");
        Scaffold.of(context)
            .showSnackBar(createSnackBar(msg: "Something Went Wrong!"));
      }
    }
  }
}
