import 'dart:io';

import 'package:dio/dio.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/my-custom-button.dart';
import 'package:market_express/Models/product-suggestion.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_autocomplete_formfield/simple_autocomplete_formfield.dart';

class SurveyForm extends StatefulWidget {
  @override
  _SurveyFormState createState() => _SurveyFormState();
}

class _SurveyFormState extends State<SurveyForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autovalidate = false;

  TextEditingController nameController = TextEditingController();
  TextEditingController numberController =
      MaskedTextController(mask: "00000000000");
  TextEditingController ageController = MaskedTextController(mask: "00");
  TextEditingController emailController = TextEditingController();
  TextEditingController interestedInController = TextEditingController();
  TextEditingController maxBudgetController = TextEditingController();
  TextEditingController minBudgetController = TextEditingController();
  TextEditingController currentPhoneController = TextEditingController();

  Future<Response<dynamic>> _futureProductSuggestions;
  List<String> productSuggestions = [];
  List<ProductSuggestion> modelProductSuggestions = [];

  @override
  void initState() {
    super.initState();
    _futureProductSuggestions = MyApi.getDataWithAuthorization("products/list");
  }

  @override
  void dispose() {
    nameController.dispose();
    numberController.dispose();
    ageController.dispose();
    emailController.dispose();
    interestedInController.dispose();
    maxBudgetController.dispose();
    minBudgetController.dispose();
    currentPhoneController.dispose();
    super.dispose();
  }

  Size screenSize;
  FocusScopeNode currentNode;
  @override
  Widget build(BuildContext context) {
    currentNode = FocusScope.of(context);
    screenSize = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        currentNode.unfocus();
      },
      child: Scaffold(
        backgroundColor: colorPrimary,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: Text(
            'Survey Form',
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: SafeArea(
          child: Container(
            clipBehavior: Clip.antiAlias,
            width: screenSize.width,
            height: screenSize.height,
            decoration: BoxDecoration(
              color:
                  //  Colors.black,
                  Color(0xfff5f5f5),
              borderRadius:
                  BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
            ),
            child: Form(
              key: _formKey,
              autovalidate: _autovalidate,
              child: SingleChildScrollView(
                padding: EdgeInsets.fromLTRB(
                  subMargin,
                  0,
                  subMargin,
                  subMargin,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(height: subMargin),
// Customer Name
                    TextFormField(
                      controller: nameController,
                      keyboardType: TextInputType.name,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please enter consumer name";
                        }
                        return null;
                      },
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(
                            RegExp(r"[a-zA-z][\s]?")),
                        FilteringTextInputFormatter.deny(RegExp("[\[]")),
                        FilteringTextInputFormatter.deny(RegExp("[\]]")),
                        FilteringTextInputFormatter.deny(RegExp("[\\/]")),
                        FilteringTextInputFormatter.deny(RegExp("[\`]")),
                        FilteringTextInputFormatter.deny(RegExp("[\_]")),
                      ],
                      style: TextStyle(
                        color: textPrimary,
                        fontWeight: FontWeight.w600,
                      ),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.grey[600],
                          ),
                        ),
                        labelText: 'Name',
                        // hintText: 'Consumer Name',
                      ),
                    ),
                    SizedBox(height: subMargin),
// Contact Number
                    TextFormField(
                      controller: numberController,
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please enter contact details";
                        } else if (value.isNotEmpty && value.length != 11) {
                          return "Please enter a valid contact number";
                        }
                        return null;
                      },
                      keyboardType: TextInputType.phone,
                      style: TextStyle(
                        color: textPrimary,
                        fontWeight: FontWeight.w600,
                      ),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.grey[600],
                          ),
                        ),
                        labelText: 'Contact Number',
                        // hintText: 'Enter contact number',
                      ),
                    ),
                    SizedBox(height: subMargin),
// Age and Email (Both Optional)
                    Table(
                      columnWidths: {
                        0: FlexColumnWidth(1.5),
                        1: FixedColumnWidth(subMargin),
                        2: FlexColumnWidth(3),
                      },
                      children: [
                        TableRow(children: [
                          TextFormField(
                            controller: ageController,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly
                            ],
                            validator: (value) {
                              if (value.isNotEmpty && value.length > 3) {
                                return "Invalid Age";
                              }
                              return null;
                            },
                            keyboardType: TextInputType.number,
                            style: TextStyle(
                              color: textPrimary,
                              fontWeight: FontWeight.w600,
                            ),
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.grey[600],
                                ),
                              ),
                              labelText: 'Age',
                              // hintText: 'Enter age',
                              helperText: "Optional",
                              helperStyle: TextStyle(
                                color: textTertiary,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          SizedBox(width: subMargin),
// Email Address
                          TextFormField(
                            controller: emailController,
                            validator: (value) {
                              if (value.isNotEmpty && !validateEmail(value)) {
                                return "Please enter a valid email";
                              }
                              return null;
                            },
                            keyboardType: TextInputType.emailAddress,
                            style: TextStyle(
                              color: textPrimary,
                              fontWeight: FontWeight.w600,
                            ),
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.grey[600],
                                ),
                              ),
                              labelText: 'Email Address',
                              // hintText: 'Enter consumer email',
                              helperText: "Optional",
                              helperStyle: TextStyle(
                                color: textTertiary,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ]),
                      ],
                    ),
                    SizedBox(height: subMargin),
// Interested In
                    FutureBuilder<Response<dynamic>>(
                      future: _futureProductSuggestions,
                      builder: (context, snapshot) {
                        if (snapshot.hasError) {
                          if (snapshot.error == SocketException ||
                              snapshot.error
                                  .toString()
                                  .contains("SocketException")) {
                            Future.delayed(Duration(milliseconds: 500))
                                .then((value) {
                              Scaffold.of(context)
                                  .showSnackBar(createSnackBar());
                            });
                          }
                        } else if (snapshot.hasData) {
                          if (productSuggestions != null &&
                              productSuggestions.length != 0) {
                            productSuggestions.clear();
                            modelProductSuggestions.clear();
                          }
                          snapshot.data.data["data"].forEach((element) {
                            // print(
                            //     "$Survey_Form ${ProductSuggestion.fromJson(element).deviceModel}");
                            productSuggestions.add(
                                ProductSuggestion.fromJson(element)
                                    .deviceModel);
                            modelProductSuggestions
                                .add(ProductSuggestion.fromJson(element));
                          });
                        }
                        return SimpleAutocompleteFormField<ProductSuggestion>(
                          controller: interestedInController,
                          validator: (value) {
                            print("$Survey_Form Value = $value");
                            // print(
                            //     "$Survey_Form TextValue = ${interestedInController.text}");
                            if (interestedInController.text == null ||
                                interestedInController.text.isEmpty ||
                                interestedInController.text.length == 0) {
                              return "Please enter a phone model";
                            }
                            return null;
                          },
                          keyboardType: TextInputType.text,
                          style: TextStyle(
                            color: textPrimary,
                            fontWeight: FontWeight.w600,
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.grey[600],
                              ),
                            ),
                            labelText: 'Interested In',
                            // hintText: 'Samsung Phone Model',
                          ),
                          itemToString: (item) =>
                              item == null ? "" : item.deviceModel,
                          onSearch: (search) async {
                            if ((productSuggestions.isEmpty ||
                                    productSuggestions.length == 0) &&
                                snapshot.connectionState ==
                                    ConnectionState.done) {
                              if (!mounted) {
                                setState(() {
                                  _futureProductSuggestions =
                                      MyApi.getDataWithAuthorization(
                                          "products/list");
                                });
                              }
                            }
                            return modelProductSuggestions
                                .where(
                                  (element) => element.deviceModel
                                      .toLowerCase()
                                      .contains(
                                        search.toLowerCase(),
                                      ),
                                )
                                .toList();
                          },
                          itemBuilder: (context, item) {
                            return Padding(
                              padding: EdgeInsets.all(12),
                              child: Text(item.deviceModel),
                            );
                          },
                          maxSuggestions: modelProductSuggestions != null &&
                                  modelProductSuggestions.length != 0
                              ? modelProductSuggestions.length
                              : 0,
                          suggestionsBuilder: (context, items) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return Card(
                                elevation: cardElevation,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        cardCornerRadius)),
                                color: Colors.white,
                                clipBehavior: Clip.antiAlias,
                                margin: EdgeInsets.symmetric(
                                  // horizontal: subMargin,
                                  vertical: subMarginHalf,
                                ),
                                child: Center(
                                  child: Container(
                                    margin: EdgeInsets.all(subMargin),
                                    child: CircularProgressIndicator(),
                                  ),
                                ),
                              );
                            } else if (snapshot.hasError) {
                              return SizedBox.shrink();
                              return Card(
                                elevation: cardElevation,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        cardCornerRadius)),
                                color: Colors.white,
                                clipBehavior: Clip.antiAlias,
                                margin: EdgeInsets.symmetric(
                                  // horizontal: subMargin,
                                  vertical: subMarginHalf,
                                ),
                                child: Center(
                                  child: Container(
                                    margin: EdgeInsets.all(subMargin),
                                    child: Text("Something Went Wrong!"),
                                  ),
                                ),
                              );
                            }
                            return Card(
                              elevation: cardElevation,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.circular(cardCornerRadius)),
                              color: Colors.white,
                              clipBehavior: Clip.antiAlias,
                              margin: EdgeInsets.symmetric(
                                // horizontal: subMargin,
                                vertical: subMarginHalf,
                              ),
                              child: SizedBox(
                                width: double.infinity,
                                height: 200,
                                child: ListView(
                                  children: items,
                                ),
                              ),
                            );
                          },
                        );
                      },
                    ),
                    SizedBox(height: subMargin),
// Budget
                    Table(
                      columnWidths: {
                        0: FlexColumnWidth(1),
                        1: FixedColumnWidth(subMargin),
                        2: FlexColumnWidth(1),
                      },
                      // defaultVerticalAlignment:
                      //     TableCellVerticalAlignment.,
                      children: [
                        TableRow(
                          children: [
// Minimum
                            TextFormField(
                              controller: minBudgetController,
                              keyboardType: TextInputType.number,
                              // onChanged: (val) {
                              //   print("$Survey_Form ${val.replaceFirst("-", " ")}");
                              //   "$Survey_Form ${val.substring(val.length - 1, val.length)}");
                              //   if (val.replaceFirst("-", " ").contains("-")) {
                              //     budgetController.text = "qwer";
                              //   }
                              //   if (val.contains("-") &&
                              //       val.substring(val.length - 1, val.length) ==
                              //           "-") {
                              //     budgetController.text = budgetController.text
                              //         .substring(0, val.length - 1);
                              //   }
                              // },
                              inputFormatters: [
                                // FilteringTextInputFormatter.allow(
                                //     RegExp(r"[0-9]+[-]?")),
                                FilteringTextInputFormatter.digitsOnly,
                              ],
                              validator: (value) {
                                if (value.isNotEmpty &&
                                    maxBudgetController.text.isNotEmpty) {
                                  if (int.parse(value) >=
                                      int.parse(maxBudgetController.text))
                                    return "Invalid Range";
                                }
                                // else if (value.isNotEmpty &&
                                //     value
                                //         .replaceFirst("-", " ")
                                //         .contains("-")) {
                                //   return "Please enter a valid range";
                                // }
                                return null;
                              },
                              style: TextStyle(
                                color: textPrimary,
                                fontWeight: FontWeight.w600,
                              ),
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.grey[600],
                                  ),
                                ),
                                labelText: 'Minimun Budget',
                                // hintText: 'Minimun Budget',
                                helperText: "Optional",
                                helperStyle: TextStyle(
                                  color: textTertiary,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            Center(
                              child: Text(
                                "-",
                                style: TextStyle(
                                  height: 3,
                                  color: textSecondary,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
// Maximum
                            TextFormField(
                              controller: maxBudgetController,
                              keyboardType: TextInputType.number,
                              // onChanged: (val) {
                              //   print("$Survey_Form ${val.replaceFirst("-", " ")}");
                              //   "$Survey_Form ${val.substring(val.length - 1, val.length)}");
                              //   if (val.replaceFirst("-", " ").contains("-")) {
                              //     budgetController.text = "qwer";
                              //   }
                              //   if (val.contains("-") &&
                              //       val.substring(val.length - 1, val.length) ==
                              //           "-") {
                              //     budgetController.text = budgetController.text
                              //         .substring(0, val.length - 1);
                              //   }
                              // },
                              inputFormatters: [
                                // FilteringTextInputFormatter.allow(
                                //     RegExp(r"[0-9]+[-]?")),
                                FilteringTextInputFormatter.digitsOnly,
                              ],
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Enter maximum budget";
                                }
                                // else if (value.isNotEmpty &&
                                //     value
                                //         .replaceFirst("-", " ")
                                //         .contains("-")) {
                                //   return "Please enter a valid range";
                                // }
                                return null;
                              },
                              style: TextStyle(
                                color: textPrimary,
                                fontWeight: FontWeight.w600,
                              ),
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.grey[600],
                                  ),
                                ),
                                labelText: 'Maximum Budget',
                                // hintText: 'Minimun Budget',
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: subMargin),
// Current Phone
                    TextFormField(
                      controller: currentPhoneController,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please enter current phone details";
                        }
                        return null;
                      },
                      keyboardType: TextInputType.text,
                      style: TextStyle(
                        color: textPrimary,
                        fontWeight: FontWeight.w600,
                      ),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.grey[600],
                          ),
                        ),
                        labelText: 'Current Phone',
                        // hintText: 'Name and Model',
                      ),
                    ),
                    SizedBox(height: subMargin),
                    MyCustomButton(
                      padding: EdgeInsets.symmetric(
                        vertical: subMargin,
                        horizontal: subMarginDouble,
                      ),
                      onPressed: () => _onSubmitSurvey(context),
                    ),
                    SizedBox(height: subMargin),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _onSubmitSurvey(BuildContext context) async {
    currentNode.unfocus();
    try {
      if (!_formKey.currentState.validate()) {
        setState(() {
          _autovalidate = true;
        });
        return;
      }
      showMyLoadingDialog(dialogText: "Submitting the survey...\nPlease Wait!");
      String userBudget;
      if (minBudgetController.text.trim() == null ||
          minBudgetController.text.trim().isEmpty) {
        userBudget = maxBudgetController.text;
      } else {
        userBudget = "${minBudgetController.text}-${maxBudgetController.text}";
      }
      print("$Survey_Form Budget $userBudget");
      Map<String, String> data = {
        "customer_name": nameController.text,
        "customer_contact_number": numberController.text,
        "customer_budget": userBudget,
        "customer_current_phone": currentPhoneController.text,
        "interested_in": interestedInController.text,
      };
      if (ageController.text.isNotEmpty) {
        data.addAll({
          'customer_age': ageController.text,
        });
      }
      if (emailController.text.isNotEmpty) {
        data.addAll({
          "customer_email": emailController.text,
        });
      }
      Response response =
          await MyApi.postDataWithAuthorization(data, "users/add-survey");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        // Survey Submitted Successfully...
        dismissMyDialog();
        Navigator.pop(context, response.data["message"]);
      } else if (response.statusCode == 422) {
        // Leave already applied for this date...
        dismissMyDialog();
        showToast(response.data["message"],
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
      } else {
        dismissMyDialog();
        Scaffold.of(context)
            .showSnackBar(createSnackBar(msg: "Something Went Wrong!"));
      }
    } on SocketException {
      dismissMyDialog();
      print("$Survey_Form Exception = $error");
      Scaffold.of(context).showSnackBar(createSnackBar());
    } catch (error) {
      dismissMyDialog();
      if (error is DioError) {
        if (error.response.statusCode == 422) {
          print("$Survey_Form Dio Error = $error");
          showToast(error.response.data["message"],
              context: context,
              animation: StyledToastAnimation.slideFromBottom,
              reverseAnimation: StyledToastAnimation.slideToBottom,
              startOffset: Offset(0.0, 3.0),
              reverseEndOffset: Offset(0.0, 3.0),
              position: StyledToastPosition.bottom,
              duration: Duration(seconds: 3),
              animDuration: Duration(seconds: 1),
              curve: Curves.elasticOut,
              reverseCurve: Curves.fastOutSlowIn);
        }
        print("$Inventory_Form Dio Error = $error");
        showToast(error.response.data["message"],
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
      } else {
        print("$Survey_Form Exception = $error");
        Scaffold.of(context)
            .showSnackBar(createSnackBar(msg: "Something Went Wrong!"));
      }
    }
  }
}
