import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Models/competitive-product.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_autocomplete_formfield/simple_autocomplete_formfield.dart';

class CompetitionInfoForm extends StatefulWidget {
  @override
  _CompetitionInfoFormState createState() => _CompetitionInfoFormState();
}

class _CompetitionInfoFormState extends State<CompetitionInfoForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  Future<Response<dynamic>> _futureCompetitiveProducts;
  List<String> competitiveProducts = [];
  List<CompetitiveProduct> modelCompetitiveProducts = [];

  Map<String, CompetitiveProduct> _selectedProducts = {};
  Map<String, TextEditingController> _priceControllers = {};
  Map<String, TextEditingController> _featureControllers = {};

  @override
  void initState() {
    super.initState();
    _futureCompetitiveProducts =
        MyApi.getDataWithAuthorization("competition/get-competitive-products");
  }

  @override
  void dispose() {
    _priceControllers.values.toList().forEach((element) {
      element.dispose();
    });
    _featureControllers.values.toList().forEach((element) {
      element.dispose();
    });
    super.dispose();
  }

  Size screenSize;
  FocusScopeNode currentNode;
  bool _autoValidate = false;

  @override
  Widget build(BuildContext context) {
    currentNode = FocusScope.of(context);
    screenSize = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        currentNode.unfocus();
      },
      child: Scaffold(
        backgroundColor: colorPrimary,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: Text(
            'Competition Info',
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: SafeArea(
          child: Container(
            clipBehavior: Clip.antiAlias,
            width: screenSize.width,
            height: screenSize.height,
            decoration: BoxDecoration(
              color: Color(0xfff5f5f5),
              borderRadius:
                  BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
            ),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: subMargin),
// Competitive Products
                  FutureBuilder<Response<dynamic>>(
                    future: _futureCompetitiveProducts,
                    builder: (context, snapshot) {
                      if (snapshot.hasError) {
                        if (snapshot.error == SocketException ||
                            snapshot.error
                                .toString()
                                .contains("SocketException")) {
                          Future.delayed(Duration(milliseconds: 500))
                              .then((value) {
                            Scaffold.of(context).showSnackBar(createSnackBar());
                          });
                        }
                      } else if (snapshot.hasData) {
                        if (competitiveProducts != null &&
                            competitiveProducts.length != 0) {
                          competitiveProducts.clear();
                          modelCompetitiveProducts.clear();
                        }
                        snapshot.data.data["data"].forEach((element) {
                          // print(
                          //     "CompetitionInfoForm ${CompetitiveProduct.fromJson(element)}");
                          competitiveProducts
                              .add(CompetitiveProduct.fromJson(element).model);
                          modelCompetitiveProducts
                              .add(CompetitiveProduct.fromJson(element));
                        });
                      }
                      return Padding(
                        padding: EdgeInsets.symmetric(horizontal: subMargin),
                        child: SimpleAutocompleteFormField<CompetitiveProduct>(
                          keyboardType: TextInputType.text,
                          style: TextStyle(
                            color: textPrimary,
                            fontWeight: FontWeight.w600,
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.grey[600],
                              ),
                            ),
                            labelText: "Select Product",
                            // hintText: 'Samsung Phone Model',
                          ),
                          onChanged: (val) {
                            //* Called when an item is tapped or the field loses focus.
                            if (val != null) {
                              print("debug val = ${val.toJson()}");

                              _selectedProducts.putIfAbsent(val.id, () => val);
                              _priceControllers.putIfAbsent(
                                  val.id, () => TextEditingController());
                              _featureControllers.putIfAbsent(
                                  val.id, () => TextEditingController());
                              // print(
                              //     "debug Added or Not = ${_selectedProducts.add(val)}");
                            }
                            // print("debug SelectedProducts = ${_selectedProducts.toSet()}");
                            print(
                                "debug SelectedProducts = $_selectedProducts");
                          },
                          itemToString: (item) => "",
                          onSearch: (search) async {
                            if ((competitiveProducts.isEmpty ||
                                    competitiveProducts.length == 0) &&
                                snapshot.connectionState ==
                                    ConnectionState.done) {
                              if (!mounted) {
                                setState(() {
                                  _futureCompetitiveProducts =
                                      MyApi.getDataWithAuthorization(
                                          "competition/get-competitive-products");
                                });
                              }
                            }
                            return modelCompetitiveProducts
                                .where((element) =>
                                    element.model.toLowerCase().contains(
                                          search.toLowerCase(),
                                        ))
                                .toList();
                          },
                          itemBuilder: (context, item) {
                            return Padding(
                              padding: EdgeInsets.all(12),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text(
                                    item.model,
                                    style: TextStyle(
                                      color: textPrimary,
                                    ),
                                  ),
                                  Text(
                                    item.brand,
                                    style: TextStyle(
                                      color: textSecondary,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                          maxSuggestions: competitiveProducts != null &&
                                  competitiveProducts.length != 0
                              ? competitiveProducts.length
                              : 0,
                          suggestionsBuilder: (context, items) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return Card(
                                elevation: cardElevation,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        cardCornerRadius)),
                                color: Colors.white,
                                clipBehavior: Clip.antiAlias,
                                margin: EdgeInsets.symmetric(
                                  // horizontal: subMargin,
                                  vertical: subMarginHalf,
                                ),
                                child: Center(
                                  child: Container(
                                    margin: EdgeInsets.all(subMargin),
                                    child: CircularProgressIndicator(),
                                  ),
                                ),
                              );
                            } else if (snapshot.hasError) {
                              return Card(
                                elevation: cardElevation,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        cardCornerRadius)),
                                color: Colors.white,
                                clipBehavior: Clip.antiAlias,
                                margin: EdgeInsets.symmetric(
                                  // horizontal: subMargin,
                                  vertical: subMarginHalf,
                                ),
                                child: Center(
                                  child: Container(
                                    margin: EdgeInsets.all(subMargin),
                                    child: Text("Something Went Wrong!"),
                                  ),
                                ),
                              );
                            }
                            return Card(
                              elevation: cardElevation,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.circular(cardCornerRadius)),
                              color: Colors.white,
                              clipBehavior: Clip.antiAlias,
                              margin: EdgeInsets.symmetric(
                                // horizontal: subMargin,
                                vertical: subMarginHalf,
                              ),
                              child: SizedBox(
                                width: double.infinity,
                                height: 200,
                                child: ListView(
                                  children: items,
                                ),
                              ),
                            );
                          },
                        ),
                      );
                    },
                  ),
                  SizedBox(height: subMargin),
                  _selectedProducts.values != null &&
                          _selectedProducts.values.length != 0
                      ? Form(
                          key: _formKey,
                          autovalidate: _autoValidate,
                          child: Column(
                            children: List.generate(
                              _selectedProducts.values.length + 1,
                              (index) {
                                if (index == _selectedProducts.values.length) {
                                  return Container(
                                    width: screenSize.width - subMarginDouble,
                                    margin: EdgeInsets.symmetric(
                                        vertical: subMargin),
                                    height: 45,
                                    child: FlatButton(
                                      color: colorPrimary,
                                      onPressed: () =>
                                          _submitCompetitionForm(context),
                                      child: Text(
                                        "SUBMIT",
                                        style: TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  );
                                }
                                return Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    ListTile(
                                      onTap: () {},
                                      dense: true,
                                      title: Text(_selectedProducts.values
                                          .toList()[index]
                                          .model),
                                      subtitle: Text(_selectedProducts.values
                                          .toList()[index]
                                          .brand),
                                      visualDensity: VisualDensity.compact,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(
                                        subMargin,
                                        0,
                                        subMargin,
                                        subMargin,
                                      ),
                                      child: Table(
                                        columnWidths: {
                                          0: FlexColumnWidth(1.5),
                                          1: FixedColumnWidth(subMarginHalf),
                                          2: FlexColumnWidth(3),
                                        },
                                        children: [
                                          TableRow(children: [
// Price
                                            TextFormField(
                                              controller: _priceControllers
                                                  .values
                                                  .toList()[index],
                                              validator: (value) {
                                                if (value.isEmpty) {
                                                  return "Invalid";
                                                }
                                                if (int.parse(value) == 0) {
                                                  return "Cannot be 0";
                                                }
                                                return null;
                                              },
                                              inputFormatters: [
                                                FilteringTextInputFormatter
                                                    .digitsOnly
                                              ],
                                              keyboardType:
                                                  TextInputType.number,
                                              style: TextStyle(
                                                color: textPrimary,
                                                fontWeight: FontWeight.w600,
                                              ),
                                              decoration: InputDecoration(
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                    color: Colors.grey[600],
                                                  ),
                                                ),
                                                labelText: 'Price',
                                                helperStyle: TextStyle(
                                                  color: textTertiary,
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                            ),
                                            SizedBox(width: subMargin),
// Feature
                                            TextFormField(
                                              controller: _featureControllers
                                                  .values
                                                  .toList()[index],
                                              validator: (value) {
                                                if (value.isEmpty) {
                                                  return "Enter a feature";
                                                }
                                                return null;
                                              },
                                              keyboardType: TextInputType.text,
                                              style: TextStyle(
                                                color: textPrimary,
                                                fontWeight: FontWeight.w600,
                                              ),
                                              decoration: InputDecoration(
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                    color: Colors.grey[600],
                                                  ),
                                                ),
                                                labelText: 'Feature',
                                                // hintText: 'Enter consumer email',
                                              ),
                                            ),
                                          ]),
                                        ],
                                      ),
                                    ),
                                    Divider(
                                      height: 0,
                                      color: Colors.grey,
                                    ),
                                  ],
                                );
                              },
                            ),
                          ),
                        )
                      : SizedBox.shrink(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _submitCompetitionForm(BuildContext context) async {
    currentNode.unfocus();
    try {
      if (!_formKey.currentState.validate()) {
        _autoValidate = true;
        return;
      }
      String ids = "";
      String prices = "";
      String features = "";
      print("debug id = $_selectedProducts");
      _selectedProducts.keys.toList().forEach((e) {
        ids += e + ",";
      });
      _priceControllers.values.toList().forEach((e) {
        prices += e.text + ",";
      });
      _featureControllers.values.toList().forEach((e) {
        features += e.text + "**";
      });
      print("debug IDS = ${ids.substring(0, ids.length)}");
      print("debug quantities = ${prices.substring(0, prices.length)}");
      print("debug features = ${features.substring(0, features.length)}");
      // return;
      showMyLoadingDialog(dialogText: "Submitting Form ...\nPlease Wait!");
      Map<String, String> data = {
        "competitive_product_ids": ids.substring(0, ids.length - 1),
        "prices": prices.substring(0, prices.length - 1),
        "features": features.substring(0, features.length - 2),
      };

      Response response = await MyApi.postDataWithAuthorization(
          data, "competition/add-competition-entry");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        // Survey Submitted Successfully...
        dismissMyDialog();
        print("$Inventory_Form Success 200");
        Navigator.pop(context, response.data["message"]);
      } else if (response.statusCode == 422) {
        // Leave already applied for this date...
        dismissMyDialog();
        print("$Inventory_Form Error 422");
        showToast(response.data["message"],
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
      } else {
        dismissMyDialog();
        print("$Inventory_Form Error => Else");
        showToast("Something Went Wrong!",
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
        // Scaffold.of(context)
        //     .showSnackBar(createSnackBar(msg: "Something Went Wrong!"));
      }
    } on SocketException {
      dismissMyDialog();
      print("$Inventory_Form Exception = $error");
      showToast("No Internet!",
          context: context,
          animation: StyledToastAnimation.slideFromBottom,
          reverseAnimation: StyledToastAnimation.slideToBottom,
          startOffset: Offset(0.0, 3.0),
          reverseEndOffset: Offset(0.0, 3.0),
          position: StyledToastPosition.bottom,
          duration: Duration(seconds: 3),
          animDuration: Duration(seconds: 1),
          curve: Curves.elasticOut,
          reverseCurve: Curves.fastOutSlowIn);
      // Scaffold.of(context).showSnackBar(createSnackBar());
    } catch (error) {
      dismissMyDialog();
      if (error is DioError) {
        if (error.response.statusCode == 422) {
          print("$Inventory_Form Dio Error 422 = $error");
          showToast(error.response.data["message"],
              context: context,
              animation: StyledToastAnimation.slideFromBottom,
              reverseAnimation: StyledToastAnimation.slideToBottom,
              startOffset: Offset(0.0, 3.0),
              reverseEndOffset: Offset(0.0, 3.0),
              position: StyledToastPosition.bottom,
              duration: Duration(seconds: 3),
              animDuration: Duration(seconds: 1),
              curve: Curves.elasticOut,
              reverseCurve: Curves.fastOutSlowIn);
        }
        print("$Inventory_Form Dio Error = $error");
        showToast(error.response.data["message"],
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
      } else {
        print("$Inventory_Form Exception = $error");
        showToast("Something Went Wrong!",
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
        // Scaffold.of(context)
        //     .showSnackBar(createSnackBar(msg: "Something Went Wrong!"));
      }
    }
  }
}
