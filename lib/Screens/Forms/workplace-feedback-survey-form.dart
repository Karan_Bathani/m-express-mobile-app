import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/feedback-survey-dropdown-widget.dart';
import 'package:market_express/Models/workplace-feedback-question.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WorkplaceFeedbackSurveyForm extends StatefulWidget {
  @override
  _WorkplaceFeedbackSurveyFormState createState() =>
      _WorkplaceFeedbackSurveyFormState();
}

class _WorkplaceFeedbackSurveyFormState
    extends State<WorkplaceFeedbackSurveyForm> {
  Future<List<WorkplaceFeedbackQuestion>> _futureQuestion;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _futureQuestion = _getQuestions();
  }

  Size screenSize;
  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: FittedBox(
          child: Text(
            "Workplace Related Feedback",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: greyBackground,
            // Colors.white,
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          child: RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () {
              setState(() {
                _futureQuestion = _getQuestions();
              });
              return _futureQuestion;
            },
            child: FutureBuilder<List<WorkplaceFeedbackQuestion>>(
              future: _futureQuestion,
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  if (snapshot.error.toString().contains("404")) {
                    return buildEmptyWidget(screenSize,
                        text: "No feedback required for now!");
                  }
                  return buildErrorWidget(screenSize);
                }
                if (snapshot.hasData &&
                    snapshot.connectionState == ConnectionState.done &&
                    snapshot.data.length != 0) {
                  return FeedbackSurveyDropdownListWidget(snapshot.data);
                } else if (snapshot.connectionState ==
                    ConnectionState.waiting) {
                  _refreshIndicatorKey.currentState?.show();
                  return snapshot.data != null
                      ? FeedbackSurveyDropdownListWidget(snapshot.data)
                      : SizedBox.shrink();
                }
                return SizedBox.expand(
                  child: ListView(),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Future<List<WorkplaceFeedbackQuestion>> _getQuestions() async {
    List<WorkplaceFeedbackQuestion> questions = [];
    try {
      Response response = await MyApi.getDataWithAuthorization(
          "users/workplace-survey-questions");
      print("$WorkplaceFeedbackSurvey_Form Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var question in response.data["data"]) {
          questions.add(WorkplaceFeedbackQuestion.fromJson(question));
        }
      } else {
        throw Future.error("Error from Future");
      }
    } on SocketException {
      print("$WorkplaceFeedbackSurvey_Form SocketException");
      throw Future.error(SocketException);
    } catch (error) {
      print("$WorkplaceFeedbackSurvey_Form Exception = $error");
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }

      throw Future.error(error);
    }
    return questions;
  }
}
