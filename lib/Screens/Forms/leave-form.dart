import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:intl/intl.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/my-custom-button.dart';
import 'package:market_express/Components/my-date-picker.dart';
import 'package:market_express/Consumer_Electronics/Screens/am-home-screen.dart';
import 'package:market_express/Consumer_Electronics/Screens/tso-home-screen.dart';
import 'package:market_express/Screens/leave-history-screen.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Screens/manager-home-screen.dart';
import 'package:market_express/Screens/sec-home-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:market_express/Visual%20Merchandiser/Screens/vm-home-screen.dart';
import 'package:market_express/Visual%20Merchandiser/Screens/vmsup-home-screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LeaveForm extends StatefulWidget {
  @override
  _LeaveFormState createState() => _LeaveFormState();
}

class _LeaveFormState extends State<LeaveForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autovalidate = false;
  Size screenSize;
  bool selectRange = false;
  String _leaveType = "please select";
  bool _isHalfDayLeave = false;
  String _startDate = DateTime.now().toIso8601String();
  String _endDate = DateTime.now().toIso8601String();

  TextEditingController reasonController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  @override
  void dispose() {
    reasonController.dispose();
    dateController.dispose();
    super.dispose();
  }

  FocusScopeNode currentNode;

  @override
  Widget build(BuildContext context) {
    currentNode = FocusScope.of(context);
    screenSize = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        currentNode.unfocus();
      },
      child: Scaffold(
        backgroundColor: colorPrimary,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: Text(
            'Leave Form',
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: SafeArea(
          child: Container(
            clipBehavior: Clip.antiAlias,
            width: screenSize.width,
            height: screenSize.height,
            decoration: BoxDecoration(
              color:
                  //  Colors.black,
                  Color(0xfff5f5f5),
              borderRadius:
                  BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
            ),
            padding: EdgeInsets.all(subMargin),
            child: Form(
              key: _formKey,
              autovalidate: _autovalidate,
              child: Scrollbar(
                child: ListView(
                  children: [
//> Date or Date-Range
                    GestureDetector(
                      onTap: () => _showMyDatePicker(context),
                      child: TextFormField(
                        controller: dateController,
                        style: TextStyle(
                          color: textPrimary,
                          fontWeight: FontWeight.w600,
                        ),
                        enabled: false,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Please select Date(s) for leave.";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.red,
                            ),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.grey[600],
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.grey[600],
                            ),
                          ),
                          labelText: "Select Date(s)",
                          labelStyle: TextStyle(
                            color: Colors.grey[600],
                          ),
                          errorStyle: TextStyle(
                            color: Colors.red,
                          ),
                        ),
                      ),
                    ),
//> Leave Type
                    Container(
                        margin: EdgeInsets.symmetric(vertical: subMargin),
                        decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.circular(miniCardCornerRadius),
                          border: Border.all(
                              color: Colors.grey[600],
                              style: BorderStyle.solid,
                              width: 0.80),
                        ),
                        child: DropdownButton(
                          underline: SizedBox.shrink(),
                          isExpanded: true,
                          hint: Text("  $_leaveType"),
                          items: [
                            DropdownMenuItem(
                              child: Text("sick leave"),
                              value: "sick",
                            ),
                            DropdownMenuItem(
                              child: Text("casual leave"),
                              value: "casual",
                            ),
                          ],
                          onChanged: (value) {
                            setState(() {
                              _leaveType = value;
                            });
                          },
                        )
                        // FutureBuilder<SharedPreferences>(
                        //   future: SharedPreferences.getInstance(),
                        //   builder: (context, snapshot) {
                        //     String sick = "sick leave";
                        //     String casual = "casual leave";
                        //     if (snapshot.hasData &&
                        //         snapshot.data
                        //             .containsKey("sick_leaves_available")) {
                        //       sick +=
                        //           " (remaining: ${snapshot.data.getInt("sick_leaves_available")})";
                        //     }
                        //     if (snapshot.hasData &&
                        //         snapshot.data
                        //             .containsKey("casual_leaves_available")) {
                        //       casual +=
                        //           " (remaining: ${snapshot.data.getInt("casual_leaves_available")})";
                        //     }
                        //     return DropdownButton(
                        //       underline: SizedBox.shrink(),
                        //       isExpanded: true,
                        //       hint: Text(_leaveType),
                        //       items: [
                        //         DropdownMenuItem(
                        //           child: Text(sick),
                        //           value: "sick",
                        //         ),
                        //         DropdownMenuItem(
                        //           child: Text(casual),
                        //           value: "casual",
                        //         ),
                        //       ],
                        //       onChanged: (value) {
                        //         setState(() {
                        //           _leaveType = value;
                        //         });
                        //       },
                        //     );
                        //   },
                        // ),
                        ),
//> Reason TextField
                    TextFormField(
                      controller: reasonController,
                      keyboardType: TextInputType.multiline,
                      minLines: 1,
                      maxLines: null,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please enter a reason for leave.";
                        }
                        return null;
                      },
                      style: TextStyle(
                        color: textPrimary,
                        fontWeight: FontWeight.w600,
                      ),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.grey[600],
                          ),
                        ),
                        labelText: 'Reason for Leave',
                        hintText: 'Enter your Reason Here',
                      ),
                    ),
                    SizedBox(height: subMarginHalf / 2),
//> HalfDay Checkbox
                    _startDate == _endDate
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              SizedBox(
                                width: screenSize.width / 2,
                                child: CheckboxListTile(
                                  title: Text("Half Day"),
                                  value: _isHalfDayLeave,
                                  onChanged: (value) {
                                    setState(() {
                                      _isHalfDayLeave = value;
                                    });
                                  },
                                ),
                              ),
                            ],
                          )
                        : SizedBox(),
                    SizedBox(height: subMarginHalf / 2),
//? Submit Button
                    MyCustomButton(
                      padding: EdgeInsets.symmetric(
                        vertical: subMargin,
                        horizontal: subMarginDouble,
                      ),
                      onPressed: () => _onSubmitLeave(context),
                    ),
                    SizedBox(height: subMargin),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _onSubmitLeave(BuildContext context) async {
    currentNode.unfocus();
    try {
      if (!_formKey.currentState.validate()) {
        setState(() {
          _autovalidate = true;
        });
        return;
      }
      // Show Dialog while the user's attendance is being registered.
      showMyLoadingDialog(
          dialogText: "Submitting your Leave Request...\nPlease Wait!");
      Map<String, String> data = {
        "start_date": _startDate,
        "end_date": _endDate,
        "type": _leaveType,
        "reason": reasonController.text,
        "on_half_day_leave": _isHalfDayLeave.toString(),
      };
      Response response =
          await MyApi.postDataWithAuthorization(data, "users/leave-request");
      // print("$Leave_Form Response = ${response}");
      // print("$Leave_Form ResponseData = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        // Leave Submitted Successfully...
        print(
            "$Leave_Form After Response Difference = ${DateTime.parse(_endDate).toUtc().difference(DateTime.parse(_startDate).toUtc()).inDays + 1}");
        dismissMyDialog();
        // SharedPreferences.getInstance().then(
        //   (value) {
        //     if (_leaveType == "sick") {
        //       value.setInt(
        //           "sick_leaves_available",
        //           value.getInt("sick_leaves_available") -
        //               (DateTime.parse(_endDate)
        //                       .toUtc()
        //                       .difference(DateTime.parse(_startDate).toUtc())
        //                       .inDays +
        //                   1));
        //     }
        //     if (_leaveType == "casual") {
        //       value.setInt(
        //         "casual_leaves_available",
        //         value.getInt("casual_leaves_available") -
        //             (DateTime.parse(_endDate)
        //                     .toUtc()
        //                     .difference(DateTime.parse(_startDate).toUtc())
        //                     .inDays +
        //                 1),
        //       );
        //     }
        //   },
        // );
        //TODO: Remove this when DashboardProvider is implemented.
        Widget nextScreen;
        SharedPreferences.getInstance().then((value) {
          switch (value.getString("user_type")) {
            case "SEC":
            case "ND. SEC":
              nextScreen = SECHomeScreen();
              break;
            case "FOE":
            case "FOM":
              nextScreen = ManagerHomeScreen();
              break;
            case "VM":
              nextScreen = VMHomeScreen();
              break;
            case "VMS":
              nextScreen = VMSupHomeScreen();
              break;
            case "TSO":
              nextScreen = TSOHomeScreen();
              break;
            case "Area Manager":
            case "AM":
              nextScreen = AMHomeScreen();
              break;
            default:
              nextScreen = LoginScreen();
          }
        });
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (context) => nextScreen,
                settings: RouteSettings(name: "/home")),
            (route) => false);
// ---------------------------------------------------
//TODO Remove Until Here after confirming yourself
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => LeaveHistoryScreen(
              message: response.data["message"],
            ),
          ),
          (route) {
            return route.settings.name == "/home";
          },
        );
      } else if (response.statusCode == 422) {
        // Leave already applied for this date...
        dismissMyDialog();
        showToast(response.data["message"],
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
      } else {
        dismissMyDialog();
        Scaffold.of(context)
            .showSnackBar(createSnackBar(msg: "Something Went Wrong!"));
        throw Future.error("$Leave_Form Error from Future");
      }
    } on SocketException {
      dismissMyDialog();
      print("$Leave_Form Exception = $error");
      Scaffold.of(context).showSnackBar(createSnackBar());
    } catch (error) {
      dismissMyDialog();
      if (error is DioError) {
        if (error.response.statusCode == 422) {
          print("$Leave_Form Dio Error = $error");
          showToast(error.response.data["message"],
              context: context,
              animation: StyledToastAnimation.slideFromBottom,
              reverseAnimation: StyledToastAnimation.slideToBottom,
              startOffset: Offset(0.0, 3.0),
              reverseEndOffset: Offset(0.0, 3.0),
              position: StyledToastPosition.bottom,
              duration: Duration(seconds: 3),
              animDuration: Duration(seconds: 1),
              curve: Curves.elasticOut,
              reverseCurve: Curves.fastOutSlowIn);
        }
        print("$Inventory_Form Dio Error = $error");
        showToast(error.response.data["message"],
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
      } else {
        print("$Leave_Form Exception = $error");
        Scaffold.of(context)
            .showSnackBar(createSnackBar(msg: "Something Went Wrong!"));
      }
    }
  }

  void _showMyDatePicker(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        barrierColor: Colors.black.withOpacity(.3),
        builder: (context) {
          return WillPopScope(
            onWillPop: () async {
              return true;
            },
            child: Dialog(
              insetPadding: EdgeInsets.all(mainMargin),
              child: MyDatePicker(),
            ),
          );
        }).then((value) {
      if (value != null) {
        print("$Leave_Form Data = $value");

        dateController.text = value[1] == null ||
                DateFormat('dd-MM-yyyy').format(value[0]).toString() ==
                    DateFormat('dd-MM-yyyy').format(value[1]).toString()
            ? DateFormat('dd-MM-yyyy').format(value[0]).toString()
            : DateFormat('dd-MM-yyyy').format(value[0]).toString() +
                " to " +
                DateFormat('dd-MM-yyyy').format(value[1]).toString();
        setState(() {
          _startDate = value[0].toIso8601String();
          _endDate = (value[1] ?? value[0]).toIso8601String();
          print(
              "$Leave_Form Difference in Days = ${DateTime.parse(_endDate).toUtc().difference(DateTime.parse(_startDate).toUtc()).inDays + 1}");
        });
      }
    });
  }

  // _selectDate(BuildContext context) async {
  //   final DateTime picked = await showDatePicker(
  //     context: context,
  //     initialDate: selectedDate,
  //     firstDate: DateTime(2020),
  //     lastDate: DateTime(2025),
  //   );
  //   if (picked != null && picked != selectedDate)
  //     setState(() {
  //       selectedDate = picked;
  //     });
  // }
}
