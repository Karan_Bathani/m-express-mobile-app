import 'dart:io';
import 'package:dio/dio.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/my-custom-button.dart';
import 'package:market_express/Models/sales-product-suggestion.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Screens/sales-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_autocomplete_formfield/simple_autocomplete_formfield.dart';

class SalesForm extends StatefulWidget {
  @override
  _SalesFormState createState() => _SalesFormState();
}

class _SalesFormState extends State<SalesForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autovalidate = false;

  Size screenSize;
  String productId;
  int productAvailableQuantity;

  TextEditingController nameController = TextEditingController();
  TextEditingController numberController =
      MaskedTextController(mask: "00000000000");
  TextEditingController ageController = MaskedTextController(mask: "00");
  TextEditingController emailController = TextEditingController();
  TextEditingController purchasedDeviceController = TextEditingController();
  // TextEditingController variantController = TextEditingController();
  TextEditingController colorController = TextEditingController();
  TextEditingController quantityController = TextEditingController();
  TextEditingController remarkController = TextEditingController();

  Future<Response<dynamic>> _futureProductSuggestions;
  List<String> productSuggestions = [];
  List<SalesProductSuggestion> modelProductSuggestions = [];

  @override
  void initState() {
    super.initState();
    _futureProductSuggestions =
        MyApi.getDataWithAuthorization("stores/product-list");
  }

  @override
  void dispose() {
    nameController.dispose();
    numberController.dispose();
    ageController.dispose();
    emailController.dispose();
    purchasedDeviceController.dispose();
    // variantController.dispose();
    colorController.dispose();
    quantityController.dispose();
    remarkController.dispose();
    super.dispose();
  }

  FocusScopeNode currentNode;
  FocusNode variantFocusNode = FocusNode();
  @override
  Widget build(BuildContext context) {
    currentNode = FocusScope.of(context);
    screenSize = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        currentNode.unfocus();
      },
      child: Scaffold(
        backgroundColor: colorPrimary,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: Text(
            'Sales Form',
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: SafeArea(
          child: Container(
            clipBehavior: Clip.antiAlias,
            width: screenSize.width,
            height: screenSize.height,
            decoration: BoxDecoration(
              color: greyBackground,
              borderRadius:
                  BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
            ),
            child: Form(
              key: _formKey,
              autovalidate: _autovalidate,
              child: SingleChildScrollView(
                padding: EdgeInsets.fromLTRB(
                  subMargin,
                  0,
                  subMargin,
                  subMargin,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(height: subMargin),
// Customer Name
                    TextFormField(
                      controller: nameController,
                      keyboardType: TextInputType.name,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please enter consumer name";
                        }
                        return null;
                      },
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(
                            RegExp(r"[a-zA-z][\s]?")),
                        FilteringTextInputFormatter.deny(RegExp("[\[]")),
                        FilteringTextInputFormatter.deny(RegExp("[\]]")),
                        FilteringTextInputFormatter.deny(RegExp("[\\/]")),
                        FilteringTextInputFormatter.deny(RegExp("[\`]")),
                        FilteringTextInputFormatter.deny(RegExp("[\_]")),
                      ],
                      style: TextStyle(
                        color: textPrimary,
                        fontWeight: FontWeight.w600,
                      ),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.grey[600],
                          ),
                        ),
                        labelText: 'Name',
                        // hintText: 'Consumer Name',
                      ),
                    ),
                    SizedBox(height: subMargin),
// Contact Number
                    TextFormField(
                      controller: numberController,
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please enter contact details";
                        } else if (value.isNotEmpty && value.length != 11) {
                          return "Please enter a valid contact number";
                        }
                        return null;
                      },
                      keyboardType: TextInputType.phone,
                      style: TextStyle(
                        color: textPrimary,
                        fontWeight: FontWeight.w600,
                      ),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.grey[600],
                          ),
                        ),
                        labelText: 'Contact Number',
                        // hintText: 'Enter contact number',
                      ),
                    ),
                    SizedBox(height: subMargin),
// Age and Email (Both Optional)
                    Table(
                      columnWidths: {
                        0: FlexColumnWidth(1.5),
                        1: FixedColumnWidth(subMarginHalf),
                        2: FlexColumnWidth(3),
                      },
                      children: [
                        TableRow(children: [
// Age
                          TextFormField(
                            controller: ageController,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly
                            ],
                            validator: (value) {
                              if (value.isNotEmpty && value.length > 3) {
                                return "Invalid Age";
                              }
                              return null;
                            },
                            keyboardType: TextInputType.number,
                            style: TextStyle(
                              color: textPrimary,
                              fontWeight: FontWeight.w600,
                            ),
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.grey[600],
                                ),
                              ),
                              labelText: 'Age',
                              // hintText: 'Enter age',
                              helperText: "Optional",
                              helperStyle: TextStyle(
                                color: textTertiary,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          SizedBox(width: subMargin),
// Email Address
                          TextFormField(
                            controller: emailController,
                            validator: (value) {
                              if (value.isNotEmpty && !validateEmail(value)) {
                                return "Please enter a valid email";
                              }
                              return null;
                            },
                            keyboardType: TextInputType.emailAddress,
                            style: TextStyle(
                              color: textPrimary,
                              fontWeight: FontWeight.w600,
                            ),
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.grey[600],
                                ),
                              ),
                              labelText: 'Email Address',
                              // hintText: 'Enter consumer email',
                              helperText: "Optional",
                              helperStyle: TextStyle(
                                color: textTertiary,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ]),
                      ],
                    ),
                    SizedBox(height: subMargin),
// Purchased Device Model
                    FutureBuilder<Response<dynamic>>(
                      future: _futureProductSuggestions,
                      builder: (context, snapshot) {
                        if (snapshot.hasError) {
                          if (snapshot.error == SocketException ||
                              snapshot.error
                                  .toString()
                                  .contains("SocketException")) {
                            Future.delayed(Duration(milliseconds: 500))
                                .then((value) {
                              Scaffold.of(context)
                                  .showSnackBar(createSnackBar());
                            });
                          }
                        } else if (snapshot.hasData) {
                          if (productSuggestions != null &&
                              productSuggestions.length != 0) {
                            productSuggestions.clear();
                            modelProductSuggestions.clear();
                          }
                          snapshot.data.data["data"].forEach((element) {
                            // print("$Sales_Form $element ");
                            // print("$Sales_Form ${SalesProductSuggestion.fromJson(element).deviceVarient}");
                            productSuggestions.add(
                                SalesProductSuggestion.fromJson(element)
                                    .deviceModel);
                            modelProductSuggestions
                                .add(SalesProductSuggestion.fromJson(element));
                          });
                        }
                        return SimpleAutocompleteFormField<
                            SalesProductSuggestion>(
                          controller: purchasedDeviceController,
                          validator: (value) {
                            // print("$Sales_Form Value = $value");
                            // print(
                            //     "$Sales_Form TextValue = ${purchasedDeviceController.text}");
                            if (purchasedDeviceController.text == null ||
                                purchasedDeviceController.text.isEmpty ||
                                purchasedDeviceController.text.length == 0) {
                              return "Please enter a device model";
                            } else if (!productSuggestions
                                .contains(purchasedDeviceController.text)) {
                              return "Select a model from the list";
                            } else if (value != null &&
                                !productSuggestions
                                    .contains(value.deviceModel)) {
                              return "Select a model from the list";
                            } else if (value != null) {
                              int tempIndex = modelProductSuggestions
                                  .indexWhere((element) =>
                                      element.deviceModel == value.deviceModel);
                              productId =
                                  modelProductSuggestions[tempIndex].productId;
                              // print(
                              //     "$Sales_Form Else ProductId = $productId");
                            }
                            return null;
                          },
                          keyboardType: TextInputType.text,
                          style: TextStyle(
                            color: textPrimary,
                            fontWeight: FontWeight.w600,
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.grey[600],
                              ),
                            ),
                            labelText: 'Purchased Device Model',
                            // hintText: 'Enter the purchased device model',
                          ),
                          onChanged: (value) {
                            // print("$Sales_Form OnChangedValue = $value");
                            // print(
                            //     "$Sales_Form OnChanged Model = ${value.deviceModel}");
                            // print(
                            //     "$Sales_Form OnChanged Varient = ${value.deviceVarient}");
                            // productAvailableQuantity =
                            //     value.availableQuantity;
                            if (value == null) {
                              productAvailableQuantity = null;
                              // variantController.clear();
                            } else if (value != null) {
                              if (value.deviceVarient != null) {
                                // variantController.text = value.deviceVarient;
                              } else {
                                // variantController.clear();
                              }
                              productAvailableQuantity =
                                  value.availableQuantity;
                              // currentNode.nextFocus();
                              variantFocusNode.requestFocus();
                            }
                          },
                          onFieldSubmitted: (val) {
                            // currentNode.nextFocus();
                            variantFocusNode.requestFocus();
                          },
                          itemToString: (item) =>
                              item == null ? "" : item.deviceModel,
                          onSearch: (search) async {
                            if ((productSuggestions.isEmpty ||
                                    productSuggestions.length == 0) &&
                                snapshot.connectionState ==
                                    ConnectionState.done) {
                              if (!mounted) {
                                setState(() {
                                  _futureProductSuggestions =
                                      MyApi.getDataWithAuthorization(
                                          "stores/product-list");
                                });
                              }
                            }
                            return modelProductSuggestions
                                .where(
                                  (element) => element.deviceModel
                                      .toLowerCase()
                                      .contains(
                                        search.toLowerCase(),
                                      ),
                                )
                                .toList();
                          },
                          itemBuilder: (context, item) {
                            return Padding(
                              padding: EdgeInsets.all(12),
                              child: Text(item.deviceModel),
                            );
                          },
                          maxSuggestions: modelProductSuggestions != null &&
                                  modelProductSuggestions.length != 0
                              ? modelProductSuggestions.length
                              : 0,
                          suggestionsBuilder: (context, items) {
                            print("$Sales_Form Snap = $snapshot");
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return Card(
                                elevation: cardElevation,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        cardCornerRadius)),
                                color: Colors.white,
                                clipBehavior: Clip.antiAlias,
                                margin: EdgeInsets.symmetric(
                                  // horizontal: subMargin,
                                  vertical: subMarginHalf,
                                ),
                                child: Center(
                                  child: Container(
                                    margin: EdgeInsets.all(subMargin),
                                    child: CircularProgressIndicator(),
                                  ),
                                ),
                              );
                            } else if (snapshot.hasError) {
                              return SizedBox.shrink();
                              return Card(
                                elevation: cardElevation,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        cardCornerRadius)),
                                color: Colors.white,
                                clipBehavior: Clip.antiAlias,
                                margin: EdgeInsets.symmetric(
                                  // horizontal: subMargin,
                                  vertical: subMarginHalf,
                                ),
                                child: Center(
                                  child: Container(
                                    margin: EdgeInsets.all(subMargin),
                                    child: Text("Something Went Wrong!"),
                                  ),
                                ),
                              );
                            }
                            return Card(
                              elevation: cardElevation,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.circular(cardCornerRadius)),
                              color: Colors.white,
                              clipBehavior: Clip.antiAlias,
                              margin: EdgeInsets.symmetric(
                                // horizontal: subMargin,
                                vertical: subMarginHalf,
                              ),
                              child: SizedBox(
                                width: double.infinity,
                                height: 200,
                                child: ListView(
                                  children: items,
                                ),
                              ),
                            );
                          },
                        );
                      },
                    ),
// Availabe Quantity
                    productAvailableQuantity == null ||
                            purchasedDeviceController.text.isEmpty
                        ? SizedBox(height: subMargin)
                        : Container(
                            alignment: Alignment.centerRight,
                            padding:
                                EdgeInsets.symmetric(vertical: subMarginHalf),
                            child: Text(
                              "Available Quantity: ${productAvailableQuantity.toString()}",
                            ),
                          ),
// Variant
                    // TextFormField(
                    //   controller: variantController,
                    //   focusNode: variantFocusNode,
                    //   onFieldSubmitted: (value) {
                    //     currentNode.nextFocus();
                    //   },
                    //   validator: (value) {
                    //     if (value.isEmpty) {
                    //       return "Please enter the variant";
                    //     }
                    //     return null;
                    //   },
                    //   keyboardType: TextInputType.text,
                    //   style: TextStyle(
                    //     color: textPrimary,
                    //     fontWeight: FontWeight.w600,
                    //   ),
                    //   decoration: InputDecoration(
                    //     border: OutlineInputBorder(
                    //       borderSide: BorderSide(
                    //         color: Colors.grey[600],
                    //       ),
                    //     ),
                    //     labelText: 'Variant',
                    //     // hintText: '6/64 GB',
                    //   ),
                    // ),
                    // SizedBox(height: subMargin),
// Quantity and Color
                    Table(
                      columnWidths: {
                        0: FlexColumnWidth(1.5),
                        1: FixedColumnWidth(subMarginHalf),
                        2: FlexColumnWidth(3),
                      },
                      children: [
                        TableRow(children: [
// Quantity
                          TextFormField(
                            controller: quantityController,
                            validator: (value) {
                              if (value.isEmpty) {
                                return "Invalid Quantity";
                              }
                              if (int.parse(value) == 0) {
                                return "Cannot be 0";
                              }
                              return null;
                            },
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly
                            ],
                            style: TextStyle(
                              color: textPrimary,
                              fontWeight: FontWeight.w600,
                            ),
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.grey[600],
                                ),
                              ),
                              labelText: 'Quantity',
                              helperStyle: TextStyle(
                                color: textTertiary,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          SizedBox(width: subMargin),
// Color
                          TextFormField(
                            controller: colorController,
                            validator: (value) {
                              if (value.isEmpty) {
                                return "Invalid color";
                              }
                              return null;
                            },
                            keyboardType: TextInputType.text,
                            style: TextStyle(
                              color: textPrimary,
                              fontWeight: FontWeight.w600,
                            ),
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.grey[600],
                                ),
                              ),
                              labelText: 'Color',
                              // hintText: "Enter color"
                            ),
                          ),
                        ]),
                      ],
                    ),
                    SizedBox(height: subMargin),
// Remarks
                    TextField(
                      controller: remarkController,
                      // validator: (value) {
                      //   if (value.isNotEmpty) {
                      //     return "Please enter remark(s)";
                      //   }
                      //   return null;
                      // },
                      keyboardType: TextInputType.text,
                      style: TextStyle(
                        color: textPrimary,
                        fontWeight: FontWeight.w600,
                      ),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.grey[600],
                          ),
                        ),
                        labelText: 'Remark(s)',
                        // hintText: 'Enter Remark(s)',
                        helperText: "Optional",
                        helperStyle: TextStyle(
                          color: textTertiary,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    SizedBox(height: subMargin),
// Submit Button
                    MyCustomButton(
                      padding: EdgeInsets.symmetric(
                        vertical: subMargin,
                        horizontal: subMarginDouble,
                      ),
                      onPressed: () => _onSubmitSales(context),
                    ),
                    SizedBox(height: subMargin),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _onSubmitSales(BuildContext context) async {
    currentNode.unfocus();

    // Navigator.pushAndRemoveUntil(
    //   context,
    //   MaterialPageRoute(
    //     builder: (context) => SalesScreen(
    //       message: "MSG from Sales Form",
    //     ),
    //   ),
    //   (route) {
    //     // print("$Leave_Form Route = ${route.settings.name}");
    //     return route.settings.name == "/home";
    //   },
    // );
    // return;
    try {
      if (!_formKey.currentState.validate() ||
          productId == null ||
          productId.isEmpty) {
        setState(() {
          _autovalidate = true;
        });
        return;
      }
      showMyLoadingDialog(dialogText: "Submitting sales data...\nPlease Wait!");
      Map<String, String> data = {
        "customer_name": nameController.text,
        "customer_contact_number": numberController.text,
        "color": colorController.text,
        "quantity": quantityController.text,
        // "varient": variantController.text,
        "remark": remarkController.text,
        "product_id": productId,
      };
      if (ageController.text.isNotEmpty) {
        data.addAll({
          'customer_age': ageController.text,
        });
      }
      if (emailController.text.isNotEmpty) {
        data.addAll({
          "customer_email": emailController.text,
        });
      }
      Response response =
          await MyApi.postDataWithAuthorization(data, "stores/new-sale");
      print("$Sales_Form Response = $response");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        // Survey Submitted Successfully...
        dismissMyDialog();

        // Navigator.pushAndRemoveUntil(
        //   context,
        //   MaterialPageRoute(
        //     builder: (context) => SalesScreen(
        //       message: response.data["message"],
        //     ),
        //   ),
        //   (route) {
        //     // print("$Leave_Form Route = ${route.settings.name}");
        //     return route.settings.name == "/home";
        //   },
        // );
        Navigator.pop(context, response.data["message"]);
      } else if (response.statusCode == 422) {
        // Leave already applied for this date...
        dismissMyDialog();
        print("$Sales_Form Error 422");
        showToast(response.data["message"],
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
      } else {
        dismissMyDialog();
        print("$Sales_Form Error => Else");
        Scaffold.of(context)
            .showSnackBar(createSnackBar(msg: "Something Went Wrong!"));
      }
    } on SocketException {
      dismissMyDialog();
      print("$Sales_Form SocketException");
      Scaffold.of(context).showSnackBar(createSnackBar());
    } catch (error) {
      dismissMyDialog();
      if (error is DioError) {
        if (error.response.statusCode == 422) {
          print("$Sales_Form Dio Error 422 = $error");
          showToast(error.response.data["message"],
              context: context,
              animation: StyledToastAnimation.slideFromBottom,
              reverseAnimation: StyledToastAnimation.slideToBottom,
              startOffset: Offset(0.0, 3.0),
              reverseEndOffset: Offset(0.0, 3.0),
              position: StyledToastPosition.bottom,
              duration: Duration(seconds: 3),
              animDuration: Duration(seconds: 1),
              curve: Curves.elasticOut,
              reverseCurve: Curves.fastOutSlowIn);
        }
        print("$Sales_Form Dio Error = $error");
        showToast(error.response.data["message"],
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
      } else {
        print("$Sales_Form Exception = $error");
        Scaffold.of(context)
            .showSnackBar(createSnackBar(msg: "Something Went Wrong!"));
      }
    }
  }
}
