import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MarkAttendanceScreen extends StatefulWidget {
  final String type;
  final String storeId;

  MarkAttendanceScreen({@required this.type, this.storeId});
  @override
  _MarkAttendanceScreenState createState() => _MarkAttendanceScreenState();
}

class _MarkAttendanceScreenState extends State<MarkAttendanceScreen> {
  TextEditingController _reasonController = TextEditingController();
  GlobalKey<FormFieldState> _reasonFormKey = GlobalKey<FormFieldState>();
  Size screenSize;
  File _image;
  Position _userPosition;
  // Future<Position> _futureLocation;
  final picker = ImagePicker();

  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  String _deviceModel;

  @override
  void initState() {
    super.initState();
    initPlatformState();
    _determinePosition().then((position) {
      _userPosition = position;
    }).catchError(
      (locationError) {
        showMyAlertDialog(
            title: Text("Enable Location"),
            wdgtContent: Text("Location has to be enabled to Mark Attendance!"),
            fnFirst: () {
              dismissMyDialog();
              _determinePosition();
            },
            fnSecond: () {
              dismissMyDialog();
            },
            txtFirst: "Retry");
      },
    );
  }

  @override
  void dispose() {
    _reasonController.dispose();
    super.dispose();
  }

  Future<void> initPlatformState() async {
    String deviceModel;
    try {
      if (Platform.isAndroid) {
        // deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
        AndroidDeviceInfo androidDeviceInfo =
            await deviceInfoPlugin.androidInfo;
        deviceModel =
            "${androidDeviceInfo.manufacturer} ${androidDeviceInfo.model}";
        // print("$Login_Screen manufacturer = ${androidDeviceInfo.manufacturer}");
        // print("$Login_Screen model = ${androidDeviceInfo.model}");
      } else if (Platform.isIOS) {
        // deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
        IosDeviceInfo iosDeviceInfo = await deviceInfoPlugin.iosInfo;
        deviceModel = "${iosDeviceInfo.name} ${iosDeviceInfo.model}";
      }
    } on PlatformException {
      deviceModel = 'Failed to get device info.';
    }
    if (!mounted) return;
    setState(() {
      // _deviceData = deviceData;
      _deviceModel = deviceModel;
    });
  }

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      floatingActionButton: Builder(
        builder: (buildContext) => FloatingActionButton.extended(
          label: _image == null
              ? Text(
                  "Add a Photo",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                  ),
                )
              : Text(
                  "Mark Attendance",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                  ),
                ),
          onPressed: _image == null
              ? getImage
              :
              // _userPosition == null ? null :
              () => _markUserAttendance(buildContext),
          backgroundColor:
              //  _image != null && _userPosition == null ? Colors.grey :
              colorSecondary,
          icon: Icon(
            _image == null ? Icons.add_a_photo : Icons.check,
            color: Colors.white,
          ),
        ),
      ),
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          'Mark Attendance',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: Color(0xfff5f5f5),
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          padding: EdgeInsets.all(subMargin),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: subMarginDouble),
                _image == null
                    ? Column(
                        children: [
                          Text('Click a clear photo of yourself.'),
                          SizedBox(height: subMarginDouble),
                          Lottie.asset(
                            "assets/lottie/profile_picture.json",
                            width: screenSize.width * .7,
                            fit: BoxFit.fitWidth,
                            repeat: true,
                            animate: true,
                          ),
                        ],
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.file(
                            _image,
                            width: screenSize.width,
                            height: screenSize.height / 2,
                          ),
                          FlatButton(
                            onPressed: () {
                              setState(() {
                                _image = null;
                              });
                            },
                            color: error,
                            child: Text(
                              "DELETE",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// Custom-Method to get the image from camera.
  Future<void> getImage() async {
    final PickedFile pickedFile = await picker.getImage(
      source: ImageSource.camera,
      imageQuality: 90,
      maxWidth: 700,
      maxHeight: 700,
    );

    if (pickedFile != null) {
      setState(() {
        _image = File(pickedFile.path);
      });
      print("$Mark_Attendance_Screen Length = ${_image.lengthSync()}");
    } else {
      print('No image selected.');
    }
  }

  /// Custom-Method which use Geo-Locator package to fetch user location.
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      if (!await Geolocator.openLocationSettings()) {
        return Future.error('Location services are disabled.');
      }
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permantly denied, we cannot request permissions.');
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        return Future.error(
            'Location permissions are denied (actual value: $permission).');
      }
    }

    return await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
  }

  /// Custom-Method to mark the user's attendance (Endpoint => attendance/mark-attendance)
  void _markUserAttendance(BuildContext context, {String reason}) async {
    try {
      if (_userPosition == null) {
        _determinePosition().then((position) {
          _userPosition = position;
        }).catchError((locationError) {
          showMyAlertDialog(
              title: Text("Enable Location"),
              wdgtContent:
                  Text("Location has to be enabled to Mark Attendance!"),
              fnFirst: () {
                dismissMyDialog();
                _determinePosition();
              },
              fnSecond: () {
                dismissMyDialog();
              },
              txtFirst: "Retry");
        });
        return;
      }
      // Show Dialog while the user's attendance is being registered.
      showMyLoadingDialog(
          dialogText: "Marking your attendance...\nPlease Wait!");
      // throw DioError(
      //     error: HttpException,
      //     response: Response(
      //         statusCode: 422, data: {"message": " qwerty qwerty qwerty"}));

      Map<String, dynamic> data = {
        "photo": await MultipartFile.fromFile(
          _image.path,
          filename: basename(_image.path),
          contentType: MediaType('image', "png"),
        ),
        "type": widget.type,
        "latitude": _userPosition.latitude,
        "longitude": _userPosition.longitude,
        "device_model": _deviceModel,
        "date": DateTime.now().toIso8601String(),
        // "is_visit_outside_pjp": false,
      };
      if (reason != null) {
        data.addAll({
          "late_reason": reason,
        });
      }
      if (widget.storeId != null && widget.type == "daily_visits") {
        data.addAll({
          "store_id": widget.storeId,
          "date":
              // DateTime(DateTime.now().year, DateTime.now().month,
              //         DateTime.now().day)
              //     .toIso8601String(),
              // "2021-03-13T18:30:00.000Z",
              DateTime.now().toIso8601String(),
        });
      }
      FormData formData = FormData.fromMap(data);
      Response response = await MyApi.postFormDataWithAuthorization(
          formData, "attendance/mark-attendance");
      print("$Mark_Attendance_Screen Response = ${response.data}");
      if (response.data['code'] == 401 || response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.data['code'] == 200) {
        // User's attendance is marked successfully
        dismissMyDialog();
        Navigator.pop(context, response.data["message"]);
      } else {
        dismissMyDialog();
        Scaffold.of(context)
            .showSnackBar(createSnackBar(msg: "Something Went Wrong!"));
        throw Future.error("$Mark_Attendance_Screen Error from Future");
      }
    } on SocketException {
      dismissMyDialog();
      print("$Mark_Attendance_Screen Exception = $error");
      Scaffold.of(context).showSnackBar(createSnackBar());
    } catch (error) {
      dismissMyDialog();
      print("$Mark_Attendance_Screen Exception = $error");
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        } else if (error.response.statusCode == 422) {
          if (widget.storeId != null && widget.type == "daily_visits") {
            // For FOE/FOM users.
            Scaffold.of(context).showSnackBar(
              createSnackBar(msg: error.response.data["message"]),
            );
          } else {
            showDialogToEnterCustomValue(
                context, error.response.data["message"]);
          }
          return Future.error(HttpException("422"));
        }
      }
      print("$Mark_Attendance_Screen Exception = $error");
      Scaffold.of(context).showSnackBar(
        createSnackBar(msg: "Something Went Wrong!"),
      );
      return Future.error(error);
    }
  }

  void showDialogToEnterCustomValue(BuildContext context, String message) {
    _reasonController.clear();
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (dialogContext) {
        return AlertDialog(
          title: Text("Late Attendance"),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(message),
              SizedBox(height: subMargin),
              TextFormField(
                controller: _reasonController,
                key: _reasonFormKey,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Enter the reason!';
                  }
                  return null;
                },
              ),
            ],
          ),
          actions: [
            FlatButton(
              child: Text(
                "CANCEL",
                style: TextStyle(color: textSecondary),
              ),
              onPressed: () {
                Navigator.of(dialogContext).pop();
              },
            ),
            FlatButton(
              child: Text(
                "MARK",
                style: TextStyle(
                  color: colorPrimary,
                ),
              ),
              onPressed: () {
                if (_reasonFormKey.currentState.validate()) {
                  Navigator.of(dialogContext).pop();
                  _markUserAttendance(context, reason: _reasonController.text);
                }
              },
            ),
          ],
        );
      },
    );
  }
}
