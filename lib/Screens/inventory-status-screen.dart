import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/inventory-status-card.dart';
import 'package:market_express/Models/sales-product-suggestion.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InventoryStatusScreen extends StatefulWidget {
  @override
  _InventoryStatusScreenState createState() => _InventoryStatusScreenState();
}

class _InventoryStatusScreenState extends State<InventoryStatusScreen> {
  Future<List<SalesProductSuggestion>> _futureInventoryStatus;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _futureInventoryStatus = _getInventoryStatus();
  }

  Size screenSize;

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          "Inventory Status",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: greyBackground,
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          child: RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () {
              setState(() {
                _futureInventoryStatus = _getInventoryStatus();
              });
              return _futureInventoryStatus;
            },
            child: FutureBuilder<List<SalesProductSuggestion>>(
              future: _futureInventoryStatus,
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  if (snapshot.error.toString().contains("404")) {
                    return buildEmptyWidget(screenSize,
                        text: "No Inventory Found!");
                  }
                  return buildErrorWidget(screenSize);
                } else if (snapshot.hasData &&
                    snapshot.connectionState == ConnectionState.done &&
                    snapshot.data.length != 0) {
                  return _buildInventoryStatusList(snapshot.data);
                } else if (snapshot.connectionState ==
                    ConnectionState.waiting) {
                  _refreshIndicatorKey.currentState?.show();
                  return _buildInventoryStatusList(snapshot.data);
                }

                return SizedBox.expand(
                  child: ListView(),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildInventoryStatusList(
      List<SalesProductSuggestion> inventoriesStatus) {
    return ListView.builder(
      itemCount: inventoriesStatus != null ? inventoriesStatus.length : 0,
      itemBuilder: (context, index) =>
          InventoryStatusCard(inventoriesStatus[index]),
    );
  }

  Future<List<SalesProductSuggestion>> _getInventoryStatus() async {
    List<SalesProductSuggestion> inventoriesStatus = [];
    try {
      Response response =
          await MyApi.getDataWithAuthorization("stores/product-list");
      print("$InventoryStatus_Screen Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var attendanceSummary in response.data["data"]) {
          inventoriesStatus
              .add(SalesProductSuggestion.fromJson(attendanceSummary));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("$InventoryStatus_Screen SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      print("$InventoryStatus_Screen Exception = $error");
      throw Future.error(error);
    }
    return inventoriesStatus;
  }
}
