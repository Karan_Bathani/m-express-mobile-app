import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/manager-attendance-card.dart';
import 'package:market_express/Models/foe-attendance-summary.dart';
import 'package:market_express/Models/manager-attendance.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FOEAttendanceSummaryScreen extends StatefulWidget {
  final DateTime selectedDateTime;
  final String userId;
  final String userName;

  FOEAttendanceSummaryScreen(this.selectedDateTime, this.userId, this.userName);

  @override
  _FOEAttendanceSummaryScreenState createState() =>
      _FOEAttendanceSummaryScreenState();
}

class _FOEAttendanceSummaryScreenState
    extends State<FOEAttendanceSummaryScreen> {
  Future<List<ManagerAttendance>> _futureFOEAttendanceSummary;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _futureFOEAttendanceSummary = _getFOEAttendanceSummary();
  }

  int totalCount;
  Size screenSize;

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          widget.userName ?? 'Attendance Summary',
          style: TextStyle(color: Colors.white),
        ),
        actions: [
          totalCount != null
              ? Container(
                  margin: EdgeInsets.only(right: subMarginHalf),
                  height: kToolbarHeight,
                  width: kTextTabBarHeight,
                  child: Center(
                    child: Text(
                      "Total:\n$totalCount",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                )
              : SizedBox.shrink(),
        ],
      ),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: greyBackground,
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          child: RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () {
              setState(() {
                _futureFOEAttendanceSummary = _getFOEAttendanceSummary();
              });
              return _futureFOEAttendanceSummary;
            },
            child: FutureBuilder<List<ManagerAttendance>>(
              future: _futureFOEAttendanceSummary,
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  if (snapshot.error.toString().contains("404")) {
                    return buildEmptyWidget(screenSize,
                        text: "No Attendance Found!");
                  }
                  return buildErrorWidget(screenSize);
                }
                if (snapshot.hasData &&
                    snapshot.connectionState == ConnectionState.done &&
                    snapshot.data.length != 0) {
                  return _buildFOEAttendanceSummaryListView(snapshot.data);
                } else if (snapshot.connectionState ==
                    ConnectionState.waiting) {
                  _refreshIndicatorKey.currentState?.show();
                  return _buildFOEAttendanceSummaryListView(snapshot.data);
                }
                return SizedBox.expand(
                  child: ListView(),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildFOEAttendanceSummaryListView(
      List<ManagerAttendance> foeAttendanceSummaries) {
    return ListView.builder(
      itemCount:
          foeAttendanceSummaries != null ? foeAttendanceSummaries.length : 0,
      itemBuilder: (context, index) =>
          ManagerAttendanceCard(foeAttendanceSummaries[index]),
    );
  }

  Future<List<ManagerAttendance>> _getFOEAttendanceSummary() async {
    List<ManagerAttendance> foeAttendanceSummaries = [];
    try {
      Response response = await MyApi.postDataWithAuthorization({
        "month": widget.selectedDateTime.month,
        "year": widget.selectedDateTime.year,
        "user_id": widget.userId
      }, "attendance/monthly-user-attendance");
      print("$FOEAttendanceSummary_Screen Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var attendance in response.data["data"]) {
          foeAttendanceSummaries.add(ManagerAttendance.fromJson(attendance));
        }
        // setState(() {
        //   totalCount = foeAttendanceSummaries.length;
        // });
      } else {
        throw Future.error("Error from Future");
      }
    } on SocketException {
      print("$FOEAttendanceSummary_Screen SocketException");
      throw Future.error(SocketException);
    } catch (error) {
      print("$FOEAttendanceSummary_Screen Exception = $error");
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }

      throw Future.error(error);
    }
    return foeAttendanceSummaries;
  }
}
