import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/survey-card.dart';
import 'package:market_express/Models/survey.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SurveysListingScreen extends StatefulWidget {
  final DateTime selectedDateTime;
  final String userId;
  final String userName;

  SurveysListingScreen(this.selectedDateTime, this.userId, this.userName);

  @override
  _SurveysListingScreenState createState() => _SurveysListingScreenState();
}

class _SurveysListingScreenState extends State<SurveysListingScreen> {
  Future<List<Survey>> _futureSurvey;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _futureSurvey = _getUserSurvey();
  }

  int totalCount;
  Size screenSize;

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          widget.userName ?? "Survey(s)",
          style: TextStyle(color: Colors.white),
        ),
        actions: [
          totalCount != null
              ? Container(
                  margin: EdgeInsets.only(right: subMarginHalf),
                  height: kToolbarHeight,
                  width: kTextTabBarHeight,
                  child: Center(
                    child: Text(
                      "Total:\n$totalCount",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                )
              : SizedBox.shrink(),
        ],
      ),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: greyBackground,
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          child: RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () {
              setState(() {
                _futureSurvey = _getUserSurvey();
              });
              return _futureSurvey;
            },
            child: FutureBuilder<List<Survey>>(
              future: _futureSurvey,
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  if (snapshot.error.toString().contains("404")) {
                    return buildEmptyWidget(screenSize,
                        text: "No Survey Found!");
                  }
                  return buildErrorWidget(screenSize);
                }
                if (snapshot.hasData &&
                    snapshot.connectionState == ConnectionState.done &&
                    snapshot.data.length != 0) {
                  return _buildSurveyListView(snapshot.data);
                } else if (snapshot.connectionState ==
                    ConnectionState.waiting) {
                  _refreshIndicatorKey.currentState?.show();
                  return _buildSurveyListView(snapshot.data);
                }
                return SizedBox.expand(
                  child: ListView(),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildSurveyListView(List<Survey> surveys) {
    return ListView.builder(
      itemCount: surveys != null ? surveys.length : 0,
      itemBuilder: (context, index) => SurveyCard(surveys[index]),
    );
  }

  Future<List<Survey>> _getUserSurvey() async {
    List<Survey> surveys = [];
    try {
      Response response = await MyApi.postDataWithAuthorization({
        "month": widget.selectedDateTime.month,
        "year": widget.selectedDateTime.year,
        "user_id": widget.userId
      }, "users/monthly-user-surveys");
      print("$SurveyListing_Screen Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var attendance in response.data["data"]) {
          surveys.add(Survey.fromJson(attendance));
        }
        // setState(() {
        //   totalCount = surveys.length;
        // });
      } else {
        throw Future.error("Error from Future");
      }
    } on SocketException {
      print("$SurveyListing_Screen SocketException");
      throw Future.error(SocketException);
    } catch (error) {
      print("$SurveyListing_Screen Exception = $error");
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }

      throw Future.error(error);
    }
    return surveys;
  }
}
