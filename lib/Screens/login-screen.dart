import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Consumer_Electronics/Screens/am-home-screen.dart';
import 'package:market_express/Consumer_Electronics/Screens/tso-home-screen.dart';
import 'package:market_express/Models/login-data.dart';
import 'package:market_express/Screens/manager-home-screen.dart';
import 'package:market_express/Screens/sec-home-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:market_express/Visual%20Merchandiser/Screens/vm-home-screen.dart';
import 'package:market_express/Visual%20Merchandiser/Screens/vmsup-home-screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autovalidate = false;
  bool isLoading = false;

  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  // Map<String, dynamic> _deviceData = <String, dynamic>{};
  String _deviceModel;
  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  Future<void> initPlatformState() async {
    Map<String, dynamic> deviceData;
    String deviceModel;
    try {
      if (Platform.isAndroid) {
        // deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
        AndroidDeviceInfo androidDeviceInfo =
            await deviceInfoPlugin.androidInfo;
        deviceModel =
            "${androidDeviceInfo.manufacturer} ${androidDeviceInfo.model}";
      } else if (Platform.isIOS) {
        // deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
        IosDeviceInfo iosDeviceInfo = await deviceInfoPlugin.iosInfo;
        deviceModel = "${iosDeviceInfo.name} ${iosDeviceInfo.model}";
      }
    } on PlatformException {
      deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }
    if (!mounted) return;
    setState(() {
      // _deviceData = deviceData;
      _deviceModel = deviceModel;
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        toolbarHeight: kToolbarHeight * 1.5,
//! TODO: Remove the Gesture Detector for Final Production
        title: GestureDetector(
          onTap: () {
            // TSO
            emailController.text = "19050154";
            passwordController.text = "83758620";
          },
          onHorizontalDragEnd: (dragEndDetails) {
            if (dragEndDetails.primaryVelocity > 0) {
              // Swiped Right
              // OM Login
              // emailController.text = "E3333";
              // passwordController.text = "55787917";
            } else {
              // AM Login
              emailController.text = "19040130";
              passwordController.text = "66590266";
            }
          },
          child: Text(
            'Welcome',
            style: TextStyle(
              color: Colors.white,
              fontSize: 26,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
      body: SafeArea(
        bottom: false,
        child: Container(
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          child: Form(
            key: _formKey,
            autovalidate: _autovalidate,
            child: ListView(
              padding: EdgeInsets.all(mainMargin),
              children: [
//! TODO: Remove the Gesture Detector for Final Production
                GestureDetector(
                  onDoubleTap: () {
                    emailController.text = "SVM019";
                    passwordController.text = "91499828";
                  },
                  onLongPress: () {
                    emailController.text = "SVM015";
                    passwordController.text = "40692212";
                  },
                  child: Text(
                    'Login',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 27),
                  ),
                ),
                SizedBox(height: subMarginHalf),
//! TODO: Remove the Gesture Detector for Final Production
                GestureDetector(
                  onTap: () {
                    emailController.text = "E1111";
                    passwordController.text = "95012483";
                  },
                  onLongPress: () {
                    emailController.text = "E007";
                    passwordController.text = "11034725";
                    //   SharedPreferences.getInstance().then((value) {
                    //     value.setString('token',
                    //         "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjAxN2FjYTM0MmFiNWY0Y2EwYzlhOGIwIiwic2Vzc2lvbl9pZCI6IjFiYTgzNTIyLWU1YjctNDBmNS05ZTBhLWYzYjZmNWRhN2FiNSIsImlhdCI6MTYxMjc4NDgxMX0.uLsO4jnASxvDYtKUUmAOUWwaCY_sTaHpEKzDpJt140M");
                    //     value.setString("user_name", "John Doe");
                    //     value.setString("user_type", "SEC");
                    //     value.setString("employee_id", "E5555");

                    //     value.setString('token',
                    //         "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjAwNTJiZmQxNDExMzIzYjc0NTQ4NWM4Iiwic2Vzc2lvbl9pZCI6IjAyZWQ4ZTg4LWMxMmMtNDgyNi04YThlLWQzODkwYmU0MGY3ZSIsImlhdCI6MTYxMjkzNzI4Mn0.2p561EeCBCdMxMifdmQTRbekqAUhawPlToSLHfNIdtE");
                    //     value.setString("user_name", "Karan Bathani");
                    //     value.setString("user_type", "FOE");
                    //     value.setString("employee_id", "E2222");
                    //     Navigator.push(
                    //       context,
                    //       MaterialPageRoute(
                    //         builder: (context) => HomeScreen(),
                    //       ),
                    //     );
                    //   });
                  },
                  onVerticalDragEnd: (dragEndDetails) {
                    if (dragEndDetails.primaryVelocity > 0) {
                      // emailController.text = "S0021";
                      // passwordController.text = "Vuw6VGNL";
                      emailController.text = "E049";
                      passwordController.text = "52373344";
                    } else {
                      emailController.text = "S0092";
                      passwordController.text = "23638516";
                    }
                  },
                  onDoubleTap: () {
                    emailController.text = "E4444";
                    passwordController.text = "53054683";
                  },
                  onHorizontalDragEnd: (dragEndDetails) {
                    if (dragEndDetails.primaryVelocity > 0) {
                      // Swiped Right
                      // FOM Login
                      emailController.text = "E3333";
                      passwordController.text = "55787917";
                    } else {
                      // FOE Login
                      emailController.text = "E2222";
                      passwordController.text = "18918375";
                    }
                  },
                  child: Text(
                    'Enter your Email/Employee Id and Password to get access to your account.',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
                SizedBox(height: subMarginDouble),
                TextFormField(
                  controller: emailController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Please enter an Email/Employee ID.";
                    }
                    if (value.contains("@") && !validateEmail(value)) {
                      return 'Enter a valid Email.';
                    }
                    return null;
                  },
                  keyboardType: TextInputType.emailAddress,
                  style: TextStyle(
                    color: textPrimary,
                    fontWeight: FontWeight.w600,
                  ),
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: myGreyColor)),
                    labelText: 'Email Address/Employee ID',
                    // labelStyle: TextStyle(color: textPrimary),
                    hintText: 'Enter your Details Here',
                  ),
                ),
                SizedBox(height: subMargin),
                PasswordField(
                  controller: passwordController,
                ),
                SizedBox(height: subMargin),
                Builder(
                  builder: (buildContext) => Container(
                    width: screenSize.width,
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                        color: colorPrimaryLight.withOpacity(.34),
                        blurRadius: 4,
                        offset: Offset(0, 4),
                      ),
                    ]),
                    child: FlatButton(
                      shape: RoundedRectangleBorder(
                        borderRadius:
                            BorderRadius.circular(miniCardCornerRadius),
                      ),
                      color: isLoading ? Colors.grey[400] : colorPrimary,
                      padding: EdgeInsets.symmetric(vertical: 18),
                      child: Text(
                        isLoading ? "Logging..." : 'Login',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                        ),
                      ),
                      onPressed: () =>
                          isLoading ? null : _handleLogin(buildContext),
                    ),
                  ),
                ),
                SizedBox(height: mainMargin),
                // Center(
                //   child: FlatButton(
                //     onPressed: () {
                //       showToast(
                //         'Coming Soon...',
                //         context: context,
                //         animation: StyledToastAnimation.slideFromBottom,
                //         reverseAnimation: StyledToastAnimation.slideToBottom,
                //         startOffset: Offset(0.0, 3.0),
                //         reverseEndOffset: Offset(0.0, 3.0),
                //         position: StyledToastPosition.bottom,
                //         duration: Duration(seconds: 3),
                //         animDuration: Duration(seconds: 1),
                //         curve: Curves.elasticOut,
                //         reverseCurve: Curves.fastOutSlowIn
                //       );
                //     },
                //     child: Text(
                //       'Forgot Password',
                //       style: TextStyle(color: colorPrimary),
                //     ),
                //   ),
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _handleLogin(BuildContext context) async {
    Response response;
    if (_formKey.currentState.validate()) {
      setState(() {
        isLoading = !isLoading;
        print('isLoading = ' + isLoading.toString());
      });
      Map<String, String> data = {
        "password": passwordController.text,
        "device_model": _deviceModel,
      };
      if (emailController.text.contains("@")) {
        data.addAll({"email": emailController.text});
      } else {
        data.addAll({"employee_id": emailController.text});
      }
      try {
        response = await MyApi.postData(data, "users/login");
        print(
            "$Login_Screen Response = $response"); // debug LoginScreen  Response = {"code":422,"message":"Invalid Credentials."}
        print(
            "$Login_Screen ResponseData = ${response.data}"); // debug LoginScreen  Response = {"code":422,"message":"Invalid Credentials."}
        print(
            "$Login_Screen Message = ${response.statusMessage}"); // debug LoginScreen  Response = {"code":422,"message":"Invalid Credentials."}
        if (response.data['code'] == 422) {
          //Validation Error from Server
          setState(() {
            isLoading = false;
          });
          Scaffold.of(context)
              .showSnackBar(createSnackBar(msg: response.data['message']));
        } else if (response.data['code'] == 200) {
          // Everything is fine
          LoginData loginData = LoginData.fromJson(response.data['data']);
          SharedPreferences localStorage =
              await SharedPreferences.getInstance();
          localStorage.setString('token', loginData.token);
          localStorage.setString("user_name", loginData.userName);
          localStorage.setString("user_type", loginData.userType);
          localStorage.setString("employee_id", loginData.employeeId);

          if (loginData.userType == "SEC" || loginData.userType == "ND. SEC") {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => SECHomeScreen(),
                settings: RouteSettings(name: "/home"),
              ),
              (route) => false,
            );
          } else if (loginData.userType == "VM") {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => VMHomeScreen(),
                settings: RouteSettings(name: "/home"),
              ),
              (route) => false,
            );
          } else if (loginData.userType == "VMS") {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => VMSupHomeScreen(),
                settings: RouteSettings(name: "/home"),
              ),
              (route) => false,
            );
          } else if (loginData.userType == "TSO") {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => TSOHomeScreen(),
                settings: RouteSettings(name: "/home"),
              ),
              (route) => false,
            );
          } else if (loginData.userType == "AM" ||
              loginData.userType == "Area Manager" ||
              loginData.userType.contains("Area Manager")) {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => AMHomeScreen(),
                settings: RouteSettings(name: "/home"),
              ),
              (route) => false,
            );
          } else {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => ManagerHomeScreen(),
                settings: RouteSettings(name: "/home"),
              ),
              (route) => false,
            );
          }
        } else {
          // For all other cases of error
          setState(() {
            isLoading = false;
          });
          Scaffold.of(context)
              .showSnackBar(createSnackBar(msg: "Something Went Wrong."));
          print("$Login_Screen LoginApiError");
        }
      } on SocketException {
        setState(() {
          isLoading = false;
        });
        Scaffold.of(context).showSnackBar(createSnackBar());
        print("$Login_Screen SocketException");
      } catch (error) {
        setState(() {
          isLoading = false;
        });
        print("$Login_Screen Exception = $error");
        if (error is DioError) {
          if (error.response.statusCode == 422) {
            setState(() {
              isLoading = false;
            });
            Scaffold.of(context).showSnackBar(
                createSnackBar(msg: error.response.data['message']));
            return;
          }
        }
        Scaffold.of(context)
            .showSnackBar(createSnackBar(msg: "Something Went Wrong."));
      }
    } else {
      setState(() {
        _autovalidate = true;
      });
    }
  }
}

class PasswordField extends StatefulWidget {
  final TextEditingController controller;

  PasswordField({this.controller});
  @override
  _PasswordFieldState createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  bool hidePass = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextFormField(
          validator: (value) {
            if (value.isEmpty) {
              return "Password is Empty.";
            }
            if (value.length < 8) {
              return 'Password length should be between 8-10.';
            }
            return null;
          },
          controller: widget.controller,
          obscureText: hidePass,
          style: TextStyle(
            color: textPrimary,
            fontWeight: FontWeight.w600,
          ),
          decoration: InputDecoration(
            border:
                OutlineInputBorder(borderSide: BorderSide(color: myGreyColor)),
            labelText: 'Password',
            hintText: "Enter your Password",
            suffixIcon: IconButton(
              icon: Icon(
                hidePass ? Icons.visibility : Icons.visibility_off,
                color: Colors.grey,
              ),
              onPressed: () {
                setState(() {
                  hidePass = !hidePass;
                });
              },
            ),
          ),
        ),
      ],
    );
  }
}
