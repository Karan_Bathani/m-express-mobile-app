import 'package:flutter/material.dart';
import 'package:market_express/Screens/OHS/OHSTabs/models-tab.dart';
import 'package:market_express/Screens/OHS/OHSTabs/outlets-tab.dart';
import 'package:market_express/Utility/constants.dart';

class OHSScreen extends StatefulWidget {
  @override
  _OHSScreenState createState() => _OHSScreenState();
}

class _OHSScreenState extends State<OHSScreen> {
  Size screenSize;

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return DefaultTabController(
      initialIndex: 0,
      length: 2,
      child: Scaffold(
        backgroundColor: colorPrimary,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: Text(
            "On Hand Stock",
            style: TextStyle(color: Colors.white),
          ),
          bottom: TabBar(
            indicatorColor: colorSecondary,
            indicatorSize: TabBarIndicatorSize.label,
            indicatorWeight: 4,
            tabs: [
              Tab(
                text: "Device Models",
              ),
              Tab(
                text: "Outlets",
              ),
            ],
          ),
        ),
        body: SafeArea(
          child: Container(
            clipBehavior: Clip.antiAlias,
            width: screenSize.width,
            height: screenSize.height,
            decoration: BoxDecoration(
              color: greyBackground,
              borderRadius:
                  BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
            ),
            child: TabBarView(
              children: [
                ModelsTab(),
                OutletsTab(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
