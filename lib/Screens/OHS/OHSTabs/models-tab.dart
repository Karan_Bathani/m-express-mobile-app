import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/model-card.dart';
import 'package:market_express/Models/product-suggestion.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ModelsTab extends StatefulWidget {
  @override
  _ModelsTabState createState() => _ModelsTabState();
}

class _ModelsTabState extends State<ModelsTab>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  Future<List<ProductSuggestion>> _futureModels;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _futureModels = _getModels();
  }

  Size screenSize;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    screenSize = MediaQuery.of(context).size;
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: () {
        setState(() {
          _futureModels = _getModels();
        });
        return _futureModels;
      },
      child: FutureBuilder<List<ProductSuggestion>>(
        future: _futureModels,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            if (snapshot.error.toString().contains("404")) {
              return buildEmptyWidget(screenSize, text: "No Product(s) Found!");
            }
            return buildErrorWidget(screenSize);
          } else if (snapshot.hasData &&
              snapshot.connectionState == ConnectionState.done &&
              snapshot.data.length != 0) {
            return _buildModelsListView(snapshot.data);
          } else if (snapshot.connectionState == ConnectionState.waiting) {
            _refreshIndicatorKey.currentState?.show();
            return _buildModelsListView(snapshot.data);
          }
          return SizedBox.expand(
            child: ListView(),
          );
        },
      ),
    );
  }

  Widget _buildModelsListView(List<ProductSuggestion> models) {
    return ListView.builder(
      itemCount: models != null ? models.length : 0,
      itemBuilder: (context, index) => ModelCard(models[index]),
    );
  }

  Future<List<ProductSuggestion>> _getModels() async {
    List<ProductSuggestion> models = [];
    try {
      Response response = await MyApi.getDataWithAuthorization("products/list");
      print("$Models_Tab Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var sale in response.data["data"]) {
          models.add(ProductSuggestion.fromJson(sale));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("$Models_Tab SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      print("$Models_Tab Exception = $error");
      return Future.error(error);
    }
    return models;
  }
}
