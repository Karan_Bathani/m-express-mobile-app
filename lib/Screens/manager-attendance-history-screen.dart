import 'package:flutter/material.dart';
import 'package:market_express/Screens/ManagerAttendanceTabs/super-manager-current-month-attendance-tab-fom.dart';
import 'package:market_express/Screens/ManagerAttendanceTabs/manager-attendance-history-tab.dart';
import 'package:market_express/Screens/ManagerAttendanceTabs/current-month-attendance-tab.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ManagerAttendanceHistoryScreen extends StatefulWidget {
  @override
  _ManagerAttendanceHistoryScreenState createState() =>
      _ManagerAttendanceHistoryScreenState();
}

class _ManagerAttendanceHistoryScreenState
    extends State<ManagerAttendanceHistoryScreen> {
  Size screenSize;

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;

    return DefaultTabController(
      initialIndex: 0,
      length: 2,
      child: Scaffold(
        backgroundColor: colorPrimary,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: Text(
            'My Attendance',
            style: TextStyle(color: Colors.white),
          ),
          bottom: TabBar(
            indicatorColor: colorSecondary,
            indicatorSize: TabBarIndicatorSize.label,
            indicatorWeight: 4,
            tabs: [
              Tab(
                text:
                    "         ${DateTime.now().formatDate(pattern: "MMM")}         ",
              ),
              Tab(
                text: "         All         ",
              ),
            ],
          ),
        ),
        body: SafeArea(
          child: Container(
            clipBehavior: Clip.antiAlias,
            width: screenSize.width,
            height: screenSize.height,
            decoration: BoxDecoration(
              color: greyBackground,
              // Colors.white,
              borderRadius:
                  BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
            ),
            child: TabBarView(
              children: [
                FutureBuilder<SharedPreferences>(
                  future: SharedPreferences.getInstance(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData &&
                        snapshot.data.containsKey("user_type")) {
                      return snapshot.data.getString("user_type") == "FOM" ||
                              snapshot.data.getString("user_type") == "VMS"
                          ? SuperManagerCurrentMonthAttendanceTab(
                              snapshot.data.getString("user_type"))
                          : CurrentMonthAttendanceTab();
                    }
                    return SizedBox.shrink();
                  },
                ),
                ManagerAttendanceHistoryTab(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
