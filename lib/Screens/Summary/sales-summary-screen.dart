import 'package:flutter/material.dart';
import 'package:market_express/Screens/SalesSummaryTabs/sales-employeewise-tab.dart';
import 'package:market_express/Screens/SalesSummaryTabs/sales-overall-tab.dart';
import 'package:market_express/Utility/constants.dart';

class SalesSummaryScreen extends StatefulWidget {
  @override
  _SalesSummaryScreenState createState() => _SalesSummaryScreenState();
}

Size screenSize;

class _SalesSummaryScreenState extends State<SalesSummaryScreen> {
  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return DefaultTabController(
      initialIndex: 0,
      length: 2,
      child: Scaffold(
        backgroundColor: colorPrimary,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: Text(
            "Sales Summary",
            style: TextStyle(color: Colors.white),
          ),
          bottom: TabBar(
            indicatorColor: colorSecondary,
            indicatorSize: TabBarIndicatorSize.label,
            indicatorWeight: 4,
            tabs: [
              Tab(
                text: "Over-All Sales",
              ),
              Tab(
                text: "Employee-Wise",
              ),
            ],
          ),
        ),
        body: SafeArea(
          child: Container(
            clipBehavior: Clip.antiAlias,
            width: screenSize.width,
            height: screenSize.height,
            decoration: BoxDecoration(
              color: greyBackground,
              borderRadius:
                  BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
            ),
            child: TabBarView(
              children: [
                SalesOverAllTab(),
                SalesEmployeeWiseTab(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
