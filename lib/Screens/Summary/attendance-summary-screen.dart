import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/attendance-summary-card.dart';
import 'package:market_express/Models/attendance-summary.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AttendanceSummaryScreen extends StatefulWidget {
  @override
  _AttendanceSummaryScreenState createState() =>
      _AttendanceSummaryScreenState();
}

class _AttendanceSummaryScreenState extends State<AttendanceSummaryScreen> {
  Future<List<AttendanceSummary>> _futureAttendanceSummary;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _selectedDateTime = todaysDateTime;
    _futureAttendanceSummary = _getAttendanceSummary();
  }

  DateTime todaysDateTime = DateTime.now();
  DateTime _selectedDateTime;
  Size screenSize;
  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          'Attendance Summary',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: greyBackground,
            // Colors.white,
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          child: RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () {
              setState(() {
                _futureAttendanceSummary = _getAttendanceSummary();
              });
              return _futureAttendanceSummary;
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
//> Date Selector
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: subMargin,
                  ),
                  color: dropDownBackground,
                  child: DropdownButton(
                    underline: SizedBox.shrink(),
                    iconSize: 30,
                    isExpanded: true,
                    hint: Text(
                      DateFormat("MMMM, yyy").format(_selectedDateTime),
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    items: List.generate(10, (index) {
                      DateTime generatedDate = DateTime(
                          todaysDateTime.year, todaysDateTime.month - index);
                      return DropdownMenuItem(
                        child:
                            Text(DateFormat("MMMM, yyy").format(generatedDate)),
                        value: generatedDate,
                      );
                    }),
                    onChanged: (value) {
                      setState(() {
                        _selectedDateTime = value;
                        _futureAttendanceSummary = _getAttendanceSummary();
                      });
                    },
                  ),
                ),
                FutureBuilder<List<AttendanceSummary>>(
                  future: _futureAttendanceSummary,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      _refreshIndicatorKey.currentState?.show();
                      return _buildAttendanceSummaryList(snapshot.data);
                    } else if (snapshot.hasError) {
                      //~ Error
                      if (snapshot.error.toString().contains("404")) {
                        return buildEmptyWidget(screenSize,
                            text: "No Attendance Summary Found");
                      }
                      return buildErrorWidget(screenSize);
                    } else if (snapshot.hasData &&
                        snapshot.connectionState == ConnectionState.done &&
                        snapshot.data.length != 0) {
                      //< SUCCESSS
                      return _buildAttendanceSummaryList(snapshot.data);
                    }
                    return Expanded(
                      child: ListView(),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildAttendanceSummaryList(
      List<AttendanceSummary> attendanceSummaries) {
    return Expanded(
      child: ListView.builder(
        itemCount: attendanceSummaries != null ? attendanceSummaries.length : 0,
        itemBuilder: (context, index) => AttendanceSummaryCard(
            attendanceSummaries[index], _selectedDateTime),
      ),
    );
  }

  Future<List<AttendanceSummary>> _getAttendanceSummary() async {
    List<AttendanceSummary> attendanceSummaries = [];
    try {
      Response response = await MyApi.postDataWithAuthorization({
        "month": _selectedDateTime.month,
        "year": _selectedDateTime.year,
        "summary_for": "attendance",
      }, "general-summary");
      print("$AttendanceSummary_Screen Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var attendanceSummary in response.data["data"]) {
          attendanceSummaries
              .add(AttendanceSummary.fromJson(attendanceSummary));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("$AttendanceSummary_Screen SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      print("$AttendanceSummary_Screen Exception = $error");
      throw Future.error(error);
    }
    return attendanceSummaries;
  }
}
