import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Models/training-materials-user.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TrianingMaterialStatusScreen extends StatefulWidget {
  final String trainingMaterialId;
  TrianingMaterialStatusScreen(this.trainingMaterialId);
  @override
  _TrianingMaterialStatusScreenState createState() =>
      _TrianingMaterialStatusScreenState();
}

class _TrianingMaterialStatusScreenState
    extends State<TrianingMaterialStatusScreen> {
  Future<List<TrainingMaterialUser>> _futureTrainingMaterialUser;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _futureTrainingMaterialUser = _getTrainingMaterialUsers();
  }

  Size screenSize;

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          "Training Materials Status",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: greyBackground,
            // Colors.white,
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          child: RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () {
              setState(() {
                _futureTrainingMaterialUser = _getTrainingMaterialUsers();
              });
              return _futureTrainingMaterialUser;
            },
            child: FutureBuilder<List<TrainingMaterialUser>>(
              future: _futureTrainingMaterialUser,
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  if (snapshot.error.toString().contains("404")) {
                    return buildEmptyWidget(screenSize, text: "No User Found!");
                  }
                  return buildErrorWidget(screenSize);
                }
                if (snapshot.hasData &&
                    snapshot.connectionState == ConnectionState.done &&
                    snapshot.data.length != 0) {
                  return _buildTrainingMaterialUsersListView(snapshot.data);
                } else if (snapshot.connectionState ==
                    ConnectionState.waiting) {
                  _refreshIndicatorKey.currentState?.show();
                  return _buildTrainingMaterialUsersListView(snapshot.data);
                }
                return SizedBox.expand(
                  child: ListView(),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTrainingMaterialUsersListView(
      List<TrainingMaterialUser> trainingMaterialUsers) {
    if (trainingMaterialUsers != null && trainingMaterialUsers.isEmpty) {
      return buildEmptyWidget(screenSize, text: "No User Found!");
    }
    return ListView.builder(
      itemCount:
          trainingMaterialUsers != null ? trainingMaterialUsers.length : 0,
      itemBuilder: (context, index) => Card(
        elevation: cardElevation,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius),
        ),
        color: Colors.white,
        clipBehavior: Clip.antiAlias,
        margin: EdgeInsets.symmetric(
          horizontal: subMargin,
          vertical: subMarginHalf,
        ),
        child: ListTile(
          contentPadding: EdgeInsets.all(cardContentPadding),
          leading: CircleAvatar(
            backgroundImage:
                NetworkImage(trainingMaterialUsers[index].userPhoto ?? " "),
            radius: 29,
          ),
          title: Text(trainingMaterialUsers[index].userName),
          subtitle: trainingMaterialUsers[index].viewedAt != null
              ? Text(
                  "Viewed on: ${trainingMaterialUsers[index].viewedAt.formatDate(pattern: "dd MMM, yyy")}\n at: ${trainingMaterialUsers[index].viewedAt.formatDate(pattern: "hh:mm a")}")
              : SizedBox(),
        ),
      ),
      // TrainingMaterialUserCard(trainingMaterialUsers[index]),
    );
  }

  Future<List<TrainingMaterialUser>> _getTrainingMaterialUsers() async {
    List<TrainingMaterialUser> trainingMaterialUsers = [];
    // print("TrainingMaterialStatusScreen ID = ${widget.trainingMaterialId}");
    try {
      Response response = await MyApi.getDataWithAuthorization(
        "training/users-by-training-material-status/${widget.trainingMaterialId}",
      );
      print("TrainingMaterialStatusScreen Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var trainingMaterial in response.data["data"]) {
          trainingMaterialUsers
              .add(TrainingMaterialUser.fromJson(trainingMaterial));
        }
      } else {
        throw Future.error("Error from Future");
      }
    } on SocketException {
      print("TrainingMaterialStatusScreen SocketException");
      throw Future.error(SocketException);
    } catch (error) {
      print("TrainingMaterialStatusScreen Exception = $error");
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }

      return Future.error(error);
    }
    return trainingMaterialUsers;
  }
}
