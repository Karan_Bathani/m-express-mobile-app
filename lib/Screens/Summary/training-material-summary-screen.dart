import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/training-material-card.dart';
import 'package:market_express/Models/training-material.dart';
import 'package:market_express/Screens/Summary/training-material-status-screen.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TrainingMaterialSummaryScreen extends StatefulWidget {
  final String userType;
  TrainingMaterialSummaryScreen(this.userType);
  @override
  _TrainingMaterialSummaryScreenState createState() =>
      _TrainingMaterialSummaryScreenState();
}

class _TrainingMaterialSummaryScreenState
    extends State<TrainingMaterialSummaryScreen> {
  Future<List<TrainingMaterial>> _futureTrainingMaterial;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _futureTrainingMaterial = _getTrainingMaterials();
  }

  Size screenSize;

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          "Training Materials Summary",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: greyBackground,
            // Colors.white,
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          // child: _buildMaterialListView(),
          child: RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () {
              setState(() {
                _futureTrainingMaterial = _getTrainingMaterials();
              });
              return _futureTrainingMaterial;
            },
            child: FutureBuilder<List<TrainingMaterial>>(
              future: _futureTrainingMaterial,
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  if (snapshot.error.toString().contains("404")) {
                    return buildEmptyWidget(screenSize,
                        text: "No Training Material Found!");
                  }
                  return buildErrorWidget(screenSize);
                }
                if (snapshot.hasData &&
                    snapshot.connectionState == ConnectionState.done &&
                    snapshot.data.length != 0) {
                  return _buildTrainingMaterialListView(snapshot.data);
                } else if (snapshot.connectionState ==
                    ConnectionState.waiting) {
                  _refreshIndicatorKey.currentState?.show();
                  return _buildTrainingMaterialListView(snapshot.data);
                }
                return SizedBox.expand(
                  child: ListView(),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTrainingMaterialListView(
      List<TrainingMaterial> trainingMaterials) {
    if (trainingMaterials != null && trainingMaterials.isEmpty) {
      return buildEmptyWidget(screenSize, text: "No Training Material Found!");
    }
    return ListView.builder(
      itemCount: trainingMaterials != null ? trainingMaterials.length : 0,
      itemBuilder: (context, index) => TrainingMaterialCard(
        trainingMaterials[index],
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  TrianingMaterialStatusScreen(trainingMaterials[index].id),
            ),
          );
        },
      ),
    );
  }

  Future<List<TrainingMaterial>> _getTrainingMaterials() async {
    List<TrainingMaterial> trainingMaterials = [];
    try {
      String userTypeToFetch;
      switch (widget.userType) {
        case "FOE":
          userTypeToFetch = "sec";
          break;
        case "VMS":
          userTypeToFetch = "vm";
          break;
        case "Area Manager":
        case "AM":
          userTypeToFetch = "tso";
          break;
        default:
          userTypeToFetch = "foe";
          break;
      }
      Response response = await MyApi.getDataWithAuthorization(
          "users/training-materials-by-user-type/$userTypeToFetch");
      print("$TrainingMaterialSummary_Screen Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var trainingMaterial in response.data["data"]) {
          trainingMaterials.add(TrainingMaterial.fromJson(trainingMaterial));
        }
        // setState(() {
        //   totalCount = attendances.length;
        // });
      } else {
        throw Future.error("Error from Future");
      }
    } on SocketException {
      print("$TrainingMaterialSummary_Screen SocketException");
      throw Future.error(SocketException);
    } catch (error) {
      print("$TrainingMaterialSummary_Screen Exception = $error");
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }

      throw Future.error(error);
    }
    return trainingMaterials;
  }
}
