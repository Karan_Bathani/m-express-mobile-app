import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/leave-summary-card.dart';
import 'package:market_express/Models/leave-summary.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LeaveSummaryScreen extends StatefulWidget {
  @override
  _LeaveSummaryScreenState createState() => _LeaveSummaryScreenState();
}

class _LeaveSummaryScreenState extends State<LeaveSummaryScreen> {
  Future<List<LeaveSummary>> _futureLeaveSummary;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _futureLeaveSummary = _getLeaveSummary();
  }

  Size screenSize;

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          'Leaves Summary',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: greyBackground,
            // Colors.white,
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          child: RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () {
              setState(() {
                _futureLeaveSummary = _getLeaveSummary();
              });
              return _futureLeaveSummary;
            },
            child: FutureBuilder<List<LeaveSummary>>(
              future: _futureLeaveSummary,
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  if (snapshot.error.toString().contains("404")) {
                    return buildEmptyWidget(screenSize,
                        text: "No Leave Summary Found");
                  }
                  return buildErrorWidget(screenSize);
                } else if (snapshot.hasData &&
                    snapshot.connectionState == ConnectionState.done &&
                    snapshot.data.length != 0) {
                  return _buildLeaveSummaryList(snapshot.data);
                } else if (snapshot.connectionState ==
                    ConnectionState.waiting) {
                  _refreshIndicatorKey.currentState?.show();
                  return _buildLeaveSummaryList(snapshot.data);
                }

                return SizedBox.expand(
                  child: ListView(),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildLeaveSummaryList(List<LeaveSummary> leaveSummaries) {
    return ListView.builder(
      itemCount: leaveSummaries != null ? leaveSummaries.length : 0,
      itemBuilder: (context, index) => LeaveSummaryCard(leaveSummaries[index]),
    );
  }

  Future<List<LeaveSummary>> _getLeaveSummary() async {
    List<LeaveSummary> leaveSummaries = [];
    try {
      Response response = await MyApi.postDataWithAuthorization({
        "month": DateTime.now().month,
        "year": DateTime.now().year,
        "summary_for": "leaves",
      }, "general-summary");
      print("$LeaveSummary_Screen Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var attendanceSummary in response.data["data"]) {
          leaveSummaries.add(LeaveSummary.fromJson(attendanceSummary));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("$LeaveSummary_Screen SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      print("$LeaveSummary_Screen Exception = $error");
      throw Future.error(error);
    }
    return leaveSummaries;
  }
}
