import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Models/store.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_autocomplete_formfield/simple_autocomplete_formfield.dart';

class SuperManagerMarkAttendanceScreen extends StatefulWidget {
  final String keyForApiCall;
  final String dataForApiCall;
  SuperManagerMarkAttendanceScreen(this.keyForApiCall, this.dataForApiCall);
  @override
  _SuperManagerMarkAttendanceScreenState createState() =>
      _SuperManagerMarkAttendanceScreenState();
}

class _SuperManagerMarkAttendanceScreenState
    extends State<SuperManagerMarkAttendanceScreen> {
  TextEditingController storesController = TextEditingController();
  bool _autoValidate = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  File _image;
  Position _userPosition;
  final picker = ImagePicker();

  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  String _deviceModel;

  Size screenSize;
  String storeId;

  Future<Response<dynamic>> _futureStores;
  List<String> stores = [];
  List<Store> modelStores = [];

  @override
  void initState() {
    super.initState();
    _futureStores = MyApi.postDataWithAuthorization({
      widget.keyForApiCall: widget.dataForApiCall,
    }, "stores/search");
    initPlatformState();
    _determinePosition().then((position) {
      _userPosition = position;
    }).catchError(
      (locationError) {
        showMyAlertDialog(
            title: Text("Enable Location"),
            wdgtContent: Text("Location has to be enabled to Mark Attendance!"),
            fnFirst: () {
              dismissMyDialog();
              _determinePosition();
            },
            fnSecond: () {
              dismissMyDialog();
            },
            txtFirst: "Retry");
      },
    );
  }

  @override
  void dispose() {
    storesController.dispose();
    super.dispose();
  }

  Future<void> initPlatformState() async {
    String deviceModel;
    try {
      if (Platform.isAndroid) {
        // deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
        AndroidDeviceInfo androidDeviceInfo =
            await deviceInfoPlugin.androidInfo;
        deviceModel =
            "${androidDeviceInfo.manufacturer} ${androidDeviceInfo.model}";
        // print("$Login_Screen manufacturer = ${androidDeviceInfo.manufacturer}");
        // print("$Login_Screen model = ${androidDeviceInfo.model}");
      } else if (Platform.isIOS) {
        // deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
        IosDeviceInfo iosDeviceInfo = await deviceInfoPlugin.iosInfo;
        deviceModel = "${iosDeviceInfo.name} ${iosDeviceInfo.model}";
      }
    } on PlatformException {
      deviceModel = 'Failed to get device info.';
    }
    if (!mounted) return;
    setState(() {
      // _deviceData = deviceData;
      _deviceModel = deviceModel;
    });
  }

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      floatingActionButton: Builder(
        builder: (buildContext) => FloatingActionButton.extended(
          label: _image == null
              ? Text(
                  "Add a Photo",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                  ),
                )
              : Text(
                  "Mark Attendance",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                  ),
                ),
          onPressed: _image == null
              ? getImage
              :
              // _userPosition == null ? null :
              () => _markUserAttendance(buildContext),
          backgroundColor:
              //  _image != null && _userPosition == null ? Colors.grey :
              colorSecondary,
          icon: Icon(
            _image == null ? Icons.add_a_photo : Icons.check,
            color: Colors.white,
          ),
        ),
      ),
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          'Mark Attendance',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: Color(0xfff5f5f5),
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          padding: EdgeInsets.fromLTRB(
            subMargin,
            0,
            subMargin,
            subMargin,
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: subMargin),
                FutureBuilder<Response<dynamic>>(
                  future: _futureStores,
                  builder: (context, snapshot) {
                    if (snapshot.hasError) {
                      if (snapshot.error == SocketException ||
                          snapshot.error
                              .toString()
                              .contains("SocketException")) {
                        Future.delayed(Duration(milliseconds: 500))
                            .then((value) {
                          Scaffold.of(context).showSnackBar(createSnackBar());
                        });
                      }
                    } else if (snapshot.hasData) {
                      if (stores != null && stores.length != 0) {
                        stores.clear();
                        modelStores.clear();
                      }
                      snapshot.data.data["data"].forEach((element) {
                        // print("$Sales_Form $element ");
                        // print("$Sales_Form ${SalesProductSuggestion.fromJson(element).deviceVarient}");
                        stores.add(Store.fromJson(element).storeName);
                        modelStores.add(Store.fromJson(element));
                      });
                    }
                    return Form(
                      key: _formKey,
                      autovalidate: _autoValidate,
                      child: SimpleAutocompleteFormField<Store>(
                        controller: storesController,
                        validator: (value) {
                          if (storesController.text == null ||
                              storesController.text.isEmpty ||
                              storesController.text.length == 0) {
                            return "Please select a store!";
                          } else if (!stores.contains(storesController.text)) {
                            return "Select a store from the list only.";
                          } else if (value != null &&
                              !stores.contains(value.storeName)) {
                            return "Select a store from the list only.";
                          } else if (value != null) {
                            int tempIndex = modelStores.indexWhere((element) =>
                                element.storeName == value.storeName);
                            storeId = modelStores[tempIndex].id;
                          }
                          return null;
                        },
                        keyboardType: TextInputType.text,
                        style: TextStyle(
                          color: textPrimary,
                          fontWeight: FontWeight.w600,
                        ),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.grey[600],
                            ),
                          ),
                          labelText: 'Select Store',
                          // hintText: 'Enter the purchased device model',
                        ),
                        onChanged: (value) {
                          if (value == null) {
                            storeId = null;
                          } else if (value != null) {
                            if (value.storeName != null) {
                              storeId = value.id;
                            } else {
                              storeId = null;
                            }
                          }
                        },
                        itemToString: (item) =>
                            item == null ? "" : item.storeName,
                        onSearch: (search) async {
                          if ((stores.isEmpty || stores.length == 0) &&
                              snapshot.connectionState ==
                                  ConnectionState.done) {
                            if (!mounted) {
                              setState(() {
                                _futureStores =
                                    MyApi.postDataWithAuthorization({
                                  widget.keyForApiCall: widget.dataForApiCall,
                                }, "stores/search");
                              });
                            }
                          }
                          return modelStores
                              .where(
                                (element) =>
                                    element.storeName.toLowerCase().contains(
                                          search.toLowerCase(),
                                        ),
                              )
                              .toList();
                        },
                        itemBuilder: (context, item) {
                          return Padding(
                            padding: EdgeInsets.all(12),
                            child: Text(item.storeName),
                          );
                        },
                        maxSuggestions:
                            modelStores != null && modelStores.length != 0
                                ? modelStores.length
                                : 0,
                        suggestionsBuilder: (context, items) {
                          print("ManagerMarkAttendanceScreen Snap = $snapshot");
                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return Card(
                              elevation: cardElevation,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.circular(cardCornerRadius)),
                              color: Colors.white,
                              clipBehavior: Clip.antiAlias,
                              margin: EdgeInsets.symmetric(
                                // horizontal: subMargin,
                                vertical: subMarginHalf,
                              ),
                              child: Center(
                                child: Container(
                                  margin: EdgeInsets.all(subMargin),
                                  child: CircularProgressIndicator(),
                                ),
                              ),
                            );
                          } else if (snapshot.hasError) {
                            return Card(
                              elevation: cardElevation,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.circular(cardCornerRadius)),
                              color: Colors.white,
                              clipBehavior: Clip.antiAlias,
                              margin: EdgeInsets.symmetric(
                                // horizontal: subMargin,
                                vertical: subMarginHalf,
                              ),
                              child: Center(
                                child: Container(
                                  margin: EdgeInsets.all(subMargin),
                                  child: Text("Something Went Wrong!"),
                                ),
                              ),
                            );
                          }
                          return Card(
                            elevation: cardElevation,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.circular(cardCornerRadius)),
                            color: Colors.white,
                            clipBehavior: Clip.antiAlias,
                            margin: EdgeInsets.symmetric(
                              // horizontal: subMargin,
                              vertical: subMarginHalf,
                            ),
                            child: SizedBox(
                              width: double.infinity,
                              height: 200,
                              child: ListView(
                                children: items,
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  },
                ),
                SizedBox(height: mainMarginDouble),
                _image == null
                    ? Column(
                        children: [
                          Text('Click a clear photo of yourself.'),
                          SizedBox(height: subMarginDouble),
                          Lottie.asset(
                            "assets/lottie/profile_picture.json",
                            width: screenSize.width * .7,
                            fit: BoxFit.fitWidth,
                            repeat: true,
                            animate: true,
                          ),
                        ],
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.file(
                            _image,
                            width: screenSize.width,
                            height: screenSize.height / 2,
                          ),
                          FlatButton(
                            onPressed: () {
                              setState(() {
                                _image = null;
                              });
                            },
                            color: error,
                            child: Text(
                              "DELETE",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// Custom-Method to get the image from camera.
  Future<void> getImage() async {
    final PickedFile pickedFile = await picker.getImage(
      source: ImageSource.camera,
      imageQuality: 90,
      maxWidth: 700,
      maxHeight: 700,
    );
    if (pickedFile != null) {
      setState(() {
        _image = File(pickedFile.path);
      });
      print("Manager_Mark_Attendance_Screen Length = ${_image.lengthSync()}");
    } else {
      print('No image selected.');
    }
  }

  /// Custom-Method which use Geo-Locator package to fetch user location.
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      if (!await Geolocator.openLocationSettings()) {
        return Future.error('Location services are disabled.');
      }
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permantly denied, we cannot request permissions.');
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        return Future.error(
            'Location permissions are denied (actual value: $permission).');
      }
    }

    return await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
  }

  /// Custom-Method to mark the user's attendance (Endpoint => attendance/mark-attendance)
  void _markUserAttendance(BuildContext context, {String reason}) async {
    try {
      if (_userPosition == null) {
        _determinePosition().then((position) {
          _userPosition = position;
        }).catchError((locationError) {
          showMyAlertDialog(
              title: Text("Enable Location"),
              wdgtContent:
                  Text("Location has to be enabled to Mark Attendance!"),
              fnFirst: () {
                dismissMyDialog();
                _determinePosition();
              },
              fnSecond: () {
                dismissMyDialog();
              },
              txtFirst: "Retry");
        });
        return;
      }
      if (!_formKey.currentState.validate() ||
          storeId == null ||
          storeId.isEmpty) {
        print("debug ManagerMarkAttendanceScreen inside If Statement");
        setState(() {
          _autoValidate = true;
        });
        return;
      }
      // Show Dialog while the user's attendance is being registered.
      showMyLoadingDialog(
          dialogText: "Marking your attendance...\nPlease Wait!");

      Map<String, dynamic> data = {
        "photo": await MultipartFile.fromFile(
          _image.path,
          filename: basename(_image.path),
          contentType: MediaType('image', "png"),
        ),
        "type": "daily_visits",
        "latitude": _userPosition.latitude,
        "longitude": _userPosition.longitude,
        "device_model": _deviceModel,
        "date": DateTime.now().toIso8601String(),
        "store_id": storeId,
        // "is_visit_outside_pjp": false,
      };
      FormData formData = FormData.fromMap(data);
      Response response = await MyApi.postFormDataWithAuthorization(
          formData, "attendance/mark-attendance");
      print("Manager_Mark_Attendance_Screen Response = ${response.data}");
      if (response.data['code'] == 401 || response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.data['code'] == 200) {
        // User's attendance is marked successfully
        dismissMyDialog();
        Navigator.pop(context, response.data["message"]);
      } else {
        dismissMyDialog();
        Scaffold.of(context)
            .showSnackBar(createSnackBar(msg: "Something Went Wrong!"));
        throw Future.error("Manager_Mark_Attendance_Screen Error from Future");
      }
    } on SocketException {
      dismissMyDialog();
      print("Manager_Mark_Attendance_Screen Exception = $error");
      Scaffold.of(context).showSnackBar(createSnackBar());
    } catch (error) {
      dismissMyDialog();
      print("Manager_Mark_Attendance_Screen Exception = $error");
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        } else if (error.response.statusCode == 422) {
          Scaffold.of(context).showSnackBar(
            createSnackBar(msg: error.response.data["message"]),
          );

          return Future.error(HttpException("422"));
        }
      }
      print("Manager_Mark_Attendance_Screen Exception = $error");
      Scaffold.of(context).showSnackBar(
        createSnackBar(msg: "Something Went Wrong!"),
      );
      return Future.error(error);
    }
  }
}
