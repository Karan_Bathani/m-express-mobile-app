import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/training-material-card.dart';
import 'package:market_express/Models/training-material.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TrainingMaterialScreen extends StatefulWidget {
  @override
  _TrainingMaterialScreenState createState() => _TrainingMaterialScreenState();
}

class _TrainingMaterialScreenState extends State<TrainingMaterialScreen> {
  Future<List<TrainingMaterial>> _futureTrainingMaterial;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _futureTrainingMaterial = _getTrainingMaterials();
  }

  Size screenSize;

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          "Training Materials",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: greyBackground,
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          child: RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () {
              setState(() {
                _futureTrainingMaterial = _getTrainingMaterials();
              });
              return _futureTrainingMaterial;
            },
            child: FutureBuilder<List<TrainingMaterial>>(
              future: _futureTrainingMaterial,
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  if (snapshot.error.toString().contains("404")) {
                    return buildEmptyWidget(screenSize,
                        text: "No Training Material Found!");
                  }
                  return buildErrorWidget(screenSize);
                }
                if (snapshot.hasData &&
                    snapshot.connectionState == ConnectionState.done &&
                    snapshot.data.length != 0) {
                  return _buildTrainingMaterialListView(snapshot.data);
                } else if (snapshot.connectionState ==
                    ConnectionState.waiting) {
                  _refreshIndicatorKey.currentState?.show();
                  return _buildTrainingMaterialListView(snapshot.data);
                }
                return SizedBox.expand(
                  child: ListView(),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTrainingMaterialListView(
      List<TrainingMaterial> trainingMaterials) {
    if (trainingMaterials != null && trainingMaterials.isEmpty) {
      return buildEmptyWidget(screenSize, text: "No Training Material Found!");
    }
    return ListView.builder(
      itemCount: trainingMaterials != null ? trainingMaterials.length : 0,
      itemBuilder: (context, index) =>
          TrainingMaterialCard(trainingMaterials[index]),
    );
  }

  Future<List<TrainingMaterial>> _getTrainingMaterials() async {
    List<TrainingMaterial> trainingMaterials = [];
    try {
      Response response =
          await MyApi.getDataWithAuthorization("training/get-material");
      print("$TrainingMaterial_Screen Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var trainingMaterial in response.data["message"]) {
          trainingMaterials.add(TrainingMaterial.fromJson(trainingMaterial));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("$TrainingMaterial_Screen SocketException");
      return Future.error(SocketException);
    } catch (error) {
      print("$TrainingMaterial_Screen Exception = $error");
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }

      return Future.error(error);
    }
    return trainingMaterials;
  }
}
