import 'package:flutter/material.dart';
import 'package:market_express/Providers/leave-provider.dart';
import 'package:market_express/Screens/LeaveTabs/approved-tab.dart';
import 'package:market_express/Screens/LeaveTabs/pending-tab.dart';
import 'package:market_express/Screens/LeaveTabs/rejected-tab.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:provider/provider.dart';

class LeaveApprovalScreen extends StatefulWidget {
  @override
  _LeaveApprovalScreenState createState() => _LeaveApprovalScreenState();
}

class _LeaveApprovalScreenState extends State<LeaveApprovalScreen> {
  Size screenSize;
  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return DefaultTabController(
      initialIndex: 0,
      length: 3,
      child: Scaffold(
        backgroundColor: colorPrimary,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: Text(
            'Leave(s) for Approval',
            style: TextStyle(color: Colors.white),
          ),
          bottom: TabBar(
            indicatorColor: colorSecondary,
            indicatorSize: TabBarIndicatorSize.label,
            indicatorWeight: 4,
            tabs: [
              Tab(
                text:
                    "PENDING ${Provider.of<LeaveProvider>(context).pendingleavesByDesignation.length}",
              ),
              Tab(
                text:
                    "APPROVED ${Provider.of<LeaveProvider>(context).approvedleavesByDesignation.length}",
              ),
              Tab(
                text:
                    "REJECTED ${Provider.of<LeaveProvider>(context).rejectedleavesByDesignation.length}",
              ),
            ],
          ),
        ),
        body: SafeArea(
          child: Container(
            clipBehavior: Clip.antiAlias,
            width: screenSize.width,
            height: screenSize.height,
            decoration: BoxDecoration(
              color: greyBackground,
              borderRadius:
                  BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
            ),
            child: TabBarView(children: [
              PendingTab(),
              ApprovedTab(),
              RejectedTab(),
            ]),
          ),
        ),
      ),
    );
  }
}
