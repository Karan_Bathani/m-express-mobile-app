import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:intl/intl.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/inventory-card.dart';
import 'package:market_express/Models/inventory-transaction.dart';
import 'package:market_express/Screens/Forms/inventory-form.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InventoryTransactionScreen extends StatefulWidget {
  final String message;
  InventoryTransactionScreen({this.message});
  @override
  _InventoryTransactionScreenState createState() =>
      _InventoryTransactionScreenState();
}

class _InventoryTransactionScreenState
    extends State<InventoryTransactionScreen> {
  Future<List<InventoryTransaction>> _futureInventoryTransaction;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    if (widget.message != null)
      WidgetsBinding.instance.addPostFrameCallback((_) {
        // executes after build
        showToast(
          widget.message,
          context: context,
          animation: StyledToastAnimation.slideFromBottom,
          reverseAnimation: StyledToastAnimation.slideToBottom,
          startOffset: Offset(0.0, 3.0),
          reverseEndOffset: Offset(0.0, 3.0),
          position: StyledToastPosition.bottom,
          duration: Duration(seconds: 3),
          animDuration: Duration(seconds: 1),
          curve: Curves.elasticOut,
          backgroundColor: success,
          reverseCurve: Curves.fastOutSlowIn,
        );
      });
    _selectedDateTime = todaysDateTime;
    _futureInventoryTransaction = _getInventoryTransactions();
  }

  DateTime todaysDateTime = DateTime.now();
  DateTime _selectedDateTime;
  Size screenSize;
  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          'Inventory Transactions',
          style: TextStyle(color: Colors.white),
        ),
      ),
      floatingActionButton: FutureBuilder<SharedPreferences>(
        future: SharedPreferences.getInstance(),
        builder: (buildContext, snapshot) {
          if (snapshot.hasData &&
              snapshot.data.getString("user_type") != "SEC") {
            return SizedBox.shrink();
          }
          return FloatingActionButton(
            backgroundColor: colorPrimary,
            child: Icon(Icons.add),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => InventoryForm(),
                ),
              ).then(
                (value) {
                  if (value != null) {
                    Scaffold.of(context).showSnackBar(
                        createSnackBar(msg: value, isSuccess: true));
                    setState(() {
                      _futureInventoryTransaction = _getInventoryTransactions();
                    });
                  }
                },
              );
            },
          );
        },
      ),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: greyBackground,
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          child: RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () {
              setState(() {
                _futureInventoryTransaction = _getInventoryTransactions();
              });
              return _futureInventoryTransaction;
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
// Date Selector
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: subMargin,
                  ),
                  color: dropDownBackground,
                  child: DropdownButton(
                    underline: SizedBox.shrink(),
                    iconSize: 30,
                    isExpanded: true,
                    hint: Text(
                      DateFormat("MMMM, yyy").format(_selectedDateTime),
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    items: List.generate(10, (index) {
                      DateTime generatedDate = DateTime(
                          todaysDateTime.year, todaysDateTime.month - index);
                      return DropdownMenuItem(
                        child:
                            Text(DateFormat("MMMM, yyy").format(generatedDate)),
                        value: generatedDate,
                      );
                    }),
                    onChanged: (value) {
                      setState(() {
                        _selectedDateTime = value;
                        _futureInventoryTransaction =
                            _getInventoryTransactions();
                      });
                    },
                  ),
                ),
                FutureBuilder<List<InventoryTransaction>>(
                  future: _futureInventoryTransaction,
                  builder: (context, snapshot) {
                    print(
                        "$Inventory_Screen FutureBuilder SNapshot = ${snapshot.data}");
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      _refreshIndicatorKey.currentState?.show();
                      return _buildInventoryTransactionsListView(snapshot.data);
                    } else if (snapshot.hasError) {
                      if (snapshot.error.toString().contains("404")) {
                        return buildEmptyWidget(screenSize,
                            text: "No Inventory-Transaction Found!");
                      }
                      return buildErrorWidget(screenSize);
                    } else if (snapshot.hasData &&
                        snapshot.connectionState == ConnectionState.done &&
                        snapshot.data.length != 0) {
                      return _buildInventoryTransactionsListView(snapshot.data);
                    }
                    return SizedBox.expand(
                      child: ListView(),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildInventoryTransactionsListView(
      List<InventoryTransaction> inventoryTransactions) {
    return Expanded(
      child: ListView.builder(
        itemCount:
            inventoryTransactions != null ? inventoryTransactions.length : 0,
        itemBuilder: (context, index) =>
            InventoryCard(inventoryTransactions[index]),
      ),
    );
  }

  Future<List<InventoryTransaction>> _getInventoryTransactions() async {
    List<InventoryTransaction> inventoryTransactions = [];
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      String userType = preferences.getString("user_type");

      Response response = await MyApi.postDataWithAuthorization({
        "month": _selectedDateTime.month,
        "year": _selectedDateTime.year,
        "supervisor_mode": !(userType == "SEC"),
      }, "stores/inventory-list");
      print("$Inventory_Screen Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var inventory in response.data["data"]) {
          inventoryTransactions.add(InventoryTransaction.fromJson(inventory));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("$Inventory_Screen SocketException");
      return Future.error(SocketException);
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        }
      }
      if (error.response.statusCode == 404) {
        return Future.error("404");
      }
      print("$Inventory_Screen Exception = $error");
      return Future.error(error);
    }
    return inventoryTransactions;
  }
}
