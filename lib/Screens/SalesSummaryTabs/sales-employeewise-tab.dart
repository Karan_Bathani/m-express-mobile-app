import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/sales-summary-card.dart';
import 'package:market_express/Models/sales-summary.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SalesEmployeeWiseTab extends StatefulWidget {
  @override
  _SalesEmployeeWiseTabState createState() => _SalesEmployeeWiseTabState();
}

class _SalesEmployeeWiseTabState extends State<SalesEmployeeWiseTab>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  Future<List<SalesSummary>> _futureEmployeeWiseSalesSummary;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _selectedDateTime = todaysDateTime;
    _futureEmployeeWiseSalesSummary = _getEmployeeWiseSalesSummary();
  }

  DateTime todaysDateTime = DateTime.now();
  DateTime _selectedDateTime;
  Size screenSize;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    screenSize = MediaQuery.of(context).size;
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: () {
        setState(() {
          _futureEmployeeWiseSalesSummary = _getEmployeeWiseSalesSummary();
        });
        return _futureEmployeeWiseSalesSummary;
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
//> Date Selector
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: subMargin,
            ),
            color: dropDownBackground,
            child: DropdownButton(
              underline: SizedBox.shrink(),
              iconSize: 30,
              isExpanded: true,
              hint: Text(
                DateFormat("MMMM, yyy").format(_selectedDateTime),
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w600,
                ),
              ),
              items: List.generate(10, (index) {
                DateTime generatedDate =
                    DateTime(todaysDateTime.year, todaysDateTime.month - index);
                return DropdownMenuItem(
                  child: Text(DateFormat("MMMM, yyy").format(generatedDate)),
                  value: generatedDate,
                );
              }),
              onChanged: (value) {
                setState(() {
                  _selectedDateTime = value;
                  _futureEmployeeWiseSalesSummary =
                      _getEmployeeWiseSalesSummary();
                });
              },
            ),
          ),
          FutureBuilder<List<SalesSummary>>(
            future: _futureEmployeeWiseSalesSummary,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                _refreshIndicatorKey.currentState?.show();
                return _buildEmployeeWiseSalesSummaryList(snapshot.data);
              } else if (snapshot.hasError) {
                if (snapshot.error.toString().contains("404")) {
                  return buildEmptyWidget(screenSize,
                      text: "No Sales Summary Found");
                }
                return buildErrorWidget(screenSize);
              } else if (snapshot.hasData &&
                  snapshot.connectionState == ConnectionState.done &&
                  snapshot.data.length != 0) {
                return _buildEmployeeWiseSalesSummaryList(snapshot.data);
              }
              return Expanded(
                child: ListView(),
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _buildEmployeeWiseSalesSummaryList(
      List<SalesSummary> employeeWiseSalesSummarries) {
    return Expanded(
      child: ListView.builder(
        itemCount: employeeWiseSalesSummarries != null
            ? employeeWiseSalesSummarries.length
            : 0,
        itemBuilder: (context, index) => SalesSummaryCard(
          employeeWiseSalesSummarries[index],
          _selectedDateTime,
        ),
      ),
    );
  }

  Future<List<SalesSummary>> _getEmployeeWiseSalesSummary() async {
    List<SalesSummary> employeeWiseSalesSummarries = [];
    try {
      Response response = await MyApi.postDataWithAuthorization({
        "month": _selectedDateTime.month,
        "year": _selectedDateTime.year,
        "summary_for": "sales",
      }, "general-summary");
      print("$SalesEmployeeWise_Tab Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var attendanceSummary in response.data["data"]) {
          employeeWiseSalesSummarries
              .add(SalesSummary.fromJson(attendanceSummary));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("$SalesEmployeeWise_Tab SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.clear();
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      print("$SalesEmployeeWise_Tab Exception = $error");
      throw Future.error(error);
    }
    return employeeWiseSalesSummarries;
  }
}
