import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/sales-card.dart';
import 'package:market_express/Models/sales.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SalesOverAllTab extends StatefulWidget {
  @override
  _SalesOverAllTabState createState() => _SalesOverAllTabState();
}

class _SalesOverAllTabState extends State<SalesOverAllTab>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  Future<List<Sales>> _futureSales;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _selectedDateTime = DateTime(
        todaysDateTime.year, todaysDateTime.month + 1, todaysDateTime.day);
    _futureSales = _getUserSalesData();
  }

  DateTime todaysDateTime = DateTime.now();
  DateTime _selectedDateTime;
  Size screenSize;
  @override
  Widget build(BuildContext context) {
    super.build(context);
    screenSize = MediaQuery.of(context).size;
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: () {
        setState(() {
          _futureSales = _getUserSalesData();
        });
        return _futureSales;
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
//> Date Selector
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: subMargin,
            ),
            decoration: BoxDecoration(
              // borderRadius: BorderRadius.vertical(
              //   top: Radius.circular(cardCornerRadius),
              // ),
              color: dropDownBackground,
            ),
            child: DropdownButton(
              underline: SizedBox.shrink(),
              iconSize: 30,
              isExpanded: true,
              hint: (_selectedDateTime.month ==
                      DateTime(todaysDateTime.year, todaysDateTime.month + 1)
                          .month)
                  ? Text(
                      "All",
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w600,
                      ),
                    )
                  : Text(
                      DateFormat("MMMM, yyy").format(_selectedDateTime),
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
              items: List.generate(11, (index) {
                DateTime generatedDate = DateTime(
                    todaysDateTime.year, todaysDateTime.month - index + 1);
                return DropdownMenuItem(
                  child: index == 0
                      ? Text("All")
                      : Text(DateFormat("MMMM, yyy").format(generatedDate)),
                  value: generatedDate,
                );
              }),
              onChanged: (value) {
                setState(() {
                  _selectedDateTime = value;
                  _futureSales = _getUserSalesData();
                });
              },
            ),
          ),
          FutureBuilder<List<Sales>>(
            future: _futureSales,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                _refreshIndicatorKey.currentState?.show();
                return _buildSalesListView(snapshot.data);
              } else if (snapshot.hasError) {
                if (snapshot.error.toString().contains("404")) {
                  return buildEmptyWidget(screenSize, text: "No Sales Found!");
                }
                return buildErrorWidget(screenSize);
              } else if (snapshot.hasData &&
                  snapshot.connectionState == ConnectionState.done &&
                  snapshot.data.length != 0) {
                return _buildSalesListView(snapshot.data);
              }
              return SizedBox.expand(
                child: ListView(),
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _buildSalesListView(List<Sales> sales) {
    return Expanded(
      child: ListView.builder(
        itemCount: sales != null ? sales.length : 0,
        itemBuilder: (context, index) => SalesCard(sales[index]),
      ),
    );
  }

  Future<List<Sales>> _getUserSalesData() async {
    List<Sales> sales = [];
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      String userType = preferences.getString("user_type");
      Map<String, dynamic> data = {
        "supervisor_mode": !(userType == "SEC"),
      };
      if (_selectedDateTime.month !=
          DateTime(todaysDateTime.year, todaysDateTime.month + 1).month) {
        data.addAll({
          "month": _selectedDateTime.month,
          "year": _selectedDateTime.year,
        });
      }
      Response response =
          await MyApi.postDataWithAuthorization(data, "stores/sales-list");
      print("$SalesOverAll_Tab Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var sale in response.data["data"]) {
          sales.add(Sales.fromJson(sale));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("$SalesOverAll_Tab SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      print("$SalesOverAll_Tab Exception = $error");
      return Future.error(error);
    }
    return sales;
  }
}
