import 'package:flutter/material.dart';
import 'package:market_express/Components/List-Items/leave-by-designation-card.dart';
import 'package:market_express/Models/leave-by-designation.dart';
import 'package:market_express/Providers/leave-provider.dart';
import 'package:provider/provider.dart';

class RejectedTab extends StatefulWidget {
  @override
  _RejectedTabState createState() => _RejectedTabState();
}

class _RejectedTabState extends State<RejectedTab> {
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Consumer<LeaveProvider>(
        builder: (context, leaveProvider, child) {
          return RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: leaveProvider.getLeavesByDesignation,
            child: Builder(
              builder: (context) {
                if (leaveProvider.rejectedleavesByDesignation != null) {
                  if (leaveProvider.rejectedleavesByDesignation.length != 0) {
                    return _buildLeavesByDesignationListView(
                        leaveProvider.rejectedleavesByDesignation);
                  } else {
                    // ! TODO: Use a lottie or something UI attractive instead
                    return SizedBox.expand(
                      child: ListView(
                        children: [
                          SizedBox(
                              height: MediaQuery.of(context).size.height / 3),
                          Center(child: Text('No Leave(s) found!')),
                        ],
                      ),
                    );
                  }
                }
                // else if (snapshot.connectionState ==
                //     ConnectionState.waiting) {
                //   _refreshIndicatorKey.currentState?.show();
                //   return _buildLeavesByDesignationListView(snapshot.data);
                // } else if (snapshot.hasError) {
                //   return Text("Something went Wrong!");
                // }

                _refreshIndicatorKey.currentState.show();
                return SizedBox.expand(
                  child: ListView(),
                );
              },
            ),
          );
        },
      ),
    );
  }

  Widget _buildLeavesByDesignationListView(
      List<LeaveByDesignation> leavesByDesignation) {
    return ListView.builder(
      itemCount: leavesByDesignation != null ? leavesByDesignation.length : 0,
      itemBuilder: (context, index) =>
          LeaveByDesignationCard(leavesByDesignation[index]),
    );
  }
}
