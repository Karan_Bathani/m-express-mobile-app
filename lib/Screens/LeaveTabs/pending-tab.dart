import 'package:flutter/material.dart';
import 'package:market_express/Components/List-Items/leave-by-designation-card.dart';
import 'package:market_express/Models/leave-by-designation.dart';
import 'package:market_express/Providers/leave-provider.dart';
import 'package:provider/provider.dart';

class PendingTab extends StatefulWidget {
  @override
  _PendingTabState createState() => _PendingTabState();
}

class _PendingTabState extends State<PendingTab> {
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Consumer<LeaveProvider>(
        builder: (context, leaveProvider, child) {
          // leaveProvider.getLeavesByDesignation();
          return RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: leaveProvider.getLeavesByDesignation,
            child: Builder(
              builder: (context) {
                if (leaveProvider.pendingleavesByDesignation != null) {
                  if (leaveProvider.pendingleavesByDesignation.length != 0) {
                    return _buildLeavesByDesignationListView(
                        leaveProvider.pendingleavesByDesignation);
                  } else {
                    // ! TODO: Use a lottie or something UI attractive instead
                    return SizedBox.expand(
                      child: ListView(
                        children: [
                          SizedBox(
                              height: MediaQuery.of(context).size.height / 3),
                          Center(child: Text('No Leave(s) found!')),
                        ],
                      ),
                    );
                  }
                }
                // else if (snapshot.connectionState ==
                //     ConnectionState.waiting) {
                //   _refreshIndicatorKey.currentState?.show();
                //   return _buildLeavesByDesignationListView(snapshot.data);
                // } else if (snapshot.hasError) {
                //   return Text("Something went Wrong!");
                // }

                _refreshIndicatorKey.currentState.show();
                return SizedBox.expand(
                  child: ListView(),
                );
              },
            ),
          );
        },
      ),
    );
  }

  Widget _buildLeavesByDesignationListView(
      List<LeaveByDesignation> leavesByDesignation) {
    return ListView.builder(
      itemCount: leavesByDesignation != null ? leavesByDesignation.length : 0,
      itemBuilder: (context, index) =>
          LeaveByDesignationCard(leavesByDesignation[index]),
    );
  }

  // void _rebuildCallBack() {
  //   setState(() {
  //     _futureLeavesByDesignation = _getLeavesByDesignation();
  //   });
  // }

//   Future<List<LeaveByDesignation>> _getLeavesByDesignation() async {
//     List<LeaveByDesignation> leavesByDesignation = [];
//     try {
//       Response response = await MyApi.postDataWithAuthorization({
//         "status": [widget.leaveStatus],
//       }, "users/leave-history-by-designation");
//       print("$Leave_Tab Response = $response");
//       if (response.statusCode == 401) {
// //Clear the localStorage
//         SharedPreferences.getInstance().then((localStorage) {
//           localStorage.remove('token');
//         });

//         Navigator.pushAndRemoveUntil(
//             context,
//             MaterialPageRoute(
//               builder: (context) => LoginScreen(),
//             ),
//             (route) => false);
//       } else if (response.statusCode == 200) {
//         for (var leaveByDesignation in response.data["data"]) {
//           leavesByDesignation.add(LeaveByDesignation.fromJson(leaveByDesignation));
//         }
//       } else {
//         throw Future.error("Error from Future");
//       }
//     } on SocketException {
//       print("$Leave_Tab SocketException");
//       throw Future.error(SocketException);
//     } catch (error) {
//       if (error is DioError) {
//         if (error.response.statusCode == 401) {
// //Clear the localStorage
//           SharedPreferences.getInstance().then((localStorage) {
//             localStorage.remove('token');
//           });

//           Navigator.pushAndRemoveUntil(
//               context,
//               MaterialPageRoute(
//                 builder: (context) => LoginScreen(),
//               ),
//               (route) => false);
//         }
//       }
//       print("$Leave_Tab Exception = $error");
//       throw Future.error(error);
//     }
//     return leavesByDesignation;
//   }

}
