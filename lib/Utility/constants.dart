import 'package:flutter/cupertino.dart';

// COLORS
final Color colorPrimary = Color(0xff1f62af);
final Color colorPrimaryLight = Color(0xff4099ff);
final Color colorSecondary = Color(0xff3acf8e);
final Color colorSecondaryLight = Color(0xff40ff80);

final Color myGreyColor = Color(0xffbfbfbf);
final Color myRed = Color(0xffc9202c);
final Color greyBackground = Color(0xfff5f5f5);
final Color dropDownBackground = Color(0xffdddddd);
final Color myYellow = Color(0xffffbf40);

final Color textPrimary = Color(0xFF25262a);
final Color textSecondary = Color(0xff818181);
// final Color textSecondary = Color(0xFF616060);
final Color textTertiary = Color(0xFF92b0ce);

final Color separator = Color(0xFF92b0ce);

final Color success = Color(0xFF0CCE6B);
final Color error = Color(0xFFff4651);
final Color pending = Color(0xFFFF7F00);

final Color successLight = Color(0xFFd5f7e1);
final Color successShadow = Color(0x660CCE6B);
final Color errorLight = Color(0xFFffdcde);
final Color errorShadow = Color(0x66ff4651);
final Color pendingLight = Color(0xFFffe7cf);

/// Colors for cards
final List<Color> randomColors = [
  Color(0xffe57373),
  Color(0xff7986CB),
  Color(0xff81C784),
  Color(0xffFF8A65),
  Color(0xffFFB74D),
  Color(0xff4DD0E1),
];

// SPACINGS AND SIZES
final double mainMargin = 20;
final double mainMarginHalf = 10;
final double mainMarginDouble = 40;

final double subMargin = 12;
final double subMarginHalf = 6;
final double subMarginDouble = 24;

final double cardElevation = 3;
final double miniCardCornerRadius = 4;
final double cardCornerRadius = 12;
final double cardContentPadding = 12;
