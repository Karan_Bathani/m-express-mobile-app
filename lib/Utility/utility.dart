import 'dart:math';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:market_express/Utility/constants.dart';

final GlobalKey<NavigatorState> _navigatorKey = GlobalKey<NavigatorState>();
GlobalKey<NavigatorState> get getNaviatorKey => _navigatorKey;

/// Custom-Method to show Loading Dialog
void showMyLoadingDialog(
    {String dialogText = "Loading...\nPlease Wait!", bool showLoader = true}) {
  showDialog(
      context: _navigatorKey.currentState.overlay.context,
      barrierDismissible: false,
      barrierColor: Colors.black.withOpacity(.3),
      builder: (context) {
        return WillPopScope(
          onWillPop: () async {
            return false;
          },
          child: Dialog(
            child: Container(
              padding: EdgeInsets.symmetric(
                  horizontal: cardContentPadding,
                  vertical: cardContentPadding * 2),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  showLoader ? CircularProgressIndicator() : SizedBox.shrink(),
                  SizedBox(height: cardContentPadding * 2),
                  Text(
                    dialogText,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
        );
      });
}

/// Custom-Method to show an Alert Dialog
void showMyAlertDialog({
  @required Widget title,
  @required Widget wdgtContent,
  @required Function fnFirst,
  @required Function fnSecond,
  @required String txtFirst,
  String txtSecond = 'CANCEL',
  bool isDismissible = false,
}) {
  showDialog(
      context: _navigatorKey.currentState.overlay.context,
      barrierDismissible: isDismissible,
      barrierColor: Colors.black.withOpacity(.3),
      builder: (context) {
        return WillPopScope(
          onWillPop: () async {
            return true;
          },
          child: AlertDialog(
            title: title,
            content: wdgtContent,
            actions: [
              FlatButton(
                  child: Text(
                    txtSecond.toUpperCase(),
                    style: TextStyle(color: textSecondary),
                  ),
                  onPressed: fnSecond),
              FlatButton(
                  child: Text(
                    txtFirst.toUpperCase(),
                    style: TextStyle(
                      color: Colors.red,
                    ),
                  ),
                  onPressed: fnFirst),
            ],
          ),
        );
      });
}

/// Custom-Method to dismiss dialog
void dismissMyDialog() {
  Navigator.of(_navigatorKey.currentState.overlay.context).pop();
}

/// Custom-Method to validate Email
bool validateEmail(String value) {
  Pattern pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = new RegExp(pattern);
  return (!regex.hasMatch(value)) ? false : true;
}

/// Custom-Method to get Random-Colors for the cards
Color getRandomColor() {
  return randomColors[Random().nextInt(6)];
}

/// Custom-Method to create a SnackBar Widget to be used in Scaffold.
SnackBar createSnackBar({
  String msg = "You are offline, Check your Internet Connection!",
  String label = 'close',
  bool isSuccess = false,

  /// Boolean to decide the color of SnackBar
  Function onPressed,
}) {
  return SnackBar(
    content: Text(msg),
    behavior: SnackBarBehavior.floating,
    backgroundColor: isSuccess ? success : error,
    // shape: RoundedRectangleBorder(
    //     borderRadius: BorderRadius.circular(cardCornerRadius)),
    action: SnackBarAction(
      label: label,
      textColor: Colors.white,
      onPressed: () => onPressed,
    ),
  );
}

/// Custom-Method to show lottie for empty-data
Widget buildEmptyWidget(Size screenSize, {String text}) {
  return SingleChildScrollView(
    // height: screenSize.height - kToolbarHeight - subMarginDouble * 5,
    physics: AlwaysScrollableScrollPhysics(),
    child: Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(vertical: mainMargin),
          child: Lottie.asset(
            "assets/lottie/empty.json",
            width: screenSize.width * .7,
            fit: BoxFit.fitWidth,
            repeat: true,
            animate: true,
          ),
        ),
        text != null
            ? Center(
                child: Text(
                  text,
                ),
              )
            : SizedBox.shrink(),
      ],
    ),
  );
}

/// Custom-Method to show lottie for error
Widget buildErrorWidget(Size screenSize,
    {String text = "Something Went Wrong!"}) {
  return SingleChildScrollView(
    // height: screenSize.height - kToolbarHeight - subMarginDouble * 5,
    physics: AlwaysScrollableScrollPhysics(),
    child: Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(vertical: mainMargin),
          child: Lottie.asset(
            "assets/lottie/error.json",
            width: screenSize.width * .7,
            fit: BoxFit.fitWidth,
            repeat: true,
            animate: true,
          ),
        ),
        Center(
          child: Text(
            text,
          ),
        )
      ],
    ),
  );
}

/// Returns a chip like card
Widget createChip(String text, {Color backgroundColor}) {
  switch (text.toLowerCase()) {
    case "install":
      backgroundColor = Color(0xff04cc26);
      break;
    case "removal":
      backgroundColor = Color(0xffef5350);
      break;
    case "replace":
      backgroundColor = Color(0xffab875d);
      break;
    case "infra":
      backgroundColor = Color(0xff04bfcc);
      break;
    default:
      backgroundColor = myGreyColor;
      break;
  }
  return Card(
    color: backgroundColor ?? myGreyColor,
    child: Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 2.0,
        horizontal: 4,
      ),
      child: Text(
        text,
        style: TextStyle(
          fontSize: 11,
          fontWeight: FontWeight.w500,
          color: Colors.white,
        ),
      ),
    ),
  );
}

extension MyDateTimeExtension on DateTime {
  /// Custom made extension to format Date from ISO string to dd/MM/yy
  String formatDate({String pattern = "dd/MM/yy"}) {
    return "${DateFormat(pattern).format(this)}";
  }
}

extension ListExtension<E> on List<E> {
  void addAllUnique(Iterable<E> iterable) {
    for (var element in iterable) {
      if (!contains(element)) {
        add(element);
      }
    }
  }
}

/// String variable to be used for printing in debug console.
const String Home_Screen = "debug HomeScreen ";
const String Login_Screen = "debug LoginScreen ";
const String My_Api = "debug MyApi ";
const String Mark_Attendance_Screen = "debug MarkAttendanceScreen ";
const String Leave_Form = "debug Leave-Form ";
const String AttendanceHistory_Screen = "debug AttendanceHistoryScreen ";
const String LeaveHistory_Screen = "debug LeaveHistoryScreen ";
const String My_DatePicker = "debug MyDatePicker ";
const String Leave_Approval_Screen = "debug LeaveApprovalScreen ";
const String Leave_By_Designation_Card = "debug LeaveApprovalCard ";
const String Pending_Tab = "debug PendingTab ";
const String Leave_Provider = "debug LeaveProvider ";
const String Survey_Form = "debug SurveyForm ";
const String Sales_Form = "debug SalesForm ";
const String Sales_Screen = "debug SalesScreen ";
const String Inventory_Form = "debug InventoryForm ";
const String Inventory_Screen = "debug InventoryScreen ";
const String AttendanceSummary_Screen = "debug AttendanceSummaryScreen ";
const String LeaveSummary_Screen = "debug LeaveSummaryScreen ";
const String SalesOverAll_Tab = "debug SalesOverAllTab ";
const String SalesEmployeeWise_Tab = "debug SlaesEmployeeWiseTab ";
const String InventoryStatus_Screen = "debug InventoryStatusScreen ";
const String AttendanceHistory_Tab = "debug AttendanceHistoryTab ";
const String CurrentMonthAttendance_Tab = "debug CurrentMonthAttendanceTab ";
const String SurveySummary_Screen = "debug SurveySummaryScreen ";
const String SurveyListing_Screen = "debug SurveyListingScreen ";
const String FOEAttendanceSummary_Screen = "debug FOEAttendanceSummaryScreen ";
const String TargetVsAchievementSummary_Screen =
    "debug TargetVsAchievementSummaryScreen ";
const String WorkplaceFeedbackSurvey_Form =
    "debug WorkplaceFeedbackSurveyForm ";
const String FeedbackSurveyDropdown_Widget =
    "debug FeedbackSurveyDropdownWidget ";
const String TrainingMaterial_Screen = "debug TrainingMaterialScreen ";
const String TrainingMaterial_Card = "debug TrainingMaterialCard ";
const String Models_Tab = "debug ModelsTab ";
const String OHSStatus_Screen = "debug OHSStatusScreen ";
const String TrainingMaterialSummary_Screen =
    "debug TrainingMaterialSummaryScreen ";
