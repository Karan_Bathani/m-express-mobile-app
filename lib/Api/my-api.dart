import 'dart:io';

import 'package:dio/dio.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

//> CE OPS
// TSO ||     Md. Anowar Hossain => 19050154, PASSWORD => 83758620
//* Store => 602a2acd6a40cb64fca7310e NAME => FCL-Bogra, Bogura
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX3R5cGUiOiJUU08iLCJ1c2VyX2lkIjoiNjAyYTJlM2JhN2ZjZjY0ZTY0YjMwOWE4Iiwic2Vzc2lvbl9pZCI6IjRhYzNmNzE3LTYxMTctNDFhYi1hZWFkLWEyNzY4MDU3MjljMiIsImlhdCI6MTYxNjQ3NzE0Nn0.Y_v5LF24Kay50onItqVPeMntP7rX5Us4M9Ptkrzx0h8

// AM ||    Khalid Shamsh Roni => 19040130, PASSWORD => 66590266
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX3R5cGUiOiJBcmVhIE1hbmFnZXIiLCJ1c2VyX2lkIjoiNjAyYTJlM2JhN2ZjZjY0ZTY0YjMwOWE0Iiwic2Vzc2lvbl9pZCI6IjJlM2RkYjBiLTc5ZmYtNDViYy1hNDY1LWU0ZTU0OWFiZmIyMiIsImlhdCI6MTYxNjQ3NzM3NX0.r2MxspPFroQ1aZY8Waeu7_lsmv0OoIB8sgN9amTtWaU

// SOM ||    Malaya Saha => 17011001, PASSWORD => 38511874
//

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//> VM OPS
// VM ||     Md. Mahbubul Alam => SVM019, PASSWORD => 91499828
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX3R5cGUiOiJWTSIsInVzZXJfaWQiOiI2MDJhMmQ2M2E3ZmNmNjRlNjRiMzA5MjEiLCJzZXNzaW9uX2lkIjoiOTcwNzI3M2QtMmQ0OS00OWNiLTg2ODctZDBlNmFmNTAwODNhIiwiaWF0IjoxNjE1NTQxNTk2fQ.e8kzX_Cdjj7If1KVEvxdnIcGmTdmhk3fF4rq-3ECJX8

// VM Supervisor ||     A.M.Rafi Sazzad	=> SVM009,	32196417	rafi.sazzad@marketaccesspl.com
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX3R5cGUiOiJWTVMiLCJ1c2VyX2lkIjoiNjA0MzFlMjEwMjg0Njg0NDMwNzBkODMzIiwic2Vzc2lvbl9pZCI6ImUzNzFkNGU2LTg3OWQtNGVlOS04MjY0LTk2MGVhYWNjMzM4YSIsImlhdCI6MTYxNTg2NTE2M30.58DTFQJsWP008W81Id7rDBjuDJnvOVfU_0db1nKxWHY

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//> SEC OPS
// SEC Meet
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX3R5cGUiOiJTRUMiLCJ1c2VyX2lkIjoiNjAyYTNiY2Y5ZTFhNmMyZDY0YjhjNDU3Iiwic2Vzc2lvbl9pZCI6Ijc1YWViMzMxLTUwOWEtNGJiYy04ZmZiLTkwOWMzN2ExNjQ1NyIsImlhdCI6MTYxNTc5NDU0MH0.83eu4aFMniNbjiCbrrq_Fd_NzamKNbKujYi1syBv7ok

// FOE Karan
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX3R5cGUiOiJGT0UiLCJ1c2VyX2lkIjoiNjAyYTNiY2Y5ZTFhNmMyZDY0YjhjNDU4Iiwic2Vzc2lvbl9pZCI6IjQxMzAyYTc4LTIzMWMtNDVmZi1iMGRjLTFlYmI3NDkzNmZkNSIsImlhdCI6MTYxNTgwNzkxNn0.g6sKZjcirSd6Z6Wvtqo2G_ufcK88Oh0lofFCXcQlmdk

// FOM Hirak
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjAyYTNiY2Y5ZTFhNmMyZDY0YjhjNDU5Iiwic2Vzc2lvbl9pZCI6ImYzNzBkMzlmLWFkMjgtNGQwMS04MzRjLTE0MTM4NGM0NWEyMyIsImlhdCI6MTYxMzM4MTM3MX0.TEGlzzfJc1XP2LI6IjoUSitZ6WIZW2-BFPRWgpXIOwA

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// SEC Monowar Hossain
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX3R5cGUiOiJTRUMiLCJ1c2VyX2lkIjoiNjAyYTJiMGI2YTQwY2I2NGZjYTczNTdkIiwic2Vzc2lvbl9pZCI6Ijk1MDA4OGU4LTNmMTYtNDMwMi1iM2FiLWRkYjdlOWY1ZDI1ZSIsImlhdCI6MTYxNzA4MDMyNH0.qKFDMqGfREbQyJJIyj4mvzGKZ-erwj1tAtXgfKDIQaw

// FOE ||   Md. Mahfujur Rahman => E049, PASSWORD => 52373344
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX3R5cGUiOiJGT0UiLCJ1c2VyX2lkIjoiNjAyYTJhY2Q2YTQwY2I2NGZjYTczMTBiIiwic2Vzc2lvbl9pZCI6IjNhYmMxNDg2LTM2NjktNDM1NC1hMDg0LWI2NTFiZWU4Y2ZlMCIsImlhdCI6MTYxNTkwMDQ1MH0.jUxmv1JJQOenwZHBOjQNrEp80IgvOz0Lr0MP47JJYXQ

// FOM ||   Md. Fahim => E007, PASSWORD => 11034725
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX3R5cGUiOiJGT00iLCJ1c2VyX2lkIjoiNjAyYTJhYzk2YTQwY2I2NGZjYTczMGU1Iiwic2Vzc2lvbl9pZCI6IjQ2ZDE3OWE2LTUwY2EtNDE5YS04MDk1LTBmNGJkZTZmZDM5NCIsImlhdCI6MTYxNTk2MTg5M30.d6SoIgaW8bNDAZGw2n5gTwX2A-9tQuMea4L4Kwox1DU

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//> For Admin
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjAyYTM5NzRjM2VhMjcwY2U0OTM0NzYzIiwic2Vzc2lvbl9pZCI6ImViYTFhZWM4LTMwZTYtNGNjNC1hMzg0LWUyZmM0M2M0ZGEzNCIsImlhdCI6MTYxMzU0NDU2Nn0.F58v6Tx4-wZOmy2R-lpbHcmCHXeCRvwdnJqsrFGnrDs
class MyApi {
  /// Base url to use for api calls.
  // static const String BASE_URL = "https://projects.xcitech.in:5006/v1/";
  // static const String BASE_URL = "https://159.65.156.147:5006/v1/";
  static const String BASE_URL = "https://ma.btlmonitor.com:5006/v1/";
  static Dio _dio = Dio();

  /// Custom method to use for Post-Request(Json) without Authorization.
  static Future<Response> postData(data, String apiUrl) {
    String fullUrl = BASE_URL + apiUrl;
    return _dio.post(
      fullUrl,
      data: data,
      options: Options(
        headers: _setJsonHeaders(),
      ),
    );
  }

  /// Custom method to use for Get-Request(Json) without Authorization.
  static Future<Response> getData(String apiUrl,
      {Map<String, dynamic> queryParams}) {
    String fullUrl = BASE_URL + apiUrl;
    return _dio.get(
      fullUrl,
      queryParameters: queryParams,
      options: Options(
        headers: _setJsonHeaders(),
      ),
    );
  }

  /// Custom method to use for Post-Request(Json) with Authorization.
  static Future<Response> postDataWithAuthorization(data, String apiUrl) async {
    try {
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      String token = localStorage.getString('token');
      // print("$My_Api token = $token");
      String fullUrl = BASE_URL + apiUrl + '?token=' + token;
      // throw SocketException("Base Throw");
      return _dio.post(
        fullUrl,
        data: data,
        options: Options(
          headers: {
            Headers.acceptHeader: "application/json",
            Headers.contentTypeHeader: Headers.jsonContentType,
            'Authorization': 'Bearer ' + token,
          },
        ),
      );
    } on SocketException {
      return Future.error(SocketException("Rethrown using future"));
    }
  }

  /// Custom method to use for Get-Request(Json) with Authorization.
  static Future<Response> getDataWithAuthorization(String apiUrl,
      {Map<String, dynamic> queryParams}) async {
    try {
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      String token = localStorage.getString('token');
      // print("$My_Api token = $token");
      String fullUrl = BASE_URL + apiUrl + '?token=' + token;
      // throw SocketException("Base");
      return _dio.get(
        fullUrl,
        queryParameters: queryParams,
        options: Options(
          headers: {
            Headers.acceptHeader: "application/json",
            Headers.contentTypeHeader: Headers.jsonContentType,
            'Authorization': 'Bearer ' + token,
          },
        ),
      );
    } on SocketException {
      print("$My_Api getDataWithAuthorization -> Socket-Exception");
      return Future.error(
          SocketException("SocketException thrown from Future"));
    }
  }

  /// Custom method to use for Post-Request(Form-Data) with Authorization.
  static Future<Response> postFormDataWithAuthorization(
      FormData formData, String apiUrl) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    String token = localStorage.getString('token');

    // print('data apiClassLogoutToken = ' + token);

    String fullUrl = BASE_URL + apiUrl + '?token=' + token;
    return _dio.post(
      fullUrl,
      data: formData,
      options: Options(
        headers: {
          Headers.acceptHeader: "application/json",
          Headers.contentTypeHeader: "multipart/form-data",
          'Authorization': 'Bearer ' + token,
        },
      ),
    );
  }

  static Map<String, dynamic> _setJsonHeaders() => {
        Headers.acceptHeader: "application/json",
        Headers.contentTypeHeader: Headers.jsonContentType,
      };

  // static Map<String, dynamic> _setFormDataHeaders() => {
  //       Headers.acceptHeader: "application/json",
  //       Headers.contentTypeHeader: "multipart/form-data",
  //     };
}
