class CETargetVsAchievementSummary {
  CETargetVsAchievementSummary({
    this.previousPage,
    this.nextPage,
    this.totalSalesTargetCount,
    this.salesTargetDocuments,
    this.currentPageSalesTargetCount,
  });

  final MyPage previousPage;
  final MyPage nextPage;
  final int totalSalesTargetCount;
  final List<SalesTargetDocument> salesTargetDocuments;
  final int currentPageSalesTargetCount;

  factory CETargetVsAchievementSummary.fromJson(Map<String, dynamic> json) =>
      CETargetVsAchievementSummary(
        previousPage: json["previousPage"] == null
            ? null
            : MyPage.fromJson(json["previousPage"]),
        nextPage:
            json["nextPage"] == null ? null : MyPage.fromJson(json["nextPage"]),
        totalSalesTargetCount: json["total_sales_target_count"] == null
            ? null
            : json["total_sales_target_count"],
        salesTargetDocuments: json["sales_target_documents"] == null
            ? null
            : List<SalesTargetDocument>.from(json["sales_target_documents"]
                .map((x) => SalesTargetDocument.fromJson(x))),
        currentPageSalesTargetCount:
            json["current_page_sales_target_count"] == null
                ? null
                : json["current_page_sales_target_count"],
      );

  Map<String, dynamic> toJson() => {
        "previousPage": previousPage == null ? null : previousPage.toJson(),
        "nextPage": nextPage == null ? null : nextPage.toJson(),
        "total_sales_target_count":
            totalSalesTargetCount == null ? null : totalSalesTargetCount,
        "sales_target_documents": salesTargetDocuments == null
            ? null
            : List<dynamic>.from(salesTargetDocuments.map((x) => x.toJson())),
        "current_page_sales_target_count": currentPageSalesTargetCount == null
            ? null
            : currentPageSalesTargetCount,
      };
}

class MyPage {
  MyPage({
    this.page,
    this.limit,
  });

  final int page;
  final int limit;

  factory MyPage.fromJson(Map<String, dynamic> json) => MyPage(
        page: json["page"] == null ? null : json["page"],
        limit: json["limit"] == null ? null : json["limit"],
      );

  Map<String, dynamic> toJson() => {
        "page": page == null ? null : page,
        "limit": limit == null ? null : limit,
      };
}

class SalesTargetDocument {
  SalesTargetDocument({
    this.id,
    this.salesTarget,
    this.storeName,
    this.storeDmsCode,
    this.storeType,
    this.region,
    this.territory,
    this.area,
    this.division,
    this.district,
    this.actualSales,
  });

  final Id id;
  final int salesTarget;
  final String storeName;
  final String storeDmsCode;
  final String storeType;
  final String region;
  final String territory;
  final String area;
  final String division;
  final String district;
  final int actualSales;

  factory SalesTargetDocument.fromJson(Map<String, dynamic> json) =>
      SalesTargetDocument(
        id: json["_id"] == null ? null : Id.fromJson(json["_id"]),
        salesTarget: json["sales_target"] == null ? null : json["sales_target"],
        storeName: json["store_name"] == null ? null : json["store_name"],
        storeDmsCode:
            json["store_dms_code"] == null ? null : json["store_dms_code"],
        storeType: json["store_type"] == null ? null : json["store_type"],
        region: json["region"] == null ? null : json["region"],
        territory: json["territory"] == null ? null : json["territory"],
        area: json["area"] == null ? null : json["area"],
        division: json["division"] == null ? null : json["division"],
        district: json["district"] == null ? null : json["district"],
        actualSales: json["actual_sales"] == null ? null : json["actual_sales"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id.toJson(),
        "sales_target": salesTarget == null ? null : salesTarget,
        "store_name": storeName == null ? null : storeName,
        "store_dms_code": storeDmsCode == null ? null : storeDmsCode,
        "store_type": storeType == null ? null : storeType,
        "region": region == null ? null : region,
        "territory": territory == null ? null : territory,
        "area": area == null ? null : area,
        "division": division == null ? null : division,
        "district": district == null ? null : district,
        "actual_sales": actualSales == null ? null : actualSales,
      };
}

class Id {
  Id({
    this.storeId,
    this.category,
  });

  final String storeId;
  final String category;

  factory Id.fromJson(Map<String, dynamic> json) => Id(
        storeId: json["store_id"] == null ? null : json["store_id"],
        category: json["category"] == null ? null : json["category"],
      );

  Map<String, dynamic> toJson() => {
        "store_id": storeId == null ? null : storeId,
        "category": category == null ? null : category,
      };
}
