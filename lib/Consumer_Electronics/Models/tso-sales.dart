class TsoSales {
  TsoSales({
    this.storeName,
    this.deviceModel,
    this.category,
    this.quantity,
    this.mrp,
    this.createdAt,
    this.updatedAt,
    this.totalPrice,
  });

  final String storeName;
  final String deviceModel;
  final String category;
  final int quantity;
  final String mrp;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int totalPrice;

  factory TsoSales.fromJson(Map<String, dynamic> json) => TsoSales(
        storeName: json["store_name"] == null ? null : json["store_name"],
        deviceModel: json["device_model"] == null ? null : json["device_model"],
        category: json["category"] == null ? null : json["category"],
        quantity: json["quantity"] == null ? null : json["quantity"],
        mrp: json["mrp"] == null ? null : json["mrp"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]).toLocal(),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]).toLocal(),
        totalPrice: json["totalPrice"] == null ? null : json["totalPrice"],
      );

  Map<String, dynamic> toJson() => {
        "store_name": storeName == null ? null : storeName,
        "device_model": deviceModel == null ? null : deviceModel,
        "category": category == null ? null : category,
        "quantity": quantity == null ? null : quantity,
        "mrp": mrp == null ? null : mrp,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "totalPrice": totalPrice == null ? null : totalPrice,
      };
}
