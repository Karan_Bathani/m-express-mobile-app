import 'package:flutter/cupertino.dart';
import 'package:market_express/Models/product-suggestion.dart';

class TSOSelectedProductsProvider with ChangeNotifier {
  Map<String, ProductSuggestion> selectedProducts = {};
  Map<String, int> qty = {};

  void addProduct(ProductSuggestion val) {
    selectedProducts.putIfAbsent(val.id, () => val);
    qty.putIfAbsent(val.id, () => 1);
    notifyListeners();
  }

  void removeProduct(ProductSuggestion val) {
    selectedProducts.remove(val.id);
    qty.remove(val.id);
    notifyListeners();
  }

  void increaseQuantity(ProductSuggestion val) {
    qty.update(val.id, (value) => value + 1);
    notifyListeners();
  }

  void decreaseQuantity(ProductSuggestion val) {
    qty.update(val.id, (value) => value - 1);
    notifyListeners();
  }

  int getCategorywiseQuantity(String cat) {
    int t = 0;
    List<String> ids = [];
    selectedProducts.values.forEach((element) {
      if (element.category == cat) {
        ids.add(element.id);
      }
    });
    qty.forEach((key, value) {
      if (ids.contains(key)) {
        t += value;
      }
    });
    return t;
  }

  int getTotalQuantity() {
    int t = 0;
    qty.values.forEach((element) {
      t += element;
    });
    return t;
  }
}
