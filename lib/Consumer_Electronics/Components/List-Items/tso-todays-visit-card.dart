import 'package:flutter/material.dart';
import 'package:market_express/Consumer_Electronics/Screens/Forms/tso-update-stock-form.dart';
import 'package:market_express/Models/store-visit.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';

class TSOTodaysVisitsCard extends StatelessWidget {
  final StoreVisit visit;
  final Function addSale;
  TSOTodaysVisitsCard(this.visit, this.addSale);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Divider(
          height: subMarginHalf,
          color: textPrimary,
        ),
        ListTile(
          trailing: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(children: [
              TextSpan(
                text: "Visited at:\n",
                style: TextStyle(
                  color: textSecondary,
                  fontSize: 12,
                ),
              ),
              TextSpan(
                text: visit.date.toLocal().formatDate(pattern: "hh:mm a"),
                style: TextStyle(
                  color: textPrimary,
                  fontWeight: FontWeight.w600,
                  fontSize: 16,
                ),
              ),
            ]),
          ),
          title: Text(
            visit.storeName,
            style: TextStyle(fontWeight: FontWeight.w600),
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(
              subMarginHalf, 0, subMarginHalf, subMarginHalf),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: colorPrimaryLight.withOpacity(.34),
                        blurRadius: 4,
                        offset: Offset(0, 4),
                      ),
                    ],
                  ),
                  child: FlatButton(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(miniCardCornerRadius),
                    ),
                    color: colorPrimary,
                    padding: EdgeInsets.symmetric(
                      vertical: subMarginHalf,
                      horizontal: subMargin,
                    ),
                    child: Text(
                      "Add Sale",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
                    onPressed: addSale,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: subMarginHalf),
                  decoration: BoxDecoration(boxShadow: [
                    BoxShadow(
                      color: colorSecondaryLight.withOpacity(.34),
                      blurRadius: 4,
                      offset: Offset(0, 4),
                    ),
                  ]),
                  child: FlatButton(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(miniCardCornerRadius),
                    ),
                    color: colorSecondary,
                    padding: EdgeInsets.symmetric(
                      vertical: subMarginHalf,
                      horizontal: subMargin,
                    ),
                    child: Text(
                      "Update Stock",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              TSOUpdateStockForm(visit.storeId),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
