import 'package:flutter/material.dart';
import 'package:market_express/Consumer_Electronics/Models/ce-target-vs-achievement-summary.dart';
import 'package:market_express/Utility/constants.dart';

class CETargetVsAchievementSummaryCard extends StatelessWidget {
  final SalesTargetDocument salesTargetDocument;
  final bool isSame;
  CETargetVsAchievementSummaryCard(this.salesTargetDocument, this.isSame);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        isSame
            ? Divider(
                color: textPrimary,
              )
            : SizedBox(),
        Card(
          elevation: cardElevation,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(cardCornerRadius)),
          color: Colors.white,
          clipBehavior: Clip.antiAlias,
          margin: EdgeInsets.symmetric(
            horizontal: subMargin,
            vertical: subMarginHalf,
          ),
          child: Table(
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            columnWidths: {
              0: FlexColumnWidth(5),
              1: FlexColumnWidth(1.5),
            },
            children: [
              TableRow(
                decoration: BoxDecoration(
                    color:
                        randomColors[salesTargetDocument.storeName.length % 6]),
                children: [
                  Container(
                    color: Colors.white,
                    child: ListTile(
                      title: Text(
                        salesTargetDocument.storeName,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: textPrimary,
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      subtitle: Text(
                          "${salesTargetDocument.area ?? ""}, ${salesTargetDocument.district ?? ""}."),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          salesTargetDocument.id.category,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        FittedBox(
                          child: Text(
                            "${salesTargetDocument.actualSales}/${salesTargetDocument.salesTarget}",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 22,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  // Column(
                  //   mainAxisAlignment: MainAxisAlignment.center,
                  //   crossAxisAlignment: CrossAxisAlignment.center,
                  //   mainAxisSize: MainAxisSize.max,
                  //   children: [
                  //     FittedBox(
                  //       child: Text(
                  //         salesTargetDocument.actualSales.toString(),
                  //         style: TextStyle(
                  //           color: Colors.white,
                  //           fontSize: 26,
                  //           fontWeight: FontWeight.w700,
                  //         ),
                  //       ),
                  //     ),
                  //     SizedBox(height: subMarginHalf / 2),
                  //     Text(
                  //       salesTargetDocument.salesTarget.toString(),
                  //       style: TextStyle(
                  //         color: Colors.white,
                  //         // fontSize: 16,
                  //         // fontWeight: FontWeight.w600,
                  //       ),
                  //     ),
                  //   ],
                  // ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
