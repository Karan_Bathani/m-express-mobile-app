import 'package:flutter/material.dart';
import 'package:market_express/Consumer_Electronics/Models/tso-sales.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';

class TSOSalesCard extends StatelessWidget {
  final TsoSales sales;
  final bool showDate;
  TSOSalesCard(this.sales, {this.showDate = false});
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.symmetric(
        horizontal: subMargin,
        vertical: subMarginHalf,
      ),
      child: Column(
        children: [
//> Header
          Container(
            color: colorPrimary,
            width: double.infinity,
            padding: EdgeInsets.symmetric(
              vertical: subMarginHalf,
              horizontal: subMargin,
            ),
            child: Table(
              columnWidths: {
                0: FlexColumnWidth(4),
                1: FlexColumnWidth(1.5),
              },
              children: [
                TableRow(children: [
                  Text(
                    sales.storeName,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      showDate
                          ? sales.createdAt.formatDate(pattern: "dd MMM, yyyy")
                          : sales.createdAt.formatDate(pattern: "hh:mm a"),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ]),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: cardContentPadding,
              vertical: subMarginHalf,
            ),
            child: Column(
              children: [
                Table(
                  columnWidths: {
                    0: FlexColumnWidth(2.2),
                    1: FlexColumnWidth(1.2),
                  },
                  children: [
                    TableRow(children: [
                      RichText(
                        text: TextSpan(children: [
                          TextSpan(
                            text: "Model: ",
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: textSecondary,
                            ),
                          ),
                          TextSpan(
                            text: "${sales.deviceModel}",
                            style: TextStyle(
                              color: textPrimary,
                            ),
                          ),
                        ]),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: RichText(
                          text: TextSpan(children: [
                            TextSpan(
                              text: "Category: ",
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                color: textSecondary,
                              ),
                            ),
                            TextSpan(
                              text: "${sales.category}",
                              style: TextStyle(
                                color: textPrimary,
                              ),
                            ),
                          ]),
                        ),
                      ),
                    ]),
                  ],
                ),
                SizedBox(height: subMarginHalf / 1.5),
                Table(
                  columnWidths: {
                    0: FlexColumnWidth(1.8),
                    1: FlexColumnWidth(1),
                  },
                  children: [
                    TableRow(children: [
                      RichText(
                        text: TextSpan(children: [
                          TextSpan(
                            text: "MRP: ",
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: textSecondary,
                            ),
                          ),
                          TextSpan(
                            text: "${sales.mrp}",
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: textPrimary,
                            ),
                          ),
                          TextSpan(
                            text: " Tk",
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: textPrimary,
                              fontSize: 12,
                            ),
                          ),
                        ]),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: RichText(
                          text: TextSpan(children: [
                            TextSpan(
                              text: "Quantity: ",
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                color: textSecondary,
                              ),
                            ),
                            TextSpan(
                              text: "${sales.quantity}",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: textPrimary,
                              ),
                            ),
                            TextSpan(
                              text: " Pc(s)",
                              style: TextStyle(
                                color: textPrimary,
                                fontSize: 12,
                              ),
                            ),
                          ]),
                        ),
                      ),
                    ]),
                  ],
                ),
                SizedBox(height: subMarginHalf / 1.5),
                Align(
                  alignment: Alignment.centerLeft,
                  child: RichText(
                    text: TextSpan(children: [
                      TextSpan(
                        text: "Total Price: ",
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: textSecondary,
                        ),
                      ),
                      TextSpan(
                        text: "${sales.totalPrice}",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: colorPrimary,
                          fontSize: 16,
                        ),
                      ),
                      TextSpan(
                        text: " Tk",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: colorPrimary,
                        ),
                      ),
                    ]),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
