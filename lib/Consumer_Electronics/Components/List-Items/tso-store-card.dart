import 'package:flutter/material.dart';
import 'package:market_express/Models/store.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';

class TSOStoreCard extends StatelessWidget {
  final Store store;
  TSOStoreCard(this.store);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.symmetric(
        horizontal: subMargin,
        vertical: subMarginHalf,
      ),
      child: Table(
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        columnWidths: {
          0: FlexColumnWidth(5.2),
          1: FlexColumnWidth(1.3),
        },
        children: [
          TableRow(
            decoration: BoxDecoration(color: getRandomColor()),
            children: [
              Container(
                color: Colors.white,
                child: ListTile(
                  isThreeLine: false,
                  visualDensity: VisualDensity.compact,
                  title: Text(
                    "${store.storeName}, ${store.district}",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  subtitle: Text(
                    "${store.storeType}",
                    style: TextStyle(
                      color: textSecondary,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    Icons.arrow_forward,
                    color: Colors.white,
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
