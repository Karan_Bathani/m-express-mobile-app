import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Consumer_Electronics/Providers/tso-selected-products-provider.dart';
import 'package:market_express/Models/product-suggestion.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:provider/provider.dart';
import 'package:simple_autocomplete_formfield/simple_autocomplete_formfield.dart';

class TSOSelectProductCategoryTab extends StatefulWidget {
  final String tabName;
  TSOSelectProductCategoryTab(this.tabName);
  @override
  _TSOSelectProductCategoryTabState createState() =>
      _TSOSelectProductCategoryTabState();
}

class _TSOSelectProductCategoryTabState
    extends State<TSOSelectProductCategoryTab>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  TextEditingController deviceModelController = TextEditingController();

  Future<Response<dynamic>> _futureProductSuggestions;
  List<String> productSuggestions = [];
  List<ProductSuggestion> modelProductSuggestions = [];

  @override
  void initState() {
    super.initState();
    _futureProductSuggestions = MyApi.getDataWithAuthorization("products/list",
        queryParams: {"business_unit": "ce_ops"});
  }

  @override
  void dispose() {
    deviceModelController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return ListView(
      padding: EdgeInsets.all(subMargin),
      children: [
//> TSO Product-Model
        FutureBuilder<Response<dynamic>>(
          future: _futureProductSuggestions,
          builder: (context, snapshot) {
            // ~ Error
            if (snapshot.hasError) {
              if (snapshot.error == SocketException ||
                  snapshot.error.toString().contains("SocketException")) {
                Future.delayed(Duration(milliseconds: 500)).then((value) {
                  showToast("You are offline, Check your Internet Connection!",
                      context: context,
                      animation: StyledToastAnimation.slideFromBottom,
                      reverseAnimation: StyledToastAnimation.slideToBottom,
                      startOffset: Offset(0.0, 3.0),
                      reverseEndOffset: Offset(0.0, 3.0),
                      position: StyledToastPosition.bottom,
                      duration: Duration(seconds: 3),
                      backgroundColor: error,
                      animDuration: Duration(seconds: 1),
                      curve: Curves.elasticOut,
                      reverseCurve: Curves.fastOutSlowIn);
                });
              }
            } else if (snapshot.hasData) {
              // < SUCCESS
              if (productSuggestions != null &&
                  productSuggestions.length != 0) {
                productSuggestions.clear();
                modelProductSuggestions.clear();
              }
              snapshot.data.data["data"].forEach((element) {
                // print(
                //     "debug TSOProductCategoryTab ${ProductSuggestion.fromJson(element).deviceModel}");
                if (ProductSuggestion.fromJson(element).category ==
                    widget.tabName) {
                  productSuggestions
                      .add(ProductSuggestion.fromJson(element).deviceModel);
                  modelProductSuggestions
                      .add(ProductSuggestion.fromJson(element));
                }
              });
            }
            return SimpleAutocompleteFormField<ProductSuggestion>(
              controller: deviceModelController,
              keyboardType: TextInputType.text,
              style: TextStyle(
                color: textPrimary,
                fontWeight: FontWeight.w600,
              ),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.grey[600],
                  ),
                ),
                labelText: 'Device Model',
                hintText: 'Enter the Device Model',
              ),
              autovalidate: true,
              // validator: (value) {
              //   print("debug TSOUpdateStockForm Value = $value");
              //   // print(
              //   //     "debug TSOUpdateStockForm TextValue = ${deviceModelController.text}");
              //   if (deviceModelController.text == null ||
              //       deviceModelController.text.isEmpty ||
              //       deviceModelController.text.length == 0) {
              //     return "Please enter a Device-Model";
              //   } else if (!productSuggestions
              //       .contains(deviceModelController.text)) {
              //     return "Select a model from the list";
              //   } else if (value != null &&
              //       !productSuggestions.contains(value.deviceModel)) {
              //     return "Select a model from the list";
              //   } else if (value != null) {
              //     int tempIndex = modelProductSuggestions.indexWhere(
              //         (element) => element.deviceModel == value.deviceModel);
              //     // productId = modelProductSuggestions[tempIndex].id;
              //     // print(
              //     //     "debug TSOUpdateStockForm Else ProductId = $productId");
              //   }
              //   return null;
              // },
              onChanged: (val) {
                //* Called when an item is tapped or the field loses focus.
                if (val != null) {
                  print("debug val = ${val.toJson()}");
                  Provider.of<TSOSelectedProductsProvider>(context,
                          listen: false)
                      .addProduct(val);
                  // print(
                  //     "debug Added or Not = ${_selectedProducts.add(val)}");
                }
                // print("debug SelectedProducts = ${_selectedProducts.toSet()}");
                // print("debug SelectedProducts = $_selectedProducts");
              },
              itemToString: (item) => "",
              onSearch: (search) async {
                if (productSuggestions.isEmpty ||
                    productSuggestions.length == 0) {
                  if (!mounted) {
                    setState(() {
                      _futureProductSuggestions =
                          MyApi.getDataWithAuthorization("products/list",
                              queryParams: {"business_unit": "ce_ops"});
                    });
                  }
                }
                return modelProductSuggestions
                    .where(
                      (element) => element.deviceModel.toLowerCase().contains(
                            search.toLowerCase(),
                          ),
                    )
                    .toList();
              },
              itemBuilder: (context, item) {
                return Padding(
                  padding: EdgeInsets.all(12),
                  child: Text(item.deviceModel),
                );
              },
              maxSuggestions: modelProductSuggestions != null &&
                      modelProductSuggestions.length != 0
                  ? modelProductSuggestions.length
                  : 0,
              suggestionsBuilder: (context, items) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Card(
                    elevation: cardElevation,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(cardCornerRadius)),
                    color: Colors.white,
                    clipBehavior: Clip.antiAlias,
                    margin: EdgeInsets.symmetric(
                      vertical: subMarginHalf,
                    ),
                    child: Center(
                      child: Container(
                        margin: EdgeInsets.all(subMargin),
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  );
                } else if (snapshot.hasError) {
                  return Card(
                    elevation: cardElevation,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(cardCornerRadius)),
                    color: Colors.white,
                    clipBehavior: Clip.antiAlias,
                    margin: EdgeInsets.symmetric(
                      // horizontal: subMargin,
                      vertical: subMarginHalf,
                    ),
                    child: Center(
                      child: Container(
                        margin: EdgeInsets.all(subMargin),
                        child: snapshot.error.toString().contains("404")
                            ? Text("No Data Found!")
                            : Text("Something Went Wrong!"),
                      ),
                    ),
                  );
                }
                return Card(
                  elevation: cardElevation,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(cardCornerRadius)),
                  color: Colors.white,
                  clipBehavior: Clip.antiAlias,
                  margin: EdgeInsets.symmetric(
                    vertical: subMarginHalf,
                  ),
                  child: SizedBox(
                    width: double.infinity,
                    height: 200,
                    child: ListView(
                      children: items,
                    ),
                  ),
                );
              },
            );
          },
        ),
        SizedBox(height: subMargin),
//> Products List
        Consumer<TSOSelectedProductsProvider>(
          builder: (context, provider, child) {
            if (provider.selectedProducts.values != null &&
                provider.selectedProducts.values.length != 0) {
              return Column(
                children: provider.selectedProducts.values
                    .where((element) => element.category == widget.tabName)
                    .map((e) {
                  return ListTile(
                    contentPadding: EdgeInsets.all(0),
                    title: Text(
                      e.deviceModel,
                      style: TextStyle(fontWeight: FontWeight.w600),
                    ),
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        IconButton(
                          visualDensity: VisualDensity.compact,
                          onPressed: () {
                            if (provider.qty[e.id] > 1) {
                              Provider.of<TSOSelectedProductsProvider>(context,
                                      listen: false)
                                  .decreaseQuantity(e);
                            } else {
                              Provider.of<TSOSelectedProductsProvider>(context,
                                      listen: false)
                                  .removeProduct(e);
                            }
                          },
                          icon: provider.qty[e.id] > 1
                              ? Icon(
                                  Icons.remove_circle,
                                  color: colorPrimary,
                                )
                              : Icon(
                                  Icons.delete,
                                  color: error,
                                ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 3),
                          child: Text(provider.qty[e.id].toString()),
                        ),
                        IconButton(
                          visualDensity: VisualDensity.compact,
                          onPressed: () =>
                              Provider.of<TSOSelectedProductsProvider>(context,
                                      listen: false)
                                  .increaseQuantity(e),
                          icon: Icon(
                            Icons.add_circle,
                            color: colorPrimary,
                          ),
                        ),
                      ],
                    ),
                    visualDensity: VisualDensity.compact,
                  );
                }).toList(),
              );
            }
            return SizedBox();
          },
        ),
      ],
    );
  }
}
