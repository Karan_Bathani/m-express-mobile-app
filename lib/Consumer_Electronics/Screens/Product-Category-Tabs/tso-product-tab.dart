import 'package:flutter/material.dart';
import 'package:market_express/Consumer_Electronics/Screens/ce-sales-screen.dart';
import 'package:market_express/Models/product-suggestion.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';

class TSOProductTab extends StatefulWidget {
  final List<ProductSuggestion> ceProducts;
  final bool isStockRoute;
  TSOProductTab(this.ceProducts, {this.isStockRoute});

  @override
  _TSOProductTabState createState() => _TSOProductTabState();
}

class _TSOProductTabState extends State<TSOProductTab>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return ListView.builder(
      padding: EdgeInsets.symmetric(vertical: subMarginHalf),
      itemCount: widget.ceProducts.length,
      itemBuilder: (context, index) {
        return Card(
          elevation: cardElevation,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(cardCornerRadius)),
          color: Colors.white,
          clipBehavior: Clip.antiAlias,
          margin: EdgeInsets.symmetric(
            horizontal: subMargin,
            vertical: subMarginHalf - 2,
          ),
          child: ListTile(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => CESalesScreen(
                      title: widget.ceProducts[index].deviceModel,
                      id: widget.ceProducts[index].id,
                      searchFor: "product",
                    ),
                  ));
            },
            contentPadding: EdgeInsets.only(left: cardContentPadding),
            title: Text(
              widget.ceProducts[index].deviceModel,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                letterSpacing: 1,
              ),
            ),
            trailing: Container(
              height: 100,
              width: 60,
              color: getRandomColor(),
              child: Icon(
                Icons.arrow_forward,
                color: Colors.white,
              ),
            ),
          ),
        );
      },
    );
  }
}
