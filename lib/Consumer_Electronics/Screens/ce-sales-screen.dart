import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Consumer_Electronics/Components/List-Items/tso-sales-card.dart';
import 'package:market_express/Consumer_Electronics/Models/tso-sales.dart';
import 'package:market_express/Models/product-suggestion.dart';
import 'package:market_express/Models/store.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CESalesScreen extends StatefulWidget {
  final String title;
  final String id;
  final String searchFor;
  CESalesScreen(
      {@required this.title, @required this.id, @required this.searchFor});
  @override
  _CESalesScreenState createState() => _CESalesScreenState();
}

class _CESalesScreenState extends State<CESalesScreen> {
  Future<List<TsoSales>> _futureSales;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _futureSales = _getUserSalesData();
  }

  Size screenSize;
  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: FittedBox(
          child: Text(
            widget.title,
            style: TextStyle(color: Colors.white),
          ),
        ),
        // actions: [
        //   Container(
        //     padding: EdgeInsets.all(subMargin),
        //     alignment: Alignment.center,
        //     child: Text(
        //       widget.product.category,
        //       style: TextStyle(
        //         color: Colors.white,
        //         fontWeight: FontWeight.w600,
        //         fontSize: 14,
        //       ),
        //     ),
        //   )
        // ],
      ),
      body: Container(
        clipBehavior: Clip.antiAlias,
        width: screenSize.width,
        height: screenSize.height,
        decoration: BoxDecoration(
          color: greyBackground,
          borderRadius:
              BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
        ),
        child: RefreshIndicator(
          key: _refreshIndicatorKey,
          onRefresh: () {
            setState(() {
              _futureSales = _getUserSalesData();
            });
            return _futureSales;
          },
          child: FutureBuilder<List<TsoSales>>(
            future: _futureSales,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                _refreshIndicatorKey.currentState?.show();
                return _buildSalesListView(snapshot.data);
              } else if (snapshot.hasError) {
                if (snapshot.error.toString().contains("404")) {
                  return buildEmptyWidget(screenSize, text: "No Sales Found!");
                }
                return buildErrorWidget(screenSize);
              } else if (snapshot.hasData &&
                  snapshot.connectionState == ConnectionState.done &&
                  snapshot.data.length != 0) {
                return _buildSalesListView(snapshot.data);
              }
              return SizedBox.expand(
                child: ListView(),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _buildSalesListView(List<TsoSales> sales) {
    return ListView.builder(
      itemCount: sales != null ? sales.length : 0,
      itemBuilder: (context, index) => TSOSalesCard(
        sales[index],
        showDate: true,
      ),
    );
  }

  Future<List<TsoSales>> _getUserSalesData() async {
    List<TsoSales> sales = [];
    try {
      Response response = await MyApi.postDataWithAuthorization({
        "search_for": widget.id,
        "search_field": widget.searchFor,
      }, "stores/sales-history");
      print("debug TSOSalesAllTab Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var sale in response.data["data"]) {
          sales.add(TsoSales.fromJson(sale));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("debug TSOSalesAllTab SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      print("debug TSOSalesAllTab Exception = $error");
      return Future.error(error);
    }
    return sales;
  }
}
