import 'package:flutter/material.dart';
import 'package:market_express/Consumer_Electronics/Components/List-Items/ce-target-vs-achievement-summary-card.dart';
import 'package:market_express/Consumer_Electronics/Models/ce-target-vs-achievement-summary.dart';
import 'package:market_express/Utility/constants.dart';

import 'dart:io';

import 'package:dio/dio.dart';
import 'package:intl/intl.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/target-vs-achievement-summary-card.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CETargetVsAchievementSummaryScreen extends StatefulWidget {
  @override
  _CETargetVsAchievementSummaryScreenState createState() =>
      _CETargetVsAchievementSummaryScreenState();
}

class _CETargetVsAchievementSummaryScreenState
    extends State<CETargetVsAchievementSummaryScreen> {
  int pageSize = 10;
  MyPage nextPage = MyPage(
    limit: 10,
    page: 1,
  );
  bool _hasError = false;
  dynamic _error;
  bool _isLoading = true;
  List<SalesTargetDocument> _globalSalesTargetDocuments = [];

  Future<CETargetVsAchievementSummary> _futureTargetVsAchievementSummary;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    _selectedDateTime = todaysDateTime;
    _futureTargetVsAchievementSummary =
        _getTargetVsAchievementSummary(nextPage.page);
    _scrollController.addListener(() {
      if (_scrollController.position.pixels >=
              _scrollController.position.maxScrollExtent - 10 &&
          // _scrollController.position.pixels ==_scrollController.position.maxScrollExtent
          this.mounted &&
          //Check for the last page
          nextPage != null &&
          !_hasError) {
        setState(() {
          _futureTargetVsAchievementSummary =
              _getTargetVsAchievementSummary(nextPage.page);
        });
      }
    });
  }

  DateTime todaysDateTime = DateTime.now();
  DateTime _selectedDateTime;
  Size screenSize;

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: FittedBox(
          child: Text(
            'Target vs Achievement',
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: greyBackground,
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          child: RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () {
              setState(() {
                _globalSalesTargetDocuments.clear();
                nextPage = MyPage(limit: 10, page: 1);
                _futureTargetVsAchievementSummary =
                    _getTargetVsAchievementSummary(nextPage.page);
              });
              return _futureTargetVsAchievementSummary;
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
// Date Selector
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: subMargin,
                  ),
                  color: dropDownBackground,
                  child: DropdownButton(
                    underline: SizedBox.shrink(),
                    iconSize: 30,
                    isExpanded: true,
                    hint: Text(
                      DateFormat("MMMM, yyy").format(_selectedDateTime),
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    items: List.generate(10, (index) {
                      DateTime generatedDate = DateTime(
                          todaysDateTime.year, todaysDateTime.month - index);
                      return DropdownMenuItem(
                        child:
                            Text(DateFormat("MMMM, yyy").format(generatedDate)),
                        value: generatedDate,
                      );
                    }),
                    onChanged: (value) {
                      setState(() {
                        _selectedDateTime = value;
                        _globalSalesTargetDocuments.clear();
                        nextPage = MyPage(limit: 10, page: 1);
                        _futureTargetVsAchievementSummary =
                            _getTargetVsAchievementSummary(nextPage.page);
                      });
                    },
                  ),
                ),
                FutureBuilder<CETargetVsAchievementSummary>(
                  future: _futureTargetVsAchievementSummary,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting &&
                        _globalSalesTargetDocuments.length == 0) {
                      _refreshIndicatorKey.currentState?.show();
                      return Expanded(
                        child: ListView(),
                      );
                      // return snapshot.data != null
                      //     ? _buildAttendanceSummaryList(
                      //          snapshot.data.salesTargetDocuments)
                      //         _globalSalesTargetDocuments)
                      //     : SizedBox.shrink();
                    } else if (snapshot.connectionState ==
                        ConnectionState.waiting) {
                      _isLoading = true;
                      _hasError = false;
                      _error = null;
                    } else if (snapshot.hasError) {
//~ Error
                      _isLoading = false;
                      _hasError = true;
                      _error = snapshot.error;
                      // if (snapshot.error.toString().contains("404")) {
                      //   return buildEmptyWidget(screenSize,
                      //       text: "No Summary Found");
                      // }
                      // return buildErrorWidget(screenSize);
                    } else if (snapshot.hasData &&
                        snapshot.connectionState == ConnectionState.done) {
//< If everything's fine append new data to the previously fetched data and show
                      _isLoading = false;
                      _hasError = false;
                      _error = null;
                      _globalSalesTargetDocuments
                          .addAll(snapshot.data.salesTargetDocuments);
                    }
                    return _buildAttendanceSummaryList(
                      _globalSalesTargetDocuments,
                      hasError: _hasError,
                      isLoading: _isLoading,
                      error: _error,
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildAttendanceSummaryList(
      List<SalesTargetDocument> salesTargetDocuments,
      {bool hasError,
      bool isLoading,
      dynamic error}) {
    if (salesTargetDocuments != null &&
        !isLoading &&
        !hasError &&
        (salesTargetDocuments.isEmpty || salesTargetDocuments.length == 0)) {
      //~ ERROR
// Empty(404) case
      return buildEmptyWidget(screenSize, text: "No Summary Found!");
    } else if (salesTargetDocuments != null &&
        hasError &&
        (salesTargetDocuments.isEmpty || salesTargetDocuments.length == 0)) {
      // Error Case
      if (error.toString().contains("404")) {
        return buildEmptyWidget(screenSize, text: "No Summary Found!");
      } else if (error.toString().contains("SocketException")) {
        return buildErrorWidget(screenSize, text: "No Internet Connection!");
      }
      return buildErrorWidget(screenSize);
    }
    return Expanded(
      child: ListView.builder(
        controller: _scrollController,
        itemCount:
            salesTargetDocuments != null ? salesTargetDocuments.length + 1 : 0,
        itemBuilder: (context, index) {
          if (index == salesTargetDocuments.length) {
            if (!hasError && isLoading) {
              return Center(
                child: Padding(
                  padding: EdgeInsets.all(subMargin),
                  child: CircularProgressIndicator(),
                ),
              );
            } else if (hasError && !isLoading) {
//After retrying add the new data to previously fecthed data.
              return _errorRetryListItem();
            } else {
              return SizedBox();
            }
          }
          bool _isSame = false;
          if (index > 0 &&
              salesTargetDocuments[index].storeName.length % 6 ==
                  salesTargetDocuments[index - 1].storeName.length % 6 &&
              salesTargetDocuments[index].id.storeId !=
                  salesTargetDocuments[index - 1].id.storeId) {
            _isSame = true;
          }
          return CETargetVsAchievementSummaryCard(
              salesTargetDocuments[index], _isSame);
        },
      ),
    );
  }

  Future<CETargetVsAchievementSummary> _getTargetVsAchievementSummary(
      int page) async {
    print("$TargetVsAchievementSummary_Screen PageNumber = $page");

    CETargetVsAchievementSummary targetVsAchievementSummary;
    try {
      Response response = await MyApi.postDataWithAuthorization({
        "month": _selectedDateTime.month,
        // "year": _selectedDateTime.year,
      }, "stores/sales-target-list-for-ce\?page=${page.toString()}\&limit=${pageSize.toString()}");
      print("$TargetVsAchievementSummary_Screen Response = ${response.data}");
      if (response.statusCode == 401) {
//? Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        //< SUCCESS
        targetVsAchievementSummary =
            CETargetVsAchievementSummary.fromJson(response.data["data"]);
        nextPage = targetVsAchievementSummary.nextPage;
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("$TargetVsAchievementSummary_Screen SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//? Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      print("$TargetVsAchievementSummary_Screen Exception = $error");
      throw Future.error(error);
    }
    return targetVsAchievementSummary;
  }

  Widget _errorRetryListItem() {
    //~ Error
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.symmetric(
        horizontal: subMargin,
        vertical: subMarginHalf,
      ),
      child: Padding(
        padding: EdgeInsets.all(cardContentPadding),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Something went Wrong!",
              style: TextStyle(color: error),
            ),
            FlatButton(
              onPressed: () {
                // After retrying add the new data to previously fecthed data.
                setState(() {
                  _futureTargetVsAchievementSummary =
                      _getTargetVsAchievementSummary(nextPage.page);
                });
              },
              child: Text("Retry"),
            ),
          ],
        ),
      ),
    );
  }
}
