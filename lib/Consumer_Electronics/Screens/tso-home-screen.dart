import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:intl/intl.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/notice-card.dart';
import 'package:market_express/Consumer_Electronics/Components/List-Items/tso-todays-visit-card.dart';
import 'package:market_express/Consumer_Electronics/Components/tso-navigation-drawer.dart';
import 'package:market_express/Consumer_Electronics/Screens/Forms/tso-sales-form.dart';
import 'package:market_express/Consumer_Electronics/Screens/Forms/tso-update-stock-form.dart';
import 'package:market_express/Models/dashboard.dart';
import 'package:market_express/Models/store-visit.dart';
import 'package:market_express/Screens/Forms/leave-form.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Screens/notification-screen.dart';
import 'package:market_express/Screens/training-material-screen.dart';
import 'package:market_express/Screens/unlisted-mark-attendance-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class TSOHomeScreen extends StatefulWidget {
  @override
  _TSOHomeScreenState createState() => _TSOHomeScreenState();
}

class _TSOHomeScreenState extends State<TSOHomeScreen> {
  Size screenSize;
  // Future<AttendanceStatus> _futureAttendanceStatus;
  // bool isMorningAttendanceMarked = false;
  Future<Dashboard> _futureDashboard;
  Future<List<StoreVisit>> _futureTodaysStoreVisits;
  // Notice _notice;
  // int _totalSurveys;
  String _userRecentAttendancePhoto;
  DateTime todaysDateTime = DateTime.now();
  @override
  void initState() {
    super.initState();
    // _futureAttendanceStatus = _getAttendanceStatus();
    _futureDashboard = _getDashboardData();
    _futureTodaysStoreVisits = _getTodaysStoreVisits();
  }

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: FutureBuilder<SharedPreferences>(
          future: SharedPreferences.getInstance(),
          builder: (context, snapshot) {
            if (snapshot.hasData && snapshot.data.containsKey("user_name")) {
              return FittedBox(
                child: Text(
                  snapshot.data.getString("user_name"),
                  style: TextStyle(color: Colors.white),
                ),
              );
            }
            return Text(
              'Home',
              style: TextStyle(color: Colors.white),
            );
          },
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.notifications,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => NotificationScreen(),
                  ));
            },
          ),
          IconButton(
            icon: Icon(
              Icons.file_download,
              // color: Colors.amberAccent,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => TrainingMaterialScreen(),
                  ));
            },
          ),
        ],
      ),
      drawer: TSONavigationDrawer(
          userRecentAttendancePhoto: _userRecentAttendancePhoto),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: greyBackground,
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          child: ListView(
            padding: EdgeInsets.all(subMargin),
            children: [
              FutureBuilder<Dashboard>(
                future: _futureDashboard,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Column(
                      children: [
                        snapshot.data.mostRecentNotice != null
                            ? NoticeCard(
                                snapshot.data.mostRecentNotice,
                                shouldHighlight: true,
                                isHomeScreen: true,
                              )
                            : SizedBox(),
                        _createFirstCard(snapshot.data),
                        // snapshot.data.actualSales != null &&
                        //         snapshot.data.salesTarget != null &&
                        //         snapshot.data.salesTarget != 0
                        //     ? _createSalesTargetCard(
                        //         context,
                        //         snapshot.data.actualSales,
                        //         snapshot.data.salesTarget)
                        //     : SizedBox.shrink(),
                      ],
                    );
                  } else if (snapshot.connectionState ==
                      ConnectionState.waiting) {
                    return Center(
                      child: Container(
                        margin: EdgeInsets.all(subMargin),
                        width: 30,
                        height: 30,
                        child: CircularProgressIndicator(),
                      ),
                    );
                  } else if (snapshot.hasError) {
                    print(
                        "debug TsoHomeScreen FutureError = ${snapshot.error}");

                    return Card(
                      elevation: cardElevation,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(cardCornerRadius)),
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.all(cardContentPadding),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Something went Wrong!",
                              style: TextStyle(color: error),
                            ),
                            FlatButton(
                              onPressed: () {
                                setState(() {
                                  _futureDashboard = _getDashboardData();
                                });
                              },
                              child: Text("Retry"),
                            ),
                          ],
                        ),
                      ),
                    );
                  }
                  return SizedBox.shrink();
                },
              ),
              // _createQuickActions(),
              createTodaysVisitsListCard(context),
            ],
          ),
        ),
      ),
    );
  }

  /// Will show manager's contact-details and assigned store-data
  Widget _createFirstCard(Dashboard dashboard) {
    return Column(
      children: [
        Card(
          elevation: cardElevation,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(cardCornerRadius)),
          color: Colors.white,
          child: Padding(
            padding: EdgeInsets.all(cardContentPadding),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                dashboard.store != null
                    ? Text(
                        dashboard.store.storeName ?? 'Store Name',
                        style: TextStyle(
                            color: textPrimary,
                            fontWeight: FontWeight.w600,
                            fontSize: 14),
                      )
                    : SizedBox.shrink(),
                dashboard.store != null
                    ? Text(
                        'Store Assigned Date ' +
                                DateFormat.yMMMd()
                                    .format(dashboard.store.updatedAt) ??
                            'Date Assigned',
                        style: TextStyle(
                          color: colorPrimary,
                          fontSize: 12,
                        ),
                      )
                    : SizedBox.shrink(),
                dashboard.store != null
                    ? Padding(
                        padding: EdgeInsets.only(top: subMarginHalf),
                        child: Text(
                          "${dashboard.store.location}, ${dashboard.store.area}, ${dashboard.store.district}, ${dashboard.store.division}." ??
                              "Assigned Store Address",
                          style: TextStyle(
                            color: textSecondary,
                            fontSize: 12,
                          ),
                        ),
                      )
                    : SizedBox.shrink(),
                ListTile(
                  contentPadding: EdgeInsets.all(0),
                  dense: true,
                  leading: Image.asset(
                    'assets/images/user.png',
                    width: 40,
                    height: 40,
                    alignment: Alignment.center,
                  ),
                  title: Text(
                    dashboard.managerName ?? 'Manager Name',
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: textPrimary,
                    ),
                  ),
                  subtitle: Text(
                    'Reporting Manager',
                    style: TextStyle(
                      color: textSecondary,
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                    ),
                  ),
                  trailing: Builder(
                    builder: (context) => Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        IconButton(
                          constraints: BoxConstraints(
                            maxWidth: 45,
                            maxHeight: 45,
                            minWidth: 45,
                            minHeight: 45,
                          ),
                          padding: EdgeInsets.all(0),
                          visualDensity: VisualDensity.compact,
                          onPressed: () => sendEmail(
                              dashboard.managerEmail ?? "test@xcitech.com",
                              context),
                          icon: Image.asset(
                            'assets/images/email.png',
                          ),
                        ),
                        SizedBox(width: subMarginHalf),
                        IconButton(
                          constraints: BoxConstraints(
                            maxWidth: 45,
                            maxHeight: 45,
                            minWidth: 45,
                            minHeight: 45,
                          ),
                          padding: EdgeInsets.all(0),
                          visualDensity: VisualDensity.compact,
                          onPressed: () => callnow(
                              dashboard.managerPhone ?? "1234567890", context),
                          icon: Image.asset(
                            'assets/images/phone.png',
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Table(
                  defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                  columnWidths: {1: FixedColumnWidth(1)},
                  children: [
                    TableRow(
                        decoration: BoxDecoration(
                          color: greyBackground,
                        ),
                        children: [
                          Center(
                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text:
                                        "${dashboard.monthlyAttendanceCount ?? 0}\n",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                    ),
                                  ),
                                  TextSpan(
                                    text: "day(s)\n",
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: textSecondary,
                                    ),
                                  ),
                                  TextSpan(
                                    text:
                                        "Attendance: (${DateTime.now().formatDate(pattern: "MMM")})",
                                    style: TextStyle(
                                      color: Colors.black,
                                      height: 1.3,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          ColoredBox(
                            color: myGreyColor,
                            child: Text(
                              "",
                              style: TextStyle(height: 4),
                            ),
                          ),
                          Center(
                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text:
                                        "${dashboard.yearlyLeavesTaken ?? 0}/${dashboard.yearlyLeavesRemaining ?? 0}\n",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                    ),
                                  ),
                                  TextSpan(
                                    text: "availed/remaining\n",
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: textSecondary,
                                    ),
                                  ),
                                  TextSpan(
                                    text:
                                        "Leaves : (${DateTime.now().formatDate(pattern: "yyyy")})",
                                    style: TextStyle(
                                      color: Colors.black,
                                      height: 1.3,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ]),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  /// Custom-Method to get Dashboard-Data (Endpoint=>users/dashboard-details)
  Future<Dashboard> _getDashboardData() async {
    Dashboard dashboard;
    try {
      Response response =
          await MyApi.getDataWithAuthorization("users/dashboard-details");
      print("debug TsoHomeScreen DashBoardResponse = ${response.data}");
      if (response.data['code'] == 401 || response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.data['code'] == 200) {
        dashboard = Dashboard.fromJson(response.data["data"]);
        setState(() {
          _userRecentAttendancePhoto = dashboard.recentAttendancePhoto;
          // _notice = dashboard.mostRecentNotice;
        });
        SharedPreferences.getInstance().then((value) {
          value.setDouble(
              "sick_leaves_available", dashboard.sickLeavesAvailable);
          value.setDouble(
              "casual_leaves_available", dashboard.casualLeavesAvailable);
          if (dashboard.store != null) {
            value.setString("store_id", dashboard.store.id);
          }
        });
      } else {
        throw Future.error("Error from Future");
      }
    } on SocketException {
      print("debug TsoHomeScreen DashBoard SocketException");
      throw Future.error(SocketException);
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage

          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        }
      }
      print("debug TsoHomeScreen DashBoard Exception = $error");
      throw Future.error(error);
    }
    return dashboard;
  }

  /// Custom-Method to get Today's-Visit for TSO (Endpoint=>users/outside-pjp-attendance)
  Future<List<StoreVisit>> _getTodaysStoreVisits() async {
    List<StoreVisit> storeVisits = [];
    try {
      Response response =
          await MyApi.getDataWithAuthorization("users/outside-pjp-attendance");
      print("debug TSOHomeScreen StoreVisitResponse = ${response.data}");
      if (response.data["code"] == 401 || response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.data["code"] == 200) {
        for (var storeVisit in response.data["data"]) {
          storeVisits.add(StoreVisit.fromJson(storeVisit));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("debug TSOHomeScreen StoreVisit SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      print("debug TSOHomeScreen StoreVisitResponse Exception = $error");
      return Future.error(error);
    }
    return storeVisits;
  }

  Widget createTodaysVisitsListCard(BuildContext context) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.fromLTRB(4, 4, 4, subMargin),
      color: Colors.white,
      child: Column(
        children: [
//> Mark Attendance ListTile
          ListTile(
            title: Text(
              "Select store and mark attendance.",
              style: TextStyle(
                color: textPrimary,
                // fontWeight: FontWeight.bold,
              ),
            ),
            trailing: Icon(Icons.arrow_forward),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => UnlistedMarkAttendanceScreen(),
                ),
              ).then(
                (value) {
                  if (value != null) {
                    showToast(value,
                        context: context,
                        animation: StyledToastAnimation.slideFromBottom,
                        reverseAnimation: StyledToastAnimation.slideToBottom,
                        startOffset: Offset(0.0, 3.0),
                        reverseEndOffset: Offset(0.0, 3.0),
                        position: StyledToastPosition.bottom,
                        duration: Duration(seconds: 3),
                        animDuration: Duration(seconds: 1),
                        curve: Curves.elasticOut,
                        reverseCurve: Curves.fastOutSlowIn);
                    setState(() {
                      _futureDashboard = _getDashboardData();
                      _futureTodaysStoreVisits = _getTodaysStoreVisits();
                    });
                  }
                },
              );
            },
          ),
          FutureBuilder<List<StoreVisit>>(
            future: _futureTodaysStoreVisits,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                //> ConnectionState.waiting
                return Center(
                  child: Container(
                    margin: EdgeInsets.all(subMargin),
                    width: 30,
                    height: 30,
                    child: CircularProgressIndicator(),
                  ),
                );
              } else if (snapshot.hasData) {
//< snapshot.hasData
                return Column(
                  children: snapshot.data.reversed.map((val) {
                    return TSOTodaysVisitsCard(
                      val,
                      () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => TSOSalesForm(val.storeId),
                          ),
                        ).then(
                          (value) {
                            print(
                                "debug TSOHomeScreen Navigator returned = $value");
                            if (value != null) {
                              showToast(value,
                                  context: context,
                                  animation:
                                      StyledToastAnimation.slideFromBottom,
                                  reverseAnimation:
                                      StyledToastAnimation.slideToBottom,
                                  startOffset: Offset(0.0, 3.0),
                                  reverseEndOffset: Offset(0.0, 3.0),
                                  position: StyledToastPosition.bottom,
                                  duration: Duration(seconds: 3),
                                  backgroundColor: success,
                                  animDuration: Duration(seconds: 1),
                                  curve: Curves.elasticOut,
                                  reverseCurve: Curves.fastOutSlowIn);
                              setState(() {
                                _futureDashboard = _getDashboardData();
                              });
                            }
                          },
                        );
                      },
                    );
                  }).toList(),
                );
              } else if (snapshot.hasError) {
//~ snapshot.hasError
                print(
                    "debug TSOHomeScreen StoreVisit FutureError = ${snapshot.error}");
                if (snapshot.error.toString().contains("401")) {
                  //> case: 401 Unauthenticated User
                  //> Clear the localStorage
                  SharedPreferences.getInstance().then((localStorage) {
                    localStorage.remove('token');
                  });

                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                        builder: (context) => LoginScreen(),
                      ),
                      (route) => false);
                } else if (snapshot.error.toString().contains("404")) {
                  //> case: 404 No Data Found
                  return ListTile(title: Text("No Visits found!"));
                }
                return ListTile(
                  title: Text(
                    "Something went Wrong!",
                    style: TextStyle(color: error),
                  ),
                  trailing: FlatButton(
                    onPressed: () {
                      setState(() {
                        _futureTodaysStoreVisits = _getTodaysStoreVisits();
                      });
                    },
                    child: Text("Retry"),
                  ),
                );
              }
              return SizedBox.shrink();
            },
          ),
        ],
      ),
    );
  }

  Future<void> callnow(String phoneNumber, BuildContext context) async {
    if (await canLaunch('tel:$phoneNumber')) {
      await launch('tel:$phoneNumber');
    } else {
      Scaffold.of(context)
          .showSnackBar(createSnackBar(msg: "Call Not Possible!"));
    }
  }

  Future<void> sendEmail(String email, BuildContext context) async {
    if (await canLaunch('mailto:$email')) {
      await launch('mailto:$email');
    } else {
      Scaffold.of(context)
          .showSnackBar(createSnackBar(msg: "Can't send Email now!"));
    }
  }
}

/// A card for Quick-Access to forms.
// Widget _createQuickActions() {
//   return Card(
//     elevation: cardElevation,
//     shape: RoundedRectangleBorder(
//         borderRadius: BorderRadius.circular(cardCornerRadius)),
//     color: Colors.white,
//     child: Padding(
//       padding: EdgeInsets.only(bottom: cardContentPadding),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Padding(
//             padding: EdgeInsets.symmetric(
//                 horizontal: subMargin, vertical: subMarginHalf),
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.start,
//               children: [
//                 Text(
//                   "Quick Actions:",
//                   style: TextStyle(
//                     color: textPrimary,
//                     fontWeight: FontWeight.w500,
//                     fontSize: 16,
//                   ),
//                 ),
//                 // _totalSurveys != null
//                 //     ? Text(
//                 //         "Total Surveys(${DateFormat("MMM").format(DateTime.now())}): ${_totalSurveys ?? 0}")
//                 //     : SizedBox.shrink(),
//               ],
//             ),
//           ),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             mainAxisSize: MainAxisSize.max,
//             children: [
//               Column(
//                 children: [
//                   IconButton(
//                     onPressed: () {
//                       Navigator.push(
//                         context,
//                         MaterialPageRoute(
//                           builder: (context) => LeaveForm(),
//                         ),
//                       );
//                     },
//                     icon: Image.asset("assets/images/calendar.png"),
//                     iconSize: 50,
//                   ),
//                   Text(
//                     "Leave",
//                     style: TextStyle(
//                       color: textPrimary,
//                       fontSize: 12,
//                       fontWeight: FontWeight.w600,
//                     ),
//                   ),
//                 ],
//               ),
//               // Column(
//               //   children: [
//               //     IconButton(
//               //       onPressed: () {
//               //         Navigator.push(
//               //           context,
//               //           MaterialPageRoute(
//               //             builder: (context) => SurveyForm(),
//               //           ),
//               //         ).then(
//               //           (value) {
//               //             if (value != null) {
//               //                showToast(value,
//               //                   context: context,
//               //                   animation:
//               //                       StyledToastAnimation.slideFromBottom,
//               //                   reverseAnimation:
//               //                       StyledToastAnimation.slideToBottom,
//               //                   startOffset: Offset(0.0, 3.0),
//               //                   reverseEndOffset: Offset(0.0, 3.0),
//               //                   position: StyledToastPosition.bottom,
//               //                   duration: Duration(seconds: 3),
//               //                   backgroundColor: success,
//               //                   animDuration: Duration(seconds: 1),
//               //                   curve: Curves.elasticOut,
//               //                   reverseCurve: Curves.fastOutSlowIn);
//               //               setState(() {
//               //                 _futureDashboard = _getDashboardData();
//               //               });
//               //             }
//               //           },
//               //         );
//               //       },
//               //       icon: Image.asset("assets/images/survey.png"),
//               //       iconSize: 50,
//               //     ),
//               //     Text(
//               //       "Survey",
//               //       style: TextStyle(
//               //         color: textPrimary,
//               //         fontSize: 12,
//               //         fontWeight: FontWeight.w600,
//               //       ),
//               //     ),
//               //   ],
//               // ),
//               Column(
//                 children: [
//                   IconButton(
//                     onPressed: () {
//                       Navigator.push(
//                         context,
//                         MaterialPageRoute(
//                           builder: (context) => TSOSalesForm(),
//                         ),
//                       ).then(
//                         (value) {
//                           print(
//                               "debug TSOHomeScreen Navigator returned = $value");
//                           if (value != null) {
//                             showToast(value,
//                                 context: context,
//                                 animation:
//                                     StyledToastAnimation.slideFromBottom,
//                                 reverseAnimation:
//                                     StyledToastAnimation.slideToBottom,
//                                 startOffset: Offset(0.0, 3.0),
//                                 reverseEndOffset: Offset(0.0, 3.0),
//                                 position: StyledToastPosition.bottom,
//                                 duration: Duration(seconds: 3),
//                                 backgroundColor: success,
//                                 animDuration: Duration(seconds: 1),
//                                 curve: Curves.elasticOut,
//                                 reverseCurve: Curves.fastOutSlowIn);
//                             setState(() {
//                               _futureDashboard = _getDashboardData();
//                             });
//                           }
//                         },
//                       );
//                     },
//                     icon: Image.asset("assets/images/sales.png"),
//                     iconSize: 50,
//                   ),
//                   Text(
//                     "Sales",
//                     style: TextStyle(
//                       color: textPrimary,
//                       fontSize: 12,
//                       fontWeight: FontWeight.w600,
//                     ),
//                   ),
//                 ],
//               ),
//               Column(
//                 children: [
//                   IconButton(
//                     onPressed: () {
//                       Navigator.push(
//                         context,
//                         MaterialPageRoute(
//                           builder: (context) => TSOUpdateStockForm(),
//                         ),
//                       );
//                     },
//                     icon: Image.asset("assets/images/inventory.png"),
//                     iconSize: 50,
//                   ),
//                   Text(
//                     "Update Stock",
//                     style: TextStyle(
//                       color: textPrimary,
//                       fontSize: 12,
//                       fontWeight: FontWeight.w600,
//                     ),
//                   ),
//                 ],
//               ),
//             ],
//           ),
//         ],
//       ),
//     ),
//   );
// }
