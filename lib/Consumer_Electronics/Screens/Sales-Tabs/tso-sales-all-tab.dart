import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/my-date-picker.dart';
import 'package:market_express/Consumer_Electronics/Components/List-Items/tso-sales-card.dart';
import 'package:market_express/Consumer_Electronics/Models/tso-sales.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TSOSalesAllTab extends StatefulWidget {
  @override
  _TSOSalesAllTabState createState() => _TSOSalesAllTabState();
}

class _TSOSalesAllTabState extends State<TSOSalesAllTab>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  Future<List<TsoSales>> _futureSales;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _selectedDateTime = _todaysDateTime;
    _futureSales = _getUserSalesData();
  }

  DateTime _todaysDateTime = DateTime.now();
  DateTime _selectedDateTime;
  Size screenSize;

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    super.build(context);
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: () {
        setState(() {
          _futureSales = _getUserSalesData();
        });
        return _futureSales;
      },
      child: Column(
        children: [
//> Date Selector
          GestureDetector(
            onTap: () => _showMyDatePicker(context),
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: subMargin,
                vertical: subMargin,
              ),
              color: dropDownBackground,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    _selectedDateTime.formatDate(pattern: "dd MMMM, yyyy"),
                    style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.w600,
                      color: Colors.grey[600],
                    ),
                  ),
                  Icon(
                    Icons.date_range,
                    color: Colors.grey[600],
                  ),
                ],
              ),
            ),
          ),
          FutureBuilder<List<TsoSales>>(
            future: _futureSales,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                _refreshIndicatorKey.currentState?.show();
                return _buildSalesListView(snapshot.data);
              } else if (snapshot.hasError) {
                if (snapshot.error.toString().contains("404")) {
                  return buildEmptyWidget(screenSize, text: "No Sales Found!");
                }
                return buildErrorWidget(screenSize);
              } else if (snapshot.hasData &&
                  snapshot.connectionState == ConnectionState.done &&
                  snapshot.data.length != 0) {
                return _buildSalesListView(snapshot.data);
              }
              return SizedBox.expand(
                child: ListView(),
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _buildSalesListView(List<TsoSales> sales) {
    return Expanded(
      child: ListView.builder(
        itemCount: sales != null ? sales.length : 0,
        itemBuilder: (context, index) => TSOSalesCard(
          sales[index],
        ),
      ),
    );
  }

  Future<List<TsoSales>> _getUserSalesData() async {
    List<TsoSales> sales = [];
    try {
      Response response = await MyApi.postDataWithAuthorization({
        "date": _selectedDateTime.toIso8601String(),
      }, "stores/sales-history");
      print("debug TSOSalesAllTab Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var sale in response.data["data"]) {
          sales.add(TsoSales.fromJson(sale));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("debug TSOSalesAllTab SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      print("debug TSOSalesAllTab Exception = $error");
      return Future.error(error);
    }
    return sales;
  }

  void _showMyDatePicker(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        barrierColor: Colors.black.withOpacity(.3),
        builder: (context) {
          return WillPopScope(
            onWillPop: () async {
              return true;
            },
            child: Dialog(
              insetPadding: EdgeInsets.all(mainMargin),
              child: MyDatePicker(
                enableRange: false,
                enablePastDates: true,
                initialSelectedDate: _selectedDateTime,
              ),
            ),
          );
        }).then((value) {
      if (value != null) {
        setState(() {
          _selectedDateTime = value[0];
          _futureSales = _getUserSalesData();
        });
      }
    });
  }
}
