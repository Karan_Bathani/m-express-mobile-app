import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Consumer_Electronics/Components/List-Items/tso-store-card.dart';
import 'package:market_express/Consumer_Electronics/Screens/ce-ohs-status-screen.dart';
import 'package:market_express/Consumer_Electronics/Screens/ce-sales-screen.dart';
import 'package:market_express/Models/store.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CEStoresTab extends StatefulWidget {
  final bool isStockRoute;
  CEStoresTab({this.isStockRoute = false});
  @override
  _CEStoresTabState createState() => _CEStoresTabState();
}

class _CEStoresTabState extends State<CEStoresTab>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  Future<List<Store>> _futureStores;
  List<Store> _storesList;
  List<Store> _filteredList;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _futureStores = _getStores();
  }

  Size screenSize;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    screenSize = MediaQuery.of(context).size;
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: () {
        setState(() {
          _futureStores = _getStores();
        });
        return _futureStores;
      },
      child: Column(
        children: [
// Filter Text-Box
          // Container(
          //   margin: EdgeInsets.symmetric(
          //       horizontal: subMargin, vertical: subMarginHalf),
          //   height: 50,
          //   child: Center(
          //     child: TextField(
          //       textAlignVertical: TextAlignVertical.center,
          //       decoration: InputDecoration(
          //         border: OutlineInputBorder(
          //           borderSide: BorderSide(
          //             color: Colors.grey[600],
          //           ),
          //         ),
          //         hintText: "Filter by name...",
          //       ),
          //       onChanged: (string) {
          //         setState(() {
          //           _filteredList = _storesList
          //               .where((element) => (element.storeName
          //                           .toLowerCase()
          //                           .contains(string.toLowerCase()) ||
          //                       element.region
          //                           .toLowerCase()
          //                           .contains(string.toLowerCase())
          //                   // || element.storeType
          //                   //     .toLowerCase()
          //                   //     .contains(string.toLowerCase())
          //                   ))
          //               .toList();
          //         });
          //       },
          //     ),
          //   ),
          // ),
          FutureBuilder<List<Store>>(
            future: _futureStores,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                _refreshIndicatorKey.currentState?.show();
                return _buildModelsListView(_filteredList);
              } else if (snapshot.hasError) {
                if (snapshot.error.toString().contains("404")) {
                  return buildEmptyWidget(screenSize,
                      text: "No Store(s) Found!");
                }
                return buildErrorWidget(screenSize);
              } else if (snapshot.hasData &&
                  snapshot.connectionState == ConnectionState.done &&
                  snapshot.data.length != 0) {
                return _buildModelsListView(_filteredList);
              }
              return SizedBox.expand(
                child: ListView(),
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _buildModelsListView(List<Store> stores) {
    return Expanded(
      child: ListView.builder(
        itemCount: stores != null ? stores.length : 0,
        itemBuilder: (context, index) => InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) {
                if (widget.isStockRoute) {
                  return CEOHSStatusScreen(
                    title: stores[index].storeName,
                    id: stores[index].id,
                    searchFor: "store",
                  );
                }
                return CESalesScreen(
                  title: stores[index].storeName,
                  id: stores[index].id,
                  searchFor: "store",
                );
              }),
            );
          },
          child: TSOStoreCard(stores[index]),
        ),
      ),
    );
  }

  Future<List<Store>> _getStores() async {
    List<Store> stores = [];
    try {
      /// this will return the stores that the TSO has visited.
      Response response = await MyApi.getDataWithAuthorization(
          "stores/get-stores-by-user-type");
      print("debug TSOSalesStoresTab Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var sale in response.data["data"]) {
          stores.add(Store.fromJson(sale));
        }
        _storesList = stores;
        _filteredList = _storesList;
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("debug TSOSalesStoresTab SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      print("debug TSOSalesStoresTab Exception = $error");
      return Future.error(error);
    }
    return stores;
  }
}
