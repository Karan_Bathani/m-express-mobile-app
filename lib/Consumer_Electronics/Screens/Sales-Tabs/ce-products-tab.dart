import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Consumer_Electronics/Screens/Product-Category-Tabs/tso-product-tab.dart';
import 'package:market_express/Consumer_Electronics/Screens/ce-ohs-status-screen.dart';
import 'package:market_express/Consumer_Electronics/Screens/ce-sales-screen.dart';
import 'package:market_express/Models/product-suggestion.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CEProductsTab extends StatefulWidget {
  final bool isStockRoute;
  CEProductsTab({this.isStockRoute = false});
  @override
  _CEProductsTabState createState() => _CEProductsTabState();
}

class _CEProductsTabState extends State<CEProductsTab>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  Future<List<ProductSuggestion>> _futureCEProducts;

  @override
  void initState() {
    super.initState();
    _futureCEProducts = _getCEProducts();
  }

  Size screenSize;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    screenSize = MediaQuery.of(context).size;
    return DefaultTabController(
      length: 5,
      child: RefreshIndicator(
        onRefresh: () {
          setState(() {
            _futureCEProducts = _getCEProducts();
          });
          return _futureCEProducts;
        },
        child: FutureBuilder<List<ProductSuggestion>>(
          future: _futureCEProducts,
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              if (snapshot.error.toString().contains("404")) {
                return buildEmptyWidget(screenSize, text: "No Data Found!");
              }
              return buildErrorWidget(screenSize);
            } else if (snapshot.hasData &&
                snapshot.connectionState == ConnectionState.done &&
                snapshot.data.length != 0) {
              return _buildProductTabs(snapshot.data);
            } else if (snapshot.connectionState == ConnectionState.waiting) {
              return _buildProductTabs(snapshot.data);
            }
            return SizedBox.expand(
              child: ListView(),
            );
          },
        ),
      ),
    );
  }

  Widget _buildProductTabs(List<ProductSuggestion> sales) {
    if (sales == null || sales.length == 0) {
      return SizedBox.expand(
        child: ListView(
          children: [
            SizedBox(height: screenSize.width / 2),
            Center(
              child: CircularProgressIndicator(),
            )
          ],
        ),
      );
    }
    return Column(
      children: [
        SizedBox(height: subMarginHalf),
        Container(
          width: screenSize.width,
          height: 35,
          margin: EdgeInsets.symmetric(horizontal: subMargin),
          child: TabBar(
            indicatorPadding: EdgeInsets.all(8),
            labelColor: Colors.white,
            unselectedLabelColor: colorPrimary,
            indicator: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
              color: colorPrimary,
            ),
            tabs: [
              Tab(child: FittedBox(child: Text("TV"))),
              Tab(child: FittedBox(child: Text("REF"))),
              Tab(child: FittedBox(child: Text("RAC"))),
              Tab(child: FittedBox(child: Text("WM"))),
              Tab(child: FittedBox(child: Text("MWO"))),
            ],
          ),
        ),
        SizedBox(height: subMarginHalf),
        Expanded(
          child: TabBarView(
            children: [
              _buildListView(
                  sales.where((element) => element.category == "TV").toList()),
              _buildListView(
                  sales.where((element) => element.category == "REF").toList()),
              _buildListView(
                  sales.where((element) => element.category == "RAC").toList()),
              _buildListView(
                  sales.where((element) => element.category == "WM").toList()),
              _buildListView(
                  sales.where((element) => element.category == "MWO").toList()),
            ],
          ),
        ),
      ],
    );
  }

  /// Builds Listview based products passed according to the Category.
  Widget _buildListView(List<ProductSuggestion> ceProducts) {
    return RefreshIndicator(
      onRefresh: () {
        setState(() {
          _futureCEProducts = _getCEProducts();
        });
        return _futureCEProducts;
      },
      child: ListView.builder(
        padding: EdgeInsets.symmetric(vertical: subMarginHalf),
        itemCount: ceProducts.length,
        itemBuilder: (context, index) {
          return Card(
            elevation: cardElevation,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(cardCornerRadius)),
            color: Colors.white,
            clipBehavior: Clip.antiAlias,
            margin: EdgeInsets.symmetric(
              horizontal: subMargin,
              vertical: subMarginHalf - 2,
            ),
            child: ListTile(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  if (widget.isStockRoute) {
                    return CEOHSStatusScreen(
                      title: ceProducts[index].deviceModel,
                      id: ceProducts[index].id,
                      searchFor: "product",
                    );
                  }
                  return CESalesScreen(
                    title: ceProducts[index].deviceModel,
                    id: ceProducts[index].id,
                    searchFor: "product",
                  );
                }));
              },
              contentPadding: EdgeInsets.only(left: cardContentPadding),
              title: Text(
                ceProducts[index].deviceModel,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  letterSpacing: 1,
                ),
              ),
              trailing: Container(
                height: 100,
                width: 60,
                color: getRandomColor(),
                child: Icon(
                  Icons.arrow_forward,
                  color: Colors.white,
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Future<List<ProductSuggestion>> _getCEProducts() async {
    List<ProductSuggestion> products = [];
    try {
      Response response = await MyApi.getDataWithAuthorization("products/list",
          queryParams: {"business_unit": "ce_ops"});
      print("debug TSOSalesProductsTab Response = ${response.data}");
      if (response.statusCode == 401) {
//> Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var sale in response.data["data"]) {
          products.add(ProductSuggestion.fromJson(sale));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("debug TSOSalesProductsTab SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//> Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      print("debug TSOSalesProductsTab Exception = $error");
      return Future.error(error);
    }
    return products;
  }
}
