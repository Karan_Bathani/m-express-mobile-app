import 'package:flutter/material.dart';
import 'package:market_express/Consumer_Electronics/Screens/Sales-Tabs/ce-stores-tab.dart';
import 'package:market_express/Utility/constants.dart';

import 'Sales-Tabs/ce-products-tab.dart';

class TSOOnHandStockScreen extends StatefulWidget {
  @override
  _TSOOnHandStockScreenState createState() => _TSOOnHandStockScreenState();
}

class _TSOOnHandStockScreenState extends State<TSOOnHandStockScreen> {
  Size screenSize;
  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return DefaultTabController(
      initialIndex: 0,
      length: 2,
      child: Scaffold(
        backgroundColor: colorPrimary,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: Text(
            "Stock Report",
            style: TextStyle(color: Colors.white),
          ),
          bottom: TabBar(
            indicatorColor: colorSecondary,
            indicatorSize: TabBarIndicatorSize.label,
            indicatorWeight: 4,
            tabs: [
              Tab(
                text: "Products",
              ),
              Tab(
                text: "Stores",
              ),
            ],
          ),
        ),
        body: SafeArea(
          child: Container(
            clipBehavior: Clip.antiAlias,
            width: screenSize.width,
            height: screenSize.height,
            decoration: BoxDecoration(
              color: greyBackground,
              borderRadius:
                  BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
            ),
            child: TabBarView(
              children: [
                CEProductsTab(isStockRoute: true),
                CEStoresTab(isStockRoute: true),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
