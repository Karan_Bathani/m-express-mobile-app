import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/ohs-card.dart';
import 'package:market_express/Components/my-date-picker.dart';
import 'package:market_express/Models/on-hand-stock.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CEOHSStatusScreen extends StatefulWidget {
  final String title;
  final String id;
  final String searchFor;
  CEOHSStatusScreen(
      {@required this.title, @required this.id, @required this.searchFor});

  @override
  _CEOHSStatusScreenState createState() => _CEOHSStatusScreenState();
}

class _CEOHSStatusScreenState extends State<CEOHSStatusScreen> {
  Future<List<OnHandStock>> _futureOnHandStock;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _selectedDateTime = todaysDateTime;
    _futureOnHandStock = _getOnHandStocks();
  }

  DateTime todaysDateTime = DateTime.now();
  DateTime _selectedDateTime;
  Size screenSize;

  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: FittedBox(
          child: Text(
            widget.title ?? "",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
      body: Container(
        clipBehavior: Clip.antiAlias,
        width: screenSize.width,
        height: screenSize.height,
        decoration: BoxDecoration(
          color: greyBackground,
          borderRadius:
              BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
        ),
        child: RefreshIndicator(
          key: _refreshIndicatorKey,
          onRefresh: () {
            setState(() {
              _futureOnHandStock = _getOnHandStocks();
            });
            return _futureOnHandStock;
          },
          child: Column(
            children: [
// Date Selector
              GestureDetector(
                onTap: () => _showMyDatePicker(context),
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: subMargin,
                    vertical: subMargin,
                  ),
                  color: dropDownBackground,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        _selectedDateTime.formatDate(pattern: "dd MMMM, yyyy"),
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w600,
                          color: Colors.grey[600],
                        ),
                      ),
                      Icon(
                        Icons.date_range,
                        color: Colors.grey[600],
                      ),
                    ],
                  ),
                ),
              ),
              FutureBuilder<List<OnHandStock>>(
                future: _futureOnHandStock,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    _refreshIndicatorKey.currentState?.show();
                    return _buildOnHandStockList(snapshot.data);
                  } else if (snapshot.hasError) {
                    if (snapshot.error.toString().contains("404")) {
                      return buildEmptyWidget(screenSize,
                          text: "No Data Found");
                    }
                    return buildErrorWidget(screenSize);
                  } else if (snapshot.hasData &&
                      snapshot.connectionState == ConnectionState.done &&
                      snapshot.data.length != 0) {
                    return _buildOnHandStockList(snapshot.data);
                  }
                  return Expanded(
                    child: ListView(),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildOnHandStockList(List<OnHandStock> onHandStocks) {
    return Expanded(
      child: ListView.builder(
        itemCount: onHandStocks != null ? onHandStocks.length : 0,
        itemBuilder: (context, index) => OHSCard(
          onHandStocks[index],
          widget.searchFor,
        ),
      ),
    );
  }

  Future<List<OnHandStock>> _getOnHandStocks() async {
    List<OnHandStock> onHandStocks = [];
    try {
      Response response = await MyApi.postDataWithAuthorization({
        "date": _selectedDateTime.toIso8601String(),
        "search_for": widget.id,
        "search_field": widget.searchFor,
      }, "stores/inventory-history");
      print("debug TSOOHSStatusScreen Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var onHandStock in response.data["data"]) {
          onHandStocks.add(OnHandStock.fromJson(onHandStock));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("debug TSOOHSStatusScreen SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      print("debug TSOOHSStatusScreen Exception = $error");
      throw Future.error(error);
    }
    return onHandStocks;
  }

  void _showMyDatePicker(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        barrierColor: Colors.black.withOpacity(.3),
        builder: (context) {
          return WillPopScope(
            onWillPop: () async {
              return true;
            },
            child: Dialog(
              insetPadding: EdgeInsets.all(mainMargin),
              child: MyDatePicker(
                enableRange: false,
                enablePastDates: true,
                initialSelectedDate: _selectedDateTime,
              ),
            ),
          );
        }).then((value) {
      if (value != null) {
        setState(() {
          _selectedDateTime = value[0];
          _futureOnHandStock = _getOnHandStocks();
        });
      }
    });
  }
}
