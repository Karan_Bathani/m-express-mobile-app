import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/manager-attendance-card.dart';
import 'package:market_express/Models/manager-attendance.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// This widget is used for TSO|Area Manager who comes under CE Ops.
class CEAttendanceHistoryScreen extends StatefulWidget {
  @override
  _CEAttendanceHistoryScreenState createState() =>
      _CEAttendanceHistoryScreenState();
}

class _CEAttendanceHistoryScreenState extends State<CEAttendanceHistoryScreen> {
  Future<List<ManagerAttendance>> _futureAttendance;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _futureAttendance = _getUserAttendance();
  }

  int totalCount;
  Size screenSize;

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          "My Attendance",
          style: TextStyle(color: Colors.white),
        ),
        actions: [
          totalCount != null
              ? Container(
                  margin: EdgeInsets.only(right: subMarginHalf),
                  height: kToolbarHeight,
                  width: kTextTabBarHeight,
                  child: Center(
                    child: Text(
                      "Total:\n$totalCount",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                )
              : SizedBox.shrink(),
        ],
      ),
      body: SafeArea(
        child: Container(
          clipBehavior: Clip.antiAlias,
          width: screenSize.width,
          height: screenSize.height,
          decoration: BoxDecoration(
            color: greyBackground,
            // Colors.white,
            borderRadius:
                BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
          ),
          child: RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () {
              setState(() {
                _futureAttendance = _getUserAttendance();
              });
              return _futureAttendance;
            },
            child: FutureBuilder<List<ManagerAttendance>>(
              future: _futureAttendance,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  _refreshIndicatorKey.currentState?.show();
                  return _buildAttendanceListView(snapshot.data);
                } else if (snapshot.hasError) {
                  if (snapshot.error.toString().contains("404")) {
                    return buildEmptyWidget(screenSize,
                        text: "No Attendance Found!");
                  }
                  return buildErrorWidget(screenSize);
                }
                if (snapshot.hasData &&
                    snapshot.connectionState == ConnectionState.done &&
                    snapshot.data.length != 0) {
                  return _buildAttendanceListView(snapshot.data);
                }
                return SizedBox.expand(
                  child: ListView(),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildAttendanceListView(List<ManagerAttendance> attendances) {
    return ListView.builder(
      itemCount: attendances != null ? attendances.length : 0,
      itemBuilder: (context, index) =>
          ManagerAttendanceCard(attendances[index]),
    );
  }

  Future<List<ManagerAttendance>> _getUserAttendance() async {
    List<ManagerAttendance> attendances = [];
    try {
      Response response =
          await MyApi.getDataWithAuthorization("attendance/user-attendance");
      print("debug TSOAttendanceHistoryScreen Response = ${response.data}");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var attendance in response.data["data"]) {
          attendances.add(ManagerAttendance.fromJson(attendance));
        }
        // setState(() {
        //   totalCount = attendances.length;
        // });
      } else {
        throw Future.error("Error from Future");
      }
    } on SocketException {
      print("debug TSOAttendanceHistoryScreen SocketException");
      throw Future.error(SocketException);
    } catch (error) {
      print("debug TSOAttendanceHistoryScreen Exception = $error");
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }

      throw Future.error(error);
    }
    return attendances;
  }
}
