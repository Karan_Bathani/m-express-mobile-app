import 'package:flutter/material.dart';
import 'package:market_express/Consumer_Electronics/Screens/Sales-Tabs/tso-sales-all-tab.dart';
import 'package:market_express/Consumer_Electronics/Screens/Sales-Tabs/ce-products-tab.dart';
import 'package:market_express/Consumer_Electronics/Screens/Sales-Tabs/ce-stores-tab.dart';
import 'package:market_express/Utility/constants.dart';

class TSOMySalesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        backgroundColor: colorPrimary,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: Text(
            "My Sales",
            style: TextStyle(color: Colors.white),
          ),
          bottom: TabBar(
            indicatorColor: colorSecondary,
            indicatorSize: TabBarIndicatorSize.label,
            indicatorWeight: 4,
            tabs: [
              Tab(text: "Products"),
              Tab(text: "Stores"),
              Tab(text: "All Sales"),
            ],
          ),
        ),
        body: SafeArea(
          child: Container(
            clipBehavior: Clip.antiAlias,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: greyBackground,
              borderRadius:
                  BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
            ),
            child: TabBarView(
              children: [
                CEProductsTab(),
                CEStoresTab(),
                TSOSalesAllTab(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
