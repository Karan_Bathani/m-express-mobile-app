import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/my-custom-button.dart';
import 'package:market_express/Consumer_Electronics/Providers/tso-selected-products-provider.dart';
import 'package:market_express/Consumer_Electronics/Screens/Product-Category-Tabs/tso-select-product-category-tab.dart';
import 'package:market_express/Models/store.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_autocomplete_formfield/simple_autocomplete_formfield.dart';

class TSOSalesForm extends StatefulWidget {
  final String storeId;
  TSOSalesForm(this.storeId);
  @override
  _TSOSalesFormState createState() => _TSOSalesFormState();
}

class _TSOSalesFormState extends State<TSOSalesForm> {
  // TextEditingController _storesController = TextEditingController();

  bool _autoValidate = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  // String storeId;

  // Future<Response<dynamic>> _futureStores;
  // List<String> stores = [];
  // List<StoreShrinkedData> modelStores = [];

  // @override
  // void initState() {
  //   super.initState();
  //   _futureStores = MyApi.getDataWithAuthorization("stores/get-all-stores");
  // }

  // @override
  // void dispose() {
  //   _storesController.dispose();
  //   super.dispose();
  // }

  Size screenSize;
  FocusScopeNode currentNode;

  @override
  Widget build(BuildContext context) {
    currentNode = FocusScope.of(context);
    screenSize = MediaQuery.of(context).size;
    return ChangeNotifierProvider(
      create: (context) => TSOSelectedProductsProvider(),
      builder: (context, child) => GestureDetector(
        onTap: () {
          currentNode.unfocus();
        },
        child: Scaffold(
          backgroundColor: colorPrimary,
          appBar: AppBar(
            automaticallyImplyLeading: true,
            backgroundColor: Colors.transparent,
            shadowColor: Colors.transparent,
            centerTitle: true,
            title: Text(
              "Sales Form",
              style: TextStyle(color: Colors.white),
            ),
          ),
          bottomNavigationBar: ColoredBox(
            color: greyBackground,
            child: Consumer<TSOSelectedProductsProvider>(
              builder: (context, provider, child) => MyCustomButton(
                enabled: widget.storeId != null &&
                    provider.selectedProducts.length != 0,
                child: Text(
                  provider.selectedProducts.length != 0
                      ? provider.getTotalQuantity() > 1
                          ? "Submit (${provider.getTotalQuantity()} Items)"
                          : "Submit (${provider.getTotalQuantity()} Item)"
                      : "Submit",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                ),
                containerMargin: EdgeInsets.all(subMargin),
                onPressed: () => _submitSalesForm(provider),
              ),
            ),
          ),
          body: DefaultTabController(
            length: 5,
            initialIndex: 0,
            child: SafeArea(
              child: Container(
                clipBehavior: Clip.antiAlias,
                width: screenSize.width,
                height: screenSize.height,
                decoration: BoxDecoration(
                  color: Color(0xfff5f5f5),
                  borderRadius: BorderRadius.vertical(
                      top: Radius.circular(cardCornerRadius)),
                ),
                child: Form(
                  key: _formKey,
                  autovalidate: _autoValidate,
                  child: ListView(
                    padding: EdgeInsets.only(bottom: subMargin),
                    children: [
                      SizedBox(height: subMargin),
//> Select Product Category
                      Container(
                        width: screenSize.width,
                        height: 35,
                        margin: EdgeInsets.symmetric(horizontal: subMargin),
                        child: Consumer<TSOSelectedProductsProvider>(
                          builder: (context, provider, child) => TabBar(
                            indicatorPadding: EdgeInsets.all(8),
                            labelColor: Colors.white,
                            unselectedLabelColor: colorPrimary,
                            isScrollable:
                                provider.getTotalQuantity() > 0 ? true : false,
                            indicator: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: colorPrimary,
                            ),
                            tabs: [
                              Tab(
                                child: FittedBox(
                                  child: provider
                                              .getCategorywiseQuantity("TV") >
                                          0
                                      ? Text(
                                          "TV (${provider.getCategorywiseQuantity("TV")})")
                                      : Text("TV"),
                                ),
                              ),
                              Tab(
                                child: FittedBox(
                                  child: provider
                                              .getCategorywiseQuantity("REF") >
                                          0
                                      ? Text(
                                          "REF (${provider.getCategorywiseQuantity("REF")})")
                                      : Text("REF"),
                                ),
                              ),
                              Tab(
                                child: FittedBox(
                                  child: provider
                                              .getCategorywiseQuantity("RAC") >
                                          0
                                      ? Text(
                                          "RAC (${provider.getCategorywiseQuantity("RAC")})")
                                      : Text("RAC"),
                                ),
                              ),
                              Tab(
                                child: FittedBox(
                                  child: provider
                                              .getCategorywiseQuantity("WM") >
                                          0
                                      ? Text(
                                          "WM (${provider.getCategorywiseQuantity("WM")})")
                                      : Text("WM"),
                                ),
                              ),
                              Tab(
                                child: FittedBox(
                                  child: provider
                                              .getCategorywiseQuantity("MWO") >
                                          0
                                      ? Text(
                                          "MWO (${provider.getCategorywiseQuantity("MWO")})")
                                      : Text("MWO"),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: subMarginHalf),
                      SizedBox(
                        width: screenSize.width,
                        height: screenSize.height * .65 - subMarginHalf,
                        child: TabBarView(
                          children: [
                            TSOSelectProductCategoryTab("TV"),
                            TSOSelectProductCategoryTab("REF"),
                            TSOSelectProductCategoryTab("RAC"),
                            TSOSelectProductCategoryTab("WM"),
                            TSOSelectProductCategoryTab("MWO"),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _submitSalesForm(TSOSelectedProductsProvider provider) async {
    try {
      if (!_formKey.currentState.validate()) {
        _autoValidate = true;
        return;
      }

      List<Map<String, dynamic>> itemsObject = [];
      provider.selectedProducts.values.forEach((element) {
        itemsObject.add({
          "product_id": element.id,
          "quantity": provider.qty[element.id],
        });
      });
      showMyLoadingDialog(dialogText: "Submitting ...\nPlease Wait!");
      Map<String, dynamic> data = {
        "store_id": widget.storeId,
        "sales_items": itemsObject,
      };
      print("debug data = $data");
      Response response =
          await MyApi.postDataWithAuthorization(data, "stores/new-sale-for-ce");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        //<  Submitted Successfully...
        dismissMyDialog();
        print("debug TsoSalesForm Success 200");
        Navigator.pop(context, response.data["message"]);
      } else if (response.statusCode == 422) {
        dismissMyDialog();
        print("debug TsoSalesForm Error 422");
        showToast(response.data["message"],
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
      } else {
        dismissMyDialog();
        print("debug TsoSalesForm Error => Else");
        showToast("Something Went Wrong!",
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
        // Scaffold.of(context)
        //     .showSnackBar(createSnackBar(msg: "Something Went Wrong!"));
      }
    } on SocketException {
      dismissMyDialog();
      print("debug TsoSalesForm Exception = $error");
      showToast("You are offline, Check your Internet Connection!",
          context: context,
          animation: StyledToastAnimation.slideFromBottom,
          reverseAnimation: StyledToastAnimation.slideToBottom,
          startOffset: Offset(0.0, 3.0),
          reverseEndOffset: Offset(0.0, 3.0),
          position: StyledToastPosition.bottom,
          duration: Duration(seconds: 3),
          animDuration: Duration(seconds: 1),
          curve: Curves.elasticOut,
          reverseCurve: Curves.fastOutSlowIn);
    } catch (e) {
      dismissMyDialog();
      if (e is DioError) {
        if (e.response.statusCode == 422) {
          print("debug TsoSalesForm Dio Error 422 = $e");
          print("debug TsoSalesForm Dio Error 422 = ${e.response.data}");
          showToast(e.response.data["message"].toString(),
              context: context,
              animation: StyledToastAnimation.slideFromBottom,
              reverseAnimation: StyledToastAnimation.slideToBottom,
              startOffset: Offset(0.0, 3.0),
              reverseEndOffset: Offset(0.0, 3.0),
              position: StyledToastPosition.bottom,
              duration: Duration(seconds: 4),
              backgroundColor: error,
              animDuration: Duration(seconds: 1),
              curve: Curves.elasticOut,
              reverseCurve: Curves.fastOutSlowIn);
        }
        print("debug TsoSalesForm Dio Error = ${e.response.data}");
        showToast(e.response.data["message"],
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 4),
            backgroundColor: error,
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
      } else {
        print("debug TsoSalesForm Exception = $e");
        showToast("Something Went Wrong!",
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 4),
            backgroundColor: error,
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
        // Scaffold.of(context)
        //     .showSnackBar(createSnackBar(msg: "Something Went Wrong!"));
      }
    }
  }
}

//> endpoint => stores/get-all-stores
// FutureBuilder<Response<dynamic>>(
//   future: _futureStores,
//   builder: (context, snapshot) {
//     if (snapshot.hasError) {
//       if (snapshot.error == SocketException ||
//           snapshot.error
//               .toString()
//               .contains("SocketException")) {
//         Future.delayed(Duration(milliseconds: 500))
//             .then((value) {
//           Scaffold.of(context)
//               .showSnackBar(createSnackBar());
//         });
//       }
//     } else if (snapshot.hasData) {
//       if (stores != null && stores.length != 0) {
//         stores.clear();
//         modelStores.clear();
//       }
//       snapshot.data.data["data"].forEach((element) {
//         // print("$Sales_Form $element ");
//         // print("$Sales_Form ${SalesProductSuggestion.fromJson(element).deviceVarient}");
//         stores.add(StoreShrinkedData.fromJson(element)
//             .storeName);
//         modelStores
//             .add(StoreShrinkedData.fromJson(element));
//       });
//     }
//     return Padding(
//       padding:
//           EdgeInsets.symmetric(horizontal: subMargin),
//       child:
//           SimpleAutocompleteFormField<StoreShrinkedData>(
//         controller: _storesController,
//         validator: (value) {
//           if (_storesController.text == null ||
//               _storesController.text.isEmpty ||
//               _storesController.text.length == 0) {
//             return "Please select a store!";
//           } else if (!stores
//               .contains(_storesController.text)) {
//             return "Select a store from the list only.";
//           } else if (value != null &&
//               !stores.contains(value.storeName)) {
//             return "Select a store from the list only.";
//           } else if (value != null) {
//             int tempIndex = modelStores.indexWhere(
//                 (element) =>
//                     element.storeName == value.storeName);
//             storeId = modelStores[tempIndex].storeId;
//           }
//           return null;
//         },
//         keyboardType: TextInputType.text,
//         style: TextStyle(
//           color: textPrimary,
//           fontWeight: FontWeight.w600,
//         ),
//         decoration: InputDecoration(
//           border: OutlineInputBorder(
//             borderSide: BorderSide(
//               color: Colors.grey[600],
//             ),
//           ),
//           labelText: 'Select Store',
//           // hintText: 'Enter the purchased device model',
//         ),
//         onChanged: (value) {
//           setState(() {
//             if (value == null) {
//               storeId = null;
//             } else if (value != null) {
//               if (value.storeName != null) {
//                 storeId = value.storeId;
//               } else {
//                 storeId = null;
//               }
//             }
//           });
//         },
//         itemToString: (item) =>
//             item == null ? "" : item.storeName,
//         onSearch: (search) async {
//           if ((stores.isEmpty || stores.length == 0) &&
//               snapshot.connectionState ==
//                   ConnectionState.done) {
//             if (!mounted) {
//               setState(() {
//                 _futureStores =
//                     MyApi.getDataWithAuthorization(
//                         "stores/get-all-stores");
//               });
//             }
//           }
//           return modelStores
//               .where(
//                 (element) => element.storeName
//                     .toLowerCase()
//                     .contains(
//                       search.toLowerCase(),
//                     ),
//               )
//               .toList();
//         },
//         itemBuilder: (context, item) {
//           return Padding(
//             padding: EdgeInsets.all(12),
//             child: Text(item.storeName),
//           );
//         },
//         maxSuggestions:
//             modelStores != null && modelStores.length != 0
//                 ? modelStores.length
//                 : 0,
//         suggestionsBuilder: (context, items) {
//           print(
//               "ManagerMarkAttendanceScreen Snap = $snapshot");
//           if (snapshot.connectionState ==
//               ConnectionState.waiting) {
//             return Card(
//               elevation: cardElevation,
//               shape: RoundedRectangleBorder(
//                   borderRadius: BorderRadius.circular(
//                       cardCornerRadius)),
//               color: Colors.white,
//               clipBehavior: Clip.antiAlias,
//               margin: EdgeInsets.symmetric(
//                 // horizontal: subMargin,
//                 vertical: subMarginHalf,
//               ),
//               child: Center(
//                 child: Container(
//                   margin: EdgeInsets.all(subMargin),
//                   child: CircularProgressIndicator(),
//                 ),
//               ),
//             );
//           } else if (snapshot.hasError) {
//             return Card(
//               elevation: cardElevation,
//               shape: RoundedRectangleBorder(
//                   borderRadius: BorderRadius.circular(
//                       cardCornerRadius)),
//               color: Colors.white,
//               clipBehavior: Clip.antiAlias,
//               margin: EdgeInsets.symmetric(
//                 // horizontal: subMargin,
//                 vertical: subMarginHalf,
//               ),
//               child: Center(
//                 child: Container(
//                   margin: EdgeInsets.all(subMargin),
//                   child: snapshot.error
//                           .toString()
//                           .contains("404")
//                       ? Text("No Stores Found!")
//                       : Text("Something Went Wrong!"),
//                 ),
//               ),
//             );
//           }
//           return Card(
//             elevation: cardElevation,
//             shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(
//                     cardCornerRadius)),
//             color: Colors.white,
//             clipBehavior: Clip.antiAlias,
//             margin: EdgeInsets.symmetric(
//               // horizontal: subMargin,
//               vertical: subMarginHalf,
//             ),
//             child: SizedBox(
//               width: double.infinity,
//               height: 200,
//               child: ListView(
//                 children: items,
//               ),
//             ),
//           );
//         },
//       ),
//     );
//   },
// ),
// SizedBox(height: subMargin),
