import 'package:flutter/material.dart';
import 'package:market_express/Components/my-custom-button.dart';
import 'package:market_express/Models/store-visit.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';

class TodaysVisitCard extends StatelessWidget {
  final StoreVisit storeVisit;
  final Function onTap;
  TodaysVisitCard(this.storeVisit, this.onTap);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        storeVisit.storeName ?? "Store Name",
        style: TextStyle(
          color: textPrimary,
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: createVisitStatusCard(storeVisit.time != null ? true : false),
      trailing: storeVisit.time != null
          ? RichText(
              textAlign: TextAlign.center,
              text: TextSpan(children: [
                TextSpan(
                  text: "Visited at:\n",
                  style: TextStyle(
                    color: textSecondary,
                    fontSize: 12,
                  ),
                ),
                TextSpan(
                  text: storeVisit.time.formatDate(pattern: "hh:mm a"),
                  style: TextStyle(
                    color: textPrimary,
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                  ),
                ),
              ]),
            )
          : MyCustomButton(
              containerMargin: EdgeInsets.fromLTRB(
                0,
                12,
                0,
                4,
              ),
              child: FittedBox(
                child: Text(
                  'Mark\n Attendance',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              padding: EdgeInsets.symmetric(
                horizontal: 0,
                vertical: 2,
              ),
              onPressed: onTap,
            ),
    );
  }

  Widget createVisitStatusCard(bool isMarked) {
    return Row(
      children: [
        Card(
          color: isMarked ? success : pending,
          child: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 2.0,
              horizontal: 4,
            ),
            child: Text(
              isMarked ? "visited" : "pending",
              style: TextStyle(
                fontSize: 11,
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
