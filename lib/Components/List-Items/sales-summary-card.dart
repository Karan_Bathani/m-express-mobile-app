import 'package:flutter/material.dart';
import 'package:market_express/Models/sales-summary.dart';
import 'package:market_express/Screens/sales-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';

class SalesSummaryCard extends StatelessWidget {
  final SalesSummary salesSummary;
  final DateTime selectedDateTime;

  SalesSummaryCard(this.salesSummary, this.selectedDateTime);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => SalesScreen(
              selectedDateTime: selectedDateTime,
              userId: salesSummary.userId,
              userName: salesSummary.userName,
            ),
          ),
        );
      },
      child: Card(
          elevation: cardElevation,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(cardCornerRadius)),
          color: Colors.white,
          clipBehavior: Clip.antiAlias,
          margin: EdgeInsets.symmetric(
            horizontal: subMargin,
            vertical: subMarginHalf,
          ),
          child: Table(
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            columnWidths: {
              0: FlexColumnWidth(5),
              1: FlexColumnWidth(1.5),
            },
            children: [
              TableRow(
                decoration: BoxDecoration(color: getRandomColor()),
                children: [
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.all(cardContentPadding),
                    child: Row(
                      children: [
                        CircleAvatar(
                          backgroundImage:
                              NetworkImage(salesSummary.userPhoto ?? ""),
                          radius: 29,
                        ),
                        SizedBox(width: subMarginHalf),
                        Expanded(
                          child: Text(
                            salesSummary.userName,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: textPrimary,
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Text(
                        "Total-Sales",
                        style: TextStyle(
                          color: Colors.white,
                          // fontSize: 16,
                          // fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(height: subMarginHalf / 2),
                      FittedBox(
                        child: Text(
                          salesSummary.salesCount.toString(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 26,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          )),
    );
  }
}
