import 'package:flutter/material.dart';
import 'package:market_express/Models/leave-summary.dart';
import 'package:market_express/Screens/leave-history-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';

class LeaveSummaryCard extends StatelessWidget {
  final LeaveSummary leaveSummary;

  LeaveSummaryCard(this.leaveSummary);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => LeaveHistoryScreen(
              userId: leaveSummary.userId,
              userName: leaveSummary.userName,
            ),
          ),
        );
      },
      child: Card(
        elevation: cardElevation,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(cardCornerRadius)),
        color: Colors.white,
        clipBehavior: Clip.antiAlias,
        margin: EdgeInsets.symmetric(
          horizontal: subMargin,
          vertical: subMarginHalf,
        ),
        child: Table(
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          columnWidths: {
            0: FlexColumnWidth(5.2),
            1: FlexColumnWidth(1.3),
          },
          children: [
            TableRow(
              decoration: BoxDecoration(color: getRandomColor()),
              children: [
                Container(
                  color: Colors.white,
                  padding: EdgeInsets.all(cardContentPadding),
                  child: Row(
                    children: [
                      CircleAvatar(
                        backgroundImage:
                            NetworkImage(leaveSummary.userPhoto ?? ""),
                        radius: 29,
                      ),
                      SizedBox(width: subMarginHalf),
                      Expanded(
                        child: Text(
                          leaveSummary.userName,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: textPrimary,
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Text(
                      "availed",
                      style: TextStyle(
                        color: Colors.white,
                        // fontSize: 16,
                        // fontWeight: FontWeight.w600,
                      ),
                    ),
                    FittedBox(
                      child: Text(
                        leaveSummary.leavesCount.toString(),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 26,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    Text(
                      "day(s)",
                      style: TextStyle(
                        color: Colors.white,
                        // fontSize: 16,
                        // fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
