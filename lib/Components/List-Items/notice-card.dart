import 'package:flutter/material.dart';
import 'package:market_express/Models/notice.dart';
import 'package:market_express/Screens/notification-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';

class NoticeCard extends StatelessWidget {
  final Notice notice;
  final bool shouldHighlight;
  final bool isHomeScreen;
  NoticeCard(this.notice,
      {this.shouldHighlight = false, this.isHomeScreen = false});
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: shouldHighlight ? colorSecondary : Colors.white,
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.symmetric(
        horizontal: isHomeScreen ? 4 : subMargin,
        vertical: isHomeScreen ? 4 : subMarginHalf,
      ),
      child: ListTile(
        onTap: isHomeScreen
            ? () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => NotificationScreen(),
                    ));
              }
            : null,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "${notice.noticeFor}",
              style: TextStyle(
                color: shouldHighlight ? Colors.white : textPrimary,
              ),
            ),
            Text(
              "${notice.date.formatDate(pattern: "dd MMM, yyyy")}",
              style: TextStyle(
                color: shouldHighlight ? Colors.white : colorPrimary,
                fontSize: 14,
                fontWeight: FontWeight.w600,
              ),
            ),
          ],
        ),
        subtitle: Text(
          notice.noticeDescription,
          maxLines: isHomeScreen ? 1 : null,
          overflow: isHomeScreen ? TextOverflow.ellipsis : null,
          style:
              TextStyle(color: shouldHighlight ? Colors.white : textSecondary),
        ),
      ),
    );
  }
}
