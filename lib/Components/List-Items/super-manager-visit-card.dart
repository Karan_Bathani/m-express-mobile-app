import 'package:flutter/material.dart';
import 'package:market_express/Models/super-manager-visit.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';

class SuperManagerVisitCard extends StatelessWidget {
  final String superManager;
  final SuperManagerVisit superManagerVisit;
  SuperManagerVisitCard(this.superManager, this.superManagerVisit);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.symmetric(
        horizontal: subMargin,
        vertical: subMarginHalf,
      ),
      child: ListTile(
        title: superManager == "FOM"
            ? Text(superManagerVisit.territory.join(", "))
            : Text(superManagerVisit.district.join(", ")),
        subtitle: Text(
          superManagerVisit.date.formatDate(pattern: "dd MMMM, yyyy"),
          style: TextStyle(
            color: textSecondary,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
