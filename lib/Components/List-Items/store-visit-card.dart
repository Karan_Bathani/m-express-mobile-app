import 'package:flutter/material.dart';
import 'package:market_express/Components/my-custom-button.dart';
import 'package:market_express/Models/store-visit.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';

class StoreVisitCard extends StatelessWidget {
  final StoreVisit storeVisit;
  StoreVisitCard(this.storeVisit);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        storeVisit.storeName ?? "Store Name",
        style: TextStyle(
          color: textPrimary,
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: Text(
        storeVisit.date.formatDate(pattern: "dd MMMM"),
        style: TextStyle(
          color: storeVisit.date.day % 2 == 0 ? Colors.redAccent : Colors.brown,
          fontWeight: FontWeight.w600,
          // fontSize: 18,
        ),
      ),
      trailing: storeVisit.time == null
          ? Card(
              color: pending,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 2.0,
                  horizontal: 4,
                ),
                child: Text(
                  "pending",
                  style: TextStyle(
                    fontSize: 11,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                ),
              ),
            )
          : RichText(
              textAlign: TextAlign.center,
              text: TextSpan(children: [
                TextSpan(
                  text: "Visited at:\n",
                  style: TextStyle(
                    color: textSecondary,
                    fontSize: 12,
                  ),
                ),
                TextSpan(
                  text: storeVisit.time.formatDate(pattern: "hh:mm a"),
                  // text: "04:43 pm",
                  style: TextStyle(
                    color: textPrimary,
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                  ),
                ),
              ]),
            ),
    );
  }
}
