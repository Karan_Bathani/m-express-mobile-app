import 'package:flutter/material.dart';
import 'package:market_express/Components/my-custom-button.dart';
import 'package:market_express/Models/leave-by-designation.dart';
import 'package:market_express/Providers/leave-provider.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:provider/provider.dart';

class LeaveByDesignationCard extends StatefulWidget {
  final LeaveByDesignation leaveByDeignation;
  // final Function reloadLeaveTab;

  LeaveByDesignationCard(
    this.leaveByDeignation,
    // this.reloadLeaveTab
  );

  @override
  _LeaveByDesignationCardState createState() => _LeaveByDesignationCardState();
}

class _LeaveByDesignationCardState extends State<LeaveByDesignationCard> {
  TextEditingController rejectReasonController =
      TextEditingController(text: "");
  final _rejectReasonFormKey = GlobalKey<FormFieldState>();

  @override
  void dispose() {
    rejectReasonController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.symmetric(
        horizontal: subMargin,
        vertical: subMarginHalf,
      ),
      child: Padding(
        padding: EdgeInsets.all(cardContentPadding),
        child: Column(
          children: [
            Table(
              defaultVerticalAlignment: TableCellVerticalAlignment.middle,
              columnWidths: {
                0: FlexColumnWidth(1.5),
                1: FixedColumnWidth(subMargin),
                2: FlexColumnWidth(5),
              },
              children: [
                TableRow(
                  children: [
                    CircleAvatar(
                      radius: 35,
                      backgroundImage: NetworkImage(
                          widget.leaveByDeignation.recentAttendancePhoto ?? ""),
                    ),
                    SizedBox(width: subMarginHalf),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RichText(
                              text: TextSpan(children: [
                                TextSpan(
                                  text: "From: ",
                                  style: TextStyle(
                                    color: textPrimary,
                                  ),
                                ),
                                TextSpan(
                                  text: widget.leaveByDeignation.startDate
                                      .formatDate(),
                                  style: TextStyle(
                                    color: textSecondary,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ]),
                            ),
                            SizedBox(width: subMargin),
                            RichText(
                              text: TextSpan(children: [
                                TextSpan(
                                  text: "To: ",
                                  style: TextStyle(
                                    color: textPrimary,
                                  ),
                                ),
                                TextSpan(
                                  text: widget.leaveByDeignation.endDate
                                      .formatDate(),
                                  style: TextStyle(
                                    color: textSecondary,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ]),
                            ),
                            // Text(
                            //   ("${widget.leaveForApproval.endDate.difference(widget.leaveForApproval.startDate).inDays} + 1")
                            //           .toString() +
                            //       "day(s)",
                            //   style: TextStyle(
                            //     color: textSecondary,
                            //     fontWeight: FontWeight.w600,
                            //   ),
                            // ),
                          ],
                        ),
                        Text(widget.leaveByDeignation.userName),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Card(
                              color: Colors.grey[300],
                              margin: EdgeInsets.symmetric(
                                  horizontal: 0, vertical: subMarginHalf / 2),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                  vertical: 2.0,
                                  horizontal: 4,
                                ),
                                child: Text(
                                  widget.leaveByDeignation.type,
                                  style: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w500,
                                    color: textPrimary,
                                  ),
                                ),
                              ),
                            ),
                            Text(
                              widget.leaveByDeignation.onHalfDayLeave
                                  ? "Half Day"
                                  : (widget.leaveByDeignation.endDate
                                                  .difference(widget
                                                      .leaveByDeignation
                                                      .startDate)
                                                  .inDays +
                                              1)
                                          .toString() +
                                      "day(s)",
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                color: textSecondary,
                              ),
                            ),
                          ],
                        ),
                        Text(
                          "${widget.leaveByDeignation.reason}",
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: textSecondary,
                          ),
                        ),
                        widget.leaveByDeignation.rejectReason != null
                            ? Padding(
                                padding: EdgeInsets.only(top: subMarginHalf),
                                child: RichText(
                                  text: TextSpan(children: [
                                    TextSpan(
                                      text: "Reason for Rejection: ",
                                      style: TextStyle(
                                        color: textPrimary,
                                      ),
                                    ),
                                    TextSpan(
                                      text:
                                          widget.leaveByDeignation.rejectReason,
                                      style: TextStyle(
                                        color: textSecondary,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ]),
                                ),
                              )
                            : SizedBox.shrink(),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(height: subMargin),
            widget.leaveByDeignation.status == "pending"
                ? Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Expanded(
                        child: MyCustomButton(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Approve",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              Icon(
                                Icons.done,
                                color: Colors.white,
                              ),
                            ],
                          ),
                          backgroundColor: success,
                          shadowColor: successShadow,
                          onPressed: () =>
                              Provider.of<LeaveProvider>(context, listen: false)
                                  .changeLeaveStatus(
                            LeaveStatus.ACCEPTED,
                            widget.leaveByDeignation.id,
                            widget.leaveByDeignation.userId,
                          ),
                          // () => _changeLeaveStatus("accepted"),
                        ),
                      ),
                      SizedBox(width: subMargin),
                      Expanded(
                        child: MyCustomButton(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Reject",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              Icon(
                                Icons.close,
                                color: Colors.white,
                              ),
                            ],
                          ),
                          backgroundColor: error,
                          shadowColor: errorShadow,
                          onPressed: _showRejectionDialog,
                        ),
                      ),
                    ],
                  )
                : Container(
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(vertical: mainMarginHalf),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(miniCardCornerRadius),
                      border: Border.all(
                          color: widget.leaveByDeignation.status == "rejected"
                              ? error
                              : success,
                          style: BorderStyle.solid,
                          width: 0.80),
                    ),
                    child: Center(
                      child: Text(
                        widget.leaveByDeignation.status.toUpperCase(),
                        style: TextStyle(
                          fontSize: 16,
                          letterSpacing: 1.1,
                          fontWeight: FontWeight.bold,
                          color: widget.leaveByDeignation.status == "rejected"
                              ? error
                              : success,
                        ),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }

//   void _changeLeaveStatus(String status, {String reason}) async {
//     try {
//       Map data = {
//         "leave_id": widget.leaveByDeignation.id,
//         "user_id": widget.leaveByDeignation.userId,
//         "status": status,
//       };
//       if (reason != null) {
//         data.addAll({"reject_reason": reason});
//       }
//       showMyLoadingDialog(
//           dialogText: "Processing the leave Request!...\nPlease Wait!");
//       Response response = await MyApi.postDataWithAuthorization(
//           data, "users/change-leave-status");
//       print("$Leave_By_Designation_Card Approve Response = $response");
//       if (response.data['code'] == 401 || response.statusCode == 401) {
// //Clear the localStorage
//         SharedPreferences.getInstance().then((localStorage) {
//           localStorage.remove('token');
//         });

//         Navigator.pushAndRemoveUntil(
//             context,
//             MaterialPageRoute(
//               builder: (context) => LoginScreen(),
//             ),
//             (route) => false);
//       } else if (response.data['code'] == 200) {
//         // User's attendance is marked successfully
//         dismissMyDialog();
//         // widget.reloadLeaveTab();
//       } else {
//         dismissMyDialog();
//         Scaffold.of(context)
//             .showSnackBar(createSnackBar(msg: "Something Went Wrong!"));
//         throw Future.error("$Leave_By_Designation_Card Error from Future");
//       }
//     } on SocketException {
//       dismissMyDialog();
//       print("$Leave_By_Designation_Card SocketException = $error");
//       Scaffold.of(context).showSnackBar(createSnackBar());
//     } catch (error) {
//       dismissMyDialog();
//       print("$Leave_By_Designation_Card Exception = $error");
//       Scaffold.of(context)
//           .showSnackBar(createSnackBar(msg: "Something Went Wrong!"));
//     }
//   }

  void _showRejectionDialog() async {
    rejectReasonController.clear();
    showMyAlertDialog(
      title: Text("Confirm Rejection"),
      wdgtContent: TextFormField(
        controller: rejectReasonController,
        key: _rejectReasonFormKey,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Enter the reason for leave rejection!';
          }
          return null;
        },
        decoration: InputDecoration(
            hintText: "Reason",
            hintStyle: TextStyle(fontWeight: FontWeight.w600)),
        keyboardType: TextInputType.multiline,
        minLines: 1,
        maxLines: null,
      ),
      fnFirst: () {
        if (_rejectReasonFormKey.currentState.validate()) {
          // If the TextFormField is not empty change the status of leave to rejection
          dismissMyDialog();
          Provider.of<LeaveProvider>(context, listen: false).changeLeaveStatus(
            LeaveStatus.REJECTED,
            widget.leaveByDeignation.id,
            widget.leaveByDeignation.userId,
            reason: rejectReasonController.text,
          );
          // _changeLeaveStatus("rejected", reason: rejectReasonController.text);
        }
      },
      fnSecond: dismissMyDialog,
      txtFirst: "Reject",
    );
  }
}
