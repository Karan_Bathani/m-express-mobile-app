import 'package:flutter/material.dart';
import 'package:market_express/Models/target-vs-achievement-summary.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';

class TargetVsAchievementSummaryCard extends StatelessWidget {
  final SalesTargetDocument salesTargetDocument;
  TargetVsAchievementSummaryCard(this.salesTargetDocument);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.symmetric(
        horizontal: subMargin,
        vertical: subMarginHalf,
      ),
      child: Table(
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        columnWidths: {
          0: FlexColumnWidth(5),
          1: FlexColumnWidth(1.5),
        },
        children: [
          TableRow(
            decoration: BoxDecoration(color: getRandomColor()),
            children: [
              Container(
                color: Colors.white,
                child: ListTile(
                  title: Text(
                    salesTargetDocument.storeDetails[0].storeName,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: textPrimary,
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  subtitle: Text(
                      "${salesTargetDocument.storeDetails[0].territory} ${salesTargetDocument.storeDetails[0].location}, ${salesTargetDocument.storeDetails[0].area}, ${salesTargetDocument.storeDetails[0].district}."),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(6.0),
                child: FittedBox(
                  child: Text(
                    "${salesTargetDocument.actualSales}/${salesTargetDocument.salesTarget}",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 22,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),

              // Column(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   crossAxisAlignment: CrossAxisAlignment.center,
              //   mainAxisSize: MainAxisSize.max,
              //   children: [
              //     FittedBox(
              //       child: Text(
              //         salesTargetDocument.actualSales.toString(),
              //         style: TextStyle(
              //           color: Colors.white,
              //           fontSize: 26,
              //           fontWeight: FontWeight.w700,
              //         ),
              //       ),
              //     ),
              //     SizedBox(height: subMarginHalf / 2),
              //     Text(
              //       salesTargetDocument.salesTarget.toString(),
              //       style: TextStyle(
              //         color: Colors.white,
              //         // fontSize: 16,
              //         // fontWeight: FontWeight.w600,
              //       ),
              //     ),
              //   ],
              // ),
            ],
          ),
        ],
      ),
    );
  }
}

//  Stack(
//                 children: [
//                   Center(
//                     child: Text(
//                       "/",
//                       style: TextStyle(
//                         color: Colors.white,
//                         fontSize: 46,
//                         fontWeight: FontWeight.w700,
//                       ),
//                     ),
//                   ),
//                   Expanded(
//                     child: Column(
//                       children: [
//                         Align(
//                           alignment: Alignment.center,
//                           child: FittedBox(
//                             child: Text(
//                               salesTargetDocument.actualSales.toString() +
//                                   "      ",
//                               style: TextStyle(
//                                 color: Colors.white,
//                                 fontSize: 24,
//                                 fontWeight: FontWeight.w600,
//                               ),
//                             ),
//                           ),
//                         ),
//                         Align(
//                           alignment: Alignment.bottomRight,
//                           child: FittedBox(
//                             child: Text(
//                               salesTargetDocument.salesTarget.toString(),
//                               style: TextStyle(
//                                 color: Colors.white,
//                                 fontSize: 24,
//                                 fontWeight: FontWeight.w600,
//                               ),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ],
//               ),
