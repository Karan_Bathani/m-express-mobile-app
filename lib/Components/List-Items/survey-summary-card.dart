import 'package:flutter/material.dart';
import 'package:market_express/Models/survey-summary.dart';
import 'package:market_express/Screens/surveys-listing-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';

class SurveySummaryCard extends StatelessWidget {
  final SurveySummary surveySummary;
  final DateTime selectedDateTime;
  SurveySummaryCard(this.surveySummary, this.selectedDateTime);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => SurveysListingScreen(
              selectedDateTime,
              surveySummary.userId,
              surveySummary.userName,
            ),
          ),
        );
      },
      child: Card(
          elevation: cardElevation,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(cardCornerRadius)),
          color: Colors.white,
          clipBehavior: Clip.antiAlias,
          margin: EdgeInsets.symmetric(
            horizontal: subMargin,
            vertical: subMarginHalf,
          ),
          child: Table(
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            columnWidths: {
              0: FlexColumnWidth(5),
              1: FlexColumnWidth(1.5),
            },
            children: [
              TableRow(
                decoration: BoxDecoration(color: getRandomColor()),
                children: [
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.all(cardContentPadding),
                    child: Row(
                      children: [
                        CircleAvatar(
                          backgroundImage:
                              NetworkImage(surveySummary.userPhoto ?? ""),
                          radius: 29,
                        ),
                        SizedBox(width: subMarginHalf),
                        Expanded(
                          child: Text(
                            surveySummary.userName,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: textPrimary,
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Text(
                        "Total Survey(s)",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          // fontSize: 16,
                          // fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(height: subMarginHalf / 2),
                      FittedBox(
                        child: Text(
                          surveySummary.surveyCount.toString(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 26,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          )),
    );
  }
}
