import 'package:flutter/material.dart';
import 'package:market_express/Models/on-hand-stock.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';

class OHSCard extends StatelessWidget {
  final OnHandStock onHandStock;
  final String searchField;
  OHSCard(this.onHandStock, this.searchField);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.symmetric(
        horizontal: subMargin,
        vertical: subMarginHalf,
      ),
      child: Table(
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        columnWidths: {
          0: FlexColumnWidth(4.5),
          1: FlexColumnWidth(2),
        },
        children: [
          TableRow(
            decoration: BoxDecoration(color: getRandomColor()),
            children: [
              Container(
                color: Colors.white,
                child: ListTile(
                  title: searchField == "product"
                      ? Text(onHandStock.storeName)
                      : Text(onHandStock.productName),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  FittedBox(
                    child: Text(
                      "available",
                      style: TextStyle(
                        color: Colors.white,
                        // fontSize: 16,
                        // fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  FittedBox(
                    child: Text(
                      "${onHandStock.availableQuantity} Pc(s)",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
