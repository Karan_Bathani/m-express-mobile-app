import 'package:flutter/material.dart';
import 'package:market_express/Models/leave.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';

class LeaveCard extends StatelessWidget {
  final Leave leave;

  LeaveCard(this.leave);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.symmetric(
        horizontal: subMargin,
        vertical: subMarginHalf,
      ),
      child: Column(
        children: [
          Container(
            color: leave.status == "pending"
                ? pending
                : leave.status == "rejected" ? error : success,
            width: double.infinity,
            padding: EdgeInsets.symmetric(
              vertical: subMarginHalf,
              horizontal: subMargin,
            ),
            child: Text(
              leave.status == "pending"
                  ? "Pending Request from ${leave.managerName}"
                  : leave.status == "rejected"
                      ? "Rejected by ${leave.managerName}"
                      : "Accepted by ${leave.managerName}",
              maxLines: 1,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: cardContentPadding,
                vertical: cardContentPadding / 2),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                leave.rejectReason != null
                    ? Padding(
                        padding: EdgeInsets.only(bottom: subMarginHalf),
                        child: RichText(
                          text: TextSpan(children: [
                            TextSpan(
                              text: "Reason for Rejection: ",
                              style: TextStyle(
                                color: textPrimary,
                              ),
                            ),
                            TextSpan(
                              text: leave.rejectReason,
                              style: TextStyle(
                                color: textSecondary,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ]),
                        ),
                      )
                    : SizedBox.shrink(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RichText(
                      text: TextSpan(children: [
                        TextSpan(
                          text: "From: ",
                          style: TextStyle(
                            color: textPrimary,
                          ),
                        ),
                        TextSpan(
                          text: leave.startDate.formatDate(),
                          style: TextStyle(
                            color: textSecondary,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ]),
                    ),
                    RichText(
                      text: TextSpan(children: [
                        TextSpan(
                          text: "To: ",
                          style: TextStyle(
                            color: textPrimary,
                          ),
                        ),
                        TextSpan(
                          text: leave.endDate.formatDate(),
                          style: TextStyle(
                            color: textSecondary,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ]),
                    ),
                    Text(
                      leave.onHalfDayLeave
                          ? "Half Day"
                          : (leave.endDate.difference(leave.startDate).inDays +
                                      1)
                                  .toString() +
                              "day(s)",
                      style: TextStyle(
                        color: textSecondary,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: subMarginHalf),
                  child: Text("Type: ${leave.type}"),
                ),
                Text("Reason: ${leave.reason}"),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
