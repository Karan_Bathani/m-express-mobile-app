import 'package:flutter/material.dart';
import 'package:market_express/Models/attendance-summary.dart';
import 'package:market_express/Screens/attendance-history-screen.dart';
import 'package:market_express/Screens/foe-attendance-summary-screen.dart';

import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AttendanceSummaryCard extends StatelessWidget {
  final AttendanceSummary attendanceSummary;
  final DateTime selectedDateTime;

  AttendanceSummaryCard(this.attendanceSummary, this.selectedDateTime);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (attendanceSummary.userType == "SEC") {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AttendanceHistoryScreen(
                selectedDateTime: selectedDateTime,
                userId: attendanceSummary.userId,
                userName: attendanceSummary.userName,
              ),
            ),
          );
        } else {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => FOEAttendanceSummaryScreen(
                selectedDateTime,
                attendanceSummary.userId,
                attendanceSummary.userName,
              ),
            ),
          );
        }
      },
      child: Card(
          elevation: cardElevation,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(cardCornerRadius)),
          color: Colors.white,
          clipBehavior: Clip.antiAlias,
          margin: EdgeInsets.symmetric(
            horizontal: subMargin,
            vertical: subMarginHalf,
          ),
          child: Table(
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            columnWidths: {
              0: FlexColumnWidth(5),
              1: FlexColumnWidth(1.5),
            },
            children: [
              TableRow(
                decoration: BoxDecoration(color: getRandomColor()),
                children: [
                  Container(
                    color: Colors.white,
                    // padding: EdgeInsets.all(cardContentPadding),
                    child: ListTile(
                      contentPadding:
                          EdgeInsets.symmetric(horizontal: cardContentPadding),
                      leading: CircleAvatar(
                        backgroundImage:
                            NetworkImage(attendanceSummary.userPhoto ?? ""),
                        radius: 29,
                      ),
                      title: Text(
                        attendanceSummary.userName,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: textPrimary,
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      subtitle: Text(
                        " (${attendanceSummary.userType})",
                        style: TextStyle(
                          color: textSecondary,
                          fontWeight: FontWeight.w600,
                          fontSize: 12,
                        ),
                      ),
                    ),
                    //  Row(
                    //   children: [
                    //     CircleAvatar(
                    //       backgroundImage:
                    //           NetworkImage(attendanceSummary.userPhoto ?? ""),
                    //       radius: 29,
                    //     ),
                    //     SizedBox(width: subMarginHalf),
                    //     Expanded(
                    //       child: Column(
                    //         crossAxisAlignment: CrossAxisAlignment.start,
                    //         mainAxisAlignment: MainAxisAlignment.start,
                    //         children: [
                    //           Text(
                    //             " (${attendanceSummary.userType})",
                    //             style: TextStyle(
                    //               color: textSecondary,
                    //               fontWeight: FontWeight.w600,
                    //             ),
                    //           ),
                    //           SizedBox(height: subMarginHalf / 2),
                    //           Text(
                    //             attendanceSummary.userName,
                    //             maxLines: 2,
                    //             overflow: TextOverflow.ellipsis,
                    //             style: TextStyle(
                    //               color: textPrimary,
                    //               fontSize: 16,
                    //               fontWeight: FontWeight.w600,
                    //             ),
                    //           ),
                    //           SizedBox(height: subMarginHalf / 2),
                    //         ],
                    //       ),
                    //     ),
                    //   ],
                    // ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      FittedBox(
                        child: Text(
                          attendanceSummary.attendanceCount.toString(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 26,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      SizedBox(height: subMarginHalf / 2),
                      Text(
                        attendanceSummary.userType == "SEC"
                            ? "day(s)"
                            : "visit(s)",
                        style: TextStyle(
                          color: Colors.white,
                          // fontSize: 16,
                          // fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          )),
    );
  }
}
