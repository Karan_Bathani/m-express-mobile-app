import 'package:flutter/material.dart';
import 'package:market_express/Models/survey.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SurveyCard extends StatelessWidget {
  final Survey survey;
  SurveyCard(this.survey);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.symmetric(
        horizontal: subMargin,
        vertical: subMarginHalf,
      ),
      child: Column(
        children: [
          Container(
            color: getRandomColor(),
            width: double.infinity,
            padding: EdgeInsets.symmetric(
              vertical: subMarginHalf,
              horizontal: subMargin,
            ),
            child: RichText(
              text: TextSpan(children: [
                TextSpan(
                  text: "Interested in: ",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                TextSpan(
                  text: survey.interestedIn,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                )
              ]),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: cardContentPadding,
              vertical: cardContentPadding / 2,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Current Phone: ${survey.customerCurrentPhone}"),
                SizedBox(height: subMarginHalf),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      survey.customerName,
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: textPrimary,
                      ),
                    ),
                    Text(
                      survey.customerContactNumber,
                      style: TextStyle(
                        color: textSecondary,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: subMarginHalf / 2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        survey.createdAt.formatDate(),
                        style: TextStyle(
                          color: textSecondary,
                        ),
                      ),
                      RichText(
                        text: TextSpan(children: [
                          TextSpan(
                            text: "Budget: ",
                            style: TextStyle(
                              color: textSecondary,
                            ),
                          ),
                          TextSpan(
                            text: survey.customerBudget.indexOf("-") == 0
                                ? survey.customerBudget.substring(1)
                                : survey.customerBudget,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: textTertiary,
                            ),
                          )
                        ]),
                      ),
                    ],
                  ),
                ),
                FutureBuilder<SharedPreferences>(
                  future: SharedPreferences.getInstance(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData &&
                        snapshot.data.containsKey("user_type")) {
                      return snapshot.data.getString("user_type") != "SEC"
                          ? Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: subMarginHalf / 2),
                              child: RichText(
                                text: TextSpan(children: [
                                  TextSpan(
                                    text: "Survey By: ",
                                    style: TextStyle(
                                      color: textSecondary,
                                    ),
                                  ),
                                  TextSpan(
                                    text: survey.userName,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: textTertiary,
                                    ),
                                  )
                                ]),
                              ),
                            )
                          : SizedBox(height: subMarginHalf / 2);
                    }
                    return SizedBox(height: subMarginHalf / 2);
                  },
                ),
                Text(survey.storeName),
              ],
            ),
          ),
        ],
      ),
      // Table(
      //   defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      //   columnWidths: {
      //     0: FlexColumnWidth(5),
      //     1: FlexColumnWidth(2),
      //   },
      //   children: [
      //     TableRow(
      //       decoration: BoxDecoration(color: getRandomColor()),
      //       children: [
      //         Container(
      //           color: Colors.white,
      //           padding: EdgeInsets.all(cardContentPadding),
      //           child:
      //            Column(
      //             crossAxisAlignment: CrossAxisAlignment.start,
      //             children: [
      //               Text(survey.customerName),
      //               Padding(
      //                 padding:
      //                     EdgeInsets.symmetric(vertical: subMarginHalf / 2),
      //                 child: Text(
      //                   survey.createdAt.formatDate(),
      //                   style: TextStyle(
      //                     color: textSecondary,
      //                   ),
      //                 ),
      //               ),
      //               Text("Interested in: ${survey.interestedIn}"),
      //               FutureBuilder<SharedPreferences>(
      //                 future: SharedPreferences.getInstance(),
      //                 builder: (context, snapshot) {
      //                   if (snapshot.hasData &&
      //                       snapshot.data.containsKey("user_type")) {
      //                     return snapshot.data.getString("user_type") != "SEC"
      //                         ? Padding(
      //                             padding: EdgeInsets.symmetric(
      //                                 vertical: subMarginHalf / 2),
      //                             child: RichText(
      //                               text: TextSpan(children: [
      //                                 TextSpan(
      //                                   text: "Survey By: ",
      //                                   style: TextStyle(
      //                                     color: textSecondary,
      //                                   ),
      //                                 ),
      //                                 TextSpan(
      //                                   text: survey.userName,
      //                                   style: TextStyle(
      //                                     fontWeight: FontWeight.bold,
      //                                     color: textTertiary,
      //                                   ),
      //                                 )
      //                               ]),
      //                             ),
      //                           )
      //                         : SizedBox(height: subMarginHalf / 2);
      //                   }
      //                   return SizedBox(height: subMarginHalf / 2);
      //                 },
      //               ),
      //               Text(survey.storeName),
      //             ],
      //           ),
      //         ),
      //         Column(
      //           mainAxisAlignment: MainAxisAlignment.center,
      //           crossAxisAlignment: CrossAxisAlignment.center,
      //           mainAxisSize: MainAxisSize.max,
      //           children: [
      //             Text(
      //               "Tk",
      //               style: TextStyle(
      //                 color: Colors.white,
      //               ),
      //             ),
      //             SizedBox(height: subMarginHalf / 2),
      //             Text(
      //               survey.customerBudget.toString(),
      //               style: TextStyle(
      //                 color: Colors.white,
      //                 fontSize: 18,
      //                 fontWeight: FontWeight.w700,
      //               ),
      //             ),
      //           ],
      //         ),
      //       ],
      //     ),
      //   ],
      // ),
    );
  }
}
