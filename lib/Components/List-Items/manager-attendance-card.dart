import 'package:flutter/material.dart';
import 'package:market_express/Models/manager-attendance.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';

class ManagerAttendanceCard extends StatelessWidget {
  final ManagerAttendance managerAttendance;

  ManagerAttendanceCard(this.managerAttendance);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(cardCornerRadius),
      ),
      // color: Colors.white,
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.symmetric(
        horizontal: subMargin,
        vertical: subMarginHalf,
      ),
      child: ListTile(
        leading: CircleAvatar(
          backgroundImage: NetworkImage(managerAttendance.photo ?? " "),
          radius: 29,
        ),
        title: Text(
          managerAttendance.storeName ?? "Store Name",
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: textPrimary,
            fontSize: 16,
            fontWeight: FontWeight.w600,
          ),
        ),
        isThreeLine: managerAttendance.outsidePjpReason != null &&
                managerAttendance.outsidePjpReason.length != 0
            ? true
            : false,
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              managerAttendance.date != null
                  ? managerAttendance.date.formatDate(pattern: "dd MMM, y") +
                      " at " +
                      managerAttendance.time.formatDate(pattern: "hh:mm a")
                  : "Date and Time",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: textSecondary,
                fontSize: 14,
              ),
            ),
            managerAttendance.outsidePjpReason != null &&
                    managerAttendance.outsidePjpReason.length != 0
                ? RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: "Reason: ",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: colorPrimary,
                          ),
                        ),
                        TextSpan(
                          text: managerAttendance.outsidePjpReason,
                          style: TextStyle(
                            // fontWeight: FontWeight.bold,
                            color: textPrimary,
                          ),
                        ),
                      ],
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
      // Table(
      //   defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      //   columnWidths: {
      //     0: FlexColumnWidth(5),
      //     1: FlexColumnWidth(1.5),
      //   },
      //   children: [
      //     TableRow(
      //       decoration: BoxDecoration(color: getRandomColor()),
      //       children: [
      //         Container(
      //           color: Colors.white,
      //           padding: EdgeInsets.all(cardContentPadding),
      //           child: Row(
      //             children: [
      //               CircleAvatar(
      //                 backgroundImage:
      //                     NetworkImage(managerAttendance.photo ?? " "),
      //                 radius: 29,
      //               ),
      //               SizedBox(width: subMarginHalf),
      //               Expanded(
      //                 child: Column(
      //                   crossAxisAlignment: CrossAxisAlignment.start,
      //                   mainAxisAlignment: MainAxisAlignment.center,
      //                   children: [
      //                     Text(
      //                       managerAttendance.storeName ?? "Store Name",
      //                       maxLines: 1,
      //                       overflow: TextOverflow.ellipsis,
      //                       style: TextStyle(
      //                         color: textPrimary,
      //                         fontSize: 16,
      //                         fontWeight: FontWeight.w600,
      //                       ),
      //                     ),
      //                     Text(
      //                       managerAttendance.date != null
      //                           ? managerAttendance.date.formatDate() +
      //                               " at " +
      //                               managerAttendance.time
      //                           : "Date and Time",
      //                       maxLines: 1,
      //                       overflow: TextOverflow.ellipsis,
      //                       style: TextStyle(
      //                         color: textSecondary,
      //                         fontSize: 14,
      //                       ),
      //                     ),
      //                   ],
      //                 ),
      //               ),
      //             ],
      //           ),
      //         ),
      //         Column(
      //           mainAxisAlignment: MainAxisAlignment.center,
      //           crossAxisAlignment: CrossAxisAlignment.center,
      //           mainAxisSize: MainAxisSize.max,
      //           children: [
      //             FittedBox(
      //               child: Text(
      //                 managerAttendance.time ?? "Time",
      //                 style: TextStyle(
      //                   color: Colors.white,
      //                   fontSize: 26,
      //                   fontWeight: FontWeight.w700,
      //                 ),
      //               ),
      //             ),
      //             SizedBox(height: subMarginHalf / 2),
      //             Text(
      //               "day(s)",
      //               style: TextStyle(
      //                 color: Colors.white,
      //                 // fontSize: 16,
      //                 // fontWeight: FontWeight.w600,
      //               ),
      //             ),
      //           ],
      //         ),
      //       ],
      //     ),
      //   ],
      // ),
    );
  }
}
