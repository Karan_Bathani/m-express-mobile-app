import 'package:flutter/material.dart';
import 'package:market_express/Models/product-suggestion.dart';
import 'package:market_express/Screens/OHS/ohs-status-screen.dart';
import 'package:market_express/Utility/constants.dart';

class ModelCard extends StatelessWidget {
  final ProductSuggestion model;
  ModelCard(this.model);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.symmetric(
        horizontal: subMargin,
        vertical: subMarginHalf,
      ),
      child: ListTile(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => OHSStatusScreen(
                appbarTitle: model.deviceModel,
                id: model.id,
                searchField: "product",
              ),
            ),
          );
        },
        leading: Image.asset(
          model.category == "Tablet"
              ? "assets/images/tablet.png"
              : "assets/images/smartphone.png",
          width: 40,
          height: 40,
        ),
        title: Text(model.deviceModel),
        subtitle: Text(
          model.subCategory,
          style: TextStyle(
            color: textSecondary,
            fontWeight: FontWeight.w400,
          ),
        ),
        trailing: model.deviceVarient != null
            ? Text(
                model.deviceVarient,
                style: TextStyle(
                  color: textPrimary,
                  // fontWeight: FontWeight.w600,
                ),
              )
            : SizedBox.shrink(),
      ),
    );
  }
}
