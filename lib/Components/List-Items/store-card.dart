import 'package:flutter/material.dart';
import 'package:market_express/Models/store.dart';
import 'package:market_express/Screens/OHS/ohs-status-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';

class StoreCard extends StatelessWidget {
  final Store store;
  StoreCard(this.store);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.symmetric(
        horizontal: subMargin,
        vertical: subMarginHalf,
      ),
      child: ListTile(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => OHSStatusScreen(
                appbarTitle: store.storeName,
                id: store.id,
                searchField: "store",
              ),
            ),
          );
        },
        leading: CircleAvatar(
          backgroundColor: getRandomColor(),
          child: FittedBox(
            child: Text(
              store.storeType,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ),
        title: Text(store.storeName),
        subtitle: Text(
            "${store.location}, ${store.area}, ${store.district}, ${store.division}."),
      ),
    );
  }
}
