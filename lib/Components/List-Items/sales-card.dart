import 'package:flutter/material.dart';
import 'package:market_express/Models/sales.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SalesCard extends StatelessWidget {
  final Sales sale;
  SalesCard(this.sale);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.symmetric(
        horizontal: subMargin,
        vertical: subMarginHalf,
      ),
      child: Table(
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        columnWidths: {
          0: FlexColumnWidth(5),
          1: FlexColumnWidth(2),
        },
        children: [
          TableRow(
            decoration: BoxDecoration(color: getRandomColor()),
            children: [
              Container(
                color: Colors.white,
                padding: EdgeInsets.all(cardContentPadding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    sale.customerName == null
                        ? SizedBox()
                        : Text(sale.customerName),
                    sale.customerContactNumber == null
                        ? SizedBox()
                        : Text(
                            "Contact: ${sale.customerContactNumber}",
                            style: TextStyle(
                              color: textPrimary,
                            ),
                          ),
                    sale.customerAge != null
                        ? Padding(
                            padding: EdgeInsets.only(top: subMarginHalf / 2),
                            child: Text(
                              "email: ${sale.customerEmail}",
                              style: TextStyle(
                                color: textPrimary,
                              ),
                            ),
                          )
                        : SizedBox.shrink(),
                    SizedBox(height: subMarginHalf / 2),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          sale.createdAt.formatDate(pattern: "dd MMM, yyyy"),
                          style: TextStyle(
                            color: textSecondary,
                          ),
                        ),
                        sale.customerAge != null
                            ? Text(
                                "Age: ${sale.customerAge}",
                                style: TextStyle(
                                  color: textSecondary,
                                ),
                              )
                            : SizedBox.shrink(),
                      ],
                    ),
                    SizedBox(height: subMarginHalf / 2),
                    Text(
                        "${sale.deviceModel} | ${sale.color ?? ""} | ${sale.quantity} Pc(s)"),
                    FutureBuilder<SharedPreferences>(
                      future: SharedPreferences.getInstance(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData &&
                            snapshot.data.containsKey("user_type")) {
                          return snapshot.data.getString("user_type") != "SEC"
                              ? Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: subMarginHalf / 2),
                                  child: RichText(
                                    text: TextSpan(children: [
                                      TextSpan(
                                        text: "Sold By: ",
                                        style: TextStyle(
                                          color: textSecondary,
                                        ),
                                      ),
                                      TextSpan(
                                        text: sale.userName,
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: textTertiary,
                                        ),
                                      )
                                    ]),
                                  ),
                                )
                              : SizedBox(height: subMarginHalf / 2);
                        }
                        return SizedBox(height: subMarginHalf / 2);
                      },
                    ),
                    Text(sale.storeName),
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Text(
                    "Tk",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(height: subMarginHalf / 2),
                  Text(
                    sale.totalPrice.toString(),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
