import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Models/training-material.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

class TrainingMaterialCard extends StatelessWidget {
  final TrainingMaterial trainingMaterial;
  final Function onTap;
  TrainingMaterialCard(this.trainingMaterial, {this.onTap});
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.symmetric(
        horizontal: subMargin,
        vertical: subMarginHalf,
      ),
      child: ListTile(
        onTap: onTap,
        leading: Image.asset(
          trainingMaterial.fileName.endsWith(".pdf")
              ? "assets/images/pdf.png"
              : "assets/images/video.png",
          width: 40,
          height: 80,
          alignment: Alignment.center,
        ),
        title: Text(trainingMaterial.fileName
                .substring(0, trainingMaterial.fileName.lastIndexOf(".")) ??
            "Filename"),
        subtitle: trainingMaterial.createdAt != null
            ? Text("uploaded on: " +
                trainingMaterial.createdAt.formatDate(pattern: "dd MMM, yyyy"))
            : Text("No Date Found."),
        trailing: onTap != null
            ? Icon(Icons.arrow_forward)
            : IconButton(
                icon: Icon(Icons.cloud_download),
                padding: EdgeInsets.all(0),
                visualDensity: VisualDensity.compact,
                onPressed: () async {
                  final status = await Permission.storage.request();
                  Directory externalDir;
                  if (Platform.isAndroid) {
                    externalDir = await getExternalStorageDirectory();
                  } else {
                    externalDir = await getApplicationDocumentsDirectory();
                  }

                  if (status.isGranted) {
                    final id = await FlutterDownloader.enqueue(
                      url: MyApi.BASE_URL + trainingMaterial.file,
                      savedDir: externalDir.path,
                      fileName: trainingMaterial.fileName,
                      showNotification: true,
                      openFileFromNotification: true,
                    ).then((value) {
                      MyApi.getDataWithAuthorization(
                              "users/mark-training-material-status/${trainingMaterial.id}")
                          .then((value) {
                        print("$TrainingMaterial_Card Update = ${value.data}");
                      }).catchError((error) {
                        print(
                            "$TrainingMaterial_Card Update = ${error.response.data}");
                      });
                    });
                  } else {}
                },
              ),
      ),
    );
  }
}
