import 'package:flutter/material.dart';
import 'package:market_express/Models/inventory-transaction.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InventoryCard extends StatelessWidget {
  final InventoryTransaction inventoryTransaction;

  InventoryCard(this.inventoryTransaction);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: cardElevation,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(cardCornerRadius)),
        color: Colors.white,
        clipBehavior: Clip.antiAlias,
        margin: EdgeInsets.symmetric(
          horizontal: subMargin,
          vertical: subMarginHalf,
        ),
        child: Table(
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          columnWidths: {
            0: FlexColumnWidth(5),
            1: FlexColumnWidth(2),
          },
          children: [
            TableRow(
              decoration: BoxDecoration(color: getRandomColor()),
              children: [
                Container(
                  color: Colors.white,
                  padding: EdgeInsets.all(cardContentPadding),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        inventoryTransaction.deviceModel,
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                          color: textPrimary,
                        ),
                      ),
                      inventoryTransaction.remark != null &&
                              inventoryTransaction.remark.isNotEmpty
                          ? Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: subMarginHalf / 2),
                              child: RichText(
                                text: TextSpan(children: [
                                  TextSpan(
                                    text: "Remark(s): ",
                                    style: TextStyle(
                                        color: textSecondary,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  TextSpan(
                                    text: inventoryTransaction.remark,
                                    style: TextStyle(
                                        color: textPrimary,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ]),
                              ),
                            )
                          : SizedBox(height: subMarginHalf / 2),
                      FutureBuilder<SharedPreferences>(
                        future: SharedPreferences.getInstance(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData &&
                              snapshot.data.containsKey("user_type")) {
                            return RichText(
                              text: TextSpan(children: [
                                TextSpan(
                                  text: snapshot.data.getString("user_type") !=
                                          "SEC"
                                      ? "Employee: "
                                      : "Manager: ",
                                  style: TextStyle(
                                    color: textSecondary,
                                  ),
                                ),
                                TextSpan(
                                  text: snapshot.data.getString("user_type") !=
                                          "SEC"
                                      ? inventoryTransaction.userName
                                      : inventoryTransaction.managerName,
                                  style: TextStyle(
                                    color: textTertiary,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ]),
                            );
                          }
                          return SizedBox.shrink();
                        },
                      ),
                      SizedBox(height: subMarginHalf / 2),
                      RichText(
                        text: TextSpan(children: [
                          TextSpan(
                            text: "Store: ",
                            style: TextStyle(
                                color: textSecondary,
                                fontWeight: FontWeight.w600),
                          ),
                          TextSpan(
                            text: inventoryTransaction.storeName,
                            style: TextStyle(
                                color: textPrimary,
                                fontWeight: FontWeight.w400),
                          ),
                        ]),
                      ),
                    ],
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Text(
                      "${inventoryTransaction.quantity} Pc(s)",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    SizedBox(height: subMarginHalf / 4),
                    Text(
                      "on",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(height: subMarginHalf / 2),
                    Text(
                      inventoryTransaction.createdAt.formatDate(),
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ));
  }
}
