import 'package:flutter/material.dart';
import 'package:market_express/Models/attendance.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';

class AttendanceCard extends StatelessWidget {
  final Attendance attendance;

  AttendanceCard(this.attendance);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.symmetric(
        horizontal: subMargin,
        vertical: subMarginHalf,
      ),
      child: Column(
        children: [
          Container(
            color: colorPrimary,
            width: double.infinity,
            padding: EdgeInsets.symmetric(
              vertical: subMarginHalf,
              horizontal: subMargin,
            ),
            child: attendance.morningStoreName != null
                ? Text(
                    "${attendance.date.formatDate(pattern: "dd MMM, yyy")}: ${attendance.morningStoreName}",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(color: Colors.white),
                  )
                : Text(
                    // attendance.date.formatDate(pattern: "dd MMMM, yyyy"),
                    attendance.date.formatDate(),
                    maxLines: 1,
                    style: TextStyle(color: Colors.white),
                  ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: subMargin),
            child: Table(
              children: [
                TableRow(
                  children: [
                    _buildAttendanceColumn(
                      imageUrl: attendance.morningPhoto,
                      periodOfDay: "Morning",
                      time: attendance.morningTime,
                    ),
                    _buildAttendanceColumn(
                      imageUrl: attendance.eveningPhoto,
                      periodOfDay: "Evening",
                      time: attendance.eveningTime,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAttendanceColumn(
      {String imageUrl, String periodOfDay, DateTime time}) {
    return Column(
      children: [
        CircleAvatar(
          radius: 35,
          backgroundImage: NetworkImage(imageUrl ??
              "https://www.brdtex.com/wp-content/uploads/2019/09/no-image-480x480.png"),
        ),
        SizedBox(height: subMarginHalf),
        Card(
          color: Colors.grey[300],
          child: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 2.0,
              horizontal: 4,
            ),
            child: Text(
              periodOfDay,
              style: TextStyle(
                fontSize: 11,
                fontWeight: FontWeight.w500,
                color: textPrimary,
              ),
            ),
          ),
        ),
        Text(
          time != null ? time.formatDate(pattern: "hh:mm a") : "N/A",
          style: TextStyle(
            color: textSecondary,
            fontWeight: FontWeight.w600,
          ),
        ),
      ],
    );
  }
}
