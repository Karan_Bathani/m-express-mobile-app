import 'package:flutter/material.dart';
import 'package:market_express/Utility/constants.dart';

class MyCustomButton extends StatelessWidget {
  final Widget child;
  final Color backgroundColor;
  final Color shadowColor;
  final Color containerColor;
  final EdgeInsetsGeometry padding;
  final EdgeInsetsGeometry containerMargin;
  final EdgeInsetsGeometry containerPadding;
  final Function onPressed;
  final bool enabled;

  MyCustomButton({
    this.backgroundColor,
    this.shadowColor,
    this.containerColor,
    this.padding,
    this.containerMargin,
    this.containerPadding,
    @required this.onPressed,
    this.child,
    this.enabled = true,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: containerMargin ?? EdgeInsets.all(0),
      padding: containerPadding ?? EdgeInsets.all(0),
      decoration: BoxDecoration(
        color: containerColor ?? Colors.transparent,
        boxShadow: [
          BoxShadow(
            color: (shadowColor ?? colorPrimaryLight).withOpacity(.34),
            blurRadius: 4,
            offset: Offset(0, 4),
          ),
        ],
      ),
      child: FlatButton(
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(miniCardCornerRadius),
        ),
        color: backgroundColor ?? colorPrimary,
        disabledColor: Colors.blueGrey[400].withOpacity(0.5),
        padding: padding ??
            EdgeInsets.symmetric(
              vertical: subMarginHalf,
              horizontal: subMargin,
            ),
        child: child ??
            Text(
              'Submit',
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
        onPressed: enabled ? onPressed ?? () {} : null,
      ),
    );
  }
}
