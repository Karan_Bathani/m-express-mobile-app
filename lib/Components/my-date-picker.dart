import 'package:flutter/material.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class MyDatePicker extends StatefulWidget {
  final bool enableRange;
  final bool enablePastDates;
  final DateTime minDate;
  final DateTime maxDate;
  final bool allowViewNavigation;
  final DateTime initialSelectedDate;
  MyDatePicker({
    this.enableRange = true,
    this.enablePastDates = false,
    this.initialSelectedDate,
    this.maxDate,
    this.minDate,
    this.allowViewNavigation = true,
  });
  @override
  _MyDatePickerState createState() => _MyDatePickerState();
}

class _MyDatePickerState extends State<MyDatePicker> {
  DateRangePickerController _dateRangePickerController =
      DateRangePickerController();
  bool selectRange = false;
  DateTime _startDate = DateTime.now();
  DateTime _endDate = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(subMargin),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          widget.enableRange
              ? SwitchListTile(
                  title: Text("Select Range"),
                  onChanged: (val) {
                    setState(() {
                      selectRange = val;
                      if (selectRange) {
                        _startDate =
                            _dateRangePickerController.selectedRange.startDate;
                        _endDate =
                            _dateRangePickerController.selectedRange.endDate;
                      }
                    });
                  },
                  value: selectRange,
                )
              : SizedBox.shrink(),
          Flexible(
            child: SfDateRangePicker(
              controller: _dateRangePickerController,
              onSelectionChanged: _onSelectionChanged,
              allowViewNavigation: widget.allowViewNavigation,
              maxDate: widget.maxDate,
              minDate: widget.minDate,
              enableMultiView: false,
              enablePastDates: widget.enablePastDates,
              showNavigationArrow: true,
              initialSelectedDate: widget.initialSelectedDate ?? DateTime.now(),
              initialSelectedRange: PickerDateRange(
                  DateTime.now(), DateTime.now().add(const Duration(days: 9))),
              selectionMode: selectRange
                  ? DateRangePickerSelectionMode.range
                  : DateRangePickerSelectionMode.single,
            ),
          ),
          Flexible(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                FlatButton(
                    child: Text(
                      "CANCEL",
                      style: TextStyle(color: textSecondary),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    }),
                FlatButton(
                    child: Text(
                      "OK",
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop([
                        _startDate,
                        selectRange ? _endDate : null,
                      ]);
                    }),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
    setState(() {
      if (args.value is PickerDateRange) {
        _startDate = args.value.startDate;
        _endDate = (args.value.endDate ?? args.value.startDate);
      } else if (args.value is DateTime) {
        // print("$Leave_Form Date Single = " + args.value);

        _startDate = args.value;
        _endDate = args.value;
      }
    });
  }
}
