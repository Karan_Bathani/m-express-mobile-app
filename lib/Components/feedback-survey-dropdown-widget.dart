import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/my-custom-button.dart';
import 'package:market_express/Models/workplace-feedback-question.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FeedbackSurveyDropdownListWidget extends StatefulWidget {
  final List<WorkplaceFeedbackQuestion> feedbackQuestions;
  FeedbackSurveyDropdownListWidget(this.feedbackQuestions);

  @override
  _FeedbackSurveyDropdownListWidgetState createState() =>
      _FeedbackSurveyDropdownListWidgetState();
}

class _FeedbackSurveyDropdownListWidgetState
    extends State<FeedbackSurveyDropdownListWidget> {
  List<String> questionIds = [];
  List<String> answers = [];

  @override
  void initState() {
    super.initState();
    questionIds.addAll(widget.feedbackQuestions.map((e) => e.id));
    answers.addAll(widget.feedbackQuestions.map((e) => "Please Select"));
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(subMargin),
      children: List.generate(
        widget.feedbackQuestions.length + 1,
        (index) {
          if (index == widget.feedbackQuestions.length) {
            return MyCustomButton(
              padding: EdgeInsets.symmetric(
                vertical: subMargin,
                horizontal: subMarginDouble,
              ),
              onPressed: submitForm,
            );
          }
          return Container(
            margin: EdgeInsets.symmetric(vertical: subMarginHalf),
            child: Column(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(widget.feedbackQuestions[index].question),
                ),
                Wrap(
                  children: widget.feedbackQuestions[index].options
                      .map((e) => Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Radio(
                                value: e,
                                groupValue: answers[index],
                                onChanged: (value) {
                                  setState(() {
                                    answers[index] = value;
                                  });
                                },
                              ),
                              Text(e),
                            ],
                          ))
                      .toList(),
                ),
                // Container(
                //   padding: EdgeInsets.only(right: subMargin),
                //   margin: EdgeInsets.symmetric(vertical: subMargin),
                //   decoration: BoxDecoration(
                //     borderRadius: BorderRadius.circular(miniCardCornerRadius),
                //     border: Border.all(
                //         color: Colors.grey[600],
                //         style: BorderStyle.solid,
                //         width: 0.80),
                //   ),
                //   child: DropdownButton(
                //     underline: SizedBox.shrink(),
                //     isExpanded: true,
                //     hint: Padding(
                //       padding: const EdgeInsets.only(left: 8.0),
                //       child: Text(answers[index]),
                //     ),
                //     items: widget.feedbackQuestions[index].options
                //         .map((e) => DropdownMenuItem(
                //               child: Text(e),
                //               value: e,
                //             ))
                //         .toList(),
                //     onChanged: (value) {
                //       setState(() {
                //         answers[index] = value;
                //       });
                //     },
                //   ),
                // ),
              ],
            ),
          );
        },
      ),
    );
  }

  void submitForm() async {
    // for (int i = 0; i < questionIds.length; i++) {
    //   print(
    //       "FeedbackSurveyDropdown_Widget \n Question $i = ${questionIds[i]}\n Answer $i = ${answers[i]}\n");
    // }
    try {
      List feedback = [];
      for (int i = 0; i < questionIds.length; i++) {
        if (answers[i] != "Please Select") {
          feedback.add({"question_id": questionIds[i], "answer": answers[i]});
        }
      }
      if (feedback.isEmpty) {
        Scaffold.of(context)
            .showSnackBar(createSnackBar(msg: "Please answer at least one!"));
        return;
      }
      showMyLoadingDialog(
          dialogText: "Submitting your Feedback...\nPlease Wait!");

      Response response = await MyApi.postDataWithAuthorization(
          {"workplace_survey": feedback}, "users/submit-workplace-survey");
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        // Feedback Submitted Successfully...
        dismissMyDialog();
        showToast(response.data["message"].toString(),
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            backgroundColor: success,
            textStyle: TextStyle(color: Colors.white),
            reverseCurve: Curves.fastOutSlowIn);
        Navigator.pop(context, response.data["message"]);
      } else if (response.statusCode == 422) {
        dismissMyDialog();
        print("Response = $response");
        showToast(response.data["message"].toString(),
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
      } else {
        dismissMyDialog();
        Scaffold.of(context)
            .showSnackBar(createSnackBar(msg: "Something Went Wrong!"));
        throw Future.error("$FeedbackSurveyDropdown_Widget Error from Future");
      }
    } on SocketException {
      dismissMyDialog();
      print("$FeedbackSurveyDropdown_Widget Exception = $error");
      Scaffold.of(context).showSnackBar(createSnackBar());
    } catch (error) {
      dismissMyDialog();
      if (error is DioError) {
        if (error.response.statusCode == 422) {
          print("$FeedbackSurveyDropdown_Widget Dio Error = $error");
          print("Response = ${error.response}");
          showToast(error.response.data["message"].toString(),
              context: context,
              animation: StyledToastAnimation.slideFromBottom,
              reverseAnimation: StyledToastAnimation.slideToBottom,
              startOffset: Offset(0.0, 3.0),
              reverseEndOffset: Offset(0.0, 3.0),
              position: StyledToastPosition.bottom,
              duration: Duration(seconds: 3),
              animDuration: Duration(seconds: 1),
              curve: Curves.elasticOut,
              reverseCurve: Curves.fastOutSlowIn);
        }
        print("$FeedbackSurveyDropdown_Widget Dio Error = $error");
        showToast(error.response.data["message"].toString(),
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
      } else {
        print("$FeedbackSurveyDropdown_Widget Exception = $error");
        Scaffold.of(context)
            .showSnackBar(createSnackBar(msg: "Something Went Wrong!"));
      }
    }
  }
}
