import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:market_express/Utility/constants.dart';

class DonutPieChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;
  final double completedPercentage;
  final double pendingPercentage;

  DonutPieChart(this.seriesList,
      {this.animate, this.completedPercentage, this.pendingPercentage});

  /// Creates a [PieChart] with desired data and transition.
  factory DonutPieChart.withDesiredData(
    int actualSales,
    int salesTarget, {
    String text1 = "Completed",
    String text2 = "Pending",
    double completedPercentage = 0.0,
    double pendingPercentage = 0.0,
  }) {
    return DonutPieChart(
      _createData(actualSales, salesTarget, text1, text2),
      animate: true,
      completedPercentage: completedPercentage,
      pendingPercentage: pendingPercentage,
    );
  }

  /// Creates a [PieChart] with sample data and transition.
  factory DonutPieChart.withSampleData() {
    return DonutPieChart(
      _createData(42, 60, "Completed", "Pending"),
      animate: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return charts.PieChart(
      seriesList,
      animate: animate,
      defaultInteractions: true,
      defaultRenderer: charts.ArcRendererConfig(
        arcRatio: 0.5,
        // arcRendererDecorators: [
        //   new charts.ArcLabelDecorator(
        //     labelPosition: charts.ArcLabelPosition.inside,
        //     insideLabelStyleSpec: new charts.TextStyleSpec(
        //       color: charts.Color.black,
        //       fontSize: 10,
        //       lineHeight: 1,
        //       // fontWeight: 12,
        //     ),
        //   ),
        // ],
      ),
      behaviors: [
        charts.DatumLegend(
          position: charts.BehaviorPosition.bottom,
          outsideJustification: charts.OutsideJustification.start,
          horizontalFirst: true,
          cellPadding: EdgeInsets.only(right: 4.0, bottom: 4.0),
          desiredMaxColumns: 2,
          desiredMaxRows: 2,
          legendDefaultMeasure: charts.LegendDefaultMeasure.firstValue,
          showMeasures:
              completedPercentage != null && completedPercentage != 0.0,
          measureFormatter: (num value) {
            // print("debug Completed ${seriesList[0].data[0].sales}");
            // print("debug Pending ${seriesList[0].data[1].sales}");
            // print("debug Value = ${value}");
            if (seriesList[0].data[0].sales == value) {
              //> Show Completed Percentage
              return "${completedPercentage.toStringAsFixed(2)}%";
            } else {
              //> Show Pending Percentag
              return "${pendingPercentage.toStringAsFixed(2)}%";
            }
          },
          entryTextStyle: charts.TextStyleSpec(
              color: charts.ColorUtil.fromDartColor(textSecondary),
              fontSize: 12),
        ),
      ],
    );
  }

  // /// Create one series with sample hard coded data.
  // static List<charts.Series<Sales, String>> _createSampleData() {
  //   final data = [
  //     Sales('Completed', 42, charts.ColorUtil.fromDartColor(colorSecondary)),
  //     Sales('Pending', 12, charts.ColorUtil.fromDartColor(myYellow)),
  //   ];

  //   return [
  //     charts.Series<Sales, String>(
  //       id: 'Sales',
  //       domainFn: (Sales sales, _) => sales.status,
  //       measureFn: (Sales sales, _) => sales.sales,
  //       data: data,
  //       colorFn: (Sales sales, _) => sales.color,
  //     )
  //   ];
  // }
  /// Create a series for DonutPieChart.
  static List<charts.Series<Sales, String>> _createData(
      int actualSales, int salesTarget, String text1, String text2) {
    final data = [
      Sales(text1, actualSales, charts.ColorUtil.fromDartColor(colorSecondary)),
      Sales(text2, salesTarget - actualSales,
          charts.ColorUtil.fromDartColor(myYellow)),
    ];

    return [
      charts.Series<Sales, String>(
        id: 'Sales',
        domainFn: (Sales sales, _) => sales.status,
        measureFn: (Sales sales, _) => sales.sales,
        data: data,
        colorFn: (Sales sales, _) => sales.color,
        // labelAccessorFn: (Sales sale, _) => '${sale.sales}'
      )
    ];
  }
}

class Sales {
  final String status;
  final int sales;
  final charts.Color color;

  Sales(this.status, this.sales, this.color);
}
