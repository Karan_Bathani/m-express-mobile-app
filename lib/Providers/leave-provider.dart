import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Models/leave-by-designation.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum LeaveStatus { PENDING, ACCEPTED, REJECTED }
LeaveStatus getLeaveStatusEnum(String status) {
  switch (status) {
    case "pending":
      return LeaveStatus.PENDING;
      break;
    case "accepted":
      return LeaveStatus.ACCEPTED;
      break;
    case "rejected":
      return LeaveStatus.REJECTED;
      break;
    default:
      return LeaveStatus.PENDING;
      break;
  }
}

String getLeaveStatusStringFromEnum(LeaveStatus enumLeaveStatus) {
  switch (enumLeaveStatus) {
    case LeaveStatus.PENDING:
      return "pending";
      break;
    case LeaveStatus.ACCEPTED:
      return "accepted";
      break;
    case LeaveStatus.REJECTED:
      return "rejected";
      break;
    default:
      return "pending";
      break;
  }
}

class LeaveProvider with ChangeNotifier {
  List<LeaveByDesignation> pendingleavesByDesignation = [];
  List<LeaveByDesignation> approvedleavesByDesignation = [];
  List<LeaveByDesignation> rejectedleavesByDesignation = [];

  LeaveProvider() {
    getLeavesByDesignation();
  }
  void clearProviderData() {
    pendingleavesByDesignation.clear();
    approvedleavesByDesignation.clear();
    rejectedleavesByDesignation.clear();
  }

  Future getLeavesByDesignation() async {
    try {
      Response response = await MyApi.postDataWithAuthorization({
        "status": ["pending", "accepted", "rejected"],
      }, "users/leave-history-by-designation");
      clearProviderData();
      print("$Leave_Provider Response = $response");
//       if (response.statusCode == 401) {
// //Clear the localStorage
//         SharedPreferences.getInstance().then((localStorage) {
//           localStorage.remove('token');
//         });

//         Navigator.pushAndRemoveUntil(
//             context,
//             MaterialPageRoute(
//               builder: (context) => LoginScreen(),
//             ),
//             (route) => false);
//       } else
      if (response.statusCode == 200) {
        for (var leaveByDesignation in response.data["data"]) {
          switch (getLeaveStatusEnum(leaveByDesignation["status"])) {
            case LeaveStatus.PENDING:
              pendingleavesByDesignation
                  .add(LeaveByDesignation.fromJson(leaveByDesignation));
              break;
            case LeaveStatus.ACCEPTED:
              approvedleavesByDesignation
                  .add(LeaveByDesignation.fromJson(leaveByDesignation));
              break;
            case LeaveStatus.REJECTED:
              rejectedleavesByDesignation
                  .add(LeaveByDesignation.fromJson(leaveByDesignation));
              break;
          }
        }
      } else {
        throw Future.error("Error from Future");
      }
      notifyListeners();
      return null;
    } on SocketException {
      print("$Leave_Provider SocketException");
      notifyListeners();
      throw Future.error(SocketException);
    } catch (error) {
      if (error is DioError) {
//         if (error.response.statusCode == 401) {
// //Clear the localStorage
//           SharedPreferences.getInstance().then((localStorage) {
//             localStorage.remove('token');
//           });

//           Navigator.pushAndRemoveUntil(
//               context,
//               MaterialPageRoute(
//                 builder: (context) => LoginScreen(),
//               ),
//               (route) => false);
//         }
      }
      print("$Leave_Provider Exception = $error");
      notifyListeners();
      throw Future.error(error);
    }
  }

  void changeLeaveStatus(LeaveStatus statusEnum, String leaveId, String userId,
      {String reason}) async {
    try {
      Map data = {
        "leave_id": leaveId,
        "user_id": userId,
        "status": getLeaveStatusStringFromEnum(statusEnum),
      };
      if (reason != null) {
        data.addAll({"reject_reason": reason});
      }
      showMyLoadingDialog(
          dialogText: "Processing the leave Request!...\nPlease Wait!");
      Response response = await MyApi.postDataWithAuthorization(
          data, "users/change-leave-status");
      print("$Leave_By_Designation_Card Approve Response = $response");
//       if (response.data['code'] == 401 || response.statusCode == 401) {
// //Clear the localStorage
//         SharedPreferences.getInstance().then((localStorage) {
//           localStorage.remove('token');
//         });

//         Navigator.pushAndRemoveUntil(
//             context,
//             MaterialPageRoute(
//               builder: (context) => LoginScreen(),
//             ),
//             (route) => false);
//       } else
      if (response.data['code'] == 200) {
        // User's attendance is marked successfully
        dismissMyDialog();
        this.getLeavesByDesignation();
        // widget.reloadLeaveTab();
      } else {
        dismissMyDialog();
        throw Future.error("$Leave_By_Designation_Card Error from Future");
      }
      notifyListeners();
    } on SocketException {
      dismissMyDialog();
      print("$Leave_By_Designation_Card SocketException");
    } catch (error) {
      dismissMyDialog();
      print("$Leave_By_Designation_Card Exception = $error");
    }
    notifyListeners();
  }
}
