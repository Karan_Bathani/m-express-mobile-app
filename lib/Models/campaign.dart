class Campaign {
  Campaign({
    this.id,
    this.campaignName,
  });

  final String id;
  final String campaignName;

  factory Campaign.fromJson(Map<String, dynamic> json) => Campaign(
        id: json["id"] == null ? null : json["id"],
        campaignName:
            json["campaign_name"] == null ? null : json["campaign_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "campaign_name": campaignName == null ? null : campaignName,
      };
}
