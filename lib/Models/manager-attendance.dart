class ManagerAttendance {
  ManagerAttendance(
      {this.date,
      this.time,
      this.photo,
      this.deviceModel,
      this.storeName,
      this.userId,
      this.outsidePjpReason});

  final DateTime date;
  final DateTime time;
  final String photo;
  final String deviceModel;
  final String storeName;
  final String userId;
  final String outsidePjpReason;

  factory ManagerAttendance.fromJson(Map<String, dynamic> json) =>
      ManagerAttendance(
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        time: json["time"] == null
            ? null
            : DateTime.parse(json["time"]).toLocal(),
        photo: json["photo"] == null ? null : json["photo"],
        deviceModel: json["device_model"] == null ? null : json["device_model"],
        storeName: json["store_name"] == null ? null : json["store_name"],
        userId: json["user_id"] == null ? null : json["user_id"],
        outsidePjpReason: json["outside_pjp_reason"] == null
            ? null
            : json["outside_pjp_reason"],
      );

  Map<String, dynamic> toJson() => {
        "date": date == null ? null : date.toIso8601String(),
        "time": time == null ? null : time.toIso8601String(),
        "photo": photo == null ? null : photo,
        "device_model": deviceModel == null ? null : deviceModel,
        "store_name": storeName == null ? null : storeName,
        "user_id": userId == null ? null : userId,
        "outside_pjp_reason":
            outsidePjpReason == null ? null : outsidePjpReason,
      };
}
