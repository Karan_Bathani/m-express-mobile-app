import 'package:market_express/Models/notice.dart';
import 'package:market_express/Models/store.dart';

class Dashboard {
  Dashboard({
    this.store,
    this.managerName,
    this.managerEmail,
    this.managerPhone,
    this.recentAttendancePhoto,
    this.totalSurveys,
    this.salesTarget,
    this.pendingLeavesCount,
    this.actualSales,
    this.sickLeavesAvailable,
    this.casualLeavesAvailable,
    this.monthlyAttendanceCount,
    this.yearlyLeavesTaken,
    this.yearlyLeavesRemaining,
    this.teamMembersCount,
    this.teamMembersPresent,
    this.teamMembersAbsent,
    this.cummulativeSurveysCount,
    this.mostRecentNotice,
    this.achievedValueOfMonth,
    this.targetValueOfMonth,
  });

  final Store store;
  final String managerName;
  final String managerEmail;
  final String managerPhone;
  final String recentAttendancePhoto;
  final int totalSurveys;
  final int salesTarget;
  final int pendingLeavesCount;
  final int actualSales;
  final double sickLeavesAvailable;
  final double casualLeavesAvailable;

  final double yearlyLeavesTaken;
  final double yearlyLeavesRemaining;
  final int monthlyAttendanceCount;
  final int teamMembersCount;
  final int teamMembersPresent;
  final int teamMembersAbsent;
  final int cummulativeSurveysCount;
  final Notice mostRecentNotice;

  final int achievedValueOfMonth;
  final int targetValueOfMonth;

  factory Dashboard.fromJson(Map<String, dynamic> json) => Dashboard(
        store: json["store"] == null ? null : Store.fromJson(json["store"]),
        managerName: json["manager_name"] == null ? null : json["manager_name"],
        managerEmail:
            json["manager_email"] == null ? null : json["manager_email"],
        managerPhone:
            json["manager_phone"] == null ? null : json["manager_phone"],
        recentAttendancePhoto: json["recent_attendance_photo"] == null
            ? null
            : json["recent_attendance_photo"],
        totalSurveys:
            json["total_surveys"] == null ? null : json["total_surveys"],
        salesTarget: json["salesTarget"] == null ? null : json["salesTarget"],
        pendingLeavesCount: json["pending_leaves_count"] == null
            ? null
            : json["pending_leaves_count"],
        actualSales: json["actualSales"] == null ? null : json["actualSales"],
        sickLeavesAvailable: json["sick_leaves_available"] == null
            ? null
            : json["sick_leaves_available"].toDouble(),
        casualLeavesAvailable: json["casual_leaves_available"] == null
            ? null
            : json["casual_leaves_available"].toDouble(),
        monthlyAttendanceCount: json["monthly_attendance_count"] == null
            ? null
            : json["monthly_attendance_count"],
        yearlyLeavesTaken: json["yearly_leaves_taken"] == null
            ? null
            : json["yearly_leaves_taken"].toDouble(),
        yearlyLeavesRemaining: json["yearly_leaves_remaining"] == null
            ? null
            : json["yearly_leaves_remaining"].toDouble(),
        teamMembersCount: json["team_members_count"] == null
            ? null
            : json["team_members_count"],
        teamMembersPresent: json["team_members_present"] == null
            ? null
            : json["team_members_present"],
        teamMembersAbsent: json["team_members_absent"] == null
            ? null
            : json["team_members_absent"],
        cummulativeSurveysCount: json["cummulative_surveys_count"] == null
            ? null
            : json["cummulative_surveys_count"],
        mostRecentNotice: json["most_recent_notice"] == null
            ? null
            : Notice.fromJson(json["most_recent_notice"]),
        achievedValueOfMonth: json["achievedValueOfMonth"] == null
            ? null
            : json["achievedValueOfMonth"],
        targetValueOfMonth: json["targetValueOfMonth"] == null
            ? null
            : json["targetValueOfMonth"],
      );

  Map<String, dynamic> toJson() => {
        "store": store == null ? null : store.toJson(),
        "manager_name": managerName == null ? null : managerName,
        "manager_email": managerEmail == null ? null : managerEmail,
        "manager_phone": managerPhone == null ? null : managerPhone,
        "recent_attendance_photo":
            recentAttendancePhoto == null ? null : recentAttendancePhoto,
        "total_surveys": totalSurveys == null ? null : totalSurveys,
        "salesTarget": salesTarget == null ? null : salesTarget,
        "pending_leaves_count":
            pendingLeavesCount == null ? null : pendingLeavesCount,
        "actualSales": actualSales == null ? null : actualSales,
        "sick_leaves_available":
            sickLeavesAvailable == null ? null : sickLeavesAvailable,
        "casual_leaves_available":
            casualLeavesAvailable == null ? null : casualLeavesAvailable,
        "monthly_attendance_count":
            monthlyAttendanceCount == null ? null : monthlyAttendanceCount,
        "yearly_leaves_taken":
            yearlyLeavesTaken == null ? null : yearlyLeavesTaken,
        "yearly_leaves_remaining":
            yearlyLeavesRemaining == null ? null : yearlyLeavesRemaining,
        "team_members_count":
            teamMembersCount == null ? null : teamMembersCount,
        "team_members_present":
            teamMembersPresent == null ? null : teamMembersPresent,
        "team_members_absent":
            teamMembersAbsent == null ? null : teamMembersAbsent,
        "cummulative_surveys_count":
            cummulativeSurveysCount == null ? null : cummulativeSurveysCount,
        "most_recent_notice":
            mostRecentNotice == null ? null : mostRecentNotice.toJson(),
        "achievedValueOfMonth":
            achievedValueOfMonth == null ? null : achievedValueOfMonth,
        "targetValueOfMonth":
            targetValueOfMonth == null ? null : targetValueOfMonth,
      };
}
