class AttendanceSummary {
  AttendanceSummary({
    this.userPhoto,
    this.userId,
    this.userType,
    this.userName,
    this.managerId,
    this.managerName,
    this.superManagerId,
    this.superManagerName,
    this.attendanceCount,
  });

  final String userPhoto;
  final String userId;
  final String userType;
  final String userName;
  final String managerId;
  final String managerName;
  final String superManagerId;
  final String superManagerName;
  final int attendanceCount;

  factory AttendanceSummary.fromJson(Map<String, dynamic> json) =>
      AttendanceSummary(
        userPhoto: json["user_photo"] == null ? null : json["user_photo"],
        userId: json["user_id"] == null ? null : json["user_id"],
        userType: json["user_type"] == null ? null : json["user_type"],
        userName: json["user_name"] == null ? null : json["user_name"],
        managerId: json["manager_id"] == null ? null : json["manager_id"],
        managerName: json["manager_name"] == null ? null : json["manager_name"],
        superManagerId:
            json["super_manager_id"] == null ? null : json["super_manager_id"],
        superManagerName: json["super_manager_name"] == null
            ? null
            : json["super_manager_name"],
        attendanceCount:
            json["attendance_count"] == null ? null : json["attendance_count"],
      );

  Map<String, dynamic> toJson() => {
        "user_photo": userPhoto == null ? null : userPhoto,
        "user_id": userId == null ? null : userId,
        "user_type": userType == null ? null : userType,
        "user_name": userName == null ? null : userName,
        "manager_id": managerId == null ? null : managerId,
        "manager_name": managerName == null ? null : managerName,
        "super_manager_id": superManagerId == null ? null : superManagerId,
        "super_manager_name":
            superManagerName == null ? null : superManagerName,
        "attendance_count": attendanceCount == null ? null : attendanceCount,
      };
}
