class OnHandStock {
  OnHandStock({
    this.id,
    this.availableQuantity,
    this.productName,
    this.storeName,
  });

  final String id;
  final int availableQuantity;
  final String productName;
  final String storeName;

  factory OnHandStock.fromJson(Map<String, dynamic> json) => OnHandStock(
        id: json["_id"] == null ? null : json["_id"],
        availableQuantity: json["available_quantity"] == null
            ? null
            : json["available_quantity"],
        productName: json["product_name"] == null ? null : json["product_name"],
        storeName: json["store_name"] == null ? null : json["store_name"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "available_quantity":
            availableQuantity == null ? null : availableQuantity,
        "product_name": productName == null ? null : productName,
        "store_name": storeName == null ? null : storeName,
      };
}
