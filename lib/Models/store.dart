class Store {
  Store({
    this.id,
    this.storeName,
    this.storeDmsCode,
    this.storeType,
    this.territory,
    this.location,
    this.area,
    this.district,
    this.division,
    this.cutOffTime,
    this.createdAt,
    this.latitude,
    this.longitude,
    this.updatedAt,
    this.v,
    this.region,
  });

  final String id;
  final String storeName;
  final String storeDmsCode;
  final String storeType;
  final String territory;
  final String location;
  final String area;
  final String district;
  final String division;
  final String cutOffTime;
  final DateTime createdAt;
  final String latitude;
  final String longitude;
  final DateTime updatedAt;
  final int v;
  final String region;

  factory Store.fromJson(Map<String, dynamic> json) => Store(
        id: json["_id"] == null ? null : json["_id"],
        storeName: json["store_name"] == null ? null : json["store_name"],
        storeDmsCode:
            json["store_dms_code"] == null ? null : json["store_dms_code"],
        storeType: json["store_type"] == null ? null : json["store_type"],
        territory: json["territory"] == null ? null : json["territory"],
        location: json["location"] == null ? null : json["location"],
        area: json["area"] == null ? null : json["area"],
        district: json["district"] == null ? null : json["district"],
        division: json["division"] == null ? null : json["division"],
        cutOffTime: json["cut_off_time"] == null ? null : json["cut_off_time"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        latitude: json["latitude"] == null ? null : json["latitude"],
        longitude: json["longitude"] == null ? null : json["longitude"],
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        region: json["region"] == null ? null : json["region"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "store_name": storeName == null ? null : storeName,
        "store_dms_code": storeDmsCode == null ? null : storeDmsCode,
        "store_type": storeType == null ? null : storeType,
        "territory": territory == null ? null : territory,
        "location": location == null ? null : location,
        "area": area == null ? null : area,
        "district": district == null ? null : district,
        "division": division == null ? null : division,
        "cut_off_time": cutOffTime == null ? null : cutOffTime,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "region": region == null ? null : region,
      };
}

class StoreShrinkedData {
  StoreShrinkedData({
    this.storeId,
    this.storeName,
  });

  final String storeId;
  final String storeName;

  factory StoreShrinkedData.fromJson(Map<String, dynamic> json) =>
      StoreShrinkedData(
        storeId: json["store_id"] == null ? null : json["store_id"],
        storeName: json["store_name"] == null ? null : json["store_name"],
      );

  Map<String, dynamic> toJson() => {
        "store_id": storeId == null ? null : storeId,
        "store_name": storeName == null ? null : storeName,
      };
}
