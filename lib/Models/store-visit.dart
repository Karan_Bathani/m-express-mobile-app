class StoreVisit {
  StoreVisit({
    this.storeName,
    this.storeId,
    this.date,
    this.time,
  });

  final String storeName;
  final String storeId;
  final DateTime date;
  final DateTime time;

  factory StoreVisit.fromJson(Map<String, dynamic> json) => StoreVisit(
        storeName: json["store_name"] == null ? null : json["store_name"],
        storeId: json["store_id"] == null ? null : json["store_id"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        time: json["time"] == null
            ? null
            : DateTime.parse(json["time"]).toLocal(),
      );

  Map<String, dynamic> toJson() => {
        "store_name": storeName == null ? null : storeName,
        "store_id": storeId == null ? null : storeId,
        "date": date == null ? null : date.toIso8601String(),
        "time": time == null ? null : time.toIso8601String(),
      };
}
