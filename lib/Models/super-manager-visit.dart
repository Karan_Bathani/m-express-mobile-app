class SuperManagerVisit {
  SuperManagerVisit({
    this.visitType,
    this.territory,
    this.id,
    this.fomId,
    this.date,
    this.district,
    this.vmsId,
    this.vmsEmployeeId,
    this.userType,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  final String visitType;
  final List<String> territory;
  final String id;
  final String fomId;
  final DateTime date;
  final List<String> district;
  final String vmsId;
  final String vmsEmployeeId;
  final String userType;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int v;

  factory SuperManagerVisit.fromJson(Map<String, dynamic> json) =>
      SuperManagerVisit(
        visitType: json["visit_type"] == null ? null : json["visit_type"],
        territory: json["territory"] == null
            ? null
            : List<String>.from(json["territory"].map((x) => x)),
        id: json["_id"] == null ? null : json["_id"],
        fomId: json["fom_id"] == null ? null : json["fom_id"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        district: json["district"] == null
            ? null
            : List<String>.from(json["district"].map((x) => x)),
        vmsId: json["vms_id"] == null ? null : json["vms_id"],
        vmsEmployeeId:
            json["vms_employee_id"] == null ? null : json["vms_employee_id"],
        userType: json["user_type"] == null ? null : json["user_type"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "visit_type": visitType == null ? null : visitType,
        "territory": territory == null
            ? null
            : List<dynamic>.from(territory.map((x) => x)),
        "_id": id == null ? null : id,
        "fom_id": fomId == null ? null : fomId,
        "date": date == null ? null : date.toIso8601String(),
        "district": district == null
            ? null
            : List<dynamic>.from(district.map((x) => x)),
        "vms_id": vmsId == null ? null : vmsId,
        "vms_employee_id": vmsEmployeeId == null ? null : vmsEmployeeId,
        "user_type": userType == null ? null : userType,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
      };
}
