class LeaveByDesignation {
  LeaveByDesignation({
    this.onHalfDayLeave,
    this.id,
    this.userId,
    this.startDate,
    this.endDate,
    this.reason,
    this.type,
    this.userType,
    this.managerId,
    this.managerName,
    this.superManagerId,
    this.superManagerName,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.rejectReason,
    this.userName,
    this.recentAttendancePhoto,
  });

  final bool onHalfDayLeave;
  final String id;
  final String userId;
  final DateTime startDate;
  final DateTime endDate;
  final String reason;
  final String type;
  final String userType;
  final String managerId;
  final String managerName;
  final String superManagerId;
  final String superManagerName;
  final String status;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int v;
  final String rejectReason;
  final String userName;
  final String recentAttendancePhoto;

  factory LeaveByDesignation.fromJson(Map<String, dynamic> json) =>
      LeaveByDesignation(
        onHalfDayLeave: json["on_half_day_leave"] == null
            ? null
            : json["on_half_day_leave"],
        id: json["_id"] == null ? null : json["_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        startDate: json["start_date"] == null
            ? null
            : DateTime.parse(json["start_date"]),
        endDate:
            json["end_date"] == null ? null : DateTime.parse(json["end_date"]),
        reason: json["reason"] == null ? null : json["reason"],
        type: json["type"] == null ? null : json["type"],
        userType: json["user_type"] == null ? null : json["user_type"],
        managerId: json["manager_id"] == null ? null : json["manager_id"],
        managerName: json["manager_name"] == null ? null : json["manager_name"],
        superManagerId:
            json["super_manager_id"] == null ? null : json["super_manager_id"],
        superManagerName: json["super_manager_name"] == null
            ? null
            : json["super_manager_name"],
        status: json["status"] == null ? null : json["status"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]).toLocal(),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]).toLocal(),
        v: json["__v"] == null ? null : json["__v"],
        rejectReason:
            json["reject_reason"] == null ? null : json["reject_reason"],
        userName: json["user_name"] == null ? null : json["user_name"],
        recentAttendancePhoto: json["recent_attendance_photo"] == null
            ? null
            : json["recent_attendance_photo"],
      );

  Map<String, dynamic> toJson() => {
        "on_half_day_leave": onHalfDayLeave == null ? null : onHalfDayLeave,
        "_id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "start_date": startDate == null ? null : startDate.toIso8601String(),
        "end_date": endDate == null ? null : endDate.toIso8601String(),
        "reason": reason == null ? null : reason,
        "type": type == null ? null : type,
        "user_type": userType == null ? null : userType,
        "manager_id": managerId == null ? null : managerId,
        "manager_name": managerName == null ? null : managerName,
        "super_manager_id": superManagerId == null ? null : superManagerId,
        "super_manager_name":
            superManagerName == null ? null : superManagerName,
        "status": status == null ? null : status,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "reject_reason": rejectReason == null ? null : rejectReason,
        "user_name": userName == null ? null : userName,
        "recent_attendance_photo":
            recentAttendancePhoto == null ? null : recentAttendancePhoto,
      };
}
