class WorkplaceFeedbackQuestion {
  WorkplaceFeedbackQuestion({
    this.options,
    this.id,
    this.question,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  final List<String> options;
  final String id;
  final String question;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int v;

  factory WorkplaceFeedbackQuestion.fromJson(Map<String, dynamic> json) =>
      WorkplaceFeedbackQuestion(
        options: json["options"] == null
            ? null
            : List<String>.from(json["options"].map((x) => x)),
        id: json["_id"] == null ? null : json["_id"],
        question: json["question"] == null ? null : json["question"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "options":
            options == null ? null : List<dynamic>.from(options.map((x) => x)),
        "_id": id == null ? null : id,
        "question": question == null ? null : question,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
      };
}
