class Item {
  Item({
    this.itemId,
    this.itemName,
    this.itemType,
  });

  final String itemId;
  final String itemName;
  final String itemType;

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        itemId: json["item_id"] == null ? null : json["item_id"],
        itemName: json["item_name"] == null ? null : json["item_name"],
        itemType: json["item_type"] == null ? null : json["item_type"],
      );

  Map<String, dynamic> toJson() => {
        "item_id": itemId == null ? null : itemId,
        "item_name": itemName == null ? null : itemName,
        "item_type": itemType == null ? null : itemType,
      };
}
