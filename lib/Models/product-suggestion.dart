class ProductSuggestion {
  ProductSuggestion({
    this.id,
    this.businessUnit,
    this.category,
    this.subCategory,
    this.deviceModel,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.deviceVarient,
  });

  final String id;
  final String businessUnit;
  final String category;
  final String subCategory;
  final String deviceModel;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int v;
  final String deviceVarient;

  factory ProductSuggestion.fromJson(Map<String, dynamic> json) =>
      ProductSuggestion(
        id: json["_id"] == null ? null : json["_id"],
        businessUnit:
            json["business_unit"] == null ? null : json["business_unit"],
        category: json["category"] == null ? null : json["category"],
        subCategory: json["sub_category"] == null ? null : json["sub_category"],
        deviceModel: json["device_model"] == null ? null : json["device_model"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        deviceVarient:
            json["device_varient"] == null ? null : json["device_varient"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "business_unit": businessUnit == null ? null : businessUnit,
        "category": category == null ? null : category,
        "sub_category": subCategory == null ? null : subCategory,
        "device_model": deviceModel == null ? null : deviceModel,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "device_varient": deviceVarient == null ? null : deviceVarient,
      };
}
