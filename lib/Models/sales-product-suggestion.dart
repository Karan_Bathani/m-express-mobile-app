class SalesProductSuggestion {
  SalesProductSuggestion({
    this.deviceVarient,
    this.productId,
    this.deviceModel,
    this.availableQuantity,
  });

  final String deviceVarient;
  final String productId;
  final String deviceModel;
  final int availableQuantity;

  factory SalesProductSuggestion.fromJson(Map<String, dynamic> json) =>
      SalesProductSuggestion(
        deviceVarient:
            json["device_varient"] == null ? null : json["device_varient"],
        productId: json["product_id"] == null ? null : json["product_id"],
        deviceModel: json["device_model"] == null ? null : json["device_model"],
        availableQuantity: json["available_quantity"] == null
            ? null
            : json["available_quantity"],
      );

  Map<String, dynamic> toJson() => {
        "device_varient": deviceVarient == null ? null : deviceVarient,
        "product_id": productId == null ? null : productId,
        "device_model": deviceModel == null ? null : deviceModel,
        "available_quantity":
            availableQuantity == null ? null : availableQuantity,
      };
}
