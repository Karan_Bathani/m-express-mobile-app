class TrainingMaterialUser {
  TrainingMaterialUser({
    this.userId,
    this.userName,
    this.userType,
    this.userPhoto,
    this.viewedAt,
  });

  final String userId;
  final String userName;
  final String userType;
  final String userPhoto;
  final DateTime viewedAt;

  factory TrainingMaterialUser.fromJson(Map<String, dynamic> json) =>
      TrainingMaterialUser(
        userId: json["user_id"] == null ? null : json["user_id"],
        userName: json["user_name"] == null ? null : json["user_name"],
        userType: json["user_type"] == null ? null : json["user_type"],
        userPhoto: json["user_photo"] == null ? null : json["user_photo"],
        viewedAt: json["viewed_at"] == null
            ? null
            : DateTime.parse(json["viewed_at"]).toLocal(),
      );

  Map<String, dynamic> toJson() => {
        "user_id": userId == null ? null : userId,
        "user_name": userName == null ? null : userName,
        "user_type": userType == null ? null : userType,
        "user_photo": userPhoto == null ? null : userPhoto,
        "viewed_at": viewedAt == null ? null : viewedAt.toIso8601String(),
      };
}
