class Survey {
  Survey({
    this.userId,
    this.userName,
    this.managerName,
    this.storeName,
    this.interestedIn,
    this.customerName,
    this.customerContactNumber,
    this.customerBudget,
    this.customerCurrentPhone,
    this.createdAt,
    this.updatedAt,
  });

  final String userId;
  final String userName;
  final String managerName;
  final String storeName;
  final String interestedIn;
  final String customerName;
  final String customerContactNumber;
  final String customerBudget;
  final String customerCurrentPhone;
  final DateTime createdAt;
  final DateTime updatedAt;

  factory Survey.fromJson(Map<String, dynamic> json) => Survey(
        userId: json["user_id"] == null ? null : json["user_id"],
        userName: json["user_name"] == null ? null : json["user_name"],
        managerName: json["manager_name"] == null ? null : json["manager_name"],
        storeName: json["store_name"] == null ? null : json["store_name"],
        interestedIn:
            json["interested_in"] == null ? null : json["interested_in"],
        customerName:
            json["customer_name"] == null ? null : json["customer_name"],
        customerContactNumber: json["customer_contact_number"] == null
            ? null
            : json["customer_contact_number"],
        customerBudget:
            json["customer_budget"] == null ? null : json["customer_budget"],
        customerCurrentPhone: json["customer_current_phone"] == null
            ? null
            : json["customer_current_phone"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
      );

  Map<String, dynamic> toJson() => {
        "user_id": userId == null ? null : userId,
        "user_name": userName == null ? null : userName,
        "manager_name": managerName == null ? null : managerName,
        "store_name": storeName == null ? null : storeName,
        "interested_in": interestedIn == null ? null : interestedIn,
        "customer_name": customerName == null ? null : customerName,
        "customer_contact_number":
            customerContactNumber == null ? null : customerContactNumber,
        "customer_budget": customerBudget == null ? null : customerBudget,
        "customer_current_phone":
            customerCurrentPhone == null ? null : customerCurrentPhone,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
      };
}
