class LoginData {
  LoginData({
    this.token,
    this.userName,
    this.userType,
    this.employeeId,
  });

  final String token;
  final String userName;
  final String userType;
  final String employeeId;

  factory LoginData.fromJson(Map<String, dynamic> json) => LoginData(
        token: json["token"] == null ? null : json["token"],
        userName: json["user_name"] == null ? null : json["user_name"],
        userType: json["user_type"] == null ? null : json["user_type"],
        employeeId: json["employee_id"] == null ? null : json["employee_id"],
      );

  Map<String, dynamic> toJson() => {
        "token": token == null ? null : token,
        "user_name": userName == null ? null : userName,
        "user_type": userType == null ? null : userType,
        "employee_id": employeeId == null ? null : employeeId,
      };
}
