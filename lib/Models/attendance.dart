class Attendance {
  Attendance({
    this.date,
    this.morningTime,
    this.morningPhoto,
    this.morningDeviceModel,
    this.morningStoreName,
    this.morningLatitude,
    this.morningLongitude,
    this.userId,
    this.eveningTime,
    this.eveningPhoto,
    this.eveningDeviceModel,
    this.eveningStoreName,
    this.eveningLatitude,
    this.eveningLongitude,
  });

  final DateTime date;
  final DateTime morningTime;
  final String morningPhoto;
  final String morningDeviceModel;
  final String morningStoreName;
  final String morningLatitude;
  final String morningLongitude;
  final String userId;
  final DateTime eveningTime;
  final String eveningPhoto;
  final String eveningDeviceModel;
  final String eveningStoreName;
  final String eveningLatitude;
  final String eveningLongitude;

  factory Attendance.fromJson(Map<String, dynamic> json) => Attendance(
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        morningTime: json["morning_time"] == null
            ? null
            : DateTime.parse(json["morning_time"]).toLocal(),
        morningPhoto:
            json["morning_photo"] == null ? null : json["morning_photo"],
        morningDeviceModel: json["morning_device_model"] == null
            ? null
            : json["morning_device_model"],
        morningStoreName: json["morning_store_name"] == null
            ? null
            : json["morning_store_name"],
        morningLatitude:
            json["morning_latitude"] == null ? null : json["morning_latitude"],
        morningLongitude: json["morning_longitude"] == null
            ? null
            : json["morning_longitude"],
        userId: json["user_id"] == null ? null : json["user_id"],
        eveningTime: json["evening_time"] == null
            ? null
            : DateTime.parse(json["evening_time"]).toLocal(),
        eveningPhoto:
            json["evening_photo"] == null ? null : json["evening_photo"],
        eveningDeviceModel: json["evening_device_model"] == null
            ? null
            : json["evening_device_model"],
        eveningStoreName: json["evening_store_name"] == null
            ? null
            : json["evening_store_name"],
        eveningLatitude:
            json["evening_latitude"] == null ? null : json["evening_latitude"],
        eveningLongitude: json["evening_longitude"] == null
            ? null
            : json["evening_longitude"],
      );

  Map<String, dynamic> toJson() => {
        "date": date == null ? null : date.toIso8601String(),
        "morning_time":
            morningTime == null ? null : morningTime.toIso8601String(),
        "morning_photo": morningPhoto == null ? null : morningPhoto,
        "morning_device_model":
            morningDeviceModel == null ? null : morningDeviceModel,
        "morning_store_name":
            morningStoreName == null ? null : morningStoreName,
        "morning_latitude": morningLatitude == null ? null : morningLatitude,
        "morning_longitude": morningLongitude == null ? null : morningLongitude,
        "user_id": userId == null ? null : userId,
        "evening_time":
            eveningTime == null ? null : eveningTime.toIso8601String(),
        "evening_photo": eveningPhoto == null ? null : eveningPhoto,
        "evening_device_model":
            eveningDeviceModel == null ? null : eveningDeviceModel,
        "evening_store_name":
            eveningStoreName == null ? null : eveningStoreName,
        "evening_latitude": eveningLatitude == null ? null : eveningLatitude,
        "evening_longitude": eveningLongitude == null ? null : eveningLongitude,
      };
}
