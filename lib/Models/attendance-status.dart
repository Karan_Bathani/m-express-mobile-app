class AttendanceStatus {
  AttendanceStatus({
    this.isMorningAttendanceMarked,
    this.isEveningAttendanceMarked,
  });

  final bool isMorningAttendanceMarked;
  final bool isEveningAttendanceMarked;

  factory AttendanceStatus.fromJson(Map<String, dynamic> json) =>
      AttendanceStatus(
        isMorningAttendanceMarked: json["isMorningAttendanceMarked"],
        isEveningAttendanceMarked: json["isEveningAttendanceMarked"],
      );

  Map<String, dynamic> toJson() => {
        "isMorningAttendanceMarked": isMorningAttendanceMarked,
        "isEveningAttendanceMarked": isEveningAttendanceMarked,
      };
}
