class Notice {
  Notice({
    this.id,
    this.date,
    this.noticeFor,
    this.noticeDescription,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  final String id;
  final DateTime date;
  final String noticeFor;
  final String noticeDescription;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int v;

  factory Notice.fromJson(Map<String, dynamic> json) => Notice(
        id: json["_id"] == null ? null : json["_id"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        noticeFor: json["notice_for"] == null ? null : json["notice_for"],
        noticeDescription: json["notice_description"] == null
            ? null
            : json["notice_description"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]).toLocal(),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]).toLocal(),
        v: json["__v"] == null ? null : json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "date": date == null ? null : date.toIso8601String(),
        "notice_for": noticeFor == null ? null : noticeFor,
        "notice_description":
            noticeDescription == null ? null : noticeDescription,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
      };
}
