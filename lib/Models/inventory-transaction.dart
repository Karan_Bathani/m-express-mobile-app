class InventoryTransaction {
  InventoryTransaction({
    this.quantity,
    this.remark,
    this.storeName,
    this.userName,
    this.managerName,
    this.deviceModel,
    this.productId,
    this.createdAt,
  });

  final int quantity;
  final String remark;
  final String storeName;
  final String userName;
  final String managerName;
  final String deviceModel;
  final String productId;
  final DateTime createdAt;

  factory InventoryTransaction.fromJson(Map<String, dynamic> json) =>
      InventoryTransaction(
        quantity: json["quantity"] == null ? null : json["quantity"],
        remark: json["remark"] == null ? null : json["remark"],
        storeName: json["store_name"] == null ? null : json["store_name"],
        userName: json["user_name"] == null ? null : json["user_name"],
        managerName: json["manager_name"] == null ? null : json["manager_name"],
        deviceModel: json["device_model"] == null ? null : json["device_model"],
        productId: json["product_id"] == null ? null : json["product_id"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "quantity": quantity == null ? null : quantity,
        "remark": remark == null ? null : remark,
        "store_name": storeName == null ? null : storeName,
        "user_name": userName == null ? null : userName,
        "manager_name": managerName == null ? null : managerName,
        "device_model": deviceModel == null ? null : deviceModel,
        "product_id": productId == null ? null : productId,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
      };
}
