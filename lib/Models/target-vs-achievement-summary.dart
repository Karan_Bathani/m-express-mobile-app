class TargetVsAchievementSummary {
  TargetVsAchievementSummary({
    this.previousPage,
    this.nextPage,
    this.totalSalesTargetCount,
    this.salesTargetDocuments,
    this.currentPageSalesTargetCount,
  });

  final MyPage previousPage;
  final MyPage nextPage;
  final int totalSalesTargetCount;
  final List<SalesTargetDocument> salesTargetDocuments;
  final int currentPageSalesTargetCount;

  factory TargetVsAchievementSummary.fromJson(Map<String, dynamic> json) =>
      TargetVsAchievementSummary(
        previousPage: json["previousPage"] == null
            ? null
            : MyPage.fromJson(json["previousPage"]),
        nextPage:
            json["nextPage"] == null ? null : MyPage.fromJson(json["nextPage"]),
        totalSalesTargetCount: json["total_sales_target_count"] == null
            ? null
            : json["total_sales_target_count"],
        salesTargetDocuments: json["sales_target_documents"] == null
            ? null
            : List<SalesTargetDocument>.from(json["sales_target_documents"]
                .map((x) => SalesTargetDocument.fromJson(x))),
        currentPageSalesTargetCount:
            json["current_page_sales_target_count"] == null
                ? null
                : json["current_page_sales_target_count"],
      );

  Map<String, dynamic> toJson() => {
        "previousPage": previousPage == null ? null : previousPage.toJson(),
        "nextPage": nextPage == null ? null : nextPage.toJson(),
        "total_sales_target_count":
            totalSalesTargetCount == null ? null : totalSalesTargetCount,
        "sales_target_documents": salesTargetDocuments == null
            ? null
            : List<dynamic>.from(salesTargetDocuments.map((x) => x.toJson())),
        "current_page_sales_target_count": currentPageSalesTargetCount == null
            ? null
            : currentPageSalesTargetCount,
      };
}

class MyPage {
  MyPage({
    this.page,
    this.limit,
  });

  final int page;
  final int limit;

  factory MyPage.fromJson(Map<String, dynamic> json) => MyPage(
        page: json["page"] == null ? null : json["page"],
        limit: json["limit"] == null ? null : json["limit"],
      );

  Map<String, dynamic> toJson() => {
        "page": page == null ? null : page,
        "limit": limit == null ? null : limit,
      };
}

class SalesTargetDocument {
  SalesTargetDocument({
    this.id,
    this.salesTarget,
    this.storeDetails,
    this.actualSales,
  });

  final String id;
  final int salesTarget;
  final List<StoreDetail> storeDetails;
  final int actualSales;

  factory SalesTargetDocument.fromJson(Map<String, dynamic> json) =>
      SalesTargetDocument(
        id: json["_id"] == null ? null : json["_id"],
        salesTarget: json["sales_target"] == null ? null : json["sales_target"],
        storeDetails: json["store_details"] == null
            ? null
            : List<StoreDetail>.from(
                json["store_details"].map((x) => StoreDetail.fromJson(x))),
        actualSales: json["actual_sales"] == null ? null : json["actual_sales"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "sales_target": salesTarget == null ? null : salesTarget,
        "store_details": storeDetails == null
            ? null
            : List<dynamic>.from(storeDetails.map((x) => x.toJson())),
        "actual_sales": actualSales == null ? null : actualSales,
      };
}

class StoreDetail {
  StoreDetail({
    this.id,
    this.storeName,
    this.storeDmsCode,
    this.storeType,
    this.territory,
    this.location,
    this.area,
    this.district,
    this.division,
    this.longitude,
    this.latitude,
    this.cutOffTime,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.region,
  });

  final String id;
  final String storeName;
  final String storeDmsCode;
  final String storeType;
  final String territory;
  final String location;
  final String area;
  final String district;
  final String division;
  final String longitude;
  final String latitude;
  final String cutOffTime;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int v;
  final String region;

  factory StoreDetail.fromJson(Map<String, dynamic> json) => StoreDetail(
        id: json["_id"] == null ? null : json["_id"],
        storeName: json["store_name"] == null ? null : json["store_name"],
        storeDmsCode:
            json["store_dms_code"] == null ? null : json["store_dms_code"],
        storeType: json["store_type"] == null ? null : json["store_type"],
        territory: json["territory"] == null ? null : json["territory"],
        location: json["location"] == null ? null : json["location"],
        area: json["area"] == null ? null : json["area"],
        district: json["district"] == null ? null : json["district"],
        division: json["division"] == null ? null : json["division"],
        longitude: json["longitude"] == null ? null : json["longitude"],
        latitude: json["latitude"] == null ? null : json["latitude"],
        cutOffTime: json["cut_off_time"] == null ? null : json["cut_off_time"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        region: json["region"] == null ? null : json["region"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "store_name": storeName == null ? null : storeName,
        "store_dms_code": storeDmsCode == null ? null : storeDmsCode,
        "store_type": storeType == null ? null : storeType,
        "territory": territory == null ? null : territory,
        "location": location == null ? null : location,
        "area": area == null ? null : area,
        "district": district == null ? null : district,
        "division": division == null ? null : division,
        "longitude": longitude == null ? null : longitude,
        "latitude": latitude == null ? null : latitude,
        "cut_off_time": cutOffTime == null ? null : cutOffTime,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "region": region == null ? null : region,
      };
}
