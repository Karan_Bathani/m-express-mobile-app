class FoeAttendanceSummary {
  FoeAttendanceSummary({
    this.date,
    this.time,
    this.photo,
    this.deviceModel,
    this.storeName,
    this.latitude,
    this.longitude,
    this.userId,
  });

  final DateTime date;
  final DateTime time;
  final String photo;
  final String deviceModel;
  final String storeName;
  final String latitude;
  final String longitude;
  final String userId;

  factory FoeAttendanceSummary.fromJson(Map<String, dynamic> json) =>
      FoeAttendanceSummary(
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        time: json["time"] == null
            ? null
            : DateTime.parse(json["time"]).toLocal(),
        photo: json["photo"] == null ? null : json["photo"],
        deviceModel: json["device_model"] == null ? null : json["device_model"],
        storeName: json["store_name"] == null ? null : json["store_name"],
        latitude: json["latitude"] == null ? null : json["latitude"],
        longitude: json["longitude"] == null ? null : json["longitude"],
        userId: json["user_id"] == null ? null : json["user_id"],
      );

  Map<String, dynamic> toJson() => {
        "date": date == null ? null : date.toIso8601String(),
        "time": time == null ? null : time.toIso8601String(),
        "photo": photo == null ? null : photo,
        "device_model": deviceModel == null ? null : deviceModel,
        "store_name": storeName == null ? null : storeName,
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "user_id": userId == null ? null : userId,
      };
}
