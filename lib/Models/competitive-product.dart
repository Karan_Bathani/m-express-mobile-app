class CompetitiveProduct {
  CompetitiveProduct({
    this.id,
    this.brand,
    this.model,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  final String id;
  final String brand;
  final String model;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int v;

  factory CompetitiveProduct.fromJson(Map<String, dynamic> json) =>
      CompetitiveProduct(
        id: json["_id"] == null ? null : json["_id"],
        brand: json["brand"] == null ? null : json["brand"],
        model: json["model"] == null ? null : json["model"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "brand": brand == null ? null : brand,
        "model": model == null ? null : model,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
      };
}
