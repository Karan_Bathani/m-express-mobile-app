class Sales {
  Sales({
    this.storeName,
    this.userName,
    this.deviceModel,
    this.managerName,
    this.customerName,
    this.customerContactNumber,
    this.customerEmail,
    this.customerAge,
    this.quantity,
    this.rrp,
    this.color,
    this.createdAt,
    this.updatedAt,
    this.totalPrice,
  });

  final String storeName;
  final String userName;
  final String deviceModel;
  final String managerName;
  final String customerName;
  final String customerContactNumber;
  final String customerEmail;
  final int customerAge;
  final int quantity;
  final String rrp;
  final String color;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int totalPrice;

  factory Sales.fromJson(Map<String, dynamic> json) => Sales(
        storeName: json["store_name"] == null ? null : json["store_name"],
        userName: json["user_name"] == null ? null : json["user_name"],
        deviceModel: json["device_model"] == null ? null : json["device_model"],
        managerName: json["manager_name"] == null ? null : json["manager_name"],
        customerName:
            json["customer_name"] == null ? null : json["customer_name"],
        customerContactNumber: json["customer_contact_number"] == null
            ? null
            : json["customer_contact_number"],
        customerEmail:
            json["customer_email"] == null ? null : json["customer_email"],
        customerAge: json["customer_age"] == null ? null : json["customer_age"],
        quantity: json["quantity"] == null ? null : json["quantity"],
        rrp: json["rrp"] == null ? null : json["rrp"],
        color: json["color"] == null ? null : json["color"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        totalPrice: json["totalPrice"] == null ? null : json["totalPrice"],
      );

  Map<String, dynamic> toJson() => {
        "store_name": storeName == null ? null : storeName,
        "user_name": userName == null ? null : userName,
        "device_model": deviceModel == null ? null : deviceModel,
        "manager_name": managerName == null ? null : managerName,
        "customer_name": customerName == null ? null : customerName,
        "customer_contact_number":
            customerContactNumber == null ? null : customerContactNumber,
        "customer_email": customerEmail == null ? null : customerEmail,
        "customer_age": customerAge == null ? null : customerAge,
        "quantity": quantity == null ? null : quantity,
        "rrp": rrp == null ? null : rrp,
        "color": color == null ? null : color,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "totalPrice": totalPrice == null ? null : totalPrice,
      };
}
