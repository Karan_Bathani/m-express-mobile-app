class TrainingMaterial {
  TrainingMaterial({
    this.userTypes,
    this.id,
    this.businessUnit,
    this.file,
    this.fileName,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  final List<String> userTypes;
  final String id;
  final String businessUnit;
  final String file;
  final String fileName;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int v;

  factory TrainingMaterial.fromJson(Map<String, dynamic> json) =>
      TrainingMaterial(
        userTypes: json["user_types"] == null
            ? null
            : List<String>.from(json["user_types"].map((x) => x)),
        id: json["_id"] == null ? null : json["_id"],
        businessUnit:
            json["business_unit"] == null ? null : json["business_unit"],
        file: json["file"] == null ? null : json["file"],
        fileName: json["file_name"] == null ? null : json["file_name"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "user_types": userTypes == null
            ? null
            : List<dynamic>.from(userTypes.map((x) => x)),
        "_id": id == null ? null : id,
        "business_unit": businessUnit == null ? null : businessUnit,
        "file": file == null ? null : file,
        "file_name": fileName == null ? null : fileName,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
      };
}
