class SurveySummary {
  SurveySummary({
    this.userPhoto,
    this.userId,
    this.userName,
    this.managerId,
    this.managerName,
    this.superManagerId,
    this.superManagerName,
    this.surveyCount,
  });

  final String userPhoto;
  final String userId;
  final String userName;
  final String managerId;
  final String managerName;
  final String superManagerId;
  final String superManagerName;
  final int surveyCount;

  factory SurveySummary.fromJson(Map<String, dynamic> json) => SurveySummary(
        userPhoto: json["user_photo"] == null ? null : json["user_photo"],
        userId: json["user_id"] == null ? null : json["user_id"],
        userName: json["user_name"] == null ? null : json["user_name"],
        managerId: json["manager_id"] == null ? null : json["manager_id"],
        managerName: json["manager_name"] == null ? null : json["manager_name"],
        superManagerId:
            json["super_manager_id"] == null ? null : json["super_manager_id"],
        superManagerName: json["super_manager_name"] == null
            ? null
            : json["super_manager_name"],
        surveyCount: json["survey_count"] == null ? null : json["survey_count"],
      );

  Map<String, dynamic> toJson() => {
        "user_photo": userPhoto == null ? null : userPhoto,
        "user_id": userId == null ? null : userId,
        "user_name": userName == null ? null : userName,
        "manager_id": managerId == null ? null : managerId,
        "manager_name": managerName == null ? null : managerName,
        "super_manager_id": superManagerId == null ? null : superManagerId,
        "super_manager_name":
            superManagerName == null ? null : superManagerName,
        "survey_count": surveyCount == null ? null : surveyCount,
      };
}
