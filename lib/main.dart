import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:in_app_update/in_app_update.dart';
import 'package:market_express/Consumer_Electronics/Screens/tso-home-screen.dart';
import 'package:market_express/Providers/leave-provider.dart';
import 'package:market_express/Screens/manager-home-screen.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Screens/sec-home-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:market_express/Visual%20Merchandiser/Screens/vm-home-screen.dart';
import 'package:market_express/Visual%20Merchandiser/Screens/vmsup-home-screen.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:splashscreen/splashscreen.dart';

import 'Consumer_Electronics/Screens/am-home-screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterDownloader.initialize(
      debug: true // optional: set false to disable printing logs to console
      );
  runApp(
    ChangeNotifierProvider(
      create: (context) => LeaveProvider(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        navigatorKey: getNaviatorKey,
        // theme: ThemeData(
        //   primarySwatch: Colors.blue,
        //   visualDensity: VisualDensity.adaptivePlatformDensity,
        // ),
        home: MyApp(),
      ),
    ),
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _isLoggedIn = false;
  // bool _isManager = false;
  String userType;
  Widget nextScreen = LoginScreen();

  @override
  void initState() {
    super.initState();
    _checkIfLoggedIn();
    checkForUpdate();
  }

  void _checkIfLoggedIn() async {
    var token;
    //Check for the token
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    token = localStorage.getString('token');

    if (token != null) {
      // print('data main token = ' + token);
      setState(() {
        _isLoggedIn = true;
        // _isManager = localStorage.getString("user_type") != "SEC" ||
        //     localStorage.getString("user_type") != "ND. SEC";
        userType = localStorage.getString("user_type");
      });
    }
  }

  Future<void> checkForUpdate() async {
    InAppUpdate.checkForUpdate().then((info) {
      if (info?.updateAvailable == true) {
        InAppUpdate.performImmediateUpdate()
            .catchError((e) => print("debug Error"));
      }
    }).catchError((e) {
      showToast(e.toString(),
          context: context,
          animation: StyledToastAnimation.slideFromBottom,
          reverseAnimation: StyledToastAnimation.slideToBottom,
          startOffset: Offset(0.0, 3.0),
          reverseEndOffset: Offset(0.0, 3.0),
          position: StyledToastPosition.bottom,
          duration: Duration(seconds: 3),
          animDuration: Duration(seconds: 1),
          curve: Curves.elasticOut,
          reverseCurve: Curves.fastOutSlowIn);
    });
  }

  @override
  Widget build(BuildContext context) {
    switch (userType) {
      case "SEC":
      case "ND. SEC":
        nextScreen = SECHomeScreen();
        break;
      case "FOE":
      case "FOM":
        nextScreen = ManagerHomeScreen();
        break;
      case "VM":
        nextScreen = VMHomeScreen();
        break;
      case "VMS":
        nextScreen = VMSupHomeScreen();
        break;
      case "TSO":
        nextScreen = TSOHomeScreen();
        break;
      case "Area Manager":
      case "AM":
        nextScreen = AMHomeScreen();
        break;
      default:
        nextScreen = LoginScreen();
    }
    return StyledToast(
      locale: const Locale('en', 'US'),
      textStyle: TextStyle(fontSize: 14.0, color: Colors.white),
      backgroundColor: Color(0x99000000),
      borderRadius: BorderRadius.circular(miniCardCornerRadius),
      textPadding: EdgeInsets.symmetric(horizontal: 17.0, vertical: 10.0),
      toastAnimation: StyledToastAnimation.slideFromBottom,
      alignment: Alignment.center,
      toastPositions: StyledToastPosition.bottom,
      dismissOtherOnShow: true,
      movingOnWindowChange: true,
      fullWidth: false,
      child: SplashScreen(
        seconds: 2,
        // navigateAfterSeconds: _isLoggedIn
        //     ? _isManager ? ManagerHomeScreen() : SECHomeScreen()
        //     : LoginScreen(),
        navigateAfterSeconds: nextScreen,
        routeName: _isLoggedIn ? "/home" : "/login",
        image: Image.asset('assets/images/logo.png'),
        backgroundColor: Colors.white,
        photoSize: 100.0,
        loadingText: Text(
          'Loading...',
          style: TextStyle(color: Colors.grey, fontSize: 15),
        ),
        useLoader: true,
        loaderColor: colorPrimary,
      ),
    );
  }
}
