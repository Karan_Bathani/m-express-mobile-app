import 'package:flutter/material.dart';
import 'package:market_express/Components/my-custom-button.dart';
import 'package:market_express/Models/store-visit.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:market_express/Visual%20Merchandiser/Screens/Forms/execution-call-form.dart';

class VMTodaysVisitCard extends StatelessWidget {
  final StoreVisit storeVisit;
  final Function onTap;
  VMTodaysVisitCard(this.storeVisit, this.onTap);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        storeVisit.storeName ?? "Store Name",
        style: TextStyle(
          color: textPrimary,
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: Row(
        children: [
          createVisitStatusCard(storeVisit.time != null ? true : false),
          storeVisit.time != null
              ? Text(
                  "at " + storeVisit.time.formatDate(pattern: "hh:mm a"),
                  style: TextStyle(
                    color: textSecondary,
                    fontWeight: FontWeight.w600,
                    fontSize: 14,
                  ),
                )
              : SizedBox(),
        ],
      ),
      trailing: storeVisit.time != null
          ? MyCustomButton(
              containerMargin: EdgeInsets.fromLTRB(
                0,
                12,
                0,
                4,
              ),
              backgroundColor: colorSecondary,
              shadowColor: colorSecondary,
              child: FittedBox(
                child: Text(
                  'Execute\n Call',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              padding: EdgeInsets.symmetric(
                horizontal: 0,
                vertical: 2,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          ExecutionCallForm(storeVisit.storeId, false),
                    ));
              },
            )
          : MyCustomButton(
              containerMargin: EdgeInsets.fromLTRB(
                0,
                12,
                0,
                4,
              ),
              child: FittedBox(
                child: Text(
                  'Mark\n Attendance',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              padding: EdgeInsets.symmetric(
                horizontal: 0,
                vertical: 2,
              ),
              onPressed: onTap,
            ),
    );
  }

  Widget createVisitStatusCard(bool isMarked) {
    return Row(
      children: [
        Card(
          color: isMarked ? success : pending,
          child: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 2.0,
              horizontal: 4,
            ),
            child: Text(
              isMarked ? "visited" : "pending",
              style: TextStyle(
                fontSize: 11,
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
