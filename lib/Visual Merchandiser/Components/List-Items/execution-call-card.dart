import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:market_express/Visual%20Merchandiser/Models/execution-call.dart';
import 'package:market_express/my-image-widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ExecutionCallCard extends StatefulWidget {
  final ExecutionCall executionCall;
  ExecutionCallCard(this.executionCall);

  @override
  _ExecutionCallCardState createState() => _ExecutionCallCardState();
}

class _ExecutionCallCardState extends State<ExecutionCallCard> {
  ExpandableController _expandableController = ExpandableController();

  List<String> imageUrls;
  @override
  void initState() {
    super.initState();
    imageUrls =
        widget.executionCall.images.map((e) => MyApi.BASE_URL + e).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      color: Colors.white,
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.symmetric(
        horizontal: subMargin,
        vertical: subMarginHalf,
      ),
      child: InkWell(
        onTap: () {
          _expandableController.toggle();
          setState(() {});
        },
        splashColor: colorPrimaryLight.withOpacity(.25),
        highlightColor: Colors.transparent,
        child: Column(
          children: [
            FutureBuilder<SharedPreferences>(
                future: SharedPreferences.getInstance(),
                builder: (context, snapshot) {
                  if (snapshot.hasData &&
                      snapshot.data.containsKey("user_type") &&
                      snapshot.data.getString("user_type") == "VMS") {
                    return Container(
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(
                        vertical: subMarginHalf,
                        horizontal: subMargin,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "By: ${widget.executionCall.userId.userName} ",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(color: textPrimary),
                          ),
                          Text(
                            "on: ${widget.executionCall.createdAt.formatDate(pattern: "dd MMM | HH : mm")}",
                            maxLines: 1,
                            style: TextStyle(color: textPrimary),
                          ),
                        ],
                      ),
                    );
                  }
                  return SizedBox();
                }),
            ListTile(
              contentPadding: EdgeInsets.symmetric(
                  horizontal: cardContentPadding, vertical: 0),
              title: ListTile(
                contentPadding: EdgeInsets.all(0),
                dense: true,
                title: Text(
                  widget.executionCall.storeId.storeName,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                subtitle: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    createChip(widget.executionCall.executionType),
                    // Total Images
                    RichText(
                      text: TextSpan(children: [
                        TextSpan(
                          text: "Total Image(s): ",
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: textSecondary,
                            fontSize: 12,
                          ),
                        ),
                        TextSpan(
                          text: "${widget.executionCall.images.length}",
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: textPrimary,
                          ),
                        ),
                      ]),
                    ),
                    //Total Items
                    RichText(
                      text: TextSpan(children: [
                        TextSpan(
                          text: "Total Item(s): ",
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: textSecondary,
                            fontSize: 12,
                          ),
                        ),
                        TextSpan(
                          text: "${widget.executionCall.items.length}",
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: textPrimary,
                          ),
                        ),
                      ]),
                    ),
                  ],
                ),
                trailing: Icon(
                  _expandableController.expanded
                      ? Icons.keyboard_arrow_up
                      : Icons.keyboard_arrow_down,
                ),
              ),
              subtitle: ExpandableNotifier(
                controller: _expandableController,
                child: ScrollOnExpand(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      widget.executionCall.comment != null
                          ? Text(
                              "Comment: ${widget.executionCall.comment}",
                              maxLines:
                                  _expandableController.expanded ? null : 1,
                              overflow: TextOverflow.ellipsis,
                            )
                          : SizedBox(),
                      widget.executionCall.campaignId != null
                          ? Text(
                              "Campaign: ${widget.executionCall.campaignId.campaignName}",
                              maxLines:
                                  _expandableController.expanded ? null : 1,
                              overflow: TextOverflow.ellipsis,
                            )
                          : SizedBox(),
                      Expandable(
                        //> Collapsed
                        collapsed: Column(
                          children: [
                            ListTile(
                                contentPadding: EdgeInsets.all(0),
                                dense: true,
                                visualDensity: VisualDensity.compact,
                                title: Text(
                                  "1. ${widget.executionCall.items[0].itemName}",
                                  // style: TextStyle(
                                  //   fontWeight: FontWeight.w600,
                                  // ),
                                ),
                                trailing: Text(
                                  "${widget.executionCall.items[0].quantity} Pc(s)",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                  ),
                                )),
                          ],
                        ),
                        //? Expanded
                        expanded: Column(
                          children: [
                            Column(
                              children: List.generate(
                                widget.executionCall.items.length,
                                (index) {
                                  return ListTile(
                                      contentPadding: EdgeInsets.all(0),
                                      // enabled: false,
                                      dense: true,
                                      visualDensity: VisualDensity.compact,
                                      title: Text(
                                        "${index + 1}. ${widget.executionCall.items[index].itemName}",
                                        style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          color: textSecondary,
                                        ),
                                      ),
                                      trailing: Text(
                                        "${widget.executionCall.items[index].quantity} Pc(s)",
                                        style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          color: textSecondary,
                                        ),
                                      ));
                                },
                              ),
                            ),
                            //> Images ListView
                            ListTile(
                              title: SizedBox(
                                height: 60,
                                width: double.infinity,
                                child: ListView(
                                  scrollDirection: Axis.horizontal,
                                  children: Iterable.generate(
                                      widget.executionCall.images.length,
                                      (index) {
                                    return Padding(
                                      padding:
                                          EdgeInsets.only(right: subMarginHalf),
                                      child: InkWell(
                                        onTap: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  MyFullScreenImageWidget(
                                                      index, imageUrls),
                                            ),
                                          );
                                        },
                                        child: Image.network(
                                          imageUrls[index],
                                          height: 60,
                                          width: 40,
                                        ),
                                      ),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
