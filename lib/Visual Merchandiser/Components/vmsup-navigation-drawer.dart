import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Providers/leave-provider.dart';
import 'package:market_express/Screens/Forms/leave-form.dart';
import 'package:market_express/Screens/leave-history-screen.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Screens/manager-attendance-history-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VMSupNavigationDrawer extends StatefulWidget {
  final String userRecentAttendancePhoto;
  VMSupNavigationDrawer({@required this.userRecentAttendancePhoto});

  @override
  _VMNavigationDrawerState createState() => _VMNavigationDrawerState();
}

class _VMNavigationDrawerState extends State<VMSupNavigationDrawer> {
  Future _futureLogout;

  Widget _comingSoon({double fontSize = 12}) {
    return Text(
      "Coming Soon...",
      style: TextStyle(
        fontSize: fontSize,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.all(0),
        children: [
          DrawerHeader(
            decoration: BoxDecoration(color: colorPrimary),
            padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Welcome!",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    widget.userRecentAttendancePhoto != null
                        ? Image.network(
                            widget.userRecentAttendancePhoto,
                            height: 90,
                            width: 70,
                            fit: BoxFit.fitHeight,
                          )
                        : Image.asset(
                            "assets/images/user.png",
                            height: 90,
                            width: 70,
                            fit: BoxFit.fitHeight,
                          ),
                    SizedBox(width: subMargin),
                    FutureBuilder<SharedPreferences>(
                      future: SharedPreferences.getInstance(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return Container(
                            padding: EdgeInsets.all(0),
                            margin: EdgeInsets.all(0),
                            width: MediaQuery.of(context).size.width * 0.8 -
                                32 -
                                subMargin -
                                70,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  snapshot.data.getString("employee_id") ??
                                      "Employee ID",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w600,
                                  ),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(height: subMarginHalf),
                                Text(
                                  snapshot.data.getString("user_name") ??
                                      "UserName",
                                  style: TextStyle(color: Colors.white),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(height: subMarginHalf),
                              ],
                            ),
                          );
                        }
                        return SizedBox.shrink();
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
          ListTile(
            dense: true,
            title: Text("View Profile"),
            trailing: _comingSoon(),
          ),
          ListTile(
            dense: true,
            title: Text("Apply Leave"),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => LeaveForm(),
                ),
              );
            },
          ),
          Divider(),
          ListTile(
            dense: true,
            title: Text("My Attendance"),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ManagerAttendanceHistoryScreen(),
                ),
              );
            },
          ),
          ListTile(
            dense: true,
            title: Text("My Leaves"),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => LeaveHistoryScreen(),
                ),
              );
            },
          ),
          Divider(),
          ListTile(
            dense: true,
            title: Text("About Us"),
            trailing: _comingSoon(),
          ),
          ListTile(
            dense: true,
            title: Text('Logout'),
            onTap: () {
              setState(() {
                _futureLogout = _logout(context);
              });
            },
            trailing: FutureBuilder(
              future: _futureLogout,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return SizedBox(
                    width: 25,
                    height: 25,
                    child: CircularProgressIndicator(strokeWidth: 3),
                  );
                }
                return SizedBox.shrink();
              },
            ),
          )
        ],
      ),
    );
  }

  Future _logout(BuildContext context) async {
    Provider.of<LeaveProvider>(context, listen: false).clearProviderData();
    try {
//Clear the localStorage
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.clear();

//Navigate user to loginscreen
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => LoginScreen(),
        ),
        (route) => false,
      );

//logout from the server
      Response response = await MyApi.getDataWithAuthorization("users/logout");
      if (response.data['code'] == 401 || response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.data['code'] == 200) {
      } else {
        Scaffold.of(context)
            .showSnackBar(createSnackBar(msg: "Something went Wrong!"));
        throw Future.error("Error from Future");
      }
    } on SocketException {
      Scaffold.of(context).showSnackBar(createSnackBar());
      throw Future.error(SocketException);
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        }
      }
      throw Future.error(error);
    }
  }
}
