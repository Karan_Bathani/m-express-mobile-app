class ExecutionCall {
  ExecutionCall({
    this.images,
    this.isOutsidePjp,
    this.id,
    this.userId,
    this.storeId,
    this.items,
    this.campaignId,
    this.comment,
    this.executionType,
    this.managerId,
    this.superManagerId,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  final List<String> images;
  final bool isOutsidePjp;
  final String id;
  final UserId userId;
  final StoreId storeId;
  final List<Item> items;
  final CampaignId campaignId;
  final String comment;
  final String executionType;
  final String managerId;
  final String superManagerId;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int v;

  factory ExecutionCall.fromJson(Map<String, dynamic> json) => ExecutionCall(
        images: json["images"] == null
            ? null
            : List<String>.from(json["images"].map((x) => x)),
        isOutsidePjp:
            json["is_outside_pjp"] == null ? null : json["is_outside_pjp"],
        id: json["_id"] == null ? null : json["_id"],
        userId:
            json["user_id"] == null ? null : UserId.fromJson(json["user_id"]),
        storeId: json["store_id"] == null
            ? null
            : StoreId.fromJson(json["store_id"]),
        items: json["items"] == null
            ? null
            : List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
        campaignId: json["campaign_id"] == null
            ? null
            : CampaignId.fromJson(json["campaign_id"]),
        comment: json["comment"] == null ? null : json["comment"],
        executionType:
            json["execution_type"] == null ? null : json["execution_type"],
        managerId: json["manager_id"] == null ? null : json["manager_id"],
        superManagerId:
            json["super_manager_id"] == null ? null : json["super_manager_id"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]).toLocal(),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]).toLocal(),
        v: json["__v"] == null ? null : json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "images":
            images == null ? null : List<dynamic>.from(images.map((x) => x)),
        "is_outside_pjp": isOutsidePjp == null ? null : isOutsidePjp,
        "_id": id == null ? null : id,
        "user_id": userId == null ? null : userId.toJson(),
        "store_id": storeId == null ? null : storeId.toJson(),
        "items": items == null
            ? null
            : List<dynamic>.from(items.map((x) => x.toJson())),
        "campaign_id": campaignId == null ? null : campaignId.toJson(),
        "comment": comment == null ? null : comment,
        "execution_type": executionType == null ? null : executionType,
        "manager_id": managerId == null ? null : managerId,
        "super_manager_id": superManagerId == null ? null : superManagerId,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
      };
}

class CampaignId {
  CampaignId({
    this.id,
    this.campaignName,
  });

  final String id;
  final String campaignName;

  factory CampaignId.fromJson(Map<String, dynamic> json) => CampaignId(
        id: json["_id"] == null ? null : json["_id"],
        campaignName:
            json["campaign_name"] == null ? null : json["campaign_name"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "campaign_name": campaignName == null ? null : campaignName,
      };
}

class Item {
  Item({
    this.id,
    this.itemId,
    this.itemName,
    this.quantity,
  });

  final String id;
  final String itemId;
  final String itemName;
  final int quantity;

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        id: json["_id"] == null ? null : json["_id"],
        itemId: json["item_id"] == null ? null : json["item_id"],
        itemName: json["item_name"] == null ? null : json["item_name"],
        quantity: json["quantity"] == null ? null : json["quantity"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "item_id": itemId == null ? null : itemId,
        "item_name": itemName == null ? null : itemName,
        "quantity": quantity == null ? null : quantity,
      };
}

class StoreId {
  StoreId({
    this.id,
    this.storeName,
  });

  final String id;
  final String storeName;

  factory StoreId.fromJson(Map<String, dynamic> json) => StoreId(
        id: json["_id"] == null ? null : json["_id"],
        storeName: json["store_name"] == null ? null : json["store_name"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "store_name": storeName == null ? null : storeName,
      };
}

class UserId {
  UserId({
    this.id,
    this.userName,
  });

  final String id;
  final String userName;

  factory UserId.fromJson(Map<String, dynamic> json) => UserId(
        id: json["_id"] == null ? null : json["_id"],
        userName: json["user_name"] == null ? null : json["user_name"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "user_name": userName == null ? null : userName,
      };
}
