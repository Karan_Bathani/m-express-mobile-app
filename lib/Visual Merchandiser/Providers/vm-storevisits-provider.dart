import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Models/store-visit.dart';

class VMStoreVisitsProvider with ChangeNotifier {
  Response response;
  List<StoreVisit> storeVisits = [];
  Response adhocResponse;
  List<StoreVisit> adhocStoreVisits = [];

  /// Custom-Method to get Today's-Visit for VM(Endpoint=>users/store-visits)
  Future<List<StoreVisit>> getTodaysStoreVisits() async {
    try {
      response = await MyApi.postDataWithAuthorization({
        "date":
            // DateTime(
            //         todaysDateTime.year, todaysDateTime.month, todaysDateTime.day)
            //     .toIso8601String(),
            // "2021-03-13T18:30:00.000Z",
            DateTime.now().toIso8601String(),
        "month": 0,
      }, "users/store-visits");
      print(
          "debug VMStoreVisits_Provider StoreVisitResponse = ${response.data}");
      if (response.data["code"] == 200) {
        if (storeVisits != null && storeVisits.length != 0) {
          storeVisits.clear();
        }
        for (var storeVisit in response.data["data"]) {
          storeVisits.add(StoreVisit.fromJson(storeVisit));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("debug VMStoreVisits_Provider StoreVisit SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      print(
          "debug VMStoreVisits_Provider StoreVisitResponse Exception = $error");
      return Future.error(error);
    }
    return storeVisits;
  }

  /// Custom-Method to get Today's AdHoc-Visits for VM(Endpoint=>users/outside-pjp-attendance)
  Future<List<StoreVisit>> getTodaysAdHocStoreVisits() async {
    try {
      adhocResponse =
          await MyApi.getDataWithAuthorization("users/outside-pjp-attendance");
      print(
          "debug VMStoreVisits_Provider StoreVisitResponse = ${adhocResponse.data}");
      if (adhocResponse.data["code"] == 200) {
        if (adhocStoreVisits != null && adhocStoreVisits.length != 0) {
          adhocStoreVisits.clear();
        }
        for (var storeVisit in adhocResponse.data["data"]) {
          adhocStoreVisits.add(StoreVisit.fromJson(storeVisit));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("debug VMStoreVisits_Provider StoreVisit SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      print(
          "debug VMStoreVisits_Provider StoreVisitResponse Exception = $error");
      return Future.error(error);
    }
    return adhocStoreVisits;
  }

  void notify() {
    notifyListeners();
  }
}
