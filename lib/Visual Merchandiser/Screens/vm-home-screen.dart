import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/List-Items/notice-card.dart';
import 'package:market_express/Models/dashboard.dart';
import 'package:market_express/Models/notice.dart';
import 'package:market_express/Screens/leave-approval-screen.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Screens/notification-screen.dart';
import 'package:market_express/Screens/training-material-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:market_express/Visual%20Merchandiser/Components/vm-navigation-drawer.dart';
import 'package:market_express/Visual%20Merchandiser/Providers/vm-storevisits-provider.dart';
import 'package:market_express/Visual%20Merchandiser/Screens/Tabs/vm-adhoc-visits-tab.dart';
import 'package:market_express/Visual%20Merchandiser/Screens/Tabs/vm-todays-store-visits-tab.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class VMHomeScreen extends StatefulWidget {
  @override
  _VMHomeScreenState createState() => _VMHomeScreenState();
}

class _VMHomeScreenState extends State<VMHomeScreen> {
  Size screenSize;
  // Future<AttendanceStatus> _futureAttendanceStatus;
  // bool isMorningAttendanceMarked = false;
  Future<Dashboard> _futureDashboard;
  Notice _recentNotice;
  // Future<List<StoreVisit>> _futureTodaysStoreVisits;
  // Future<List<TerritoryVisit>> _futureTodaysTerritoryVisits;
  String _userRecentAttendancePhoto;
  DateTime todaysDateTime = DateTime.now();
  @override
  void initState() {
    super.initState();
    // _futureAttendanceStatus = _getAttendanceStatus();
    _futureDashboard = _getDashboardData();
    // _futureTodaysStoreVisits = _getTodaysStoreVisits();
    // _futureTodaysTerritoryVisits = _getTodaysTerritoryVisits();
  }

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return ChangeNotifierProvider(
      create: (context) => VMStoreVisitsProvider(),
      child: Scaffold(
        backgroundColor: colorPrimary,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: FutureBuilder<SharedPreferences>(
            future: SharedPreferences.getInstance(),
            builder: (context, snapshot) {
              if (snapshot.hasData && snapshot.data.containsKey("user_name")) {
                return FittedBox(
                  child: Text(
                    snapshot.data.getString("user_name"),
                    style: TextStyle(color: Colors.white),
                  ),
                );
              }
              return Text(
                'Home',
                style: TextStyle(color: Colors.white),
              );
            },
          ),
          actions: [
            IconButton(
              icon: Icon(
                Icons.notifications,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => NotificationScreen(),
                    ));
              },
            ),
            IconButton(
              icon: Icon(
                Icons.file_download,
                // color: Colors.amberAccent,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => TrainingMaterialScreen(),
                    ));
              },
            ),
          ],
        ),
        drawer: VMNavigationDrawer(
          userRecentAttendancePhoto: _userRecentAttendancePhoto,
        ),
        body: SafeArea(
          child: Container(
            clipBehavior: Clip.antiAlias,
            width: screenSize.width,
            height: screenSize.height,
            decoration: BoxDecoration(
              color: greyBackground,
              borderRadius:
                  BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
            ),
            child: DefaultTabController(
              length: 2,
              child: ListView(
                padding: EdgeInsets.all(subMargin),
                children: [
                  FutureBuilder<Dashboard>(
                    future: _futureDashboard,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return Column(
                          children: [
                            snapshot.data.mostRecentNotice != null
                                ? NoticeCard(
                                    snapshot.data.mostRecentNotice,
                                    shouldHighlight: true,
                                    isHomeScreen: true,
                                  )
                                : SizedBox(),
                            _createFirstCard(snapshot.data),
                            // snapshot.data.actualSales != null &&
                            //         snapshot.data.salesTarget != null &&
                            //         snapshot.data.salesTarget != 0
                            //     ? _createSalesTargetCard(
                            //         context,
                            //         snapshot.data.actualSales,
                            //         snapshot.data.salesTarget)
                            // : SizedBox.shrink(),
                          ],
                        );
                      } else if (snapshot.connectionState ==
                          ConnectionState.waiting) {
                        return Center(
                          child: Container(
                            margin: EdgeInsets.all(subMargin),
                            width: 30,
                            height: 30,
                            child: CircularProgressIndicator(),
                          ),
                        );
                      } else if (snapshot.hasError) {
                        print("VMHomeScreen FutureError = ${snapshot.error}");

                        return Card(
                          elevation: cardElevation,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.circular(cardCornerRadius)),
                          color: Colors.white,
                          child: Padding(
                            padding: EdgeInsets.all(cardContentPadding),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Something went Wrong!",
                                  style: TextStyle(color: error),
                                ),
                                FlatButton(
                                  onPressed: () {
                                    setState(() {
                                      _futureDashboard = _getDashboardData();
                                    });
                                  },
                                  child: Text("Retry"),
                                ),
                              ],
                            ),
                          ),
                        );
                      }
                      return SizedBox.shrink();
                    },
                  ),
//> Store-Visits Tabs
                  SizedBox(
                    width: screenSize.width / 2 - kToolbarHeight,
                    child: TabBar(
                      tabs: [
                        Tab(
                          child: Text(
                            "PJP",
                            style: TextStyle(
                              color: textPrimary,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        Tab(
                          child: Text(
                            "Ad-Hoc",
                            style: TextStyle(
                              color: textPrimary,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: screenSize.width,
                    height: _recentNotice == null
                        ? screenSize.height / 1.5 + subMargin
                        : screenSize.height / 1.76 + subMargin,
                    child: TabBarView(
                      children: [
                        VMTodaysStoreVisitsTab(),
                        VMAdHocVisitsTab(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  /// Will show manager's contact-details and assigned store-data
  Widget _createFirstCard(Dashboard dashboard) {
    return Column(
      children: [
        Card(
          elevation: cardElevation,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(cardCornerRadius)),
          color: Colors.white,
          child: Padding(
            padding: EdgeInsets.all(cardContentPadding),
            child: ListTile(
              contentPadding: EdgeInsets.all(0),
              // visualDensity: VisualDensity.compact,
              dense: true,
              leading: Image.asset(
                'assets/images/user.png',
                width: 40,
                height: 40,
                alignment: Alignment.center,
              ),
              title: Text(
                dashboard.managerName ?? 'Manager Name',
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: textPrimary,
                ),
              ),
              subtitle: Text(
                'Reporting Manager',
                style: TextStyle(
                  color: textSecondary,
                  fontWeight: FontWeight.w500,
                  fontSize: 12,
                ),
              ),
              trailing: Builder(
                builder: (context) => Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    IconButton(
                      constraints: BoxConstraints(
                        maxWidth: 45,
                        maxHeight: 45,
                        minWidth: 45,
                        minHeight: 45,
                      ),
                      padding: EdgeInsets.all(0),
                      visualDensity: VisualDensity.compact,
                      onPressed: () => sendEmail(
                          dashboard.managerEmail ?? "test@xcitech.com",
                          context),
                      icon: Image.asset(
                        'assets/images/email.png',
                      ),
                    ),
                    SizedBox(width: subMarginHalf),
                    IconButton(
                      constraints: BoxConstraints(
                        maxWidth: 45,
                        maxHeight: 45,
                        minWidth: 45,
                        minHeight: 45,
                      ),
                      padding: EdgeInsets.all(0),
                      visualDensity: VisualDensity.compact,
                      onPressed: () => callnow(
                          dashboard.managerPhone ?? "1234567890", context),
                      icon: Image.asset(
                        'assets/images/phone.png',
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
// Leave approval card for the senior users to be shown when there is a leave application by junior users.
        dashboard.pendingLeavesCount != null
            ? Card(
                clipBehavior: Clip.antiAlias,
                elevation: cardElevation,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(cardCornerRadius)),
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ListTile(
                      // trailing: FlatButton(
                      //   onPressed: () {},
                      //   child: Text("See All",
                      //       style: TextStyle(color: colorPrimaryLight)),
                      // ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => LeaveApprovalScreen(),
                          ),
                        ).then((value) {
                          setState(() {
                            _futureDashboard = _getDashboardData();
                          });
                        });
                      },
                      title: Text(
                        "You have ${dashboard.pendingLeavesCount} pending leave request(s).",
                        maxLines: 2,
                      ),
                    ),
                  ],
                ),
              )
            : SizedBox.shrink(),
      ],
    );
  }

  /// Custom-Method to get Dashboard-Data (Endpoint=>users/dashboard-details)
  Future<Dashboard> _getDashboardData() async {
    Dashboard dashboard;
    try {
      Response response =
          await MyApi.getDataWithAuthorization("users/dashboard-details");
      print("VM_Home_Screen DashBoardResponse = ${response.data}");
      if (response.data['code'] == 401 || response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.data['code'] == 200) {
        dashboard = Dashboard.fromJson(response.data["data"]);
        setState(() {
          _userRecentAttendancePhoto = dashboard.recentAttendancePhoto;
          _recentNotice = dashboard.mostRecentNotice;
        });
        SharedPreferences.getInstance().then((value) {
          value.setDouble(
              "sick_leaves_available", dashboard.sickLeavesAvailable);
          value.setDouble(
              "casual_leaves_available", dashboard.casualLeavesAvailable);
        });
      } else {
        throw Future.error("Error from Future");
      }
    } on SocketException {
      print("VM_Home_Screen DashBoard SocketException");
      throw Future.error(SocketException);
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//Clear the localStorage

          SharedPreferences.getInstance().then((localStorage) {
            localStorage.remove('token');
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        }
      }
      print("VM_Home_Screen DashBoard Exception = $error");
      throw Future.error(error);
    }
    return dashboard;
  }

  Future<void> callnow(String phoneNumber, BuildContext context) async {
    if (await canLaunch('tel:$phoneNumber')) {
      await launch('tel:$phoneNumber');
    } else {
      Scaffold.of(context)
          .showSnackBar(createSnackBar(msg: "Call Not Possible!"));
    }
  }

  Future<void> sendEmail(String email, BuildContext context) async {
    if (await canLaunch('mailto:$email')) {
      await launch('mailto:$email');
    } else {
      Scaffold.of(context)
          .showSnackBar(createSnackBar(msg: "Can't send Email now!"));
    }
  }
}
