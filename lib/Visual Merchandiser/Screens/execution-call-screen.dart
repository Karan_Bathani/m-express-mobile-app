import 'package:flutter/material.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:market_express/Visual%20Merchandiser/Screens/Tabs/vm-all-executioncalls-tab.dart';
import 'package:market_express/Visual%20Merchandiser/Screens/Tabs/vm-todays-executioncalls-tab.dart';

class ExecutionCallScreen extends StatefulWidget {
  @override
  _ExecutionCallScreenState createState() => _ExecutionCallScreenState();
}

class _ExecutionCallScreenState extends State<ExecutionCallScreen> {
  Size screenSize;
  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return DefaultTabController(
      initialIndex: 0,
      length: 2,
      child: Scaffold(
        backgroundColor: colorPrimary,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: Text(
            'My Execution Calls',
            style: TextStyle(color: Colors.white),
          ),
          bottom: TabBar(
            indicatorColor: colorSecondary,
            indicatorSize: TabBarIndicatorSize.label,
            indicatorWeight: 4,
            tabs: [
              Tab(text: "Today (${DateTime.now().formatDate(pattern: "E")})"),
              Tab(text: "         All         "),
            ],
          ),
        ),
        body: SafeArea(
          child: Container(
            clipBehavior: Clip.antiAlias,
            width: screenSize.width,
            height: screenSize.height,
            decoration: BoxDecoration(
              color: greyBackground,
              borderRadius:
                  BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
            ),
            child: TabBarView(
              children: [
                VMTodaysExecutionCallsTab(),
                VMAllExecutionCallsTab(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
