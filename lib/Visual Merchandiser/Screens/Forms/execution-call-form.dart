import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/my-custom-button.dart';
import 'package:market_express/Models/campaign.dart';
import 'package:market_express/Models/items.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:path/path.dart' as path;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_autocomplete_formfield/simple_autocomplete_formfield.dart';

class ExecutionCallForm extends StatefulWidget {
  final String storeId;
  final bool isOutsidePjp;
  ExecutionCallForm(this.storeId, this.isOutsidePjp);
  @override
  _ExecutionCallFormState createState() => _ExecutionCallFormState();
}

class _ExecutionCallFormState extends State<ExecutionCallForm>
    with SingleTickerProviderStateMixin {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autovalidate = false;
  FocusNode _qtyNode = FocusNode();
  Future<Response<dynamic>> _futureCampaigns;
  Campaign _selectedCampaign;
  List<String> campaigns = [];
  List<Campaign> modelCampaigns = [];

  ImagePicker picker = ImagePicker();
  int maxImagesAllowed = 5;
  List<File> _pickedImages = [];

  Future<Response<dynamic>> _futureItems;
  Item _selectedItem;
  List<String> itemStrings = [];
  List<Item> modelItems = [];
  Map<String, List> _selectedItems = {};

  TabController _tabController;
  TextEditingController _itemsController = TextEditingController();
  TextEditingController _quantityController = MaskedTextController(mask: "00");
  TextEditingController _commentController = TextEditingController();
  TextEditingController _campaignController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 4);
    _futureCampaigns = MyApi.getDataWithAuthorization("users/get-campaigns");
    _futureItems = MyApi.getDataWithAuthorization("users/get-execution-items");
  }

  @override
  void dispose() {
    _itemsController.dispose();
    _quantityController.dispose();
    _commentController.dispose();
    _campaignController.dispose();
    super.dispose();
  }

  bool itemsIsEmpty = true;
  Size screenSize;
  FocusScopeNode currentNode;
  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    currentNode = FocusScope.of(context);
    return GestureDetector(
      onTap: () {
        currentNode.unfocus();
      },
      child: Scaffold(
        backgroundColor: colorPrimary,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: Text(
            "Execution Call",
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: SafeArea(
          child: Container(
            clipBehavior: Clip.antiAlias,
            width: screenSize.width,
            height: screenSize.height,
            decoration: BoxDecoration(
              color: Color(0xfff5f5f5),
              borderRadius:
                  BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
            ),
            child: Form(
              key: _formKey,
              autovalidate: _autovalidate,
              child: ListView(
                children: [
                  SizedBox(height: subMargin),
//> Execution Type
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: subMargin),
                    child: Text(
                      "Select Execution Type:",
                      style: TextStyle(
                        color: textSecondary,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  Container(
                    width: screenSize.width,
                    margin: EdgeInsets.all(subMargin),
                    height: 35,
                    child: TabBar(
                      controller: _tabController,
                      indicatorPadding: EdgeInsets.all(8),
                      labelColor: Colors.white,
                      unselectedLabelColor: colorPrimary,
                      indicator: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: colorPrimary,
                      ),
                      tabs: [
                        Tab(
                          child: FittedBox(
                            child: Text("Install"),
                          ),
                        ),
                        Tab(
                          child: FittedBox(
                            child: Text("Removal"),
                          ),
                        ),
                        Tab(
                          child: FittedBox(
                            child: Text("Replace"),
                          ),
                        ),
                        Tab(
                          child: FittedBox(
                            child: Text("Infra"),
                          ),
                        ),
                      ],
                    ),
                  ),
//> Add Items and Quantity with add button
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: subMargin),
                    child: Table(
                      columnWidths: {
                        1: FixedColumnWidth(subMarginHalf),
                        2: FixedColumnWidth(kToolbarHeight),
                      },
                      children: [
                        TableRow(
                          children: [
//> Add Items AutoComplete-TextField
                            FutureBuilder<Response<dynamic>>(
                              future: _futureItems,
                              builder: (context, snapshot) {
                                if (snapshot.hasError) {
                                  if (snapshot.error == SocketException ||
                                      snapshot.error
                                          .toString()
                                          .contains("SocketException")) {
                                    Future.delayed(Duration(milliseconds: 500))
                                        .then((value) {
                                      Scaffold.of(context)
                                          .showSnackBar(createSnackBar());
                                    });
                                  }
                                } else if (snapshot.hasData) {
                                  if (itemStrings != null &&
                                      itemStrings.length != 0) {
                                    itemStrings.clear();
                                    modelItems.clear();
                                  }
                                  snapshot.data.data["data"].forEach((element) {
                                    // print(
                                    //     "CompetitionInfoForm ${Item.fromJson(element)}");
                                    itemStrings
                                        .add(Item.fromJson(element).itemName);
                                    modelItems.add(Item.fromJson(element));
                                  });
                                }
                                return SimpleAutocompleteFormField<Item>(
                                  controller: _itemsController,
                                  keyboardType: TextInputType.text,
                                  style: TextStyle(
                                    color: textPrimary,
                                    fontWeight: FontWeight.w600,
                                  ),
                                  validator: (value) {
                                    print(
                                        "debug ExecutionCallForm Item = $value");
                                    print(
                                        "debug ExecutionCallForm ItemText = ${_itemsController.text}");

                                    if (_itemsController.text.isNotEmpty &&
                                        value == null) {
                                      return "Select an item from the list";
                                    } else if (_itemsController
                                            .text.isNotEmpty &&
                                        value != null &&
                                        !itemStrings.contains(value.itemName)) {
                                      return "Select an item from the list";
                                    } else if (_itemsController
                                            .text.isNotEmpty &&
                                        value != null) {
                                      return "Add this item using Add Item button.";
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.grey[600],
                                      ),
                                    ),
                                    labelText: "Add Item(s)",
                                    hintText: "Enter the items here",
                                  ),
                                  onChanged: (val) {
                                    //* Called when an item is tapped or the field loses focus.
                                    if (val != null) {
                                      _selectedItem = val;
                                      _qtyNode.requestFocus();
                                      print("debug val = ${val.toJson()}");
                                      //   _selectedProducts.putIfAbsent(
                                      //       val.id, () => val);
                                      //   print(
                                      //        "debug Added or Not = ${_selectedProducts.add(val)}");
                                    } else if (val == null) {
                                      _selectedItem = null;
                                    }
                                    // print("debug SelectedProducts = ${_selectedProducts.toSet()}");
                                    print(
                                        "debug SelectedProducts = $_selectedItems");
                                  },
                                  itemToString: (item) =>
                                      item == null ? "" : item.itemName,
                                  onSearch: (search) async {
                                    if ((itemStrings.isEmpty ||
                                            itemStrings.length == 0) &&
                                        snapshot.connectionState ==
                                            ConnectionState.done) {
                                      if (!mounted) {
                                        setState(() {
                                          _futureItems =
                                              MyApi.getDataWithAuthorization(
                                                  "users/get-execution-items");
                                        });
                                      }
                                    }
                                    return modelItems
                                        .where((element) => element.itemName
                                            .toLowerCase()
                                            .contains(
                                              search.toLowerCase(),
                                            ))
                                        .toList();
                                  },
                                  itemBuilder: (context, item) {
                                    return Padding(
                                      padding: EdgeInsets.all(12),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Text(
                                            item.itemName,
                                            style: TextStyle(
                                              color: textPrimary,
                                            ),
                                          ),
                                          Text(
                                            item.itemType,
                                            style: TextStyle(
                                              color: textSecondary,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                  maxSuggestions: itemStrings != null &&
                                          itemStrings.length != 0
                                      ? itemStrings.length
                                      : 0,
                                  suggestionsBuilder: (context, items) {
                                    if (snapshot.connectionState ==
                                        ConnectionState.waiting) {
                                      return Card(
                                        elevation: cardElevation,
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                cardCornerRadius)),
                                        color: Colors.white,
                                        clipBehavior: Clip.antiAlias,
                                        margin: EdgeInsets.symmetric(
                                          // horizontal: subMargin,
                                          vertical: subMarginHalf,
                                        ),
                                        child: Center(
                                          child: Container(
                                            margin: EdgeInsets.all(subMargin),
                                            child: CircularProgressIndicator(),
                                          ),
                                        ),
                                      );
                                    } else if (snapshot.hasError) {
                                      return Card(
                                        elevation: cardElevation,
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                cardCornerRadius)),
                                        color: Colors.white,
                                        clipBehavior: Clip.antiAlias,
                                        margin: EdgeInsets.symmetric(
                                          // horizontal: subMargin,
                                          vertical: subMarginHalf,
                                        ),
                                        child: Center(
                                          child: Container(
                                            margin: EdgeInsets.all(subMargin),
                                            child:
                                                Text("Something Went Wrong!"),
                                          ),
                                        ),
                                      );
                                    }
                                    return Card(
                                      elevation: cardElevation,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              cardCornerRadius)),
                                      color: Colors.white,
                                      clipBehavior: Clip.antiAlias,
                                      margin: EdgeInsets.symmetric(
                                          vertical: subMarginHalf),
                                      child: SizedBox(
                                        width: double.infinity,
                                        height: 200,
                                        child: ListView(
                                          children: items,
                                        ),
                                      ),
                                    );
                                  },
                                );
                              },
                            ),
                            SizedBox(width: subMarginHalf),
//> Quantity TextField
                            TextField(
                              focusNode: _qtyNode,
                              controller: _quantityController,
                              style: TextStyle(
                                color: textPrimary,
                                fontWeight: FontWeight.w600,
                              ),
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.grey[600],
                                  ),
                                ),
                                labelText: "Qty",
                              ),
                              keyboardType: TextInputType.number,
                              inputFormatters: [
                                FilteringTextInputFormatter.digitsOnly
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
//> Add Button
                  MyCustomButton(
                    containerMargin:
                        EdgeInsets.fromLTRB(subMargin, subMargin, subMargin, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Add Item",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                          ),
                        ),
                        Icon(
                          Icons.add,
                          color: Colors.white,
                        ),
                      ],
                    ),
                    onPressed: _addItemToList,
                  ),
//> Items List
                  _selectedItems.values != null &&
                          _selectedItems.values.length != 0
                      ? Column(
                          children: List.generate(
                            _selectedItems.values.length,
                            (index) {
                              // To get the last Item at the Top
                              int lastIndex = _selectedItems.values.length - 1;
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  ListTile(
                                    dense: true,
                                    title: Text(
                                      _selectedItems.values
                                          .toList()[lastIndex - index][0]
                                          .itemName,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 16,
                                      ),
                                    ),
                                    subtitle: Text(
                                        _selectedItems.values
                                            .toList()[lastIndex - index][1]
                                            .toString(),
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 14,
                                        )),
                                    trailing: IconButton(
                                      visualDensity: VisualDensity.compact,
                                      onPressed: () {
                                        setState(() {
                                          _selectedItems.remove(_selectedItems
                                              .keys
                                              .toList()[lastIndex - index]);
                                        });
                                      },
                                      icon: Icon(
                                        Icons.delete,
                                        color: error,
                                      ),
                                    ),
                                    visualDensity: VisualDensity.compact,
                                  ),
                                  Divider(
                                    height: 0,
                                    color: Colors.grey,
                                  ),
                                ],
                              );
                            },
                          ),
                        )
                      : SizedBox.shrink(),
//> Comment (Optional)
                  Padding(
                    padding: EdgeInsets.all(subMargin),
                    child: TextField(
                      controller: _commentController,
                      keyboardType: TextInputType.multiline,
                      minLines: 1,
                      maxLines: 3,
                      style: TextStyle(
                        color: textPrimary,
                        fontWeight: FontWeight.w600,
                      ),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.grey[600],
                          ),
                        ),
                        labelText: "Comment",
                        hintText: "Enter your comment here.",
                        helperText: "Optional",
                        helperStyle: TextStyle(
                          color: textTertiary,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
//> Campaign (Optional)
                  FutureBuilder<Response<dynamic>>(
                    future: _futureCampaigns,
                    builder: (context, snapshot) {
                      if (snapshot.hasError) {
                        if (snapshot.error == SocketException ||
                            snapshot.error
                                .toString()
                                .contains("SocketException")) {
                          Future.delayed(Duration(milliseconds: 500))
                              .then((value) {
                            Scaffold.of(context).showSnackBar(createSnackBar());
                          });
                        }
                      } else if (snapshot.hasData) {
                        if (campaigns != null && campaigns.length != 0) {
                          campaigns.clear();
                          modelCampaigns.clear();
                        }
                        snapshot.data.data["data"].forEach((element) {
                          // print(
                          //     "$Inventory_Form ${Campaign.fromJson(element).deviceModel}");
                          campaigns
                              .add(Campaign.fromJson(element).campaignName);
                          modelCampaigns.add(Campaign.fromJson(element));
                        });
                      }
                      return Padding(
                        padding: EdgeInsets.symmetric(horizontal: subMargin),
                        child: SimpleAutocompleteFormField<Campaign>(
                          controller: _campaignController,
                          keyboardType: TextInputType.text,
                          style: TextStyle(
                            color: textPrimary,
                            fontWeight: FontWeight.w600,
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.grey[600],
                              ),
                            ),
                            labelText: "Campaign",
                            hintText: "Select Campaign",
                            helperText: "Optional",
                            helperStyle: TextStyle(
                              color: textTertiary,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          validator: (value) {
                            if (_campaignController.text.isNotEmpty &&
                                value == null) {
                              return "Select a Campaign from the list";
                            } else if (_campaignController.text != null &&
                                value != null &&
                                (!campaigns.contains(value.campaignName) ||
                                    campaigns.length == 0)) {
                              return "Select a Campaign from the list";
                            }
                            return null;
                          },
                          onChanged: (val) {
                            //* Called when an item is tapped or the field loses focus.
                            if (val != null) {
                              _selectedCampaign = val;
                            } else {
                              _selectedCampaign = null;
                            }
                          },
                          itemToString: (item) =>
                              item == null ? "" : item.campaignName,
                          onSearch: (search) async {
                            if (campaigns.isEmpty || campaigns.length == 0) {
                              if (!mounted) {
                                setState(() {
                                  _futureCampaigns =
                                      MyApi.getDataWithAuthorization(
                                          "users/get-campaigns");
                                });
                              }
                            }
                            return modelCampaigns
                                .where(
                                  (element) => element.campaignName
                                      .toLowerCase()
                                      .contains(
                                        search.toLowerCase(),
                                      ),
                                )
                                .toList();
                          },
                          itemBuilder: (context, item) {
                            return Padding(
                              padding: EdgeInsets.all(12),
                              child: Text(item.campaignName),
                            );
                          },
                          maxSuggestions: modelCampaigns != null &&
                                  modelCampaigns.length != 0
                              ? modelCampaigns.length
                              : 0,
                          suggestionsBuilder: (context, items) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return Card(
                                elevation: cardElevation,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        cardCornerRadius)),
                                color: Colors.white,
                                clipBehavior: Clip.antiAlias,
                                margin: EdgeInsets.symmetric(
                                  // horizontal: subMargin,
                                  vertical: subMarginHalf,
                                ),
                                child: Center(
                                  child: Container(
                                    margin: EdgeInsets.all(subMargin),
                                    child: CircularProgressIndicator(),
                                  ),
                                ),
                              );
                            } else if (snapshot.hasError) {
                              String errorText = "Something Went Wrong!";
                              if (snapshot.error.toString().contains("404")) {
                                errorText = "No Data Found";
                              }
                              return Card(
                                elevation: cardElevation,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        cardCornerRadius)),
                                color: Colors.white,
                                clipBehavior: Clip.antiAlias,
                                margin: EdgeInsets.symmetric(
                                  // horizontal: subMargin,
                                  vertical: subMarginHalf,
                                ),
                                child: Center(
                                  child: Container(
                                    margin: EdgeInsets.all(subMargin),
                                    child: Text(errorText),
                                  ),
                                ),
                              );
                            }
                            return Card(
                              elevation: cardElevation,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.circular(cardCornerRadius)),
                              color: Colors.white,
                              clipBehavior: Clip.antiAlias,
                              margin: EdgeInsets.symmetric(
                                // horizontal: subMargin,
                                vertical: subMarginHalf,
                              ),
                              child: SizedBox(
                                width: double.infinity,
                                height: 200,
                                child: ListView(
                                  children: items,
                                ),
                              ),
                            );
                          },
                        ),
                      );
                    },
                  ),
//> Images
                  Padding(
                    padding: EdgeInsets.all(subMargin),
                    child: Text(
                      "Add Image(s): Maximum $maxImagesAllowed",
                      style: TextStyle(
                          color: textPrimary, fontWeight: FontWeight.w600),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: subMargin),
                    child: _buildImageListView(),
                  ),
                  SizedBox(height: subMargin),
//> Submit Button
                  MyCustomButton(
                    enabled:
                        _selectedItems.length != 0 && _pickedImages.length != 0
                            ? true
                            : false,
                    containerMargin: EdgeInsets.all(subMargin),
                    padding: EdgeInsets.symmetric(
                      vertical: subMargin,
                      horizontal: subMarginDouble,
                    ),
                    onPressed: () => _onSubmitExecutionCall(),
                  ),
                  SizedBox(height: subMargin),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _addItemToList() {
    if (_selectedItem != null && _quantityController.text.isNotEmpty) {
      setState(() {
        _selectedItems.putIfAbsent(_selectedItem.itemId,
            () => [_selectedItem, _quantityController.text]);
        _selectedItem = null;
        _itemsController.clear();
        _quantityController.clear();
        // print(
        //     "debug Added or Not = ${_selectedProducts.add(val)}");
      });
    } else if (_selectedItem == null) {
      showToast("Select an Item",
          context: context,
          animation: StyledToastAnimation.slideFromBottom,
          reverseAnimation: StyledToastAnimation.fade,
          startOffset: Offset(0.0, 3.0),
          // reverseEndOffset: Offset(0.0, 3.0),
          position: StyledToastPosition.top,
          duration: Duration(seconds: 3),
          animDuration: Duration(seconds: 1),
          backgroundColor: error,
          textStyle: TextStyle(color: Colors.white),
          curve: Curves.elasticOut,
          reverseCurve: Curves.fastOutSlowIn);
    } else if (_quantityController.text.isEmpty) {
      showToast("Enter Quantity",
          context: context,
          animation: StyledToastAnimation.slideFromBottom,
          reverseAnimation: StyledToastAnimation.fade,
          startOffset: Offset(0.0, 3.0),
          // reverseEndOffset: Offset(0.0, 3.0),
          position: StyledToastPosition.top,
          duration: Duration(seconds: 3),
          animDuration: Duration(seconds: 1),
          backgroundColor: error,
          textStyle: TextStyle(color: Colors.white),
          curve: Curves.elasticOut,
          reverseCurve: Curves.fastOutSlowIn);
    }
  }

  /// Custom-Method to get the image from camera.
  Future<void> _getImage() async {
    final PickedFile pickedFile = await picker.getImage(
      source: ImageSource.camera,
      imageQuality: 90,
      maxWidth: 700,
      maxHeight: 700,
    );

    if (pickedFile != null) {
      setState(() {
        _pickedImages.add(File(pickedFile.path));
      });
      print(
          "$Mark_Attendance_Screen Length = ${File(pickedFile.path).lengthSync()}");
      print("$Mark_Attendance_Screen Path = ${File(pickedFile.path).path}");
    } else {
      print('No image selected.');
    }
  }

  Widget _buildImageListView() {
    return GridView.builder(
      shrinkWrap: true,
      padding: EdgeInsets.all(0),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        crossAxisSpacing: mainMarginHalf,
        mainAxisSpacing: mainMarginHalf,
      ),
      physics: NeverScrollableScrollPhysics(),
      itemCount: _pickedImages.length < maxImagesAllowed
          ? _pickedImages.length + 1
          : maxImagesAllowed,
      itemBuilder: (context, index) {
        if (_pickedImages.length < maxImagesAllowed &&
            index == _pickedImages.length) {
          return Padding(
            padding:
                EdgeInsets.fromLTRB(0, subMarginHalf, subMargin, subMarginHalf),
            child: FlatButton(
              onPressed: _getImage,
              color: Colors.grey[400],
              child: Icon(
                Icons.add,
                size: 50,
              ),
            ),
          );
        }
        return Stack(
          children: [
            Padding(
              padding: EdgeInsets.all(0),
              child: Image.file(
                File(_pickedImages[index].path),
                fit: BoxFit.cover,
                width: MediaQuery.of(context).size.width,
              ),
            ),
            Positioned(
                top: -10,
                right: 0,
                child: IconButton(
                  icon: Icon(
                    Icons.remove_circle,
                    color: error,
                  ),
                  onPressed: () {
                    setState(() {
                      _pickedImages.removeWhere((element) =>
                          element.path == _pickedImages[index].path);
                    });
                  },
                ))
          ],
        );
      },
    );
  }

  void _onSubmitExecutionCall() async {
    try {
      if (!_formKey.currentState.validate()) {
        setState(() {
          _autovalidate = true;
        });
        return;
      }
      List<Map<String, dynamic>> itemsObject = List.generate(
          _selectedItems.length,
          (index) => {
                "item_id": _selectedItems.keys.toList()[index],
                "item_name": _selectedItems.values.toList()[index][0].itemName,
                "quantity": _selectedItems.values.toList()[index][1],
              });
      print("debug Lists = $itemsObject");
      currentNode.unfocus();
      showMyLoadingDialog(dialogText: "Submitting the data ...\nPlease Wait!");
      Map<String, dynamic> data = {
        "store_id": widget.storeId,
        "is_outside_pjp": widget.isOutsidePjp,
        "items": jsonEncode(itemsObject),
      };

      switch (_tabController.index) {
        case 0:
          data.addAll({
            "execution_type": "install",
          });
          break;
        case 1:
          data.addAll({
            "execution_type": "removal",
          });
          break;
        case 2:
          data.addAll({
            "execution_type": "replace",
          });
          break;
        case 3:
          data.addAll({
            "execution_type": "infra",
          });
          break;
        default:
          data.addAll({
            "execution_type": "install",
          });
          break;
      }
      if (_commentController.text.isNotEmpty) {
        data.addAll({
          "comment": _commentController.text,
        });
      }
      if (_selectedCampaign != null) {
        data.addAll({
          "campaign_id": _selectedCampaign.campaignName,
        });
      }
      FormData formData = FormData.fromMap(data);
      int i = 0;
      if (_pickedImages != null) {
        for (var file in _pickedImages) {
          String _basename = path.basename(_pickedImages[i].path);
          String _extension = _basename.substring(_basename.indexOf(".") + 1);
          formData.files.addAll([
            MapEntry(
              "files",
              await MultipartFile.fromFile(
                file.path,
                filename: path.basename(_pickedImages[i].path),
                contentType: MediaType('image', _extension),
              ),
            ),
          ]);
          i = i + 1;
        }
      }
      print("debug FormData = ${formData.fields}");
      // return;
      Response response = await MyApi.postFormDataWithAuthorization(
          formData, "stores/add-execution-call-summary");

// Check the response code
      if (response.statusCode == 401) {
//Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        //< CODE = 200
        dismissMyDialog();
        showToast(response.data["message"],
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.bottom,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            backgroundColor: success,
            textStyle: TextStyle(color: Colors.white),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
        Navigator.pop(context);
      } else {
        // For all other cases of error
        dismissMyDialog();
        showToast("Something Went Wrong!",
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.center,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            backgroundColor: error,
            textStyle: TextStyle(color: Colors.white),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
      }
    } on SocketException {
      dismissMyDialog();
      print("debug ExecutionCallForm2 SocketException");
      showToast("You are offline, Check your Internet Connection!",
          context: context,
          animation: StyledToastAnimation.slideFromBottom,
          reverseAnimation: StyledToastAnimation.slideToBottom,
          startOffset: Offset(0.0, 3.0),
          reverseEndOffset: Offset(0.0, 3.0),
          position: StyledToastPosition.center,
          duration: Duration(seconds: 3),
          animDuration: Duration(seconds: 1),
          backgroundColor: error,
          textStyle: TextStyle(color: Colors.white),
          curve: Curves.elasticOut,
          reverseCurve: Curves.fastOutSlowIn);
    } catch (e) {
      print("debug ExecutionCallForm2 Exception = ${e.response.data}");
      dismissMyDialog();
      if (e is DioError) {
        print("debug ExecutionCallForm2 DioError = ${e.response.statusCode}");
        print("debug Request ${e.response.request}");
        if (e.response.statusCode == 413) {
          showToast(e.response.data["message"].toString(),
              context: context,
              animation: StyledToastAnimation.slideFromBottom,
              reverseAnimation: StyledToastAnimation.slideToBottom,
              startOffset: Offset(0.0, 3.0),
              reverseEndOffset: Offset(0.0, 3.0),
              position: StyledToastPosition.center,
              duration: Duration(seconds: 3),
              animDuration: Duration(seconds: 1),
              backgroundColor: error,
              textStyle: TextStyle(color: Colors.white),
              curve: Curves.elasticOut,
              reverseCurve: Curves.fastOutSlowIn);
        } else if (e.response.statusCode == 422) {
          showToast(e.response.data["message"].toString(),
              context: context,
              animation: StyledToastAnimation.slideFromBottom,
              reverseAnimation: StyledToastAnimation.slideToBottom,
              startOffset: Offset(0.0, 3.0),
              reverseEndOffset: Offset(0.0, 3.0),
              position: StyledToastPosition.center,
              duration: Duration(seconds: 3),
              animDuration: Duration(seconds: 1),
              backgroundColor: error,
              textStyle: TextStyle(color: Colors.white),
              curve: Curves.elasticOut,
              reverseCurve: Curves.fastOutSlowIn);
        } else {
          showToast(e.response.data["message"].toString(),
              context: context,
              animation: StyledToastAnimation.slideFromBottom,
              reverseAnimation: StyledToastAnimation.slideToBottom,
              startOffset: Offset(0.0, 3.0),
              reverseEndOffset: Offset(0.0, 3.0),
              position: StyledToastPosition.center,
              duration: Duration(seconds: 3),
              animDuration: Duration(seconds: 1),
              backgroundColor: error,
              textStyle: TextStyle(color: Colors.white),
              curve: Curves.elasticOut,
              reverseCurve: Curves.fastOutSlowIn);
        }
      } else {
        showToast(e.response.data["message"].toString(),
            context: context,
            animation: StyledToastAnimation.slideFromBottom,
            reverseAnimation: StyledToastAnimation.slideToBottom,
            startOffset: Offset(0.0, 3.0),
            reverseEndOffset: Offset(0.0, 3.0),
            position: StyledToastPosition.center,
            duration: Duration(seconds: 3),
            animDuration: Duration(seconds: 1),
            backgroundColor: error,
            textStyle: TextStyle(color: Colors.white),
            curve: Curves.elasticOut,
            reverseCurve: Curves.fastOutSlowIn);
      }
    }
  }
}
