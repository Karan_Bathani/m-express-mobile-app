import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Components/my-date-picker.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:market_express/Visual%20Merchandiser/Components/List-Items/execution-call-card.dart';
import 'package:market_express/Visual%20Merchandiser/Models/execution-call.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VMTodaysExecutionCallsTab extends StatefulWidget {
  @override
  _VMTodaysExecutionCallsTabState createState() =>
      _VMTodaysExecutionCallsTabState();
}

class _VMTodaysExecutionCallsTabState extends State<VMTodaysExecutionCallsTab>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  Future<List<ExecutionCall>> _futureExecutionCalls;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _selectedDateTime = todaysDateTime;
    _futureExecutionCalls = _getExecutionCalls();
  }

  DateTime todaysDateTime = DateTime.now();
  DateTime _selectedDateTime;
  Size screenSize;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    screenSize = MediaQuery.of(context).size;
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: () {
        setState(() {
          _futureExecutionCalls = _getExecutionCalls();
        });
        return _futureExecutionCalls;
      },
      child: Column(
        children: [
//> Date Selector
          // FutureBuilder<SharedPreferences>(
          //     future: SharedPreferences.getInstance(),
          //     builder: (context, snapshot) {
          //       if (snapshot.hasData &&
          //           snapshot.data.containsKey("user_type") &&
          //           snapshot.data.getString("user_type") == "VMS") {
          //         return GestureDetector(
          //           onTap: () => _showMyDatePicker(context),
          //           child: Container(
          //             padding: EdgeInsets.symmetric(
          //               horizontal: subMargin,
          //               vertical: subMargin,
          //             ),
          //             color: dropDownBackground,
          //             child: Row(
          //               mainAxisAlignment: MainAxisAlignment.center,
          //               children: [
          //                 Text(
          //                   _selectedDateTime.formatDate(
          //                       pattern: "dd MMMM, yyyy"),
          //                   style: TextStyle(
          //                     fontSize: 17,
          //                     fontWeight: FontWeight.w600,
          //                     color: Colors.grey[600],
          //                   ),
          //                 ),
          //                 Icon(
          //                   Icons.date_range,
          //                   color: Colors.grey[600],
          //                 ),
          //               ],
          //             ),
          //           ),
          //         );
          //       }
          //       return SizedBox();
          //     }),
          FutureBuilder<List<ExecutionCall>>(
            future: _futureExecutionCalls,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                if (snapshot.error.toString().contains("404")) {
                  return buildEmptyWidget(screenSize,
                      text: "No Execution Calls Found");
                }
                return buildErrorWidget(screenSize);
              } else if (snapshot.hasData &&
                  snapshot.connectionState == ConnectionState.done &&
                  snapshot.data.length != 0) {
                return _buildExeuctionCallsList(snapshot.data);
              } else if (snapshot.connectionState == ConnectionState.waiting) {
                _refreshIndicatorKey.currentState?.show();
                return _buildExeuctionCallsList(snapshot.data);
              }
              return Expanded(
                child: ListView(),
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _buildExeuctionCallsList(List<ExecutionCall> executionCalls) {
    return Expanded(
      child: ListView.builder(
        itemCount: executionCalls != null ? executionCalls.length : 0,
        itemBuilder: (context, index) =>
            ExecutionCallCard(executionCalls[index]),
      ),
    );
  }

  Future<List<ExecutionCall>> _getExecutionCalls() async {
    List<ExecutionCall> executionCalls = [];
    try {
      Response response = await MyApi.postDataWithAuthorization({
        "date": _selectedDateTime.toIso8601String(),
        "month": _selectedDateTime.month,
        "year": _selectedDateTime.year,
      }, "stores/get-execution-call-summary");
      print("debug VMTodaysExecutionCallsTab Response = ${response.data}");
      if (response.statusCode == 401) {
//> Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var attendanceSummary in response.data["data"]) {
          executionCalls.add(ExecutionCall.fromJson(attendanceSummary));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("debug VMTodaysExecutionCallsTab SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//> Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.clear();
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      print("debug VMTodaysExecutionCallsTab Exception = $error");
      throw Future.error(error);
    }
    return executionCalls;
  }

  void _showMyDatePicker(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        barrierColor: Colors.black.withOpacity(.3),
        builder: (context) {
          return WillPopScope(
            onWillPop: () async {
              return true;
            },
            child: Dialog(
              insetPadding: EdgeInsets.all(mainMargin),
              child: MyDatePicker(
                enableRange: false,
                enablePastDates: true,
                initialSelectedDate: _selectedDateTime,
              ),
            ),
          );
        }).then((value) {
      if (value != null) {
        setState(() {
          _selectedDateTime = value[0];
          _futureExecutionCalls = _getExecutionCalls();
        });
      }
    });
  }
}
