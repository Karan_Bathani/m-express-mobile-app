import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:market_express/Api/my-api.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:market_express/Visual%20Merchandiser/Components/List-Items/execution-call-card.dart';
import 'package:market_express/Visual%20Merchandiser/Models/execution-call.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VMAllExecutionCallsTab extends StatefulWidget {
  @override
  _VMAllExecutionCallsTabState createState() => _VMAllExecutionCallsTabState();
}

class _VMAllExecutionCallsTabState extends State<VMAllExecutionCallsTab>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  Future<List<ExecutionCall>> _futureExecutionCalls;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _selectedDateTime = todaysDateTime;
    _futureExecutionCalls = _getExecutionCalls();
  }

  DateTime todaysDateTime = DateTime.now();
  DateTime _selectedDateTime;
  Size screenSize;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    screenSize = MediaQuery.of(context).size;
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: () {
        setState(() {
          _futureExecutionCalls = _getExecutionCalls();
        });
        return _futureExecutionCalls;
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
//> Date Selector
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: subMargin,
            ),
            clipBehavior: Clip.antiAlias,
            decoration: BoxDecoration(
              color: dropDownBackground,
              borderRadius:
                  BorderRadius.vertical(top: Radius.circular(cardCornerRadius)),
            ),
            child: DropdownButton(
              underline: SizedBox.shrink(),
              iconSize: 30,
              isExpanded: true,
              hint: Text(
                DateFormat("MMMM, yyy").format(_selectedDateTime),
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w600,
                ),
              ),
              items: List.generate(10, (index) {
                DateTime generatedDate =
                    DateTime(todaysDateTime.year, todaysDateTime.month - index);
                return DropdownMenuItem(
                  child: Text(DateFormat("MMMM, yyy").format(generatedDate)),
                  value: generatedDate,
                );
              }),
              onChanged: (value) {
                setState(() {
                  _selectedDateTime = value;
                  _futureExecutionCalls = _getExecutionCalls();
                });
              },
            ),
          ),
          FutureBuilder<List<ExecutionCall>>(
            future: _futureExecutionCalls,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                _refreshIndicatorKey.currentState?.show();
                return _buildExeuctionCallsList(snapshot.data);
              } else if (snapshot.hasError) {
                if (snapshot.error.toString().contains("404")) {
                  return buildEmptyWidget(screenSize,
                      text: "No Execution Call(s) Found");
                }
                return buildErrorWidget(screenSize);
              } else if (snapshot.hasData &&
                  snapshot.connectionState == ConnectionState.done &&
                  snapshot.data.length != 0) {
                return _buildExeuctionCallsList(snapshot.data);
              }
              return Expanded(
                child: ListView(),
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _buildExeuctionCallsList(List<ExecutionCall> executionCalls) {
    return Expanded(
      child: ListView.builder(
        itemCount: executionCalls != null ? executionCalls.length : 0,
        itemBuilder: (context, index) =>
            ExecutionCallCard(executionCalls[index]),
      ),
    );
  }

  Future<List<ExecutionCall>> _getExecutionCalls() async {
    List<ExecutionCall> executionCalls = [];
    try {
      Response response = await MyApi.postDataWithAuthorization({
        "month": _selectedDateTime.month,
        "year": _selectedDateTime.year,
      }, "stores/get-execution-call-summary");
      print("debug VMTodaysExecutionCallsTab Response = ${response.data}");
      if (response.statusCode == 401) {
//> Clear the localStorage
        SharedPreferences.getInstance().then((localStorage) {
          localStorage.remove('token');
        });

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
            (route) => false);
      } else if (response.statusCode == 200) {
        for (var attendanceSummary in response.data["data"]) {
          executionCalls.add(ExecutionCall.fromJson(attendanceSummary));
        }
      } else {
        return Future.error("Error from Future");
      }
    } on SocketException {
      print("debug VMTodaysExecutionCallsTab SocketException");
      return Future.error(SocketException("SocketException"));
    } catch (error) {
      if (error is DioError) {
        if (error.response.statusCode == 401) {
//> Clear the localStorage
          SharedPreferences.getInstance().then((localStorage) {
            localStorage.clear();
          });

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
              (route) => false);
        } else if (error.response.statusCode == 404) {
          return Future.error(HttpException("404"));
        }
      }
      print("debug VMTodaysExecutionCallsTab Exception = $error");
      throw Future.error(error);
    }
    return executionCalls;
  }
}
