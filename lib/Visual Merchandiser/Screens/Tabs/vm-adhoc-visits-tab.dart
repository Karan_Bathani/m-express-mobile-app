import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:market_express/Models/store-visit.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Screens/unlisted-mark-attendance-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:market_express/Visual%20Merchandiser/Components/List-Items/vm-todays-adhoc-visits-card.dart';
import 'package:market_express/Visual%20Merchandiser/Providers/vm-storevisits-provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VMAdHocVisitsTab extends StatefulWidget {
  @override
  _MVAdHocVisitsTabState createState() => _MVAdHocVisitsTabState();
}

class _MVAdHocVisitsTabState extends State<VMAdHocVisitsTab>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  Future<List<StoreVisit>> _futureTodaysAdHocStoreVisits;
  VMStoreVisitsProvider _vmStoreVisitsProvider;
  @override
  void initState() {
    super.initState();
    _vmStoreVisitsProvider =
        Provider.of<VMStoreVisitsProvider>(context, listen: false);
    _futureTodaysAdHocStoreVisits =
        _vmStoreVisitsProvider.getTodaysAdHocStoreVisits();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.fromLTRB(4, 4, 4, subMargin),
      color: Colors.white,
      child: ListView(
        children: [
          ListTile(
            title: Text(
              "Mark Attendance for unlisted store.",
              style: TextStyle(
                color: textPrimary,
                // fontWeight: FontWeight.bold,
              ),
            ),
            trailing: Icon(Icons.arrow_forward),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => UnlistedMarkAttendanceScreen(),
                ),
              ).then(
                (value) {
                  if (value != null) {
                    showToast(value,
                        context: context,
                        animation: StyledToastAnimation.slideFromBottom,
                        reverseAnimation: StyledToastAnimation.slideToBottom,
                        startOffset: Offset(0.0, 3.0),
                        reverseEndOffset: Offset(0.0, 3.0),
                        position: StyledToastPosition.bottom,
                        duration: Duration(seconds: 3),
                        animDuration: Duration(seconds: 1),
                        curve: Curves.elasticOut,
                        reverseCurve: Curves.fastOutSlowIn);
                    setState(() {
                      _futureTodaysAdHocStoreVisits =
                          _vmStoreVisitsProvider.getTodaysAdHocStoreVisits();
                      _vmStoreVisitsProvider.notify();
                    });
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //     builder: (context) =>
                    //         ExecutionCallForm(val.storeId, true),
                    //   ),
                    // );
                  }
                },
              );
            },
          ),
          FutureBuilder<List<StoreVisit>>(
            future: _futureTodaysAdHocStoreVisits,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                //> ConnectionState.waiting
                return Center(
                  child: Container(
                    margin: EdgeInsets.all(subMargin),
                    width: 30,
                    height: 30,
                    child: CircularProgressIndicator(),
                  ),
                );
              } else if (snapshot.hasData) {
//< snapshot.hasData
                return Column(
                  children: snapshot.data.reversed.map((val) {
                    return VMTodaysAdHocVisitsCard(val);
                  }).toList(),
                );
              } else if (snapshot.hasError) {
//~ snapshot.hasError
                print(
                    "$Home_Screen StoreVisit FutureError = ${snapshot.error}");
                if (snapshot.error.toString().contains("401")) {
                  //> case: 401 Unauthenticated User
                  //> Clear the localStorage
                  SharedPreferences.getInstance().then((localStorage) {
                    localStorage.remove('token');
                  });

                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                        builder: (context) => LoginScreen(),
                      ),
                      (route) => false);
                } else if (snapshot.error.toString().contains("404")) {
                  //> case: 404 No Data Found
                  return ListTile(title: Text("No Visits found!"));
                }
                return ListTile(
                  title: Text(
                    "Something went Wrong!",
                    style: TextStyle(color: error),
                  ),
                  trailing: FlatButton(
                    onPressed: () {
                      setState(() {
                        _futureTodaysAdHocStoreVisits =
                            _vmStoreVisitsProvider.getTodaysAdHocStoreVisits();
                      });
                    },
                    child: Text("Retry"),
                  ),
                );
              }
              return SizedBox.shrink();
            },
          ),
        ],
      ),
    );
  }
}
