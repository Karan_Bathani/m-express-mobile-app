import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:market_express/Models/store-visit.dart';
import 'package:market_express/Screens/login-screen.dart';
import 'package:market_express/Screens/mark-attendance-screen.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:market_express/Utility/utility.dart';
import 'package:market_express/Visual%20Merchandiser/Components/List-Items/vm-todays-pjp-visits-card.dart';
import 'package:market_express/Visual%20Merchandiser/Providers/vm-storevisits-provider.dart';
import 'package:market_express/Visual%20Merchandiser/Screens/Forms/execution-call-form.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VMTodaysStoreVisitsTab extends StatefulWidget {
  @override
  _VMTodaysStoreVisitsTabState createState() => _VMTodaysStoreVisitsTabState();
}

class _VMTodaysStoreVisitsTabState extends State<VMTodaysStoreVisitsTab>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  Future<List<StoreVisit>> _futureTodaysStoreVisits;
  VMStoreVisitsProvider _vmStoreVisitsProvider;
  @override
  void initState() {
    super.initState();
    _vmStoreVisitsProvider =
        Provider.of<VMStoreVisitsProvider>(context, listen: false);
    _futureTodaysStoreVisits = _vmStoreVisitsProvider.getTodaysStoreVisits();
    _vmStoreVisitsProvider.addListener(() {
      setState(() {
        _futureTodaysStoreVisits =
            _vmStoreVisitsProvider.getTodaysStoreVisits();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Card(
      elevation: cardElevation,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(cardCornerRadius)),
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.fromLTRB(4, 4, 4, subMargin),
      color: Colors.white,
      child: ListView(
        children: [
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(top: subMargin),
            child: Text(
              "Today's Visit(s): ${DateTime.now().formatDate(pattern: "dd MMM, yy")}",
              style: TextStyle(
                color: textPrimary,
                fontWeight: FontWeight.w500,
                fontSize: 16,
              ),
            ),
          ),
          FutureBuilder<List<StoreVisit>>(
            future: _futureTodaysStoreVisits,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                //> ConnectionState.waiting
                return Center(
                  child: Container(
                    margin: EdgeInsets.all(subMargin),
                    width: 30,
                    height: 30,
                    child: CircularProgressIndicator(),
                  ),
                );
              } else if (snapshot.hasData) {
//< snapshot.hasData
                return Column(
                  children: snapshot.data.map((val) {
                    return VMTodaysVisitCard(val, () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => MarkAttendanceScreen(
                            type: "daily_visits",
                            storeId: val.storeId,
                          ),
                        ),
                      ).then(
                        (value) {
                          if (value != null) {
                            showToast(value,
                                context: context,
                                animation: StyledToastAnimation.slideFromBottom,
                                reverseAnimation:
                                    StyledToastAnimation.slideToBottom,
                                startOffset: Offset(0.0, 3.0),
                                reverseEndOffset: Offset(0.0, 3.0),
                                position: StyledToastPosition.bottom,
                                duration: Duration(seconds: 3),
                                animDuration: Duration(seconds: 1),
                                curve: Curves.elasticOut,
                                reverseCurve: Curves.fastOutSlowIn);
                            setState(() {
                              _futureTodaysStoreVisits =
                                  _vmStoreVisitsProvider.getTodaysStoreVisits();
                            });
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    ExecutionCallForm(val.storeId, false),
                              ),
                            );
                          }
                        },
                      );
                    });
                  }).toList(),
                );
              } else if (snapshot.hasError) {
//~ snapshot.hasError
                print(
                    "$Home_Screen StoreVisit FutureError = ${snapshot.error}");
                if (snapshot.error.toString().contains("401")) {
                  //> case: 401 Unauthenticated User
                  //> Clear the localStorage
                  SharedPreferences.getInstance().then((localStorage) {
                    localStorage.remove('token');
                  });
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                        builder: (context) => LoginScreen(),
                      ),
                      (route) => false);
                } else if (snapshot.error.toString().contains("404")) {
                  //> case: 404 No Data Found
                  return ListTile(title: Text("No Visits found for today!"));
                }
                return ListTile(
                  title: Text(
                    "Something went Wrong!",
                    style: TextStyle(color: error),
                  ),
                  trailing: FlatButton(
                    onPressed: () {
                      setState(() {
                        _futureTodaysStoreVisits =
                            _vmStoreVisitsProvider.getTodaysStoreVisits();
                      });
                    },
                    child: Text("Retry"),
                  ),
                );
              }
              return SizedBox.shrink();
            },
          ),
        ],
      ),
    );
  }
}
