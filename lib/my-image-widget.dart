import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:market_express/Utility/constants.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class MyFullScreenImageWidget extends StatefulWidget {
  final int initialIndex;
  final List<String> listOfImages;

  MyFullScreenImageWidget(this.initialIndex, this.listOfImages);

  @override
  _MyFullScreenImageWidgetState createState() =>
      _MyFullScreenImageWidgetState();
}

class _MyFullScreenImageWidgetState extends State<MyFullScreenImageWidget>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  PageController _pageController;

  @override
  void initState() {
    _pageController = PageController(initialPage: widget.initialIndex);
    _tabController = TabController(
        initialIndex: widget.initialIndex,
        length: widget.listOfImages.length,
        vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(vertical: subMargin),
        color: Colors.black,
        child: TabBar(
          isScrollable: true,
          controller: _tabController,
          onTap: (tabIndex) {
            _pageController.animateToPage(tabIndex,
                duration: Duration(milliseconds: 270), curve: Curves.linear);
          },
          tabs: widget.listOfImages
              .map((image) => Tab(
                    child: Image.network(
                      image,
                      width: 60,
                      height: 60,
                    ),
                  ))
              .toList(),
        ),
      ),
      body: Stack(
        children: [
          PhotoViewGallery.builder(
            itemCount: widget.listOfImages.length,
            pageController: _pageController,
            onPageChanged: (pageIndex) {
              _tabController.animateTo(pageIndex);
            },
            builder: (context, index) {
              return PhotoViewGalleryPageOptions(
                imageProvider: NetworkImage(
                  widget.listOfImages.length != 0
                      ? "${widget.listOfImages[index]}"
                      : 'https://www.brdtex.com/wp-content/uploads/2019/09/no-image-480x480.png',
                ),
                // Contained = the smallest possible size to fit one dimension of the screen
                minScale: PhotoViewComputedScale.contained * 0.8,
                // Covered = the smallest possible size to fit the whole screen
                maxScale: PhotoViewComputedScale.covered * 2,
              );
            },
            backgroundDecoration: BoxDecoration(color: Colors.black),
            loadingBuilder: (context, imageChunkEvent) {
              // if (loadingProgress == null) return child;
              return CircularProgressIndicator();
            },
          ),
          Positioned(
            top: 30,
            right: 10,
            // width: 200,
            // height: 25,
            child: IconButton(
              icon: Icon(
                Icons.close,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
    );
  }
}
